-- ddd-demo.orders definition
DROP TABLE IF EXISTS orders;
CREATE TABLE orders (
	id bigint(20) NOT NULL COMMENT '订单ID',
	order_price decimal(10, 2) NOT NULL COMMENT '订单价格',
	receive_address varchar(256) NOT NULL COMMENT '收货地址',
	order_status int(11) NOT NULL COMMENT '订单状态(10已创建, 20已支付, 30已发货, 40已完成, 50已取消)',
	create_time datetime NOT NULL COMMENT '创建时间',
	pay_time datetime DEFAULT NULL COMMENT '支付时间',
	express_code varchar(128) DEFAULT NULL COMMENT '快递单号',
	deliver_time datetime DEFAULT NULL COMMENT '发货时间',
	complete_time datetime DEFAULT NULL COMMENT '完成时间',
	cancel_time datetime DEFAULT NULL COMMENT '取消时间',
	update_time datetime DEFAULT NULL COMMENT '更新时间',
    primary key(id)
) ENGINE = InnoDB;
-- ddd-demo.order_goods definition
DROP TABLE IF EXISTS order_goods;
CREATE TABLE order_goods (
	id bigint(20) NOT NULL COMMENT '主键ID',
	goods_id bigint(20) NOT NULL COMMENT '商品ID',
	order_id bigint(20) NOT NULL COMMENT '订单ID',
	goods_count int(11) NOT NULL COMMENT '商品数量',
	goods_price decimal(10, 2) NOT NULL COMMENT '商品单价',
	goods_sum_price decimal(10, 2) NOT NULL COMMENT '商品总价',
	create_time datetime NOT NULL COMMENT '创建时间',
	update_time datetime DEFAULT NULL COMMENT '更新时间',
	primary key(id)
) ENGINE = InnoDB;
-- ddd-demo.category definition
DROP TABLE IF EXISTS category;
CREATE TABLE category (
	id bigint(20) NOT NULL COMMENT '主键ID',
	parent_category_id bigint(20) DEFAULT NULL COMMENT '上级分类ID',
	category_name varchar(64) NOT NULL COMMENT '分类名称',
	category_desc varchar(512) DEFAULT NULL COMMENT '分类描述',
	create_time datetime NOT NULL COMMENT '创建时间',
	update_time datetime DEFAULT NULL COMMENT '修改时间',
    primary key(id)
) ENGINE = InnoDB;
-- ddd-demo.goods definition
DROP TABLE IF EXISTS goods;
CREATE TABLE goods (
	id bigint(20) NOT NULL COMMENT '主键ID',
	category_id bigint(20) NOT NULL COMMENT '所属分类ID',
	goods_name varchar(120) NOT NULL COMMENT '商品名称',
	manufacture_date date NOT NULL COMMENT '生成日期',
	expiration_date date NOT NULL COMMENT '过期日期',
	goods_weight decimal(10, 2) NOT NULL COMMENT '商品重量',
	goods_weight_unit varchar(16) NOT NULL COMMENT '重量单位',
	goods_desc varchar(1024) NOT NULL COMMENT '商品描述',
	goods_price decimal(20, 2) NOT NULL COMMENT '商品价格',
	goods_status int(11) NOT NULL COMMENT '商品状态(10已上架, 20已下架)',
	create_time datetime NOT NULL COMMENT '创建时间',
	update_time datetime DEFAULT NULL COMMENT '修改时间',
    primary key(id)
) ENGINE = InnoDB;
-- ddd-demo.goods_tag definition
DROP TABLE IF EXISTS goods_tag;
CREATE TABLE goods_tag (
	id bigint(20) NOT NULL COMMENT '主键ID',
	tag_name varchar(64) NOT NULL COMMENT '标签名称',
	tag_desc varchar(512) DEFAULT NULL COMMENT '标签描述',
	create_time datetime NOT NULL COMMENT '创建时间',
	update_time datetime DEFAULT NULL COMMENT '修改时间',
    primary key(id)
) ENGINE = InnoDB;
-- ddd-demo.goods_tag_binding definition
DROP TABLE IF EXISTS goods_tag_binding;
CREATE TABLE goods_tag_binding (
	goods_id bigint(20) NOT NULL COMMENT '商品ID',
	tag_id bigint(20) NOT NULL COMMENT '商品标签ID',
    primary key(goods_id, tag_id)
) ENGINE = InnoDB;


-- domain_event definition
DROP TABLE IF EXISTS domain_event;
CREATE TABLE domain_event (
	id char(32) NOT NULL COMMENT '主键ID',
	aggregate_id varchar(64) DEFAULT NULL COMMENT '聚合根ID',
	json_type varchar(256) NOT NULL COMMENT '事件Json类型（对应Java Class名称）',
	json_content varchar(4096) NOT NULL COMMENT '事件Json内容',
	status tinyint(4) NOT NULL COMMENT '事件状态（10已创建，20已发送）',
	create_time datetime NOT NULL COMMENT '创建时间',
	update_time datetime DEFAULT NULL COMMENT '更新时间',
	primary key(id)
) ENGINE = InnoDB;
-- domain_event_record definition
DROP TABLE IF EXISTS domain_event_record;
CREATE TABLE domain_event_record (
	event_id char(32) NOT NULL COMMENT '事件ID',
	record_time datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '记录时间',
	primary key(event_id)
) ENGINE = InnoDB;
-- shedlock definition
DROP TABLE IF EXISTS shedlock;
CREATE TABLE shedlock (
	name VARCHAR(64) NOT NULL,
	lock_until TIMESTAMP(3) NOT NULL,
	locked_at TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
	locked_by VARCHAR(255) NOT NULL,
	primary key(name)
);