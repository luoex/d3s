package com.luo.d3s.ec.base;

import com.luo.d3s.ext.component.scan.D3sComponentScan;
import com.luo.d3s.ext.test.mock.BaseSpringMockMvcTest;
import com.luo.d3s.ext.test.mock.RabbitMqBrokerMockConfiguration;
import com.luo.d3s.ext.test.mock.RedisServerMockConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;


/**
 * 基础测试类 - 集成Spring基础测试配置
 *
 * @author luohq
 * @date 2021-12-11 11:19
 */
//使用application-test.yaml配置
@ActiveProfiles("test")
//导入配置（Mock RabbitMq Broker, Redis Server）
@Import({RabbitMqBrokerMockConfiguration.class, RedisServerMockConfiguration.class})
@D3sComponentScan(scanBasePackages = "com.luo.d3s.ec")
public class BaseSpringTest extends BaseSpringMockMvcTest {

}
