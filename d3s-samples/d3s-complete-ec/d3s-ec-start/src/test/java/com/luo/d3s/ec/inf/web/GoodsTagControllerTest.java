package com.luo.d3s.ec.inf.web;

import com.luo.d3s.core.application.dto.PageQuery;
import com.luo.d3s.core.exception.ValidateException;
import com.luo.d3s.ext.test.junit5.params.JsonFileSource;
import com.luo.d3s.ext.test.matcher.MatcherFactory;
import com.luo.d3s.ec.base.BaseSpringTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * 商品标签Controller - 单元测试
 *
 * @author luohq
 * @date 2023-08-03
 */
public class GoodsTagControllerTest extends BaseSpringTest {

    /**
     * Junit5 Json参数文件路径
     */
    private static final String JSON_SOURCE_PATH = "classpath:jsonSource/goodsTag.json";

    @Test
    void findGoodsTagTest() throws Exception {
        Long goodsTagId = 1L;
        //get pathParam参数
        this.mockMvc.perform(get(String.format("/api/v1/goods_tags/%s", goodsTagId)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_SUCCESS).value(true))
                .andExpect(jsonPath(JSON_PATH_DATA).isNotEmpty())
                .andExpect(jsonPath(JSON_PATH_DATA + ".tagName").value("标签1"));
    }

    @ParameterizedTest
    @CsvSource(value = {
            "1,3",
            "2,3",
            "3,3"
    })
    void findGoodsTagPageTest_differentPages(String pageIndex, String pageSize) throws Exception {
        //get pathParam参数
        this.mockMvc.perform(get("/api/v1/goods_tags")
                        .queryParam(PARAM_PAGE_INDEX, pageIndex)
                        .queryParam(PARAM_PAGE_SIZE, pageSize)
                        //此处orderBy列名直接对应数据库列名
                        .queryParam(PARAM_ORDER_BY, "tag_name")
                        .queryParam(PARAM_ORDER_DIRECTION, PageQuery.ASC)
                        .queryParam("tagName", "标签")
                        .queryParam("createTimeStart", "2023-01-01 00:00:00")
                        .queryParam("createTimeEnd", "2030-01-01 12:33:10")
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_SUCCESS).value(true))
                .andExpect(jsonPath(JSON_PATH_DATA).isNotEmpty())
                .andExpect(jsonPath(JSON_PATH_TOTAL_COUNT).value(MatcherFactory.greaterThan(0, "totalCount should be gt 0")));
    }

    @Test
    void findGoodsTagPageTest_nullParam() throws Exception {
        //get pathParam参数
        this.mockMvc.perform(get("/api/v1/goods_tags")
                        .queryParam(PARAM_PAGE_INDEX, DEFAULT_PAGE_INDEX)
                        .queryParam(PARAM_PAGE_SIZE, DEFAULT_PAGE_SIZE)
                                //此处orderBy列名直接对应数据库列名
                        .queryParam(PARAM_ORDER_BY, "tag_name")
                        .queryParam(PARAM_ORDER_DIRECTION, PageQuery.DESC)
                        //.queryParam("createTimeStart", "2023-01-01 00:00:00")
                        //.queryParam("createTimeEnd", "2030-01-01 12:33:10")
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_SUCCESS).value(true))
                .andExpect(jsonPath(JSON_PATH_DATA).isNotEmpty())
                .andExpect(jsonPath(JSON_PATH_TOTAL_COUNT).value(MatcherFactory.greaterThan(0, "totalCount should be gt 0")));
    }

    @ParameterizedTest
    @JsonFileSource(jsonKey = "$.goodsTagCreateCommand", resource = JSON_SOURCE_PATH)
    void createGoodsTagTest(String createGoodsTagCommandJson) throws Exception {
        this.mockMvc.perform(post("/api/v1/goods_tags")
                        //application/json请求体
                        .contentType(MediaType.APPLICATION_JSON)
                        //具体请求Json参数
                        .content(createGoodsTagCommandJson))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_SUCCESS).value(true))
                .andExpect(jsonPath(JSON_PATH_DATA + ".id").isNotEmpty());
    }

    @ParameterizedTest
    @JsonFileSource(jsonKey = "$.goodsTagCreateCommandFailure", resource = JSON_SOURCE_PATH)
    void createGoodsTagFailureTest(String createGoodsTagCommandJson) throws Exception {
        this.mockMvc.perform(post("/api/v1/goods_tags")
                        //application/json请求体
                        .contentType(MediaType.APPLICATION_JSON)
                        //具体请求Json参数
                        .content(createGoodsTagCommandJson))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_SUCCESS).value(false))
                .andExpect(jsonPath(JSON_PATH_ERR_CODE).value(ValidateException.DEFAULT_ERR_CODE))
                .andExpect(jsonPath(JSON_PATH_ERR_MESSAGE).isNotEmpty());
    }

    @ParameterizedTest
    @JsonFileSource(jsonKey = "$.goodsTagModifyCommand", resource = JSON_SOURCE_PATH)
    void modifyGoodsTagTest(String modifyGoodsTagCommandJson) throws Exception {
        this.mockMvc.perform(put("/api/v1/goods_tags")
                        //application/json请求体
                        .contentType(MediaType.APPLICATION_JSON)
                        //具体请求Json参数
                        .content(modifyGoodsTagCommandJson))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_SUCCESS).value(true));
    }

    @ParameterizedTest
    @JsonFileSource(jsonKey = "$.goodsTagModifyCommandFailure", resource = JSON_SOURCE_PATH)
    void modifyGoodsTagFailureTest(String modifyGoodsTagCommandJson) throws Exception {
        this.mockMvc.perform(put("/api/v1/goods_tags")
                        //application/json请求体
                        .contentType(MediaType.APPLICATION_JSON)
                        //具体请求Json参数
                        .content(modifyGoodsTagCommandJson))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_SUCCESS).value(false))
                .andExpect(jsonPath(JSON_PATH_ERR_CODE).value(ValidateException.DEFAULT_ERR_CODE))
                .andExpect(jsonPath(JSON_PATH_ERR_MESSAGE).isNotEmpty());
    }


    @ParameterizedTest
    @ValueSource(longs = {11})
    void removeGoodsTagTest(Long goodsTagId) throws Exception {
        this.mockMvc.perform(delete("/api/v1/goods_tags/{goodsTagId}", goodsTagId)
                        //application/json请求体
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_SUCCESS).value(true));
    }

    @ParameterizedTest
    @ValueSource(longs = {2})
    void removeGoodsTagFailureTest(Long goodsTagId) throws Exception {
        this.mockMvc.perform(delete("/api/v1/goods_tags/{goodsTagId}", goodsTagId)
                        //application/json请求体
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_SUCCESS).value(false))
                .andExpect(jsonPath(JSON_PATH_ERR_CODE).value(ValidateException.DEFAULT_ERR_CODE))
                .andExpect(jsonPath(JSON_PATH_ERR_MESSAGE).isNotEmpty());
    }
}
