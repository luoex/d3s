package com.luo.d3s.ec.domain;

import com.luo.d3s.core.util.JsonUtils;
import com.luo.d3s.ec.domain.category.model.Category;
import com.luo.d3s.ec.domain.category.model.CategoryDesc;
import com.luo.d3s.ec.domain.category.model.CategoryFactory;
import com.luo.d3s.ec.domain.category.model.CategoryName;
import com.luo.d3s.share.model.BizId;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

/**
 * Json序列化测试
 *
 * @author luohq
 * @date 2023-05-05 16:26
 */
@Slf4j
public class JsonSerializeTest {

    @Test
    void test() {
        Category category = CategoryFactory.createCategory(BizId.of(1L), CategoryName.of("hello"), CategoryDesc.of("desc"));
        String json = JsonUtils.toJson(category);
        log.info("categoryJson: {}", json);
        category = JsonUtils.fromJson(json, Category.class);
        log.info("category: {}", category);
    }
}
