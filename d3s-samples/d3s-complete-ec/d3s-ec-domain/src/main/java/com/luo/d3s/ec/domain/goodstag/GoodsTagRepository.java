package com.luo.d3s.ec.domain.goodstag;

import com.luo.d3s.core.application.dto.PageQuery;
import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.core.domain.adpator.CrudRepository;
import com.luo.d3s.share.model.BizId;

import java.time.LocalDateTime;
import java.util.Collection;

/**
 * GoodsTag repository
 *
 * @author luohq
 * @date 2023-08-02
 */
public interface GoodsTagRepository extends CrudRepository<GoodsTag, BizId> {

    /**
     * 查询指定标签ID集合对应的标签数量
     *
     * @param ids 标签ID集合
     * @return 存在的标签数量
     */
    Integer countByIdIn(Collection<BizId> ids);

    /**
     * 根据标签名称查询商品标签
     *
     * @param tagName 标签名称
     * @return 商品标签
     */
    GoodsTag findByTagName(String tagName);

    /**
     * 是否存在商品标签ID
     *
     * @param goodsTagId 商品标签ID
     * @return 是否存在
     */
    Boolean existGoodsTagId(BizId goodsTagId);

    /**
     * 查询商品标签分页列表
     *
     * @param pageQuery       page query param
     * @param createTimeStart createTime Start
     * @param createTimeEnd   createTime end
     * @return page response
     */
    PageResponse<GoodsTag> findPage(PageQuery pageQuery, LocalDateTime createTimeStart, LocalDateTime createTimeEnd);
}
