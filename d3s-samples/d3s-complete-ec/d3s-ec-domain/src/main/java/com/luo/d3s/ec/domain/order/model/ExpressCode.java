package com.luo.d3s.ec.domain.order.model;

import com.luo.d3s.core.domain.model.ValueObject;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.util.Objects;

/**
 * 快递单号
 *
 * @author luohq
 * @date 2022-11-27 16:03
 */
public class ExpressCode implements ValueObject {
    /**
     * 快递单号
     */
    @NotBlank
    @Length(min = 1, max = 128)
    private String value;

    ExpressCode() {
    }

    ExpressCode(String value) {
        this.value = value;
        this.validateSelf();
    }

    public static ExpressCode of(String expressCode) {
       return new ExpressCode(expressCode);
    }

    public String getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ExpressCode that = (ExpressCode) o;
        return Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public String toString() {
        return "ExpressCode{" +
                "value='" + value + '\'' +
                '}';
    }
}
