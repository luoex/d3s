package com.luo.d3s.ec.domain.category.model;

import com.luo.d3s.core.util.IdGenUtils;
import com.luo.d3s.share.model.BizId;

import java.time.LocalDateTime;

/**
 * 分类创建工厂<br/>
 * 注：领域对象创建工厂，强调初始创建领域对象的操作（区别于技术层面的构造函数）
 *
 * @author luohq
 * @date 2023-01-04 16:44
 */
public class CategoryFactory {

    /**
     * 创建分类
     *
     * @param parentCategoryId 上级分类ID
     * @param categoryName     分类名称
     * @param categoryDesc     分类描述
     * @return 初始分类
     */
    public static Category createCategory(BizId parentCategoryId, CategoryName categoryName, CategoryDesc categoryDesc) {
        LocalDateTime now = LocalDateTime.now();
        return new Category(
                BizId.of(IdGenUtils.nextDigitalId()),
                parentCategoryId,
                categoryName,
                categoryDesc
        );
    }
}
