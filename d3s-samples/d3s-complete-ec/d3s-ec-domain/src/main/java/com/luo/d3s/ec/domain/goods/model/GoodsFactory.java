package com.luo.d3s.ec.domain.goods.model;

import com.luo.d3s.core.util.IdGenUtils;
import com.luo.d3s.share.model.BizId;
import com.luo.d3s.share.model.Price;

import java.util.Set;

/**
 * 商品创建工厂<br/>
 * 注：领域对象创建工厂，强调初始创建领域对象的操作（区别于技术层面的构造函数）
 *
 * @author luohq
 * @date 2022-12-28 10:02
 */
public class GoodsFactory {


    /**
     * 创建初始商品
     *
     * @param categoryId 商品分类ID
     * @param goodsName  商品名称
     * @param goodsSpec  商品规格
     * @param goodsPrice 商品价格
     * @return 初始商品
     */
    public static Goods createGoods(BizId categoryId, GoodsName goodsName, GoodsSpec goodsSpec, Price goodsPrice, Set<BizId> tagIds) {
        return new Goods(
                BizId.of(IdGenUtils.nextDigitalId()),
                categoryId,
                goodsName,
                goodsSpec,
                goodsPrice,
                GoodsStatus.UNSHELVED,
                tagIds
                );
    }
}
