package com.luo.d3s.ec.domain.goods.model;

import com.luo.d3s.core.domain.model.ValueObject;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.util.Objects;

/**
 * 商品名称
 *
 * @author luohq
 * @date 2022-11-27 18:06
 */
public class GoodsName implements ValueObject {

    /**
     * 商品名称
     */
    @NotBlank
    @Length(min = 1, max =  120)
    private String value;

    GoodsName() {
    }

    GoodsName(String value) {
        this.value = value;
        this.validateSelf();
    }

    public static GoodsName of(String goodsName) {
        return new GoodsName(goodsName);
    }
    public String getValue() {
        return value;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GoodsName goodsName = (GoodsName) o;
        return Objects.equals(value, goodsName.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public String toString() {
        return "GoodsName{" +
                "value='" + value + '\'' +
                '}';
    }
}
