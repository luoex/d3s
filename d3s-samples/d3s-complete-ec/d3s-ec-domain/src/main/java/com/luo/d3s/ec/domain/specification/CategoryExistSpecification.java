package com.luo.d3s.ec.domain.specification;

import com.luo.d3s.core.domain.service.specifcation.AbstractSpecification;
import com.luo.d3s.core.util.validation.Validates;
import com.luo.d3s.ec.domain.category.repository.CategoryRepository;
import com.luo.d3s.share.model.BizId;

/**
 * 分类是否存在验证规则
 *
 * @author luohq
 * @date 2023-01-06 15:38
 */
public class CategoryExistSpecification extends AbstractSpecification<BizId> {

    private CategoryRepository categoryRepository;

    public CategoryExistSpecification(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public boolean isSatisfiedBy(BizId categoryId) {
        //验证分类ID是否存在
        Boolean existParentId = this.categoryRepository.existCategoryId(categoryId);
        Validates.isTrue(existParentId, "category id does not exist");
        return true;
    }
}
