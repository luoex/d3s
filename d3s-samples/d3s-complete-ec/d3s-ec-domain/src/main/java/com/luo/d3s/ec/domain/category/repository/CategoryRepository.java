package com.luo.d3s.ec.domain.category.repository;

import com.luo.d3s.core.application.dto.PageQuery;
import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.core.domain.adpator.CrudRepository;
import com.luo.d3s.ec.domain.category.model.Category;
import com.luo.d3s.ec.domain.category.model.CategoryName;
import com.luo.d3s.share.model.BizId;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Category repository
 *
 * @author luohq
 * @date 2022-11-27 18:35
 */
public interface CategoryRepository extends CrudRepository<Category, BizId> {

    /**
     * 是否已存在分类名称（若excludeCategoryId非空，则排除excludeCategoryId所对应的分类后再做比较）
     *
     * @param categoryName      分类名称
     * @param excludeCategoryId 不包含的分类ID
     * @return 是否存在
     */
    Boolean existCategoryName(CategoryName categoryName, BizId excludeCategoryId);

    /**
     * 是否存在分类ID
     *
     * @param categoryId 分类ID
     * @return 是否存在
     */
    Boolean existCategoryId(BizId categoryId);

    /**
     * 根据上级分类ID查询子分类列表
     *
     * @param parentCategoryId 上级分类ID
     * @return 根据上级分类ID查询子分类列表
     */
    List<Category> findByParentId(BizId parentCategoryId);

    /**
     * 根据分类ID批量删除分类
     *
     * @param categoryIds 分类ID列表
     */
    void deleteByIds(List<BizId> categoryIds);


    /**
     * find page
     *
     * @param pageQuery       page query param
     * @param createTimeStart createTime Start
     * @param createTimeEnd   createTime end
     * @return page response
     */
    PageResponse<Category> findPage(PageQuery pageQuery, LocalDateTime createTimeStart, LocalDateTime createTimeEnd);

}
