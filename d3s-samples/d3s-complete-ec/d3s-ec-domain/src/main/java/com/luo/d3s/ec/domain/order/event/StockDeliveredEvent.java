package com.luo.d3s.ec.domain.order.event;

import com.luo.d3s.core.domain.event.DomainEvent;
import com.luo.d3s.core.util.IdGenUtils;
import com.luo.d3s.ec.domain.order.model.ExpressCode;
import com.luo.d3s.share.model.BizId;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.Value;

import java.time.LocalDateTime;

/**
 * 物流已发货事件
 *
 * @author luohq
 * @date 2022-11-27 18:44
 */
@Value
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class StockDeliveredEvent implements DomainEvent<String, BizId> {
    /**
     * 事件ID
     */
    @EqualsAndHashCode.Include
    private String eventId;
    /**
     * 事件创建时间
     */
    private LocalDateTime createTime;

    /**
     * 发货记录ID
     */
    private BizId deliveryRecordId;

    /**
     * 订单ID
     */
    private BizId orderId;

    /**
     * 快递单号
     */
    private ExpressCode expressCode;

    /**
     * 发货时间
     */
    private LocalDateTime deliveryTime;

    public StockDeliveredEvent(BizId deliveryRecordId, BizId orderId, ExpressCode expressCode, LocalDateTime deliveryTime) {
        this.deliveryRecordId = deliveryRecordId;
        this.orderId = orderId;
        this.expressCode = expressCode;
        this.deliveryTime = deliveryTime;
        this.eventId = IdGenUtils.nextTextId();
        this.createTime = LocalDateTime.now();
    }

    @Override
    public BizId getAggregateRootId() {
        return this.orderId;
    }
}
