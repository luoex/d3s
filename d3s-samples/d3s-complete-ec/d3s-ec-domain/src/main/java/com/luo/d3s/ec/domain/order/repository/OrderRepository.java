package com.luo.d3s.ec.domain.order.repository;

import com.luo.d3s.core.domain.adpator.CrudRepository;
import com.luo.d3s.ec.domain.order.model.Order;
import com.luo.d3s.share.model.BizId;

/**
 * Order repository
 *
 * @author luohq
 * @date 2022-11-27 18:35
 */
public interface OrderRepository extends CrudRepository<Order, BizId> {
    /**
     * 是否存在订单ID
     *
     * @param orderId 订单ID
     * @return 是否存在
     */
    Boolean existOrderId(BizId orderId);
}
