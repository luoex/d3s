package com.luo.d3s.ec.domain.goods.repository;

import com.luo.d3s.core.domain.adpator.CrudRepository;
import com.luo.d3s.ec.domain.goods.model.Goods;
import com.luo.d3s.ec.domain.goods.model.GoodsStatus;
import com.luo.d3s.share.model.BizId;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Goods repository
 *
 * @author luohq
 * @date 2022-11-27 18:35
 */
public interface GoodsRepository extends CrudRepository<Goods, BizId> {

    /**
     * 分类下是否存在商品
     *
     * @param categoryIds 分类ID列表
     * @return 是否存在
     */
    Boolean existGoodsInCategories(List<BizId> categoryIds);

    /**
     * 是否存在上架状态的商品
     *
     * @param goodsIds 商品ID列表
     * @return 是否存在
     */
    Boolean existGoodsShelved(List<BizId> goodsIds);

    /**
     * 是否存在商品ID
     *
     * @param goodsId 商品ID
     * @return 是否存在
     */
    Boolean existGoodsId(BizId goodsId);

    /**
     * 标签下是否存在商品
     *
     * @param tagIds 标签ID列表
     * @return 是否存在
     */
    Boolean existsByTagIds(Collection<BizId> tagIds);

    /**
     * 根据商品ID批量删除商品
     *
     * @param goodsIds 商品ID列表
     */
    void deleteByIds(List<BizId> goodsIds);

    /**
     * 批量更新商品状态
     *
     * @param goodsIds    商品ID列表
     * @param goodsStatus 商品状态
     */
    void batchModifyGoodsStatus(List<BizId> goodsIds, GoodsStatus goodsStatus);
}
