package com.luo.d3s.ec.domain.specification;

import com.luo.d3s.core.domain.service.specifcation.AbstractSpecification;
import com.luo.d3s.core.util.validation.Validates;
import com.luo.d3s.ec.domain.goods.repository.GoodsRepository;
import com.luo.d3s.share.model.BizId;

import java.util.List;

/**
 * 不存在已上架商品验证规则
 *
 * @author luohq
 * @date 2023-01-05 10:31
 */
public class GoodsShelvedNotExistSpecification extends AbstractSpecification<List<BizId>> {

    private GoodsRepository goodsRepository;

    public GoodsShelvedNotExistSpecification(GoodsRepository goodsRepository) {
        this.goodsRepository = goodsRepository;
    }

    @Override
    public boolean isSatisfiedBy(List<BizId> goodsIds) {
        //存在已上架商品
        Boolean existGoodsShelved = this.goodsRepository.existGoodsShelved(goodsIds);
        Validates.isFalse(existGoodsShelved, "exist shelved goods");
        return true;
    }
}
