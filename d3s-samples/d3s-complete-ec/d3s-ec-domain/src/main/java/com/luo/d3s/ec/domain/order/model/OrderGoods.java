package com.luo.d3s.ec.domain.order.model;

import com.luo.d3s.core.domain.model.Entity;
import com.luo.d3s.share.model.BizId;
import com.luo.d3s.share.model.Count;
import com.luo.d3s.share.model.Price;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * 商品ID
 *
 * @author luohq
 * @date 2022-11-27 16:14
 */
public class OrderGoods implements Entity<BizId> {
    /**
     * 商品ID
     */
    @NotNull
    private BizId goodsId;

    /**
     * 商品数量
     */
    @NotNull
    private Count goodsCount;

    /**
     * 商品单价
     */
    @NotNull
    private Price goodsPrice;

    /**
     * 商品总价
     */
    @NotNull
    private Price goodsSumPrice;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    OrderGoods() {
    }



    /**
     * 初始创建订单商品
     *
     * @param goodsId       商品ID
     * @param goodsCount    商品数量
     * @param goodsPrice    商品单价
     * @param goodsSumPrice 商品总价
     */
    OrderGoods(BizId goodsId, Count goodsCount, Price goodsPrice, Price goodsSumPrice) {
        this.goodsId = goodsId;
        this.goodsCount = goodsCount;
        this.goodsPrice = goodsPrice;
        this.goodsSumPrice = goodsSumPrice;
        this.createTime = LocalDateTime.now();
        this.validateSelf();
    }


    public OrderGoods(BizId goodsId, Count goodsCount, Price goodsPrice, Price goodsSumPrice, LocalDateTime createTime, LocalDateTime updateTime) {
        this.goodsId = goodsId;
        this.goodsCount = goodsCount;
        this.goodsPrice = goodsPrice;
        this.goodsSumPrice = goodsSumPrice;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    @Override
    public BizId getId() {
        return this.goodsId;
    }

    public Count getGoodsCount() {
        return goodsCount;
    }

    public Price getGoodsPrice() {
        return goodsPrice;
    }

    public Price getGoodsSumPrice() {
        return goodsSumPrice;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public BizId getGoodsId() {
        return goodsId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OrderGoods that = (OrderGoods) o;
        return Objects.equals(goodsId, that.goodsId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(goodsId);
    }

    @Override
    public String toString() {
        return "OrderGoods{" +
                "goodsId=" + goodsId +
                ", goodsCount=" + goodsCount +
                ", goodsPrice=" + goodsPrice +
                ", goodsSumPrice=" + goodsSumPrice +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
