package com.luo.d3s.ec.domain.specification;

import com.luo.d3s.core.domain.service.specifcation.AbstractSpecification;
import com.luo.d3s.core.util.validation.Validates;
import com.luo.d3s.ec.domain.goods.repository.GoodsRepository;
import com.luo.d3s.share.model.BizId;

import java.util.List;

/**
 * 分类下不存在商品验证规则
 *
 * @author luohq
 * @date 2023-01-05 10:31
 */
public class GoodsNotExistInCategoriesSpecification extends AbstractSpecification<List<BizId>> {

    private GoodsRepository goodsRepository;

    public GoodsNotExistInCategoriesSpecification(GoodsRepository goodsRepository) {
        this.goodsRepository = goodsRepository;
    }

    @Override
    public boolean isSatisfiedBy(List<BizId> categoryIds) {
        //分类下不存在商品
        Boolean existGoodsInCategory = this.goodsRepository.existGoodsInCategories(categoryIds);
        Validates.isFalse(existGoodsInCategory, "exist goods in categories");
        return true;
    }
}
