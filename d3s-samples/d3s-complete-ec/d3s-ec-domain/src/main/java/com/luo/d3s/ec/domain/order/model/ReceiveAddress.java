package com.luo.d3s.ec.domain.order.model;

import com.luo.d3s.core.domain.model.ValueObject;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.util.Objects;

/**
 * 收货地址
 *
 * @author luohq
 * @date 2022-11-27 16:10
 */
public class ReceiveAddress implements ValueObject {
    /**
     * 收货地址
     */
    @NotBlank
    @Length(min = 1, max = 256)
    private String value;

    ReceiveAddress() {
    }

    ReceiveAddress(String value) {
        this.value = value;
        this.validateSelf();
    }

    public static ReceiveAddress of(String receiveAddress) {
        return new ReceiveAddress(receiveAddress);
    }

    public String getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ReceiveAddress that = (ReceiveAddress) o;
        return Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public String toString() {
        return "ReceiveAddress{" +
                "value='" + value + '\'' +
                '}';
    }
}
