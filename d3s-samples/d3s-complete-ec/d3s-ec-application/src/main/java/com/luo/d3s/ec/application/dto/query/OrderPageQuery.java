package com.luo.d3s.ec.application.dto.query;

import com.luo.d3s.core.application.dto.PageQuery;
import lombok.Data;
import lombok.ToString;

import java.time.LocalDateTime;

/**
 * 订单分页查询
 *
 * @author luohq
 * @date 2022-11-27 19:07
 */
@Data
@ToString(callSuper = true)
public class OrderPageQuery extends PageQuery {
    /**
     * 起始创建时间
     */
    private LocalDateTime createTimeStart;

    /**
     * 结束创建时间
     */
    private LocalDateTime createTimeEnd;
}
