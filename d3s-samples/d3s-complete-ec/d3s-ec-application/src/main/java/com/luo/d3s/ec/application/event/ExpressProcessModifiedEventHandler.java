package com.luo.d3s.ec.application.event;

import com.luo.d3s.core.domain.event.DomainEventHandler;
import com.luo.d3s.ec.application.service.OrderCommandService;
import com.luo.d3s.ec.domain.order.event.ExpressProcessModifiedEvent;
import org.springframework.stereotype.Component;

/**
 * 物流进度事件处理器
 *
 * @author luohq
 * @date 2022-11-27 19:29
 */
@Component
public class ExpressProcessModifiedEventHandler implements DomainEventHandler<ExpressProcessModifiedEvent> {

    private OrderCommandService orderCommandService;

    public ExpressProcessModifiedEventHandler(OrderCommandService orderCommandService) {
        this.orderCommandService = orderCommandService;
    }

    @Override
    public void handle(ExpressProcessModifiedEvent domainEvent) {
        if ("Complete".equals(domainEvent.getProcessStatus())) {
            this.orderCommandService.completeOrder(domainEvent.getOrderId().getValue());
        }
    }
}
