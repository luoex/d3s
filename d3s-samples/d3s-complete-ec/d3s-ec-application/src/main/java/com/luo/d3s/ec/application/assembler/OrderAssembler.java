package com.luo.d3s.ec.application.assembler;

import com.luo.d3s.core.exception.ExceptionFactory;
import com.luo.d3s.ec.application.dto.command.OrderCreateCommand;
import com.luo.d3s.ec.application.dto.vo.OrderGoodsVo;
import com.luo.d3s.ec.application.dto.vo.OrderVo;
import com.luo.d3s.ec.domain.order.model.Order;
import com.luo.d3s.ec.domain.order.model.OrderFactory;
import com.luo.d3s.ec.domain.order.model.OrderGoods;
import com.luo.d3s.ec.domain.order.model.ReceiveAddress;
import com.luo.d3s.share.model.BizId;
import com.luo.d3s.share.model.Count;
import com.luo.d3s.share.model.Price;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Order转换器
 *
 * @author luohq
 * @date 2022-11-28 14:00
 */
public class OrderAssembler {

    /**
     * 转换创建订单Command为Order信息
     *
     * @param orderCreateCommand 创建订单Command
     * @return Order信息
     */
    public static Order createOrder(OrderCreateCommand orderCreateCommand) {
        //验证订单商品非空
        if (CollectionUtils.isEmpty(orderCreateCommand.getOrderGoods())) {
            throw ExceptionFactory.bizException("orderGoods is empty");
        }
        //调用领域对象创建工厂OrderFactory.createOrderGoods批量创建订单商品
        List<OrderGoods> orderGoodsList = orderCreateCommand.getOrderGoods().stream()
                .map(orderGoodsVo -> OrderFactory.createOrderGoods(
                        BizId.of(orderGoodsVo.getGoodsId()),
                        Count.of(orderGoodsVo.getGoodsCount()),
                        Price.of(orderGoodsVo.getGoodsPrice()),
                        Price.of(orderGoodsVo.getGoodsSumPrice()))
                )
                .collect(Collectors.toList());
        //调用领域对象创建工厂OrderFactory.createOrder创建订单
        Order order = OrderFactory.createOrder(
                Price.of(orderCreateCommand.getOrderPrice()),
                ReceiveAddress.of(orderCreateCommand.getReceiveAddress()),
                orderGoodsList);
        return order;
    }

    /**
     * 转换Order信息为订单VO
     *
     * @param order Order信息
     * @return 订单VO
     */
    public static OrderVo toOrderVo(Order order) {
        if (null == order) {
            return null;
        }
        OrderVo orderVo = new OrderVo();
        List<OrderGoodsVo> orderGoodsVoList = Collections.emptyList();
        if (!CollectionUtils.isEmpty(order.getOrderGoods())) {
            orderGoodsVoList = order.getOrderGoods().stream().map(OrderAssembler::toOrderGoodsVo).collect(Collectors.toList());
        }
        orderVo.setOrderGoods(orderGoodsVoList);
        orderVo.setId(order.getId().getValue());
        orderVo.setOrderPrice(order.getOrderPrice().getValue());
        orderVo.setOrderStatus(order.getOrderStatus().getValue());
        orderVo.setCreateTime(order.getCreateTime());
        orderVo.setReceiveAddress(order.getReceiveAddress().getValue());
        return orderVo;
    }

    /**
     * 转换订单商品为VO
     *
     * @param orderGoods 订单商品信息
     * @return 订单商品信息VO
     */
    public static OrderGoodsVo toOrderGoodsVo(OrderGoods orderGoods) {
        OrderGoodsVo orderGoodsVo = new OrderGoodsVo();
        orderGoodsVo.setGoodsId(orderGoods.getGoodsId().getValue());
        orderGoodsVo.setGoodsCount(orderGoods.getGoodsCount().getValue());
        orderGoodsVo.setGoodsPrice(orderGoods.getGoodsPrice().getValue());
        orderGoodsVo.setGoodsSumPrice(orderGoods.getGoodsSumPrice().getValue());
        return orderGoodsVo;
    }

}
