package com.luo.d3s.ec.application.service;

import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.core.application.dto.SingleResponse;
import com.luo.d3s.core.application.service.QueryService;
import com.luo.d3s.ec.application.dto.query.GoodsTagPageQuery;
import com.luo.d3s.ec.application.dto.vo.GoodsTagVo;

/**
 * 商品标签查询服务接口类
 *
 * @author luohq
 * @date 2023-01-05 11:19
 */
public interface GoodsTagQueryService extends QueryService {

    /**
     * 查询商品标签详情
     *
     * @param goodsTagId 商品标签ID
     * @return 商品标签详情
     */
    SingleResponse<GoodsTagVo> findGoodsTag(Long goodsTagId);

    /**
     * 查询商品标签分页列表
     *
     * @param goodsTagPageQuery 商品标签分页查询参数
     * @return 分页查询结果
     */
    PageResponse<GoodsTagVo> findGoodsTagPage(GoodsTagPageQuery goodsTagPageQuery);

}
