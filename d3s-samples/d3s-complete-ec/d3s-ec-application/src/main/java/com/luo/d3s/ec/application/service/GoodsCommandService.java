package com.luo.d3s.ec.application.service;

import com.luo.d3s.core.application.dto.Response;
import com.luo.d3s.core.application.dto.SingleResponse;
import com.luo.d3s.core.application.service.CommandService;
import com.luo.d3s.ec.application.dto.vo.GoodsVo;
import com.luo.d3s.ec.application.dto.command.GoodsCreateCommand;
import com.luo.d3s.ec.application.dto.command.GoodsModifyCommand;

import java.util.List;

/**
 * 商品命令处理服务接口
 *
 * @author luohq
 * @date 2023-01-04 17:00
 */
public interface GoodsCommandService extends CommandService {

    /**
     * 创建商品
     *
     * @param goodsCreateCommand 创建商品命令
     * @return 响应结果（商品信息）
     */
    SingleResponse<GoodsVo> createGoods(GoodsCreateCommand goodsCreateCommand);

    /**
     * 修改商品
     *
     * @param goodsModifyCommand 修改商品命令
     * @return 响应结果
     */
    Response modifyGoods(GoodsModifyCommand goodsModifyCommand);

    /**
     * 批量移除商品
     *
     * @param goodsIds 商品ID列表
     * @return 响应结果
     */
    Response removeGoods(List<Long> goodsIds);

    /**
     * 批量上架商品
     *
     * @param goodsIds 商品ID列表
     * @return 响应结果
     */
    Response shelveGoods(List<Long> goodsIds);

    /**
     * 批量下架商品
     *
     * @param goodsIds 商品ID列表
     * @return 响应结果
     */
    Response unshelveGoods(List<Long> goodsIds);
}
