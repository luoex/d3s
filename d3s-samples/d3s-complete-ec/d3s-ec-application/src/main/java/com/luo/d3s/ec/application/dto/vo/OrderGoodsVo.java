package com.luo.d3s.ec.application.dto.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 订单商品VO
 *
 * @author luohq
 * @date 2022-11-27 19:05
 */
@Data
public class OrderGoodsVo {

    /**
     * 商品ID
     */
    private Long goodsId;

    /**
     * 商品数量
     */
    private Integer goodsCount;

    /**
     * 商品单价
     */
    private BigDecimal goodsPrice;

    /**
     * 商品总价
     */
    private BigDecimal goodsSumPrice;
}
