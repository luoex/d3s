package com.luo.d3s.ec.application.service.impl;

import com.luo.d3s.core.application.assmebler.PageResponseAssembler;
import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.core.application.dto.SingleResponse;
import com.luo.d3s.ec.application.assembler.CategoryAssembler;
import com.luo.d3s.ec.application.dto.query.CategoryPageQuery;
import com.luo.d3s.ec.application.dto.vo.CategoryVo;
import com.luo.d3s.ec.application.service.CategoryQueryService;
import com.luo.d3s.ec.domain.category.model.Category;
import com.luo.d3s.ec.domain.category.repository.CategoryRepository;
import com.luo.d3s.share.model.BizId;

import javax.annotation.Resource;

/**
 * 分类查询服务实现类（复杂的Query交由Infrastructure层进行实现）
 *
 * @author luohq
 * @date 2023-01-05 11:23
 */
public abstract class AbstractCategoryQueryService implements CategoryQueryService {


    @Resource
    private CategoryRepository categoryRepository;

    @Override
    public SingleResponse<CategoryVo> findCategory(Long categoryId) {
        Category category = this.categoryRepository.findById(BizId.of(categoryId));
        return SingleResponse.of(CategoryAssembler.toCategoryVo(category));
    }

    @Override
    public PageResponse<CategoryVo> findCategoryPage(CategoryPageQuery categoryPageQuery) {
        PageResponse<Category> pageResp = this.categoryRepository.findPage(categoryPageQuery,
                categoryPageQuery.getCreateTimeStart(), categoryPageQuery.getCreateTimeEnd());
        return PageResponseAssembler.toPageResp(pageResp, CategoryAssembler::toCategoryVo);
    }
}
