package com.luo.d3s.ec.application.service;

import com.luo.d3s.core.application.dto.Response;
import com.luo.d3s.core.application.dto.SingleResponse;
import com.luo.d3s.core.application.service.CommandService;
import com.luo.d3s.ec.application.dto.command.OrderCreateCommand;
import com.luo.d3s.ec.application.dto.command.OrderDeliverCommand;
import com.luo.d3s.ec.application.dto.vo.OrderVo;

/**
 * 订单命令处理服务接口类
 *
 * @author luohq
 * @date 2022-11-27 19:09
 */
public interface OrderCommandService extends CommandService {

    /**
     * 创建订单
     *
     * @param orderCreateCommand 创建订单命令
     * @return 响应结果（订单详情）
     */
    SingleResponse<OrderVo> createOrder(OrderCreateCommand orderCreateCommand);

    /**
     * 发货订单
     *
     * @param orderDeliverCommand 发货订单命令
     * @return 响应结果
     */
    Response deliverOrder(OrderDeliverCommand orderDeliverCommand);

    /**
     * 支付订单
     *
     * @param orderId 订单ID
     * @return 响应结果
     */
    Response payOrder(Long orderId);

    /**
     * 完成订单
     *
     * @param orderId 订单ID
     * @return 响应结果
     */
    Response completeOrder(Long orderId);

    /**
     * 取消订单
     *
     * @param orderId 订单ID
     * @return 响应结果
     */
    Response cancelOrder(Long orderId);
}
