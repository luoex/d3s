package com.luo.d3s.ec.application.service;

import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.core.application.dto.SingleResponse;
import com.luo.d3s.core.application.service.QueryService;
import com.luo.d3s.ec.application.dto.vo.OrderVo;
import com.luo.d3s.ec.application.dto.query.OrderPageQuery;

/**
 * 订单查询服务接口类
 *
 * @author luohq
 * @date 2022-11-27 19:10
 */
public interface OrderQueryService extends QueryService {

    /**
     * 查询订单详情
     *
     * @param orderId 订单ID
     * @return 订单详情
     */
    SingleResponse<OrderVo> findOrder(Long orderId);

    /**
     * 查询订单分页列表
     *
     * @param orderPageQuery 订单分页查询参数
     * @return 分页查询结果
     */
    PageResponse<OrderVo> findOrderPage(OrderPageQuery orderPageQuery);
}
