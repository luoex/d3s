package com.luo.d3s.ec.application.assembler;

import com.luo.d3s.ec.application.dto.vo.GoodsVo;
import com.luo.d3s.ec.domain.goods.model.Goods;

/**
 * Goods转换器
 *
 * @author luohq
 * @date 2023-01-04 17:10
 */
public class GoodsAssembler {

    /**
     * 转换商品实体为VO
     *
     * @param goods 商品实体
     * @return 商品VO
     */
    public static GoodsVo toGoodsVo(Goods goods) {
        if (null == goods) {
            return null;
        }
        GoodsVo goodsVo = new GoodsVo();
        goodsVo.setId(goods.getId().getValue());
        goodsVo.setCategoryId(goods.getCategoryId().getValue());
        goodsVo.setGoodsName(goods.getGoodsName().getValue());
        goodsVo.setManufactureDate(goods.getGoodsSpec().getManufactureDate());
        goodsVo.setExpirationDate(goods.getGoodsSpec().getExpirationDate());
        goodsVo.setGoodsWeight(goods.getGoodsSpec().getGoodsWeight().getWeight());
        goodsVo.setGoodsWeightUnit(goods.getGoodsSpec().getGoodsWeight().getUnit().getValue());
        goodsVo.setGoodsDesc(goods.getGoodsSpec().getGoodsDesc());
        goodsVo.setGoodsPrice(goods.getGoodsPrice().getValue());
        goodsVo.setCreateTime(goods.getCreateTime());
        goodsVo.setUpdateTime(goods.getUpdateTime());
        return goodsVo;
    }
}
