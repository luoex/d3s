package com.luo.d3s.ec.application.service.impl;

import com.luo.d3s.core.application.assmebler.PageResponseAssembler;
import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.core.application.dto.SingleResponse;
import com.luo.d3s.ec.application.assembler.GoodsTagAssembler;
import com.luo.d3s.ec.application.dto.query.GoodsTagPageQuery;
import com.luo.d3s.ec.application.dto.vo.GoodsTagVo;
import com.luo.d3s.ec.application.service.GoodsTagQueryService;
import com.luo.d3s.ec.domain.goodstag.GoodsTag;
import com.luo.d3s.ec.domain.goodstag.GoodsTagRepository;
import com.luo.d3s.share.model.BizId;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 商品标签查询服务实现类
 *
 * @author luohq
 * @date 2023-08-03
 */
@Service
public class GoodsTagQueryServiceImpl implements GoodsTagQueryService {

    @Resource
    private GoodsTagRepository goodsTagRepository;

    @Override
    public SingleResponse<GoodsTagVo> findGoodsTag(Long goodsTagId) {
        GoodsTag goodsTag = this.goodsTagRepository.findById(BizId.of(goodsTagId));
        return SingleResponse.of(GoodsTagAssembler.toGoodsTagVo(goodsTag));
    }

    @Override
    public PageResponse<GoodsTagVo> findGoodsTagPage(GoodsTagPageQuery goodsTagPageQuery) {
        PageResponse<GoodsTag> pageResp = this.goodsTagRepository.findPage(goodsTagPageQuery,
                goodsTagPageQuery.getCreateTimeStart(), goodsTagPageQuery.getCreateTimeEnd());
        return PageResponseAssembler.toPageResp(pageResp, GoodsTagAssembler::toGoodsTagVo);
    }
}
