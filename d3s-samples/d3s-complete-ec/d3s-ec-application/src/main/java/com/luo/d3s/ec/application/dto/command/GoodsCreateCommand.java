package com.luo.d3s.ec.application.dto.command;

import com.luo.d3s.ec.domain.goods.model.*;
import com.luo.d3s.share.model.BizId;
import com.luo.d3s.share.model.Price;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

/**
 * 创建商品Command
 *
 * @author luohq
 * @date 2023-01-04 15:29
 */
@Data
public class GoodsCreateCommand {

    /**
     * 所属分类ID
     */
    private Long categoryId;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 生产日期
     */
    private LocalDate manufactureDate;

    /**
     * 过期日期
     */
    private LocalDate expirationDate;

    /**
     * 商品重量
     */
    private BigDecimal goodsWeight;

    /**
     * 商品重量单位
     */
    private String goodsWeightUnit;

    /**
     * 商品介绍
     */
    private String goodsDesc;

    /**
     * 商品价格
     */
    private BigDecimal goodsPrice;

    /**
     * 商品标签ID集合
     */
    private Set<Long> tagIds;

    /**
     * 创建商品
     *
     * @return 商品
     */
    public Goods createGoods() {
        return GoodsFactory.createGoods(
                BizId.of(this.getCategoryId()),
                GoodsName.of(this.getGoodsName()),
                GoodsSpec.builder()
                        .manufactureDate(this.getManufactureDate())
                        .expirationDate(this.getExpirationDate())
                        .goodsWeight(GoodsWeight.of(this.getGoodsWeight(), WeightUnit.of(this.getGoodsWeightUnit())))
                        .goodsDesc(this.getGoodsDesc())
                        .build(),
                Price.of(this.getGoodsPrice()),
                BizId.ofNullable(tagIds)
        );

    }
}
