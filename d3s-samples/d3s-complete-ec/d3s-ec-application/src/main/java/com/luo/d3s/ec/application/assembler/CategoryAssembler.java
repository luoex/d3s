package com.luo.d3s.ec.application.assembler;

import com.luo.d3s.core.util.Optionals;
import com.luo.d3s.ec.application.dto.vo.CategoryVo;
import com.luo.d3s.ec.domain.category.model.Category;
import com.luo.d3s.ec.domain.category.model.CategoryDesc;
import com.luo.d3s.share.model.BizId;

/**
 * Category转换器
 *
 * @author luohq
 * @date 2023-01-04 17:10
 */
public class CategoryAssembler {

    /**
     * 转换分类实体为VO
     *
     * @param category 分类实体
     * @return 分类VO
     */
    public static CategoryVo toCategoryVo(Category category) {
        if (null == category) {
            return null;
        }
        CategoryVo categoryVo = new CategoryVo();
        categoryVo.setId(category.getId().getValue());
        categoryVo.setParentCategoryId(Optionals.defaultNull(category.getParentCategoryId(), BizId::getValue));
        categoryVo.setCategoryName(category.getCategoryName().getValue());
        categoryVo.setCategoryDesc(Optionals.defaultNull(category.getCategoryDesc(), CategoryDesc::getValue));
        categoryVo.setCreateTime(category.getCreateTime());
        categoryVo.setUpdateTime(category.getUpdateTime());
        return categoryVo;
    }
}
