package com.luo.d3s.ec.application.service.impl;

import com.luo.d3s.core.application.dto.Response;
import com.luo.d3s.core.application.dto.SingleResponse;
import com.luo.d3s.core.util.validation.Validates;
import com.luo.d3s.ec.application.assembler.GoodsTagAssembler;
import com.luo.d3s.ec.application.dto.command.GoodsTagCreateCommand;
import com.luo.d3s.ec.application.dto.command.GoodsTagModifyCommand;
import com.luo.d3s.ec.application.dto.vo.GoodsTagVo;
import com.luo.d3s.ec.application.service.GoodsTagCommandService;
import com.luo.d3s.ec.domain.goodstag.GoodsTag;
import com.luo.d3s.ec.domain.goodstag.GoodsTagFactory;
import com.luo.d3s.ec.domain.goodstag.GoodsTagRepository;
import com.luo.d3s.ec.domain.specification.GoodsTagNotExistBindingGoodsSpecification;
import com.luo.d3s.ec.domain.specification.GoodsTagSpecification;
import com.luo.d3s.share.model.BizId;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;

/**
 * 商品标签命令处理服务实现类
 *
 * @author luohq
 * @date 2023-08-03
 */
@Service
public class GoodsTagCommandServiceImpl implements GoodsTagCommandService {

    private GoodsTagRepository goodsTagRepository;

    private GoodsTagSpecification goodsTagSpecification;

    private GoodsTagNotExistBindingGoodsSpecification goodsTagNotExistBindingGoodsSpecification;


    public GoodsTagCommandServiceImpl(GoodsTagRepository goodsTagRepository,
                                      GoodsTagSpecification goodsTagSpecification,
                                      GoodsTagNotExistBindingGoodsSpecification goodsTagNotExistBindingGoodsSpecification) {
        this.goodsTagRepository = goodsTagRepository;
        this.goodsTagSpecification = goodsTagSpecification;
        this.goodsTagNotExistBindingGoodsSpecification = goodsTagNotExistBindingGoodsSpecification;
    }

    @Transactional(rollbackFor = Throwable.class)
    @Override
    public SingleResponse<GoodsTagVo> createGoodsTag(GoodsTagCreateCommand goodsTagCreateCommand) {
        //创建商品标签实体
        GoodsTag goodsTag = GoodsTagFactory.createGoodsTag(goodsTagCreateCommand.getTagName(), goodsTagCreateCommand.getTagDesc());
        //验证新增商品标签是否合法
        this.goodsTagSpecification.isSatisfiedBy(goodsTag);
        //保存商品标签
        goodsTag = this.goodsTagRepository.save(goodsTag);
        //转换GoodsTagVo
        return SingleResponse.of(GoodsTagAssembler.toGoodsTagVo(goodsTag));
    }

    @Transactional(rollbackFor = Throwable.class)
    @Override
    public Response modifyGoodsTag(GoodsTagModifyCommand goodsTagModifyCommand) {
        //查询商品标签实体
        GoodsTag goodsTag = this.goodsTagRepository.findById(BizId.of(goodsTagModifyCommand.getId()));
        Validates.notNull(goodsTag, "goodsTag does not exist");
        //修改基础信息
        goodsTag.modifyBasicInfo(goodsTagModifyCommand.getTagName(), goodsTagModifyCommand.getTagDesc());
        //验证待修改商品标签是否合法
        this.goodsTagSpecification.isSatisfiedBy(goodsTag);
        //保存实体
        this.goodsTagRepository.save(goodsTag);
        return Response.buildSuccess();
    }

    @Transactional(rollbackFor = Throwable.class)
    @Override
    public Response removeGoodsTag(Long goodsTagId) {
        //转换商品标签ID
        BizId goodsTagEntityId = BizId.of(goodsTagId);
        //验证待删除的商品标签ID下不存在商品
        this.goodsTagNotExistBindingGoodsSpecification.isSatisfiedBy(Collections.singletonList(goodsTagEntityId));
        //批量删除当前商品标签
        this.goodsTagRepository.removeById(goodsTagEntityId);
        return Response.buildSuccess();
    }
}
