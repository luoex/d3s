package com.luo.d3s.ec.application.service.impl;

import com.luo.d3s.core.application.dto.SingleResponse;
import com.luo.d3s.ec.application.assembler.OrderAssembler;
import com.luo.d3s.ec.application.dto.vo.OrderVo;
import com.luo.d3s.ec.application.service.OrderQueryService;
import com.luo.d3s.ec.domain.order.model.Order;
import com.luo.d3s.ec.domain.order.repository.OrderRepository;
import com.luo.d3s.share.model.BizId;

import javax.annotation.Resource;

/**
 * 订单查询服务实现类
 *
 * @author luohq
 * @date 2022-11-27 19:18
 */
public abstract class AbstractOrderQueryService implements OrderQueryService {
    @Resource
    private OrderRepository orderRepository;

    @Override
    public SingleResponse<OrderVo> findOrder(Long orderId) {
        Order order = this.orderRepository.findById(BizId.of(orderId));
        return SingleResponse.of(OrderAssembler.toOrderVo(order));
    }
}
