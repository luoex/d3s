package com.luo.d3s.ec.application.service.impl;

import com.luo.d3s.core.application.dto.Response;
import com.luo.d3s.core.application.dto.SingleResponse;
import com.luo.d3s.core.util.validation.Validates;
import com.luo.d3s.ec.application.assembler.GoodsAssembler;
import com.luo.d3s.ec.application.dto.command.GoodsCreateCommand;
import com.luo.d3s.ec.application.dto.command.GoodsModifyCommand;
import com.luo.d3s.ec.application.dto.vo.GoodsVo;
import com.luo.d3s.ec.application.service.GoodsCommandService;
import com.luo.d3s.ec.domain.goods.model.*;
import com.luo.d3s.ec.domain.goods.repository.GoodsRepository;
import com.luo.d3s.ec.domain.specification.CategoryExistSpecification;
import com.luo.d3s.ec.domain.specification.GoodsShelvedNotExistSpecification;
import com.luo.d3s.share.model.BizId;
import com.luo.d3s.share.model.Price;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 商品命令处理服务实现类
 *
 * @author luohq
 * @date 2023-01-04 17:06
 */
@Service
public class GoodsCommandServiceImpl implements GoodsCommandService {

    private GoodsRepository goodsRepository;

    private CategoryExistSpecification categoryExistSpecification;
    private GoodsShelvedNotExistSpecification goodsShelvedNotExistSpecification;

    public GoodsCommandServiceImpl(GoodsRepository goodsRepository,
                                   CategoryExistSpecification categoryExistSpecification,
                                   GoodsShelvedNotExistSpecification goodsShelvedNotExistSpecification) {
        this.goodsRepository = goodsRepository;
        this.categoryExistSpecification = categoryExistSpecification;
        this.goodsShelvedNotExistSpecification = goodsShelvedNotExistSpecification;
    }

    @Transactional(rollbackFor = Throwable.class)
    @Override
    public SingleResponse<GoodsVo> createGoods(GoodsCreateCommand goodsCreateCommand) {
        //创建商品实体
        Goods goods = goodsCreateCommand.createGoods();
        //验证新增商品分类是否存在
        this.categoryExistSpecification.isSatisfiedBy(goods.getCategoryId());
        //保存商品
        goods = this.goodsRepository.save(goods);
        //转换GoodsDto
        return SingleResponse.of(GoodsAssembler.toGoodsVo(goods));
    }

    @Transactional(rollbackFor = Throwable.class)
    @Override
    public Response modifyGoods(GoodsModifyCommand goodsModifyCommand) {
        //查询商品实体
        Goods goods = this.goodsRepository.findById(BizId.of(goodsModifyCommand.getId()));
        Validates.notNull(goods, "goods does not exist");
        //修改基础信息
        goods.modifyBasicInfo(
                BizId.of(goodsModifyCommand.getCategoryId()),
                GoodsName.of(goodsModifyCommand.getGoodsName()),
                GoodsSpec.builder()
                        .manufactureDate(goodsModifyCommand.getManufactureDate())
                        .expirationDate(goodsModifyCommand.getExpirationDate())
                        .goodsWeight(GoodsWeight.builder()
                                .weight(goodsModifyCommand.getGoodsWeight())
                                .unit(WeightUnit.of(goodsModifyCommand.getGoodsWeightUnit()))
                                .build())
                        .goodsDesc(goodsModifyCommand.getGoodsDesc())
                        .build(),
                Price.of(goodsModifyCommand.getGoodsPrice()),
                BizId.ofNullable(goodsModifyCommand.getTagIds())
        );
        //验证待修改商品分类是否存在
        this.categoryExistSpecification.isSatisfiedBy(goods.getCategoryId());
        //保存商品
        goods = this.goodsRepository.save(goods);
        return Response.buildSuccess();
    }

    @Transactional(rollbackFor = Throwable.class)
    @Override
    public Response removeGoods(List<Long> goodsIds) {
        //转换商品ID
        List<BizId> goodsEntityIds = BizId.of(goodsIds);
        //验证是不存在已上架状态的商品才可批量删除
        this.goodsShelvedNotExistSpecification.isSatisfiedBy(goodsEntityIds);
        //批量删除商品
        this.goodsRepository.deleteByIds(goodsEntityIds);
        return Response.buildSuccess();
    }

    /**
     * 上架商品 - 事务脚本实现方式
     *
     * @param goodsIds 商品ID列表
     * @return 响应结果
     */
    @Transactional(rollbackFor = Throwable.class)
    @Override
    public Response shelveGoods(List<Long> goodsIds) {
        //批量更新商品状态为已上架
        this.goodsRepository.batchModifyGoodsStatus(BizId.of(goodsIds), GoodsStatus.SHELVED);
        return Response.buildSuccess();
    }

    /**
     * 下架商品 - 事务脚本实现方式
     *
     * @param goodsIds 商品ID列表
     * @return 响应结果
     */
    @Transactional(rollbackFor = Throwable.class)
    @Override
    public Response unshelveGoods(List<Long> goodsIds) {
        //批量更新商品状态为已下架
        this.goodsRepository.batchModifyGoodsStatus(BizId.of(goodsIds), GoodsStatus.UNSHELVED);
        return Response.buildSuccess();
    }


    /**
     * 上架商品 - DDD实现方式<br/>
     * 注：由于多次查询、更新数据库，出于性能考虑，暂未使用该实现方式。
     *
     * @param goodsIds 商品ID列表
     * @return 响应结果
     */
    @Deprecated
    @Transactional(rollbackFor = Throwable.class)
    public Response shelveGoodsDDD(List<Long> goodsIds) {
        //依次遍历商品ID并设置上架状态
        BizId.of(goodsIds)
                .forEach(goodsId -> {
                    //查询商品实体
                    Goods goods = this.goodsRepository.findById(goodsId);
                    Validates.notNull(goods, String.format("goods %s does not exist", goods));
                    //设置商品上架
                    goods.shelve();
                    //保存商品信息
                    this.goodsRepository.save(goods);
                });
        return Response.buildSuccess();
    }

    /**
     * 下架商品 - DDD实现方式<br/>
     * 注：由于多次查询、更新数据库，出于性能考虑，暂未使用该实现方式。
     *
     * @param goodsIds 商品ID列表
     * @return 响应结果
     */
    @Deprecated
    @Transactional(rollbackFor = Throwable.class)
    public Response unshelveGoodsDDD(List<Long> goodsIds) {
        //依次遍历商品ID并设置上架状态
        BizId.of(goodsIds)
                .forEach(goodsId -> {
                    //查询商品实体
                    Goods goods = this.goodsRepository.findById(goodsId);
                    Validates.notNull(goods, String.format("goods %s does not exist", goods));
                    //设置商品下架
                    goods.unshelve();
                    //保存商品信息
                    this.goodsRepository.save(goods);
                });
        return Response.buildSuccess();
    }

}
