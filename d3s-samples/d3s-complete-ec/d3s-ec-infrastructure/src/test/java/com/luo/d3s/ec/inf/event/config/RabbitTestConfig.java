package com.luo.d3s.ec.inf.event.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.luo.d3s.ec.infrastructure.event.config.RabbitMqConfig;
import com.luo.d3s.ext.test.mock.RabbitMqBrokerMockConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * RabbitMq测试配置
 *
 * @author luohq
 * @date 2022-12-23 13:50
 */
@Configuration
@Import({
        RabbitMqConfig.class,
        //Mock RabbitMq环境
        RabbitMqBrokerMockConfiguration.class
})
@EnableAutoConfiguration
public class RabbitTestConfig {

    @Bean
    ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        // 注册一个时间序列化及反序列化的处理模块，用于解决jdk8中localDateTime等的序列化问题
        objectMapper.registerModule(new JavaTimeModule());
        return objectMapper;
    }
}
