package com.luo.d3s.ec.inf.event;

import com.fasterxml.jackson.core.type.TypeReference;
import com.luo.d3s.core.util.JsonUtils;
import com.luo.d3s.ec.domain.order.event.ExpressProcessModifiedEvent;
import com.luo.d3s.ec.domain.order.event.OrderPaidEvent;
import com.luo.d3s.ec.domain.order.event.StockDeliveredEvent;
import com.luo.d3s.ec.domain.order.model.*;
import com.luo.d3s.ext.event.jdbc.convertor.DomainEventConvertor;
import com.luo.d3s.ext.event.jdbc.dataobject.DomainEventDo;
import com.luo.d3s.share.model.BizId;
import com.luo.d3s.share.model.Count;
import com.luo.d3s.share.model.Price;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

/**
 * DomainEvent测试
 *
 * @author luohq
 * @date 2022-12-23 8:18
 */
@Slf4j
public class DomainEventTest {

    @Test
    void testOrderPaidEvent() {
        OrderPaidEvent event = DomainEventTest.buildOrderPaidEvent();
        Order order = event.getOrder();
        List<OrderGoods> orderGoodsList = order.getOrderGoods();

        /** 测试领域对象序列化 */
        String orderJson = JsonUtils.toJson(order);
        String orderGoodsJson = JsonUtils.toJson(orderGoodsList);
        String eventJson = JsonUtils.toJson(event);
        log.info("orderJson: {}", orderJson);
        log.info("orderGoodsJson: {}", orderGoodsJson);
        log.info("eventJson: {}", eventJson);

        /** 测试领域对象反序列化 */
        order = JsonUtils.fromJson(orderJson, Order.class);
        log.info("order: {}", orderJson);
        orderGoodsList = JsonUtils.fromJson(orderGoodsJson, new TypeReference<List<OrderGoods>>() {
        });
        log.info("orderGoodsList: {}", orderGoodsList);
        event = JsonUtils.fromJson(eventJson, OrderPaidEvent.class);
        log.info("event: {}", event);

        /** 测试领域事件和事件DO转换 */
        DomainEventDo domainEventDo = DomainEventConvertor.toEventDo(event);
        log.info("eventDo: {}", domainEventDo);
        event = DomainEventConvertor.toEvent(domainEventDo);
        log.info("event: {}", event);
    }

    @Test
    void testStockDeliveredEvent() {
        StockDeliveredEvent event = DomainEventTest.buildStockDeliveredEvent();

        /** 测试领域事件序列化 */
        String eventJson = JsonUtils.toJson(event);
        log.info("eventJson: {}", eventJson);

        /** 测试领域事件反序列化 */
        event = JsonUtils.fromJson(eventJson, StockDeliveredEvent.class);
        log.info("event: {}", event);

        /** 测试领域事件和事件DO转换 */
        DomainEventDo domainEventDo = DomainEventConvertor.toEventDo(event);
        log.info("eventDo: {}", domainEventDo);
        event = DomainEventConvertor.toEvent(domainEventDo);
        log.info("event: {}", event);
    }


    @Test
    void testExpressProcessModifiedEvent() {
        ExpressProcessModifiedEvent event = DomainEventTest.buildExpressProcessModifiedEvent();

        /** 测试领域事件序列化 */
        String eventJson = JsonUtils.toJson(event);
        log.info("eventJson: {}", eventJson);

        /** 测试领域事件反序列化 */
        event = JsonUtils.fromJson(eventJson, ExpressProcessModifiedEvent.class);
        log.info("event: {}", event);

        /** 测试领域事件和事件DO转换 */
        DomainEventDo domainEventDo = DomainEventConvertor.toEventDo(event);
        log.info("eventDo: {}", domainEventDo);
        event = DomainEventConvertor.toEvent(domainEventDo);
        log.info("event: {}", event);
    }


    public static OrderPaidEvent buildOrderPaidEvent() {
        BizId orderId = BizId.of(1L);
        BizId goodsId = BizId.of(2L);
        Price price = Price.of(new BigDecimal(5));
        List<OrderGoods> orderGoodsList = Arrays.asList(new OrderGoods(goodsId, Count.of(1), price, price, LocalDateTime.now(), LocalDateTime.now()));
        Order order = new Order(orderId, orderGoodsList, price, ReceiveAddress.of("Shenyang"), OrderStatus.CREATED, LocalDateTime.now(), null, null, null, null, null, null);
        OrderPaidEvent domainEvent = new OrderPaidEvent(order);
        return domainEvent;
    }

    public static ExpressProcessModifiedEvent buildExpressProcessModifiedEvent() {
        BizId expressProcessId = BizId.of(1L);
        BizId expressDeliveryId = BizId.of(2L);
        BizId orderId = BizId.of(1606130219024879618L);
        ExpressProcessModifiedEvent domainEvent = new ExpressProcessModifiedEvent(expressProcessId, expressDeliveryId, orderId, LocalDateTime.now(), "已达到沈阳市", "Processing");
        return domainEvent;
    }

    public static StockDeliveredEvent buildStockDeliveredEvent() {
        BizId deliveryRecordId = BizId.of(1L);
        BizId orderId = BizId.of(1610922213492273153L);
        ExpressCode expressCode = ExpressCode.of("111");
        StockDeliveredEvent domainEvent = new StockDeliveredEvent(deliveryRecordId, orderId, expressCode, LocalDateTime.now());
        //domainEvent.setEventId("eb8b197375ce420e93f4aa3a8879b2fc");
        return domainEvent;
    }
}
