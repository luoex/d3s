package com.luo.d3s.ec.inf.event;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.luo.d3s.core.domain.event.DomainEvent;
import com.luo.d3s.ec.inf.event.config.RabbitTestConfig;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

/**
 * RabbitMq序列化器测试
 *
 * @author luohq
 * @date 2022-12-23 13:41
 */
@SpringBootTest(classes = {RabbitTestConfig.class})
@Slf4j
public class RabbitMqSerializerTest {

    @Test
    void testRabbitMqStringMessageConvertor() {
        List<DomainEvent> domainEventList = Arrays.asList(
                DomainEventTest.buildOrderPaidEvent(),
                DomainEventTest.buildExpressProcessModifiedEvent(),
                DomainEventTest.buildStockDeliveredEvent()
        );

        /**
         * SimpleMessageConvertor支持String|Serializable|byte arrays 的序列化和反序列化
         * 1）支持String, byte[]类型
         * 2）对象需实现Serializable接口，即通过ObjectOutputStream|ObjectInputStream进行Jdk序列化和反序列化
         * 3）Entity, ValueObject, DomainEvent等接口皆扩展了Serializable接口，所以领域对象皆支持Jdk序列化
         * */
        MessageConverter messageConverter = new SimpleMessageConverter();

        domainEventList.forEach(domainEvent -> {
            Message message = messageConverter.toMessage(domainEvent, new MessageProperties());
            log.info("message: {}", message);

            DomainEvent domainEventConvert = (DomainEvent) messageConverter.fromMessage(message);
            log.info("domainEvent[{}]: {}", domainEventConvert.getClass().getName(), domainEventConvert);
        });

    }

    @Test
    void testRabbitMqJackson2JsonMessageConvertor() {
        List<DomainEvent> domainEventList = Arrays.asList(
                DomainEventTest.buildOrderPaidEvent(),
                DomainEventTest.buildExpressProcessModifiedEvent(),
                DomainEventTest.buildStockDeliveredEvent()
        );


        /**
         * 使用ObjectMapper进行序列化（支持Java8日期，忽略unknown属性报错），
         * 存在通过构造函数反序列化异常问题（no default creator），解决方案如下：
         * 1）可以通过@JsonCreator、@JsonProperty 或者 @ConstructorProperties指定反序列化构造函数
         * 2）又或者定义默认无参数构造函数（目前使用）
         */
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        MessageConverter messageConverter = new Jackson2JsonMessageConverter(objectMapper);
        //messageConverter.setClassMapper(classMapper());

        domainEventList.forEach(domainEvent -> {
            Message message = messageConverter.toMessage(domainEvent, new MessageProperties());
            log.info("message: {}", message);

            DomainEvent domainEventConvert = (DomainEvent) messageConverter.fromMessage(message);
            log.info("domainEvent[{}]: {}", domainEventConvert.getClass().getName(), domainEventConvert);
        });
    }

}
