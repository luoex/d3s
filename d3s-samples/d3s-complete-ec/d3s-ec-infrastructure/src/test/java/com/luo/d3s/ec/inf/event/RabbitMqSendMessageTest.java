package com.luo.d3s.ec.inf.event;

import com.luo.d3s.ec.domain.order.event.ExpressProcessModifiedEvent;
import com.luo.d3s.ec.domain.order.event.StockDeliveredEvent;
import com.luo.d3s.ec.inf.event.config.RabbitTestConfig;
import com.luo.d3s.ec.infrastructure.event.config.RabbitBaseConfig;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * RabbitMq发送消息测试
 *
 * @author luohq
 * @date 2022-12-23 13:41
 */
@SpringBootTest(classes = {RabbitTestConfig.class})
@Slf4j
public class RabbitMqSendMessageTest {

    @Resource
    private RabbitTemplate rabbitTemplate;

    @Resource
    private RabbitBaseConfig rabbitExpressProcessConfig;

    @Resource
    private RabbitBaseConfig rabbitStockDeliveryConfig;


    @Test
    void testSendExpressProcessEvent() {
        ExpressProcessModifiedEvent domainEvent = DomainEventTest.buildExpressProcessModifiedEvent();
        log.info("[RabbitMq SEND] exchange: {}, routingKey: {}, event:{}",
                this.rabbitExpressProcessConfig.getExchange().getName(),
                this.rabbitExpressProcessConfig.getExchange().getRoutingKey(),
                domainEvent);
        //使用RabbitMq发送事件
        this.rabbitTemplate.convertAndSend(
                this.rabbitExpressProcessConfig.getExchange().getName(),
                this.rabbitExpressProcessConfig.getExchange().getRoutingKey(),
                domainEvent
        );
    }


    @Test
    void testSendStockDeliveryEvent() {
        StockDeliveredEvent domainEvent = DomainEventTest.buildStockDeliveredEvent();
        log.info("[RabbitMq SEND] exchange: {}, routingKey: {}, event:{}",
                this.rabbitStockDeliveryConfig.getExchange().getName(),
                this.rabbitStockDeliveryConfig.getExchange().getRoutingKey(),
                domainEvent);
        //使用RabbitMq发送事件
        this.rabbitTemplate.convertAndSend(
                this.rabbitStockDeliveryConfig.getExchange().getName(),
                this.rabbitStockDeliveryConfig.getExchange().getRoutingKey(),
                domainEvent
        );
    }
}
