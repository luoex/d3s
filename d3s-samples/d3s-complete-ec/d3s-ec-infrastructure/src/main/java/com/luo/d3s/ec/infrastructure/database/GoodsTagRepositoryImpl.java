package com.luo.d3s.ec.infrastructure.database;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.luo.d3s.core.application.dto.PageQuery;
import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.ec.domain.goodstag.GoodsTag;
import com.luo.d3s.ec.domain.goodstag.GoodsTagRepository;
import com.luo.d3s.ec.infrastructure.database.convertor.GoodsTagConvertor;
import com.luo.d3s.ec.infrastructure.database.convertor.PageConvertor;
import com.luo.d3s.ec.infrastructure.database.dao.GoodsTagMapper;
import com.luo.d3s.ec.infrastructure.database.dataobject.GoodsTagDo;
import com.luo.d3s.share.model.BizId;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * GoodsTag仓库实现类
 *
 * @author luohq
 * @date 2023-01-05 14:26
 */
@Repository
public class GoodsTagRepositoryImpl implements GoodsTagRepository {

    private GoodsTagMapper goodsTagMapper;

    public GoodsTagRepositoryImpl(GoodsTagMapper goodsTagMapper) {
        this.goodsTagMapper = goodsTagMapper;
    }

    @Override
    public GoodsTag save(GoodsTag goodsTag) {
        //转换分类数据对象
        GoodsTagDo goodsTagDo = GoodsTagConvertor.toGoodsTagDo(goodsTag);
        //ID已存在，则修改
        Boolean existGoodsTag = this.existGoodsTagId(goodsTag.getId());
        if (existGoodsTag) {
            this.goodsTagMapper.updateById(goodsTagDo);
            return goodsTag;
        }
        //ID不存在，则新增
        this.goodsTagMapper.insert(goodsTagDo);
        //返回分类实体信息
        return GoodsTagConvertor.toGoodsTag(goodsTagDo);
    }

    @Override
    public GoodsTag findById(BizId bizId) {
        //查询分类信息
        GoodsTagDo goodsTagDo = this.goodsTagMapper.selectById(bizId.getValue());
        if (null == goodsTagDo) {
            return null;
        }
        //转换分类实体
        return GoodsTagConvertor.toGoodsTag(goodsTagDo);
    }

    @Override
    public void removeById(BizId bizId) {
        //根据ID删除分类信息
        this.goodsTagMapper.deleteById(bizId.getValue());
    }

    @Override
    public Boolean existGoodsTagId(BizId goodsTagId) {
        //验证分类ID是否存在
        return this.goodsTagMapper.exists(Wrappers.<GoodsTagDo>lambdaQuery()
                .eq(GoodsTagDo::getId, goodsTagId.getValue())
        );
    }

    @Override
    public Integer countByIdIn(Collection<BizId> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return 0;
        }
        List<Long> batchIds = ids.stream()
                .map(BizId::getValue)
                .collect(Collectors.toList());
        return this.goodsTagMapper.selectCount(Wrappers.<GoodsTagDo>lambdaQuery()
                .in(GoodsTagDo::getId, batchIds)).intValue();
    }

    @Override
    public GoodsTag findByTagName(String tagName) {
        GoodsTagDo goodsTagDo = this.goodsTagMapper.selectOne(Wrappers.<GoodsTagDo>lambdaQuery()
                .eq(GoodsTagDo::getTagName, tagName));
        if (null == goodsTagDo) {
            return null;
        }
        return GoodsTagConvertor.toGoodsTag(goodsTagDo);
    }

    @Override
    public PageResponse<GoodsTag> findPage(PageQuery pageQuery, LocalDateTime createTimeStart, LocalDateTime createTimeEnd) {
        Page<GoodsTagDo> pageResult = this.goodsTagMapper.selectPage(
                PageConvertor.toPage(pageQuery),
                Wrappers.<GoodsTagDo>lambdaQuery()
                        .ge(ObjectUtils.isNotNull(createTimeStart), GoodsTagDo::getCreateTime, createTimeStart)
                        .le(ObjectUtils.isNotNull(createTimeEnd), GoodsTagDo::getCreateTime, createTimeEnd));
        return PageConvertor.toPageResponse(pageResult, GoodsTagConvertor::toGoodsTag);
    }
}
