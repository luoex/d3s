package com.luo.d3s.ec.infrastructure.database.dataobject;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

/**
 * Category数据对象
 *
 * @author luohq
 * @date 2022-12-21 9:42
 */
@Data
@ToString(callSuper = true)
@TableName("category")
public class CategoryDo extends BaseDo {

    /**
     * 上级分类ID
     */
    private Long parentCategoryId;

    /**
     * 分类名称
     */
    private String categoryName;

    /**
     * 分类描述
     */
    private String categoryDesc;

}
