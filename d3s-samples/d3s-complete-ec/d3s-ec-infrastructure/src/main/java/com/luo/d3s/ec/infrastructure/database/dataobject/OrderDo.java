package com.luo.d3s.ec.infrastructure.database.dataobject;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Order数据对象
 *
 * @author luohq
 * @date 2022-12-21 9:42
 */
@Data
@ToString(callSuper = true)
@TableName("orders")
public class OrderDo extends BaseDo {

    private BigDecimal orderPrice;

    private String receiveAddress;

    private Integer orderStatus;

    private LocalDateTime payTime;

    private String expressCode;

    private LocalDateTime deliverTime;

    private LocalDateTime completeTime;

    private LocalDateTime cancelTime;
}
