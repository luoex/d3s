package com.luo.d3s.ec.infrastructure.database.convertor;

import com.luo.d3s.core.util.Optionals;
import com.luo.d3s.ec.application.dto.vo.CategoryTreeVo;
import com.luo.d3s.ec.application.dto.vo.CategoryVo;
import com.luo.d3s.ec.domain.category.model.Category;
import com.luo.d3s.ec.domain.category.model.CategoryDesc;
import com.luo.d3s.ec.domain.category.model.CategoryName;
import com.luo.d3s.ec.infrastructure.database.dataobject.CategoryDo;
import com.luo.d3s.share.model.BizId;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 分类对象转换器
 *
 * @author luohq
 * @date 2022-12-21 10:30
 */
public class CategoryConvertor {

    /**
     * 转换分类实体为数据对象
     *
     * @param category 分类实体
     * @return 分类数据对象
     */
    public static CategoryDo toCategoryDo(Category category) {
        CategoryDo categoryDo = new CategoryDo();
        categoryDo.setId(category.getId().getValue());
        categoryDo.setParentCategoryId(Optionals.defaultNull(category.getParentCategoryId(), BizId::getValue));
        categoryDo.setCategoryName(category.getCategoryName().getValue());
        categoryDo.setCategoryDesc(Optionals.defaultNull(category.getCategoryDesc(), CategoryDesc::getValue));
        categoryDo.setCreateTime(category.getCreateTime());
        categoryDo.setUpdateTime(category.getUpdateTime());
        return categoryDo;
    }

    /**
     * 转换分类数据对象为实体
     *
     * @param categoryDo 分类数据对象
     * @return 分类实体
     */
    public static Category toCategory(CategoryDo categoryDo) {
        Category category = new Category(
                BizId.of(categoryDo.getId()),
                Optionals.defaultNull(categoryDo.getParentCategoryId(), BizId::of),
                CategoryName.of(categoryDo.getCategoryName()),
                Optionals.defaultNull(categoryDo.getCategoryDesc(), CategoryDesc::of),
                categoryDo.getCreateTime(),
                categoryDo.getUpdateTime()
        );
        return category;
    }

    /**
     * 转换分类数据对象为分类VO
     *
     * @param categoryDo 分类数据对象
     * @return 分类VO
     */
    public static CategoryVo toCategoryVo(CategoryDo categoryDo) {
        CategoryVo categoryVo = new CategoryVo();
        categoryVo.setId(categoryDo.getId());
        categoryVo.setParentCategoryId(categoryDo.getParentCategoryId());
        categoryVo.setCategoryName(categoryDo.getCategoryName());
        categoryVo.setCategoryDesc(categoryDo.getCategoryDesc());
        categoryVo.setCreateTime(categoryDo.getCreateTime());
        categoryVo.setUpdateTime(categoryDo.getUpdateTime());
        return categoryVo;
    }


    /**
     * 转换分类数据对象为分类树形VO
     *
     * @param categoryDo 分类数据对象
     * @return 分类树形VO
     */
    @Deprecated
    public static CategoryTreeVo toCategoryTreeDto(CategoryDo categoryDo) {
        CategoryTreeVo categoryTreeVo = new CategoryTreeVo();
        categoryTreeVo.setId(categoryDo.getId());
        categoryTreeVo.setParentCategoryId(categoryDo.getParentCategoryId());
        categoryTreeVo.setCategoryName(categoryDo.getCategoryName());
        categoryTreeVo.setCategoryDesc(categoryDo.getCategoryDesc());
        categoryTreeVo.setCreateTime(categoryDo.getCreateTime());
        categoryTreeVo.setUpdateTime(categoryDo.getUpdateTime());
        return categoryTreeVo;
    }

    /**
     * 转换分类数据对象列表为树形VO列表
     *
     * @param categoryDoList 分类数据对象列表
     * @return 分类树形VO列表
     */
    @Deprecated
    public static List<CategoryTreeVo> toCategoryTreeVoList(List<CategoryDo> categoryDoList) {
        if (CollectionUtils.isEmpty(categoryDoList)) {
            return null;
        }
        //转换子分类列表为TreeDto列表
        List<CategoryTreeVo> subCategoryTreeVoList = categoryDoList.stream()
                .map(CategoryConvertor::toCategoryTreeDto)
                .collect(Collectors.toList());
        return subCategoryTreeVoList;
    }
}
