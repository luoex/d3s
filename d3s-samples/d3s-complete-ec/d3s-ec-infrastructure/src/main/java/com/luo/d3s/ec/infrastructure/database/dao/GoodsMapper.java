package com.luo.d3s.ec.infrastructure.database.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.luo.d3s.ec.application.dto.vo.GoodsVo;
import com.luo.d3s.ec.application.dto.query.GoodsPageQuery;
import com.luo.d3s.ec.infrastructure.database.dataobject.GoodsDo;
import org.apache.ibatis.annotations.Param;

/**
 * 商品信息 Mapper 接口
 *
 * @author luohq
 * @date 2022-12-21
 */
public interface GoodsMapper extends BaseMapper<GoodsDo> {

    /**
     * 根据ID查询商品详情
     *
     * @param id 商品ID
     * @return 商品详情
     */
    GoodsVo findGoodsWithCNameById(@Param("id") Long id);
    /**
     * 查询商品分页列表
     *
     * @param page           mp分页参数
     * @param goodsPageQuery 具体查询参数
     * @return 分页查询结果
     */
    IPage<GoodsVo> findGoodsWithCNamePage(IPage<GoodsVo> page, @Param("goodsPageQuery") GoodsPageQuery goodsPageQuery);
}
