package com.luo.d3s.ec.infrastructure.query;

import com.luo.d3s.core.application.dto.MultiResponse;
import com.luo.d3s.ec.application.dto.vo.CategoryTreeVo;
import com.luo.d3s.ec.application.service.CategoryQueryService;
import com.luo.d3s.ec.application.service.impl.AbstractCategoryQueryService;
import com.luo.d3s.ec.infrastructure.database.dao.CategoryMapper;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * 分类查询实现类 - infrastructure实现（适用于CQRS中的Query处理）
 *
 * @author luohq
 * @date 2023-01-05 15:30
 */
@Service
@CacheConfig(cacheNames = CategoryQueryService.CATEGORY_TREE_CACHE_NAME)
public class CategoryQueryServiceImpl extends AbstractCategoryQueryService {

    @Resource
    private CategoryMapper categoryMapper;

    @Cacheable(key = "#p0?:'root'")
    @Override
    public MultiResponse<CategoryTreeVo> findCategoryTree(Long categoryId) {
        //查询分类ID对应的根分类TreeDto
        List<CategoryTreeVo> rootCategoryTreeVoList = this.categoryMapper.findRootCategoryTreeVoList(categoryId);
        //递归查询子分类列表
        this.extractCategoryTree(rootCategoryTreeVoList);
        //返回递归查询结果
        return MultiResponse.of(rootCategoryTreeVoList);
    }

    private void extractCategoryTree(List<CategoryTreeVo> rootCategoryTreeVoList) {
        //若上级分类列表为空，则直接返回
        if (CollectionUtils.isEmpty(rootCategoryTreeVoList)) {
            return;
        }
        //遍历上级分类列表，依次查询其子分类李彪
        rootCategoryTreeVoList.stream()
                .forEach(curParentCategoryTreeDto -> {
                    //查询当前分类的子分类TreeDto列表
                    List<CategoryTreeVo> subCategoryTreeVoList = this.categoryMapper.findSubCategoryTreeVoList(curParentCategoryTreeDto.getId());
                    //设置当前分类的子分类TreeDto列表
                    curParentCategoryTreeDto.setSubCategories(subCategoryTreeVoList);
                    //递归设置子分类
                    extractCategoryTree(subCategoryTreeVoList);
                });
    }


}
