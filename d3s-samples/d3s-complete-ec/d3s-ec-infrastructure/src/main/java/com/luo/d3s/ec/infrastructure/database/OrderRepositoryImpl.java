package com.luo.d3s.ec.infrastructure.database;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.luo.d3s.ec.domain.order.model.Order;
import com.luo.d3s.ec.domain.order.repository.OrderRepository;
import com.luo.d3s.ec.infrastructure.database.convertor.OrderConvertor;
import com.luo.d3s.ec.infrastructure.database.dao.OrderGoodsMapper;
import com.luo.d3s.ec.infrastructure.database.dao.OrderMapper;
import com.luo.d3s.ec.infrastructure.database.dataobject.OrderDo;
import com.luo.d3s.ec.infrastructure.database.dataobject.OrderGoodsDo;
import com.luo.d3s.share.model.BizId;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 订单仓库实现
 *
 * @author luohq
 * @date 2022-12-21 9:40
 */
@Repository
public class OrderRepositoryImpl implements OrderRepository {

    private OrderMapper orderMapper;
    private OrderGoodsMapper orderGoodsMapper;

    public OrderRepositoryImpl(OrderMapper orderMapper, OrderGoodsMapper orderGoodsMapper) {
        this.orderMapper = orderMapper;
        this.orderGoodsMapper = orderGoodsMapper;
    }

    @Override
    public Order save(Order order) {
        //转换订单信息
        OrderDo orderDo = OrderConvertor.toOrderDo(order);
        /** ID存在，则修改 */
        Boolean existOrder = this.existOrderId(order.getId());
        if (existOrder) {
            //修改订单信息
            this.orderMapper.updateById(orderDo);
            //返回订单信息
            return order;
        }

        /** ID不存在，则新增 */
        //插入订单信息
        this.orderMapper.insert(orderDo);
        //转换订单商品信息
        List<OrderGoodsDo> orderGoodsDoList = order.getOrderGoods().stream()
                //转换领域对象为DO（同时设置orderId）
                .map(orderGoods -> OrderConvertor.toOrderGoodsDo(orderDo.getId(), orderGoods))
                .collect(Collectors.toList());
        //批量插入订单商品信息
        orderGoodsDoList.forEach(this.orderGoodsMapper::insert);
        //返回订单信息
        return OrderConvertor.toOrder(orderDo, orderGoodsDoList);
    }

    @Override
    public Order findById(BizId bizId) {
        //查询订单信息
        OrderDo orderDo = this.orderMapper.selectById(bizId.getValue());
        if (null == orderDo) {
            return null;
        }
        //查询订单商品信息
        List<OrderGoodsDo> orderGoodsDoList = this.orderGoodsMapper.selectList(Wrappers.<OrderGoodsDo>lambdaQuery()
                .eq(OrderGoodsDo::getOrderId, bizId.getValue()));
        //转换订单、订单商品信息
        return OrderConvertor.toOrder(orderDo, orderGoodsDoList);
    }

    @Override
    public void removeById(BizId bizId) {
        //删除订单信息
        this.orderMapper.deleteById(bizId.getValue());
        //删除订单商品信息
        this.orderGoodsMapper.delete(Wrappers.<OrderGoodsDo>lambdaQuery().eq(OrderGoodsDo::getOrderId, bizId.getValue()));
    }

    @Override
    public Boolean existOrderId(BizId orderId) {
        return this.orderMapper.exists(Wrappers.<OrderDo>lambdaQuery()
                .eq(OrderDo::getId, orderId.getValue()));
    }
}
