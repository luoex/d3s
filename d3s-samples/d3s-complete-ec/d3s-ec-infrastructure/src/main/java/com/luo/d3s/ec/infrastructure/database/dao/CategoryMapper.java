package com.luo.d3s.ec.infrastructure.database.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.luo.d3s.ec.application.dto.vo.CategoryTreeVo;
import com.luo.d3s.ec.infrastructure.database.dataobject.CategoryDo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 分类信息 Mapper 接口
 *
 * @author luohq
 * @date 2022-12-21
 */
public interface CategoryMapper extends BaseMapper<CategoryDo> {

    /**
     * 根据分类ID查询分类TreeVo列表，若分类ID为空则查询根分类TreeVo列表
     *
     * @param categoryId 分类ID
     * @return 分类TreeVo列表
     */
    List<CategoryTreeVo> findRootCategoryTreeVoList(@Param("categoryId") Long categoryId);

    /**
     * 根据上级分类ID查询子分类TreeVo列表
     *
     * @param parentCategoryId 上级分类ID
     * @return 子分类TreeVo列表
     */
    List<CategoryTreeVo> findSubCategoryTreeVoList(@Param("parentCategoryId") Long parentCategoryId);


}
