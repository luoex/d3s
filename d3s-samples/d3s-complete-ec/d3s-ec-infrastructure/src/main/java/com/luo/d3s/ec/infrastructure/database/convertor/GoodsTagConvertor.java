package com.luo.d3s.ec.infrastructure.database.convertor;

import com.luo.d3s.ec.application.dto.vo.GoodsTagVo;
import com.luo.d3s.ec.domain.goodstag.GoodsTag;
import com.luo.d3s.ec.infrastructure.database.dataobject.GoodsTagDo;
import com.luo.d3s.share.model.BizId;

/**
 * 商品标签对象转换器
 *
 * @author luohq
 * @date 2022-12-21 10:30
 */
public class GoodsTagConvertor {

    /**
     * 转换商品标签实体为数据对象
     *
     * @param goodsTag 商品标签实体
     * @return 商品标签数据对象
     */
    public static GoodsTagDo toGoodsTagDo(GoodsTag goodsTag) {
        GoodsTagDo goodsTagDo = new GoodsTagDo();
        goodsTagDo.setId(goodsTag.getId().getValue());
        goodsTagDo.setTagName(goodsTag.getTagName());
        goodsTagDo.setTagDesc(goodsTag.getTagDesc());
        goodsTagDo.setCreateTime(goodsTag.getCreateTime());
        goodsTagDo.setUpdateTime(goodsTag.getUpdateTime());
        return goodsTagDo;
    }

    /**
     * 转换商品标签数据对象为实体
     *
     * @param goodsTagDo 商品标签数据对象
     * @return 商品标签实体
     */
    public static GoodsTag toGoodsTag(GoodsTagDo goodsTagDo) {
        GoodsTag goodsTag = new GoodsTag(
                BizId.of(goodsTagDo.getId()),
                goodsTagDo.getTagName(),
                goodsTagDo.getTagDesc(),
                goodsTagDo.getCreateTime(),
                goodsTagDo.getUpdateTime()
        );
        return goodsTag;
    }

    /**
     * 转换商品标签数据对象为商品标签VO
     *
     * @param goodsTagDo 商品标签数据对象
     * @return 商品标签VO
     */
    public static GoodsTagVo toGoodsTagVo(GoodsTagDo goodsTagDo) {
        GoodsTagVo goodsTagVo = new GoodsTagVo();
        goodsTagVo.setId(goodsTagDo.getId());
        goodsTagVo.setTagName(goodsTagDo.getTagName());
        goodsTagVo.setTagDesc(goodsTagDo.getTagDesc());
        goodsTagVo.setCreateTime(goodsTagDo.getCreateTime());
        goodsTagVo.setUpdateTime(goodsTagDo.getUpdateTime());
        return goodsTagVo;
    }

}
