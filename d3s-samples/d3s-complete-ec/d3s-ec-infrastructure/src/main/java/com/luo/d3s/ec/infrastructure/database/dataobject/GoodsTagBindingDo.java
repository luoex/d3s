package com.luo.d3s.ec.infrastructure.database.dataobject;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 商品与标签绑定关系 - 数据对象
 *
 * @author luohq
 * @date 2023-08-14
 */
@Data
@TableName("goods_tag_binding")
public class GoodsTagBindingDo {

    /**
     * 商品ID
     */
    private Long goodsId;

    /**
     * 标签ID
     */
    private Long tagId;

    public GoodsTagBindingDo() {
    }

    public GoodsTagBindingDo(Long goodsId, Long tagId) {
        this.goodsId = goodsId;
        this.tagId = tagId;
    }
}
