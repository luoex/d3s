package com.luo.d3s.ec.infrastructure.rpc;

import com.luo.d3s.ec.domain.order.acl.StockAcl;
import com.luo.d3s.ec.domain.order.model.Order;
import com.luo.d3s.ec.infrastructure.rpc.dto.LockStockDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 库存ACL实现类
 *
 * @author luohq
 * @date 2022-12-21 9:44
 */
@Component
@Slf4j
public class StockAclImpl implements StockAcl {

    @Override
    public void lockStock(Order order) {
        log.info("[MOCK RPC] lock stock, order: {}", order);
        List<LockStockDto> lockStockDtoList = order.getOrderGoods().stream().map(orderGoods -> {
            LockStockDto lockStockDto = new LockStockDto();
            lockStockDto.setGoodsId(orderGoods.getGoodsId().getValue());
            lockStockDto.setLockCount(orderGoods.getGoodsCount().getValue());
            return lockStockDto;
        }).collect(Collectors.toList());
        log.info("[MOCK RPC] lock stock, param: {}", lockStockDtoList);
        //TODO RPC
        //this.feignClient.lockStock(lockStockDtoList);
    }

    @Override
    public void deductStock(Order order) {
        log.info("[MOCK RPC] deduct stock, order: {}", order);
    }
}
