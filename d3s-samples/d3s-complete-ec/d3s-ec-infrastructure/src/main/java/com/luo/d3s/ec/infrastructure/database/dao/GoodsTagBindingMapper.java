package com.luo.d3s.ec.infrastructure.database.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.luo.d3s.ec.infrastructure.database.dataobject.GoodsTagBindingDo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Collection;
import java.util.Set;

/**
 * 商品标签信息 Mapper 接口
 *
 * @author luohq
 * @date 2023-08-14
 */
public interface GoodsTagBindingMapper extends BaseMapper<GoodsTagBindingDo> {

    /**
     * 根据商品ID集合删除绑定关系
     *
     * @param goodsIds 商品ID集合
     * @return
     */
    default Integer deleteByGoodsIds(Collection<Long> goodsIds) {
        return this.delete(Wrappers.<GoodsTagBindingDo>lambdaQuery()
                .in(GoodsTagBindingDo::getGoodsId, goodsIds));
    }

    /**
     * 标签是否关联商品
     *
     * @param tagIds 标签ID集合
     * @return 是否存在
     */
    default Boolean existByTagIds(Collection<Long> tagIds) {
        return this.exists(Wrappers.<GoodsTagBindingDo>lambdaQuery()
                .in(GoodsTagBindingDo::getTagId, tagIds));
    }

    /**
     * 查询商品绑定的标签ID集合
     *
     * @param goodsId 商品ID
     * @return 标签ID集合
     */
    @Select("select tag_id from goods_tag_binding where goods_id = #{goodsId}")
    Set<Long> findTagIdsOfGoods(@Param("goodsId") Long goodsId);

}
