package com.luo.d3s.ec.infrastructure.database.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.luo.d3s.ec.infrastructure.database.dataobject.OrderDo;

/**
 * 订单信息 Mapper 接口
 *
 * @author luohq
 * @date 2022-12-21
 */
public interface OrderMapper extends BaseMapper<OrderDo> {

}
