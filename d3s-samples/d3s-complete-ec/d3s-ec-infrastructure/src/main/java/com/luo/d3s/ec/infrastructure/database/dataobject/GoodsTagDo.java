package com.luo.d3s.ec.infrastructure.database.dataobject;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

/**
 * 商品标签
 *
 * @author luohq
 * @date 2023-08-02
 */
@Data
@ToString(callSuper = true)
@TableName("goods_tag")
public class GoodsTagDo extends BaseDo {

    /**
     *标签名称
     */
    private String tagName;

    /**
     *标签描述
     */
    private String tagDesc;
}
