package com.luo.d3s.ec.infrastructure.query;

import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.ec.application.dto.vo.OrderVo;
import com.luo.d3s.ec.application.dto.query.OrderPageQuery;
import com.luo.d3s.ec.application.service.impl.AbstractOrderQueryService;
import com.luo.d3s.ec.infrastructure.database.convertor.OrderConvertor;
import com.luo.d3s.ec.infrastructure.database.convertor.PageConvertor;
import com.luo.d3s.ec.infrastructure.database.dao.OrderMapper;
import com.luo.d3s.ec.infrastructure.database.dataobject.OrderDo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 订单查询服务实现类 - infrastructure实现（适用于CQRS中的Query处理）
 *
 * @author luohq
 * @date 2023-01-07 11:12
 */
@Service
public class OrderQueryServiceImpl extends AbstractOrderQueryService {

    @Resource
    private OrderMapper orderMapper;

    @Override
    public PageResponse<OrderVo> findOrderPage(OrderPageQuery orderPageQuery) {
        Page<OrderDo> pageResult = this.orderMapper.selectPage(
                PageConvertor.toPage(orderPageQuery),
                Wrappers.<OrderDo>lambdaQuery()
                        .ge(ObjectUtils.isNotNull(orderPageQuery.getCreateTimeStart()), OrderDo::getCreateTime, orderPageQuery.getCreateTimeStart())
                        .le(ObjectUtils.isNotNull(orderPageQuery.getCreateTimeEnd()), OrderDo::getCreateTime, orderPageQuery.getCreateTimeEnd()));
        return PageConvertor.toPageResponse(pageResult, OrderConvertor::toOrderVo);
    }
}
