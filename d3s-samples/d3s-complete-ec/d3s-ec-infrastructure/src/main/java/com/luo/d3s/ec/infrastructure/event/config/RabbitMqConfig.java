package com.luo.d3s.ec.infrastructure.event.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.support.converter.DefaultClassMapper;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * RabbitMq相关配置
 *
 * @author luohq
 * @date 2022-12-22 14:27
 */
@Configuration
public class RabbitMqConfig {

    /**
     * 【推荐】支持RabbitMq Jackson JSON序列化
     */
    @Bean
    public MessageConverter jsonMessageConverter(ObjectMapper objectMapper) {
        Jackson2JsonMessageConverter messageConverter = new Jackson2JsonMessageConverter(objectMapper);
        messageConverter.setClassMapper(classMapper());
        return messageConverter;
    }

    @Bean
    public DefaultClassMapper classMapper() {
        DefaultClassMapper classMapper = new DefaultClassMapper();
        classMapper.setTrustedPackages("*");
        return classMapper;
    }


    ///**
    // * 【不推荐，仅支持Java语言】支持RabbitMq String、Serializable、Byte[]的序列化
    // */
    //@Bean
    //public MessageConverter simpleMessageConvertor() {
    //    return new SimpleMessageConverter();
    //}



    /**
     * 订单已支付事件 - RabbitMq配置
     *
     * @return
     */
    @Bean
    @ConfigurationProperties("spring.rabbitmq.events.order-paid")
    public RabbitBaseConfig rabbitOrderPaidConfig() {
        return new RabbitBaseConfig();
    }

    /**
     * 库存已发货事件 - RabbitMq配置
     *
     * @return
     */
    @Bean
    @ConfigurationProperties("spring.rabbitmq.events.stock-delivery")
    public RabbitBaseConfig rabbitStockDeliveryConfig() {
        return new RabbitBaseConfig();
    }

    /**
     * 库存已发货事件 - RabbitMq配置
     *
     * @return
     */
    @Bean
    @ConfigurationProperties("spring.rabbitmq.events.express-process")
    public RabbitBaseConfig rabbitExpressProcessConfig() {
        return new RabbitBaseConfig();
    }


}
