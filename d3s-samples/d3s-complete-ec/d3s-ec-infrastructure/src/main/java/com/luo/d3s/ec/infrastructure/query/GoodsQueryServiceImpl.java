package com.luo.d3s.ec.infrastructure.query;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.core.application.dto.SingleResponse;
import com.luo.d3s.ec.application.dto.vo.GoodsVo;
import com.luo.d3s.ec.application.dto.query.GoodsPageQuery;
import com.luo.d3s.ec.application.service.GoodsQueryService;
import com.luo.d3s.ec.infrastructure.database.dao.GoodsMapper;
import com.luo.d3s.ec.infrastructure.database.convertor.PageConvertor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.function.Function;

/**
 * 商品查询服务实现类 - infrastructure实现（适用于CQRS中的Query处理）
 *
 * @author luohq
 * @date 2023-01-07 11:12
 */
@Service
public class GoodsQueryServiceImpl implements GoodsQueryService {

    @Resource
    private GoodsMapper goodsMapper;


    @Override
    public SingleResponse<GoodsVo> findGoods(Long goodsId) {
        GoodsVo goodsVo = this.goodsMapper.findGoodsWithCNameById(goodsId);
        return SingleResponse.of(goodsVo);
    }

    @Override
    public PageResponse<GoodsVo> findGoodsPage(GoodsPageQuery goodsPageQuery) {
        IPage<GoodsVo> goodsPage = this.goodsMapper.findGoodsWithCNamePage(PageConvertor.toPage(goodsPageQuery), goodsPageQuery);
        return PageConvertor.toPageResponse(goodsPage, Function.identity());
    }
}
