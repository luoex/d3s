package com.luo.d3s.ec.interfaces.web;

import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.core.application.dto.Response;
import com.luo.d3s.core.application.dto.SingleResponse;
import com.luo.d3s.ec.application.dto.vo.OrderVo;
import com.luo.d3s.ec.application.dto.command.OrderCreateCommand;
import com.luo.d3s.ec.application.dto.query.OrderPageQuery;
import com.luo.d3s.ec.application.service.OrderCommandService;
import com.luo.d3s.ec.application.service.OrderQueryService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 订单Controller
 *
 * @author luohq
 * @date 2022-12-22 16:12
 */
@RestController
@RequestMapping("/api/v1/orders")
public class OrderController {
    @Resource
    private OrderQueryService orderQueryService;

    @Resource
    private OrderCommandService orderCommandService;

    /**
     * 根据订单ID查询订单信息
     *
     * @param orderId 订单ID
     * @return 订单信息
     */
    @GetMapping(value = "/{orderId}")
    public SingleResponse<OrderVo> findOrder(@PathVariable Long orderId) {
        return this.orderQueryService.findOrder(orderId);
    }

    /**
     * 查询订单分页列表
     *
     * @param orderPageQuery 订单分页查询参数
     * @return 订单分页查询结果
     */
    @GetMapping
    public PageResponse<OrderVo> findOrderPage(OrderPageQuery orderPageQuery) {
        return this.orderQueryService.findOrderPage(orderPageQuery);
    }

    /**
     * 创建订单
     *
     * @param orderCreateCommand 订单创建命令
     * @return 创建结果
     */
    @PostMapping
    public SingleResponse<OrderVo> createOrder(@RequestBody OrderCreateCommand orderCreateCommand) {
        return this.orderCommandService.createOrder(orderCreateCommand);
    }

    /**
     * 支付订单
     *
     * @param orderId 订单ID
     * @return 支付结果
     */
    @PostMapping("/pay/{orderId}")
    public Response payOrder(@PathVariable Long orderId) {
        return this.orderCommandService.payOrder(orderId);
    }

}
