package com.luo.d3s.light.jpa.ec.jpa;

import com.luo.d3s.ext.component.scan.D3sComponentScan;
import com.luo.d3s.ext.test.mock.BaseSpringMockMvcTest;
import com.luo.d3s.ext.test.mock.RabbitMqBrokerMockConfiguration;
import com.luo.d3s.ext.test.mock.RedisServerMockConfiguration;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;

/**
 * JPA自动生成DDL测试
 *
 * @author luohq
 * @date 2023-12-15 12:49
 */
@Disabled
@ActiveProfiles("jpa-h2-autoddl")
@Import({RabbitMqBrokerMockConfiguration.class, RedisServerMockConfiguration.class})
@D3sComponentScan(scanBasePackages = "com.luo.d3s.light.jpa.ec")
public class JpaAutoDdlTest extends BaseSpringMockMvcTest {

    private static final Logger log = LoggerFactory.getLogger(JpaAutoDdlTest.class);

    @Test
    void testStartupApplication() {
        log.info("查看当期module根目录下/jpa文件夹：jpa/autoddl-create.sql, jpa/autoddl-drop.sql");
    }
}
