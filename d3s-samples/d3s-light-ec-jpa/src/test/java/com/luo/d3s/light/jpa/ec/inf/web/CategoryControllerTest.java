package com.luo.d3s.light.jpa.ec.inf.web;

import com.luo.d3s.core.application.dto.PageQuery;
import com.luo.d3s.core.exception.ValidateException;
import com.luo.d3s.ext.test.junit5.params.JsonFileSource;
import com.luo.d3s.ext.test.matcher.MatcherFactory;
import com.luo.d3s.light.jpa.ec.base.BaseSpringTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.http.MediaType;
import org.springframework.util.ObjectUtils;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * 分类Controller - 单元测试
 *
 * @author luohq
 * @date 2023-01-07 14:50
 */
public class CategoryControllerTest extends BaseSpringTest {

    /**
     * Junit5 Json参数文件路径
     */
    private static final String JSON_SOURCE_PATH = "classpath:jsonSource/category.json";

    @Test
    void findCategoryTest() throws Exception {
        Long categoryId = 1L;
        //get pathParam参数
        this.mockMvc.perform(get(String.format("/api/v1/categories/%s", categoryId)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_SUCCESS).value(true))
                .andExpect(jsonPath(JSON_PATH_DATA).isNotEmpty())
                .andExpect(jsonPath(JSON_PATH_DATA + ".categoryName").value("分类1"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"", "1", "1", "2", "11", "21", "33"})
    void findCategoryTreeTest(String categoryId) throws Exception {
        String apiPath = categoryId.isEmpty()
                ? "/api/v1/categories/tree"
                : String.format("/api/v1/categories/tree/%s", categoryId);
        //get pathParam参数
        this.mockMvc.perform(get(apiPath))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_SUCCESS).value(true))
                .andExpect(jsonPath(JSON_PATH_DATA).value(MatcherFactory.match("category tree result error", data -> {
                    //仅categoryId为33时，不存在数据
                    Boolean shouldEmpty = "33".equals(categoryId);
                    Boolean isEmptyData = ObjectUtils.isEmpty(data);
                    return shouldEmpty ? isEmptyData : !isEmptyData;
                })));
    }

    @ParameterizedTest
    @CsvSource(value = {
            "1,3",
            "2,3",
            "3,3"
    })
    void findCategoryPageTest_differentPages(String pageIndex, String pageSize) throws Exception {
        //get pathParam参数
        this.mockMvc.perform(get("/api/v1/categories")
                        .queryParam(PARAM_PAGE_INDEX, pageIndex)
                        .queryParam(PARAM_PAGE_SIZE, pageSize)
                        //此处orderBy列名直接对应Entity属性名（由JPA自动转换为底层数据库列名category_name）
                        .queryParam(PARAM_ORDER_BY, "categoryName")
                        .queryParam(PARAM_ORDER_DIRECTION, PageQuery.ASC)
                        .queryParam("createTimeStart", "2023-01-01 00:00:00")
                        .queryParam("createTimeEnd", "2030-01-01 12:33:10")
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_SUCCESS).value(true))
                .andExpect(jsonPath(JSON_PATH_DATA).isNotEmpty())
                .andExpect(jsonPath(JSON_PATH_TOTAL_COUNT).value(MatcherFactory.greaterThan(0, "totalCount should be gt 0")));
    }

    @Test
    void findCategoryPageTest_nullParam() throws Exception {
        //get pathParam参数
        this.mockMvc.perform(get("/api/v1/categories")
                        .queryParam(PARAM_PAGE_INDEX, DEFAULT_PAGE_INDEX)
                        .queryParam(PARAM_PAGE_SIZE, DEFAULT_PAGE_SIZE)
                         //此处orderBy列名直接对应Entity属性名（由JPA自动转换为底层数据库列名category_name）
                        .queryParam(PARAM_ORDER_BY, "categoryName")
                        .queryParam(PARAM_ORDER_DIRECTION, PageQuery.DESC)
                        //.queryParam("createTimeStart", "2023-01-01 00:00:00")
                        //.queryParam("createTimeEnd", "2030-01-01 12:33:10")
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_SUCCESS).value(true))
                .andExpect(jsonPath(JSON_PATH_DATA).isNotEmpty())
                .andExpect(jsonPath(JSON_PATH_TOTAL_COUNT).value(MatcherFactory.greaterThan(0, "totalCount should be gt 0")));
    }

    @ParameterizedTest
    @JsonFileSource(jsonKey = "$.categoryCreateCommand", resource = JSON_SOURCE_PATH)
    void createCategoryTest(String createCategoryCommandJson) throws Exception {
        this.mockMvc.perform(post("/api/v1/categories")
                        //application/json请求体
                        .contentType(MediaType.APPLICATION_JSON)
                        //具体请求Json参数
                        .content(createCategoryCommandJson))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_SUCCESS).value(true))
                .andExpect(jsonPath(JSON_PATH_DATA + ".id").isNotEmpty());
    }

    @ParameterizedTest
    @JsonFileSource(jsonKey = "$.categoryCreateCommandFailure", resource = JSON_SOURCE_PATH)
    void createCategoryFailureTest(String createCategoryCommandJson) throws Exception {
        this.mockMvc.perform(post("/api/v1/categories")
                        //application/json请求体
                        .contentType(MediaType.APPLICATION_JSON)
                        //具体请求Json参数
                        .content(createCategoryCommandJson))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_SUCCESS).value(false))
                .andExpect(jsonPath(JSON_PATH_ERR_CODE).value(ValidateException.DEFAULT_ERR_CODE))
                .andExpect(jsonPath(JSON_PATH_ERR_MESSAGE).isNotEmpty());
    }

    @ParameterizedTest
    @JsonFileSource(jsonKey = "$.categoryModifyCommand", resource = JSON_SOURCE_PATH)
    void modifyCategoryTest(String modifyCategoryCommandJson) throws Exception {
        this.mockMvc.perform(put("/api/v1/categories")
                        //application/json请求体
                        .contentType(MediaType.APPLICATION_JSON)
                        //具体请求Json参数
                        .content(modifyCategoryCommandJson))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_SUCCESS).value(true));
    }

    @ParameterizedTest
    @JsonFileSource(jsonKey = "$.categoryModifyCommandFailure", resource = JSON_SOURCE_PATH)
    void modifyCategoryFailureTest(String modifyCategoryCommandJson) throws Exception {
        this.mockMvc.perform(put("/api/v1/categories")
                        //application/json请求体
                        .contentType(MediaType.APPLICATION_JSON)
                        //具体请求Json参数
                        .content(modifyCategoryCommandJson))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_SUCCESS).value(false))
                .andExpect(jsonPath(JSON_PATH_ERR_CODE).value(ValidateException.DEFAULT_ERR_CODE))
                .andExpect(jsonPath(JSON_PATH_ERR_MESSAGE).isNotEmpty());
    }


    @ParameterizedTest
    @ValueSource(longs = {12, 42, 4})
    void removeCategoryTest(Long categoryId) throws Exception {
        this.mockMvc.perform(delete("/api/v1/categories/{categoryId}", categoryId)
                        //application/json请求体
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_SUCCESS).value(true));
    }

    //@ParameterizedTest
    //@ValueSource(longs = {1, 11, 2})
    //void removeCategoryFailureTest(Long categoryId) throws Exception {
    //    this.mockMvc.perform(delete("/api/v1/categories/{categoryId}", categoryId)
    //                    //application/json请求体
    //                    .contentType(MediaType.APPLICATION_JSON))
    //            .andDo(print())
    //            .andExpect(status().isOk())
    //            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    //            .andExpect(jsonPath(JSON_PATH_SUCCESS).value(false))
    //            .andExpect(jsonPath(JSON_PATH_ERR_CODE).value(ValidateException.DEFAULT_ERR_CODE))
    //            .andExpect(jsonPath(JSON_PATH_ERR_MESSAGE).isNotEmpty());
    //}
}
