package com.luo.d3s.light.jpa.ec.app;

import com.luo.d3s.ext.component.bus.DomainRegistry;
import com.luo.d3s.light.jpa.ec.base.BaseSpringTest;
import com.luo.d3s.light.jpa.ec.domain.service.CategorySubIdsService;
import com.luo.d3s.light.jpa.ec.domain.specification.CategorySpecification;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * 领域注册中心测试
 *
 * @author luohq
 * @date 2023-01-18 10:14
 */
public class DomainRegistryTest extends BaseSpringTest {

    @Test
    void testDomainRegistry() {
        //OrderCommandService orderCommandService = DomainRegistry.applicationService(OrderCommandService.class);
        //Assertions.assertNotNull(orderCommandService);
        //
        //OrderQueryService orderQueryService = DomainRegistry.applicationService(OrderQueryService.class);
        //Assertions.assertNotNull(orderQueryService);

        CategorySubIdsService categorySubIdsService = DomainRegistry.domainService(CategorySubIdsService.class);
        Assertions.assertNotNull(categorySubIdsService);

        CategorySpecification categorySpecification = DomainRegistry.specification(CategorySpecification.class);
        Assertions.assertNotNull(categorySpecification);
    }
}
