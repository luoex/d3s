package com.luo.d3s.light.jpa.ec.interfaces.web;

import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.core.application.dto.Response;
import com.luo.d3s.core.application.dto.SingleResponse;
import com.luo.d3s.light.jpa.ec.application.dto.command.GoodsTagCreateCommand;
import com.luo.d3s.light.jpa.ec.application.dto.command.GoodsTagModifyCommand;
import com.luo.d3s.light.jpa.ec.application.dto.query.GoodsTagPageQuery;
import com.luo.d3s.light.jpa.ec.application.dto.vo.GoodsTagVo;
import com.luo.d3s.light.jpa.ec.application.service.GoodsTagCommandService;
import com.luo.d3s.light.jpa.ec.application.service.GoodsTagQueryService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 商品标签Controller
 *
 * @author luohq
 * @date 2023-08-03
 */
@RestController
@RequestMapping("/api/v1/goods_tags")
public class GoodsTagController {
    @Resource
    private GoodsTagQueryService goodsTagQueryService;

    @Resource
    private GoodsTagCommandService goodsTagCommandService;

    /**
     * 根据商品标签ID查询商品标签信息
     *
     * @param goodsTagId 商品标签ID
     * @return 商品标签信息
     */
    @GetMapping("/{goodsTagId}")
    public SingleResponse<GoodsTagVo> findGoodsTag(@PathVariable Long goodsTagId) {
        return this.goodsTagQueryService.findGoodsTag(goodsTagId);
    }

    /**
     * 查询商品标签分页列表
     *
     * @param goodsTagPageQuery 商品标签分页查询参数
     * @return 商品标签分页查询结果
     */
    @GetMapping
    public PageResponse<GoodsTagVo> findGoodsTagPage(GoodsTagPageQuery goodsTagPageQuery) {
        return this.goodsTagQueryService.findGoodsTagPage(goodsTagPageQuery);
    }

    /**
     * 创建商品标签
     *
     * @param goodsTagCreateCommand 商品标签创建命令
     * @return 创建结果
     */
    @PostMapping
    public SingleResponse<GoodsTagVo> createGoodsTag(@RequestBody GoodsTagCreateCommand goodsTagCreateCommand) {
        return this.goodsTagCommandService.createGoodsTag(goodsTagCreateCommand);
    }

    /**
     * 修改商品标签
     *
     * @param goodsTagModifyCommand 商品标签修改命令
     * @return 修改结果
     */
    @PutMapping
    public Response modifyGoodsTag(@RequestBody GoodsTagModifyCommand goodsTagModifyCommand) {
        return this.goodsTagCommandService.modifyGoodsTag(goodsTagModifyCommand);
    }

    /**
     * 删除商品标签
     *
     * @param goodsTagId 商品标签ID
     * @return 删除结果
     */
    @DeleteMapping("/{goodsTagId}")
    public Response removeGoodsTag(@PathVariable Long goodsTagId) {
        return this.goodsTagCommandService.removeGoodsTag(goodsTagId);
    }

}
