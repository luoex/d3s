package com.luo.d3s.light.jpa.ec.infrastructure.database;

/**
 * sql语句定义
 *
 * @author luohq
 * @date 2023-07-26
 */
public interface SqlStatements {

    interface Category {
        String findRootCategoryTreeVoList_name = "Category.findRootCategoryTreeVoList";
        String findRootCategoryTreeVoList_sql = "select " +
                "id, " +
                "parent_category_id, " +
                "category_name, " +
                "category_desc, " +
                "create_time, " +
                "update_time " +
                "from category " +
                "where " +
                "(:categoryId is not null and id = :categoryId) " +
                "or " +
                "(:categoryId is null and parent_category_id is null)";
    }
}
