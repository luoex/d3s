package com.luo.d3s.light.jpa.ec.domain.order.model;

import com.luo.d3s.light.jpa.ec.domain.base.BaseEntity;
import com.luo.d3s.light.jpa.ec.domain.base.BizId;
import com.luo.d3s.light.jpa.ec.domain.base.Count;
import com.luo.d3s.light.jpa.ec.domain.base.Price;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * 商品ID
 *
 * @author luohq
 * @date 2022-11-27 16:14
 */
@Entity
public class OrderGoods extends BaseEntity {
    /**
     * 商品ID
     */
    @Embedded
    @AttributeOverride(name = "id", column = @Column(name = "goods_id"))
    @NotNull
    private BizId goodsId;

    /**
     * 订单ID
     */
    @Embedded
    @AttributeOverride(name = "id", column = @Column(name = "order_id"))
    @NotNull
    private BizId orderId;

    /**
     * 商品数量
     */
    @Embedded
    @AttributeOverride(name = "count", column = @Column(name = "goods_count"))
    @NotNull
    private Count goodsCount;

    /**
     * 商品单价
     */
    @Embedded
    @AttributeOverride(name = "price", column = @Column(name = "goods_price"))
    @NotNull
    private Price goodsPrice;

    /**
     * 商品总价
     */
    @Embedded
    @AttributeOverride(name = "price", column = @Column(name = "goods_sum_price"))
    @NotNull
    private Price goodsSumPrice;


    OrderGoods() {
    }

    /**
     * 初始创建订单商品
     *
     * @param id            订单商品ID
     * @param goodsId       商品ID
     * @param orderId       订单ID
     * @param goodsCount    商品数量
     * @param goodsPrice    商品单价
     * @param goodsSumPrice 商品总价
     */
    public OrderGoods(BizId id, BizId goodsId, BizId orderId, Count goodsCount, Price goodsPrice, Price goodsSumPrice) {
        //订单商品相关属性
        this.id = id;
        this.goodsId = goodsId;
        this.orderId = orderId;
        this.goodsCount = goodsCount;
        this.goodsPrice = goodsPrice;
        this.goodsSumPrice = goodsSumPrice;
        this.createTime = LocalDateTime.now();
        this.updateTime = this.createTime;
        this.validateSelf();
    }

    public BizId getGoodsId() {
        return goodsId;
    }

    public BizId getOrderId() {
        return orderId;
    }

    public Count getGoodsCount() {
        return goodsCount;
    }

    public Price getGoodsPrice() {
        return goodsPrice;
    }

    public Price getGoodsSumPrice() {
        return goodsSumPrice;
    }

    @Override
    public String toString() {
        return "OrderGoods{" +
                "id=" + id +
                ", goodsId=" + goodsId +
                ", orderId=" + orderId +
                ", goodsCount=" + goodsCount +
                ", goodsPrice=" + goodsPrice +
                ", goodsSumPrice=" + goodsSumPrice +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
