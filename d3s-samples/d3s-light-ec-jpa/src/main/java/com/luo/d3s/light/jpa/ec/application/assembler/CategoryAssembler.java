package com.luo.d3s.light.jpa.ec.application.assembler;

import com.luo.d3s.core.util.Optionals;
import com.luo.d3s.light.jpa.ec.application.dto.vo.CategoryVo;
import com.luo.d3s.light.jpa.ec.domain.base.BizId;
import com.luo.d3s.light.jpa.ec.domain.category.Category;
import com.luo.d3s.light.jpa.ec.domain.category.CategoryDesc;


/**
 * Category转换器
 *
 * @author luohq
 * @date 2023-01-04 17:10
 */
public class CategoryAssembler {

    /**
     * 转换分类实体为VO
     *
     * @param category 分类实体
     * @return 分类VO
     */
    public static CategoryVo toCategoryVo(Category category) {
        if (null == category) {
            return null;
        }
        CategoryVo categoryVo = new CategoryVo();
        categoryVo.setId(category.getId().getId());
        categoryVo.setParentCategoryId(Optionals.defaultNull(category.getParentCategoryId(), BizId::getId));
        categoryVo.setCategoryName(category.getCategoryName().getCategoryName());
        categoryVo.setCategoryDesc(Optionals.defaultNull(category.getCategoryDesc(), CategoryDesc::getCategoryDesc));
        categoryVo.setCreateTime(category.getCreateTime());
        categoryVo.setUpdateTime(category.getUpdateTime());
        return categoryVo;
    }
}
