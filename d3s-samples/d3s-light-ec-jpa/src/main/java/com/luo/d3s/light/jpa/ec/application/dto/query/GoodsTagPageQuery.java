package com.luo.d3s.light.jpa.ec.application.dto.query;

import com.luo.d3s.core.application.dto.PageQuery;
import lombok.Data;
import lombok.ToString;

import java.time.LocalDateTime;

/**
 * 标签分页查询
 *
 * @author luohq
 * @date 2023-08-03
 */
@Data
@ToString(callSuper = true)
public class GoodsTagPageQuery extends PageQuery {
    /**
     * 标签名称
     */
    private String tagName;

    /**
     * 起始创建时间
     */
    private LocalDateTime createTimeStart;

    /**
     * 结束创建时间
     */
    private LocalDateTime createTimeEnd;

}
