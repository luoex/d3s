package com.luo.d3s.light.jpa.ec.application.service.impl;

import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.core.application.dto.SingleResponse;
import com.luo.d3s.light.jpa.ec.application.dto.query.GoodsPageQuery;
import com.luo.d3s.light.jpa.ec.application.dto.vo.GoodsVo;
import com.luo.d3s.light.jpa.ec.application.service.GoodsQueryService;
import com.luo.d3s.light.jpa.ec.domain.goods.GoodsRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 商品查询服务实现类 - infrastructure实现（适用于CQRS中的Query处理）
 *
 * @author luohq
 * @date 2023-01-07 11:12
 */
@Service
public class GoodsQueryServiceImpl implements GoodsQueryService {

    @Resource
    private GoodsRepository goodsRepository;


    @Override
    public SingleResponse<GoodsVo> findGoods(Long goodsId) {
        //查询商品基础信息
        GoodsVo goodsVo = this.goodsRepository.findGoodsWithCNameById(goodsId);
        //商品标签集合
        goodsVo.setTags(this.goodsRepository.findGoodsTags(goodsId));
        return SingleResponse.of(goodsVo);
    }

    @Override
    public PageResponse<GoodsVo> findGoodsPage(GoodsPageQuery goodsPageQuery) {
        return this.goodsRepository.findGoodsWithCNamePage(goodsPageQuery);
    }
}
