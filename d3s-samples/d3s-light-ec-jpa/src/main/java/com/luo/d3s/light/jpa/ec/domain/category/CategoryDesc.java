package com.luo.d3s.light.jpa.ec.domain.category;

import com.luo.d3s.core.domain.model.ValueObject;

import javax.persistence.Embeddable;
import javax.validation.constraints.Size;

/**
 * 分类描述
 *
 * @author luohq
 * @date 2023-01-04 13:21
 */
@Embeddable
public class CategoryDesc implements ValueObject {

    /**
     * 分类描述
     */
    @Size(min = 1, max = 512)
    private String categoryDesc;

    public CategoryDesc() {
    }

    public CategoryDesc(String categoryDesc) {
        this.categoryDesc = categoryDesc;
        this.validateSelf();
    }

    public String getCategoryDesc() {
        return categoryDesc;
    }

    @Override
    public String toString() {
        return "CategoryDesc{" +
                "categoryDesc='" + categoryDesc + '\'' +
                '}';
    }
}
