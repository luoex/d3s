package com.luo.d3s.light.jpa.ec.domain.order.model;

import com.luo.d3s.core.domain.model.ValueObject;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * 快递单号
 *
 * @author luohq
 * @date 2022-11-27 16:03
 */
@Embeddable
public class ExpressCode implements ValueObject {
    /**
     * 快递单号
     */
    @NotBlank
    @Size(min = 1, max = 128)
    private String expressCode;

    ExpressCode() {
    }

    public ExpressCode(String expressCode) {
        this.expressCode = expressCode;
        this.validateSelf();
    }

    public String getExpressCode() {
        return expressCode;
    }

    @Override
    public String toString() {
        return "ExpressCode{" +
                "expressCode='" + expressCode + '\'' +
                '}';
    }
}
