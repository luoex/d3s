package com.luo.d3s.light.jpa.ec.domain.category;

import com.luo.d3s.core.util.Optionals;
import com.luo.d3s.light.jpa.ec.application.dto.command.CategoryCreateCommand;
import com.luo.d3s.light.jpa.ec.domain.base.BizId;

import java.time.LocalDateTime;

/**
 * 分类创建工厂<br/>
 * 注：领域对象创建工厂，强调初始创建领域对象的操作（区别于技术层面的构造函数）
 *
 * @author luohq
 * @date 2023-01-04 16:44
 */
public class CategoryFactory {

    /**
     * 创建分类
     *
     * @param parentCategoryId 上级分类ID
     * @param categoryName     分类名称
     * @param categoryDesc     分类描述
     * @return 初始分类
     */
    public static Category createCategory(BizId parentCategoryId, CategoryName categoryName, CategoryDesc categoryDesc) {
        LocalDateTime now = LocalDateTime.now();
        return new Category(
                BizId.newBizId(),
                parentCategoryId,
                categoryName,
                categoryDesc
        );
    }

    /**
     * 根据创建命令创建分类
     *
     * @param categoryCreateCommand 创建命令
     * @return 初始分类
     */
    public static Category createCategory(CategoryCreateCommand categoryCreateCommand) {
        return new Category(
                BizId.newBizId(),
                Optionals.defaultNull(categoryCreateCommand.getParentCategoryId(), BizId::fromValue),
                new CategoryName(categoryCreateCommand.getCategoryName()),
                Optionals.defaultNull(categoryCreateCommand.getCategoryDesc(), CategoryDesc::new)
        );
    }
}
