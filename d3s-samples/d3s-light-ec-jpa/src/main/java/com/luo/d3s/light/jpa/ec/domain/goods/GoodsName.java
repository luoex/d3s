package com.luo.d3s.light.jpa.ec.domain.goods;

import com.luo.d3s.core.domain.model.ValueObject;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * 商品名称
 *
 * @author luohq
 * @date 2022-11-27 18:06
 */
@Embeddable
public class GoodsName implements ValueObject {

    /**
     * 商品名称
     */
    @NotBlank
    @Size(max = 120)
    private String goodsName;

    private GoodsName() {

    }

    public GoodsName(String goodsName) {
        this.goodsName = goodsName;
        this.validateSelf();
    }

    public String getGoodsName() {
        return goodsName;
    }

    @Override
    public String toString() {
        return "GoodsName{" +
                "goodsName='" + goodsName + '\'' +
                '}';
    }
}
