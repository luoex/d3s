package com.luo.d3s.light.jpa.ec.domain.category;

import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.core.infrastructure.convertor.JpaPageConvertor;
import com.luo.d3s.light.jpa.ec.application.dto.query.CategoryPageQuery;
import com.luo.d3s.light.jpa.ec.application.dto.vo.CategoryTreeVo;
import com.luo.d3s.light.jpa.ec.domain.base.BaseRepository;
import com.luo.d3s.light.jpa.ec.domain.base.BizId;
import com.luo.d3s.light.jpa.ec.infrastructure.database.specification.CategoryPageQuerySpecification;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Category repository
 *
 * @author luohq
 * @date 2022-11-27 18:35
 */
public interface CategoryRepository extends BaseRepository<Category, BizId> {

    /**
     * 根据ID查询分类<br/>
     * 等价于：findById(new BizId(id))
     *
     * @param id
     * @return
     */
    @Deprecated
    Category findById_id(Long id);

    /**
     * 是否已存在分类名称（若excludeCategoryId非空，则排除excludeCategoryId所对应的分类后再做比较）
     *
     * @param categoryName      分类名称
     * @param excludeCategoryId 不包含的分类ID
     * @return 是否存在
     */
    Boolean existsByCategoryNameAndIdNot(CategoryName categoryName, BizId excludeCategoryId);

    /**
     * 是否已存在分类名称
     *
     * @param categoryName      分类名称
     * @return 是否存在
     */
    Boolean existsByCategoryName(CategoryName categoryName);

    /**
     * 根据上级分类ID查询子分类列表
     *
     * @param parentCategoryId 上级分类ID
     * @return 根据上级分类ID查询子分类列表
     */
    List<Category> findByParentCategoryId(BizId parentCategoryId);

    /**
     * find page
     *
     * @param categoryPageQuery page query param
     * @return page response
     */
    default PageResponse<Category> findPage(CategoryPageQuery categoryPageQuery) {
        Page<Category> categoryPageResult = this.findAll(
                new CategoryPageQuerySpecification(categoryPageQuery),
                JpaPageConvertor.toPage(categoryPageQuery)
        );
        return JpaPageConvertor.toPageResponse(categoryPageResult);
    }

    /**
     * 根据分类ID查询分类TreeVo列表，若分类ID为空则查询根分类TreeVo列表
     *
     * @param categoryId 分类ID
     * @return 分类TreeVo列表
     */
    @Query(nativeQuery = true)
    List<CategoryTreeVo> findRootCategoryTreeVoList(@Param("categoryId") Long categoryId);

    /**
     * 根据上级分类ID查询子分类TreeVo列表
     *
     * @param parentCategoryId 上级分类ID
     * @return 子分类TreeVo列表
     */
    @Query(nativeQuery = true)
    List<CategoryTreeVo> findSubCategoryTreeVoList(@Param("parentCategoryId") Long parentCategoryId);

}
