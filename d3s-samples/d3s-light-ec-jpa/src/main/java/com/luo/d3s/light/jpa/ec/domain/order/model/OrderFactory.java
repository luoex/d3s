package com.luo.d3s.light.jpa.ec.domain.order.model;

import com.luo.d3s.light.jpa.ec.domain.base.BizId;
import com.luo.d3s.light.jpa.ec.domain.base.Count;
import com.luo.d3s.light.jpa.ec.domain.base.Price;

import java.util.List;

/**
 * 订单创建工厂<br/>
 * 注：领域对象创建工厂，强调初始创建领域对象的操作（区别于技术层面的构造函数）
 *
 * @author luohq
 * @date 2022-12-28 10:02
 */
public class OrderFactory {


    /**
     * 创建初始订单
     * @param orderId        订单ID
     * @param orderPrice     订单价格
     * @param receiveAddress 订单收货地址
     * @param orderGoodsList 订单商品项
     * @return 初始订单
     */
    public static Order createOrder(BizId orderId, Price orderPrice, ReceiveAddress receiveAddress, List<OrderGoods> orderGoodsList) {
        return new Order(
                orderId,
                orderPrice,
                receiveAddress,
                orderGoodsList
        );
    }

    /**
     * 创建初始订单商品项
     *
     * @param goodsId       商品ID
     * @param orderId       订单ID
     * @param goodsCount    商品数量
     * @param goodsPrice    商品单价
     * @param goodsSumPrice 商品总价
     * @return 初始订单商品项
     */
    public static OrderGoods createOrderGoods(BizId goodsId, BizId orderId, Count goodsCount, Price goodsPrice, Price goodsSumPrice) {
        return new OrderGoods(
                BizId.newBizId(),
                goodsId,
                orderId,
                goodsCount,
                goodsPrice,
                goodsSumPrice
        );
    }
}
