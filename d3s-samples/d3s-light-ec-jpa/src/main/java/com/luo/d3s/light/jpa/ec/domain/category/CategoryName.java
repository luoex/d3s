package com.luo.d3s.light.jpa.ec.domain.category;

import com.luo.d3s.core.domain.model.ValueObject;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * 分类名称
 *
 * @author luohq
 * @date 2023-01-04 13:21
 */
@Embeddable
public class CategoryName implements ValueObject {

    /**
     * 分类名称
     */
    @NotBlank
    @Size(min = 1, max = 64)
    private String categoryName;

    private CategoryName() {
    }


    public CategoryName(String categoryName) {
        this.categoryName = categoryName;
        this.validateSelf();
    }

    public String getCategoryName() {
        return categoryName;
    }

    @Override
    public String toString() {
        return "CategoryName{" +
                "categoryName='" + categoryName + '\'' +
                '}';
    }
}
