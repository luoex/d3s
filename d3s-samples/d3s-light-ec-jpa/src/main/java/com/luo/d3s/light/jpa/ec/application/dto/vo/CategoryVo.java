package com.luo.d3s.light.jpa.ec.application.dto.vo;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * 分类VO
 *
 * @author luohq
 * @date 2023-01-04 16:00
 */
@Data
public class CategoryVo {
    /**
     * 分类ID
     */
    private Long id;

    /**
     * 上级分类ID
     */
    private Long parentCategoryId;

    /**
     * 分类名称
     */
    private String categoryName;

    /**
     * 分类描述
     */
    private String categoryDesc;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 修改时间
     */
    private LocalDateTime updateTime;
}
