package com.luo.d3s.light.jpa.ec.domain.order.repository;

import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.core.infrastructure.convertor.JpaPageConvertor;
import com.luo.d3s.light.jpa.ec.application.dto.query.OrderPageQuery;
import com.luo.d3s.light.jpa.ec.domain.base.BaseRepository;
import com.luo.d3s.light.jpa.ec.domain.base.BizId;
import com.luo.d3s.light.jpa.ec.domain.order.model.Order;
import com.luo.d3s.light.jpa.ec.infrastructure.database.specification.OrderPageQuerySpecification;
import org.springframework.data.domain.Page;

/**
 * Order repository
 *
 * @author luohq
 * @date 2022-11-27 18:35
 */
public interface OrderRepository extends BaseRepository<Order, BizId> {

    /**
     * find page
     *
     * @param orderPageQuery page query param
     * @return page response
     */
    default PageResponse<Order> findPage(OrderPageQuery orderPageQuery) {
        Page<Order> orderPageResult = this.findAll(
                new OrderPageQuerySpecification(orderPageQuery),
                JpaPageConvertor.toPage(orderPageQuery));
        return JpaPageConvertor.toPageResponse(orderPageResult);
    }
}
