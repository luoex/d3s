package com.luo.d3s.light.jpa.ec.application.dto.vo;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * 商品标签VO
 *
 * @author luohq
 * @date 2023-08-02
 */
@Data
public class GoodsTagVo {

    /**
     * 商品ID
     */
    private Long id;

    /**
     *标签名称
     */
    private String tagName;

    /**
     *标签描述
     */
    private String tagDesc;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    public GoodsTagVo() {
    }

    /**
     * 构造函数 - JPA sql-result-set-mapping使用
     */
    public GoodsTagVo(Long id, String tagName, String tagDesc, LocalDateTime createTime, LocalDateTime updateTime) {
        this.id = id;
        this.tagName = tagName;
        this.tagDesc = tagDesc;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }
}
