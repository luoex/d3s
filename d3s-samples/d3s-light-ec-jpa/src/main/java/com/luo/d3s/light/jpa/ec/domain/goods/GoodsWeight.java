package com.luo.d3s.light.jpa.ec.domain.goods;

import com.luo.d3s.core.domain.model.ValueObject;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

/**
 * 商品重量
 *
 * @author luohq
 * @date 2023-01-04 16:22
 */
@Embeddable
public class GoodsWeight implements ValueObject {
    /**
     * 重量值
     */
    @NotNull
    @PositiveOrZero
    private BigDecimal weight;
    /**
     * 重量单位
     */
    @NotNull
    private WeightUnit unit;

    private GoodsWeight() {
    }

    public GoodsWeight(BigDecimal weight, WeightUnit unit) {
        this.weight = weight;
        this.unit = unit;
        this.validateSelf();
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public WeightUnit getUnit() {
        return unit;
    }

    @Override
    public String toString() {
        return "GoodsWeight{" +
                "weight=" + weight +
                ", unit=" + unit +
                '}';
    }

    public static GoodsWeightBuilder builder() {
        return new GoodsWeightBuilder();
    }

    public static final class GoodsWeightBuilder {
        private BigDecimal weight;
        private WeightUnit unit;

        private GoodsWeightBuilder() {
        }

        public GoodsWeightBuilder weight(BigDecimal weight) {
            this.weight = weight;
            return this;
        }

        public GoodsWeightBuilder unit(WeightUnit unit) {
            this.unit = unit;
            return this;
        }

        public GoodsWeight build() {
            return new GoodsWeight(weight, unit);
        }
    }
}
