package com.luo.d3s.light.jpa.ec.domain.specification;

import com.luo.d3s.core.domain.service.specifcation.AbstractSpecification;
import com.luo.d3s.core.util.validation.Validates;
import com.luo.d3s.light.jpa.ec.domain.base.BizId;
import com.luo.d3s.light.jpa.ec.domain.goods.GoodsRepository;

import java.util.Collection;

/**
 * 商品是否存在验证规则
 *
 * @author luohq
 * @date 2023-01-06 15:38
 */
public class GoodsExistSpecification extends AbstractSpecification<Collection<BizId>> {

    private GoodsRepository goodsRepository;

    public GoodsExistSpecification(GoodsRepository goodsRepository) {
        this.goodsRepository = goodsRepository;
    }

    @Override
    public boolean isSatisfiedBy(Collection<BizId> goodsIds) {
        //查询存在的商品数量
        Integer existGoodsCount = this.goodsRepository.countByIdIn(goodsIds);
        Validates.isTrue(existGoodsCount.equals(goodsIds.size()), "goods don't exist");
        return true;
    }
}
