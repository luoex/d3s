package com.luo.d3s.light.jpa.ec.application.dto.vo;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 分类树形VO
 *
 * @author luohq
 * @date 2023-01-04 16:00
 */
@Data
public class CategoryTreeVo {
    /**
     * 分类ID
     */
    private Long id;

    /**
     * 上级分类ID
     */
    private Long parentCategoryId;

    /**
     * 分类名称
     */
    private String categoryName;

    /**
     * 分类描述
     */
    private String categoryDesc;

    /**
     * 下级分类列表
     */
    private List<CategoryTreeVo> subCategories;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    public CategoryTreeVo() {
    }

    public CategoryTreeVo(Long id, Long parentCategoryId, String categoryName, String categoryDesc, LocalDateTime createTime, LocalDateTime updateTime) {
        //该构造函数用于JPA sql-result-set-mapping映射
        this.id = id;
        this.parentCategoryId = parentCategoryId;
        this.categoryName = categoryName;
        this.categoryDesc = categoryDesc;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }
}
