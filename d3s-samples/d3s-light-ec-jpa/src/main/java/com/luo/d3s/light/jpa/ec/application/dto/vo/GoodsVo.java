package com.luo.d3s.light.jpa.ec.application.dto.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Goods VO
 *
 * @author luohq
 * @date 2022-12-21 9:42
 */
@Data
public class GoodsVo {

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 所属分类ID
     */
    private Long categoryId;
    /**
     * 分类名称
     */
    private String categoryName;

    /**
     * 商品标签集合
     */
    private List<GoodsTagVo> tags;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 生产日期
     */
    private LocalDate manufactureDate;

    /**
     * 过期日期
     */
    private LocalDate expirationDate;

    /**
     * 商品重量
     */
    private BigDecimal goodsWeight;

    /**
     * 商品重量单位
     */
    private String goodsWeightUnit;

    /**
     * 商品介绍
     */
    private String goodsDesc;

    /**
     * 商品价格
     */
    private BigDecimal goodsPrice;

    /**
     * 商品状态(10已上架, 20已下架)
     */
    private Integer goodsStatus;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    public GoodsVo() {
    }

    /**
     * 构造参数 - JPA sql-result-set-mapping使用
     */
    public GoodsVo(Long id,
                   Long categoryId,
                   String categoryName,
                   String goodsName,
                   LocalDate manufactureDate,
                   LocalDate expirationDate,
                   BigDecimal goodsWeight,
                   String goodsWeightUnit,
                   String goodsDesc,
                   BigDecimal goodsPrice,
                   Integer goodsStatus,
                   LocalDateTime createTime,
                   LocalDateTime updateTime) {
        this.id = id;
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.goodsName = goodsName;
        this.manufactureDate = manufactureDate;
        this.expirationDate = expirationDate;
        this.goodsWeight = goodsWeight;
        this.goodsWeightUnit = goodsWeightUnit;
        this.goodsDesc = goodsDesc;
        this.goodsPrice = goodsPrice;
        this.goodsStatus = goodsStatus;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }
}
