package com.luo.d3s.light.jpa.ec.domain.goodstag;

import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.core.infrastructure.convertor.JpaPageConvertor;
import com.luo.d3s.light.jpa.ec.application.dto.query.GoodsTagPageQuery;
import com.luo.d3s.light.jpa.ec.domain.base.BaseRepository;
import com.luo.d3s.light.jpa.ec.domain.base.BizId;
import com.luo.d3s.light.jpa.ec.infrastructure.database.specification.GoodsTagPageQuerySpecification;
import org.springframework.data.domain.Page;

import java.util.Collection;

/**
 * GoodsTag repository
 *
 * @author luohq
 * @date 2023-08-02
 */
public interface GoodsTagRepository extends BaseRepository<GoodsTag, BizId> {

    /**
     * 查询指定标签ID集合对应的标签数量
     *
     * @param ids 标签ID集合
     * @return 存在的标签数量
     */
    Integer countByIdIn(Collection<BizId> ids);

    /**
     * 根据标签名称查询商品标签
     *
     * @param tagName 标签名称
     * @return 商品标签
     */
    GoodsTag findByTagName(String tagName);

    /**
     * 查询商品标签分页列表
     *
     * @param goodsTagPageQuery 查询参数
     * @return 品标签分页列表
     */
    default PageResponse<GoodsTag> findPage(GoodsTagPageQuery goodsTagPageQuery) {
        Page<GoodsTag> goodsTagPageResult = this.findAll(
                new GoodsTagPageQuerySpecification(goodsTagPageQuery),
                JpaPageConvertor.toPage(goodsTagPageQuery)
        );
        return JpaPageConvertor.toPageResponse(goodsTagPageResult);
    }
}
