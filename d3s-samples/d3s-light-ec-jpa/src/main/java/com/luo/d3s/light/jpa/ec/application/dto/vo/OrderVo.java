package com.luo.d3s.light.jpa.ec.application.dto.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 订单VO
 *
 * @author luohq
 * @date 2022-11-27 19:11
 */
@Data
public class OrderVo {
    /**
     * 商品ID
     */
    private Long id;

    /**
     * 订单总价
     */
    private BigDecimal orderPrice;

    /**
     * 收货地址
     */
    private String receiveAddress;

    /**
     * 订单状态
     */
    private Integer orderStatus;

    /**
     * 订单创建时间
     */
    private LocalDateTime createTime;

    /**
     * 订单商品列表
     */
    private List<OrderGoodsVo> orderGoods;
}
