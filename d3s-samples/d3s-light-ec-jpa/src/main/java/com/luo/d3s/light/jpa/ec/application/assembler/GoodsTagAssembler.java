package com.luo.d3s.light.jpa.ec.application.assembler;


import com.luo.d3s.light.jpa.ec.application.dto.vo.GoodsTagVo;
import com.luo.d3s.light.jpa.ec.domain.goodstag.GoodsTag;

/**
 * GoodsTag转换器
 *
 * @author luohq
 * @date 2023-08-03
 */
public class GoodsTagAssembler {

    /**
     * 转换商品标签实体为VO
     *
     * @param goodsTag 商品标签实体
     * @return 商品标签VO
     */
    public static GoodsTagVo toGoodsTagVo(GoodsTag goodsTag) {
        if (null == goodsTag) {
            return null;
        }
        GoodsTagVo goodsTagVo = new GoodsTagVo();
        goodsTagVo.setId(goodsTag.getId().getId());
        goodsTagVo.setTagName(goodsTag.getTagName());
        goodsTagVo.setTagDesc(goodsTag.getTagDesc());
        goodsTagVo.setCreateTime(goodsTag.getCreateTime());
        goodsTagVo.setUpdateTime(goodsTag.getUpdateTime());
        return goodsTagVo;
    }
}
