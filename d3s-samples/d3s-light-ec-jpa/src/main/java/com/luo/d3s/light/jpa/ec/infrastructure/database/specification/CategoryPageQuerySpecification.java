package com.luo.d3s.light.jpa.ec.infrastructure.database.specification;

import com.luo.d3s.light.jpa.ec.application.dto.query.CategoryPageQuery;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.List;
import java.util.Objects;

/**
 * 分类分页列表查询条件（Spring Data JPA）
 *
 * @author luohq
 * @date 2023-07-26 11:07
 */
public class CategoryPageQuerySpecification implements Specification {

    private CategoryPageQuery categoryPageQuery;

    public CategoryPageQuerySpecification(CategoryPageQuery categoryPageQuery) {
        this.categoryPageQuery = categoryPageQuery;
    }

    @Override
    public Predicate toPredicate(Root root, CriteriaQuery cq, CriteriaBuilder cb) {

        Predicate conjunctionPredicate = cb.conjunction();
        List<Expression<Boolean>> expressions = conjunctionPredicate.getExpressions();
        //create_time >= :createTimeStart and create_time <= :createTimeEnd
        if (Objects.nonNull(this.categoryPageQuery.getCreateTimeStart())) {
            expressions.add(cb.greaterThanOrEqualTo(root.get("createTime"), this.categoryPageQuery.getCreateTimeStart()));
        }
        if (Objects.nonNull(this.categoryPageQuery.getCreateTimeEnd())) {
            expressions.add(cb.lessThanOrEqualTo(root.get("createTime"), this.categoryPageQuery.getCreateTimeEnd()));
        }
        return conjunctionPredicate;
    }
}
