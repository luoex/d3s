package com.luo.d3s.light.jpa.ec.infrastructure.database.convertor;

import com.luo.d3s.core.infrastructure.convertor.JpaVoEnumAttrConverter;
import com.luo.d3s.light.jpa.ec.domain.goods.GoodsStatus;

import javax.persistence.Converter;

/**
 * 商品状态 - JPA值对象枚举属性转换器
 *
 * @author luohq
 * @date 2023-07-27
 */
@Converter(autoApply = true)
public class GoodsStatusConverter extends JpaVoEnumAttrConverter<GoodsStatus, Integer> {
}