package com.luo.d3s.light.jpa.ec.domain.base;

import com.luo.d3s.core.domain.model.ValueObject;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

/**
 * 数量
 *
 * @author luohq
 * @date 2022-11-27 18:00
 */
@Embeddable
public class Count implements ValueObject {

    /**
     * 数量值
     */
    @NotNull
    @Positive
    private Integer count;

    private Count() {
    }

    public Count(Integer count) {
        this.count = count;
        this.validateSelf();
    }

    public Integer getCount() {
        return count;
    }
}
