package com.luo.d3s.light.jpa.ec.domain.base;

import com.luo.d3s.core.domain.model.ValueObject;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

/**
 * 订单金额
 *
 * @author luohq
 * @date 2022-11-27 15:56
 */
@Embeddable
public class Price implements ValueObject {

    /**
     * 价格值
     */
    @NotNull
    @Positive
    private BigDecimal price;

    private Price() {
    }

    public Price(BigDecimal price) {
        this.price = price;
        this.validateSelf();
    }

    public BigDecimal getPrice() {
        return price;
    }
}
