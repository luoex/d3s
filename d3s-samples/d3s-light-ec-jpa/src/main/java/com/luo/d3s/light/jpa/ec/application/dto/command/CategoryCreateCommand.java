package com.luo.d3s.light.jpa.ec.application.dto.command;

import lombok.Data;

/**
 * 创建分类Command
 *
 * @author luohq
 * @date 2023-01-04 15:29
 */
@Data
public class CategoryCreateCommand {
    /**
     * 上级分类ID
     */
    private Long parentCategoryId;
    /**
     * 分类名称
     */
    private String categoryName;
    /**
     * 分类描述
     */
    private String categoryDesc;
}
