package com.luo.d3s.light.jpa.ec.domain.order.model;

import com.luo.d3s.core.util.validation.Validates;
import com.luo.d3s.light.jpa.ec.domain.base.BaseEntity;
import com.luo.d3s.light.jpa.ec.domain.base.BizId;
import com.luo.d3s.light.jpa.ec.domain.base.Price;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 订单
 *
 * @author luohq
 * @date 2022-11-27 16:08
 */
@Entity
@Table(name = "orders")
public class Order extends BaseEntity {
    /**
     * 订单商品列表
     */
    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    @JoinColumn(name = "order_id")
    @NotEmpty
    private List<OrderGoods> orderGoods;

    /**
     * 订单价格
     */
    @Embedded
    @AttributeOverride(name = "price", column = @Column(name = "order_price"))
    @NotNull
    private Price orderPrice;

    /**
     * 收货地址
     */
    @Embedded
    @NotNull
    private ReceiveAddress receiveAddress;

    /**
     * 订单状态
     */
    @NotNull
    private OrderStatus orderStatus;

    /**
     * 支付时间
     */
    private LocalDateTime payTime;

    /**
     * 快递单号
     */
    @Embedded
    private ExpressCode expressCode;

    /**
     * 配送时间
     */
    private LocalDateTime deliverTime;

    /**
     * 完成时间
     */
    private LocalDateTime completeTime;

    /**
     * 取消时间
     */
    private LocalDateTime cancelTime;

    Order() {
    }

    /**
     * 初始创建订单信息
     *
     * @param id             订单ID
     * @param orderGoods     订单商品
     * @param orderPrice     订单总价
     * @param receiveAddress 收货地址
     */
    public Order(BizId id, Price orderPrice, ReceiveAddress receiveAddress, List<OrderGoods> orderGoods) {
        this.id = id;
        this.orderPrice = orderPrice;
        this.receiveAddress = receiveAddress;
        this.orderGoods = orderGoods;
        this.orderStatus = OrderStatus.CREATED;
        this.createTime = LocalDateTime.now();
        this.updateTime = this.createTime;
        this.validateSelf();
    }

    /**
     * 支付订单
     */
    public void payOrder() {
        Validates.isTrue(OrderStatus.CREATED.equals(this.getOrderStatus()), "order status is not CREATED");
        this.orderStatus = OrderStatus.PAID;
        this.payTime = this.updateTime = LocalDateTime.now();
    }

    /**
     * 发货订单
     * @param expressCode 快递单号
     * @param deliverTime 发货时间
     */
    public void deliverOrder(ExpressCode expressCode, LocalDateTime deliverTime) {
        Validates.isTrue(OrderStatus.PAID.equals(this.getOrderStatus()), "order status is not PAID");
        this.orderStatus = OrderStatus.DELIVERED;
        this.expressCode = expressCode;
        this.deliverTime = deliverTime;
        this.updateTime = LocalDateTime.now();
    }

    /**
     * 完成订单
     */
    public void completeOrder() {
        Validates.isTrue(OrderStatus.DELIVERED.equals(this.getOrderStatus()), "order status is not DELIVERED");
        this.orderStatus = OrderStatus.COMPLETED;
        this.completeTime = this.updateTime = LocalDateTime.now();
    }

    /**
     * 取消订单
     */
    public void cancelOrder() {
        this.orderStatus = OrderStatus.CANCELED;
        this.cancelTime = this.updateTime = LocalDateTime.now();
    }


    public List<OrderGoods> getOrderGoods() {
        return orderGoods;
    }

    public List<BizId> getGoodsIds() {
        return this.orderGoods.stream()
                .map(OrderGoods::getGoodsId)
                .collect(Collectors.toList());
    }

    public Price getOrderPrice() {
        return orderPrice;
    }

    public ReceiveAddress getReceiveAddress() {
        return receiveAddress;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public LocalDateTime getPayTime() {
        return payTime;
    }

    public ExpressCode getExpressCode() {
        return expressCode;
    }

    public LocalDateTime getDeliverTime() {
        return deliverTime;
    }

    public LocalDateTime getCompleteTime() {
        return completeTime;
    }

    public LocalDateTime getCancelTime() {
        return cancelTime;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", orderGoods=" + orderGoods +
                ", orderPrice=" + orderPrice +
                ", receiveAddress=" + receiveAddress +
                ", orderStatus=" + orderStatus +
                ", payTime=" + payTime +
                ", expressCode=" + expressCode +
                ", deliverTime=" + deliverTime +
                ", completeTime=" + completeTime +
                ", cancelTime=" + cancelTime +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
