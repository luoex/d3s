package com.luo.d3s.light.jpa.ec.application.service.impl;

import com.luo.d3s.core.application.assmebler.PageResponseAssembler;
import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.core.application.dto.SingleResponse;
import com.luo.d3s.light.jpa.ec.application.assembler.OrderAssembler;
import com.luo.d3s.light.jpa.ec.application.dto.query.OrderPageQuery;
import com.luo.d3s.light.jpa.ec.application.dto.vo.OrderVo;
import com.luo.d3s.light.jpa.ec.application.service.OrderQueryService;
import com.luo.d3s.light.jpa.ec.domain.base.BizId;
import com.luo.d3s.light.jpa.ec.domain.order.model.Order;
import com.luo.d3s.light.jpa.ec.domain.order.repository.OrderRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 订单查询服务实现类
 *
 * @author luohq
 * @date 2022-11-27 19:18
 */
@Service
public class OrderQueryServiceImpl implements OrderQueryService {
    @Resource
    private OrderRepository orderRepository;

    @Override
    public SingleResponse<OrderVo> findOrder(Long orderId) {
        Order order = this.orderRepository.findById(BizId.fromValue(orderId)).orElse(null);
        return SingleResponse.of(OrderAssembler.toOrderVo(order));
    }

    @Override
    public PageResponse<OrderVo> findOrderPage(OrderPageQuery orderPageQuery) {
        PageResponse<Order> orderPageResp = this.orderRepository.findPage(orderPageQuery);
        return PageResponseAssembler.toPageResp(orderPageResp, OrderAssembler::toOrderVoWithoutGoods);
    }
}
