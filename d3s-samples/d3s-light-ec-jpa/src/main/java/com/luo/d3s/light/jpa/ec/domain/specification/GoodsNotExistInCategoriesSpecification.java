package com.luo.d3s.light.jpa.ec.domain.specification;

import com.luo.d3s.core.domain.service.specifcation.AbstractSpecification;
import com.luo.d3s.core.util.validation.Validates;
import com.luo.d3s.light.jpa.ec.domain.base.BizId;
import com.luo.d3s.light.jpa.ec.domain.goods.GoodsRepository;

import java.util.Collection;

/**
 * 分类下不存在商品验证规则
 *
 * @author luohq
 * @date 2023-01-05 10:31
 */
public class GoodsNotExistInCategoriesSpecification extends AbstractSpecification<Collection<BizId>> {

    private GoodsRepository goodsRepository;

    public GoodsNotExistInCategoriesSpecification(GoodsRepository goodsRepository) {
        this.goodsRepository = goodsRepository;
    }

    @Override
    public boolean isSatisfiedBy(Collection<BizId> categoryIds) {
        //分类下不存在商品
        Boolean existGoodsInCategory = this.goodsRepository.existsByCategoryIdIn(categoryIds);
        Validates.isFalse(existGoodsInCategory, "exist goods in categories");
        return true;
    }
}
