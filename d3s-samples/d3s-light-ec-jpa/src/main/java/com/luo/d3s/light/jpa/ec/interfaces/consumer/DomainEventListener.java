package com.luo.d3s.light.jpa.ec.interfaces.consumer;

import com.luo.d3s.ext.event.jdbc.EventBus;
import com.luo.d3s.light.jpa.ec.domain.order.event.ExpressProcessModifiedEvent;
import com.luo.d3s.light.jpa.ec.domain.order.event.StockDeliveredEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 领域事件监听器
 *
 * @author luohq
 * @date 2022-12-22 14:30
 */
@Component
@Slf4j
public class DomainEventListener {

    /**
     * 库存已发货事件监听器
     *
     * @param stockDeliveredEvent 库存已发货事件
     */
    @RabbitListener(queues = {"${spring.rabbitmq.events.stock-delivery.queue.name}"})
    public void on(StockDeliveredEvent stockDeliveredEvent) {
        log.info("[RabbitMq RECV] Stock Delivery: {}", stockDeliveredEvent);
        //分发事件
        EventBus.dispatchBatch(stockDeliveredEvent);
    }

    /**
     * 物流进度事件监听器
     *
     * @param expressProcessModifiedEvent 物流进度事件
     */
    @RabbitListener(queues = {"${spring.rabbitmq.events.express-process.queue.name}"})
    public void on(ExpressProcessModifiedEvent expressProcessModifiedEvent) {
        log.info("[RabbitMq RECV] Express Process: {}", expressProcessModifiedEvent);
        //分发事件
        EventBus.dispatchBatch(expressProcessModifiedEvent);
    }
}
