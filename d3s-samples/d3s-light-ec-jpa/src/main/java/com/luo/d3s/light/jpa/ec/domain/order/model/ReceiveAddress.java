package com.luo.d3s.light.jpa.ec.domain.order.model;

import com.luo.d3s.core.domain.model.ValueObject;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * 收货地址
 *
 * @author luohq
 * @date 2022-11-27 16:10
 */
@Embeddable
public class ReceiveAddress implements ValueObject {
    /**
     * 收货地址
     */
    @NotBlank
    @Size(min = 1, max = 256)
    private String receiveAddress;

    ReceiveAddress() {
    }

    public ReceiveAddress(String receiveAddress) {
        this.receiveAddress = receiveAddress;
        this.validateSelf();
    }

    public String getReceiveAddress() {
        return receiveAddress;
    }

    @Override
    public String toString() {
        return "ReceiveAddress{" +
                "receiveAddress='" + receiveAddress + '\'' +
                '}';
    }
}
