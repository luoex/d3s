package com.luo.d3s.light.jpa.ec.domain.order.event;

import com.luo.d3s.core.domain.event.DomainEvent;
import com.luo.d3s.core.util.IdGenUtils;
import com.luo.d3s.light.jpa.ec.domain.base.BizId;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.Value;

import java.time.LocalDateTime;

/**
 * 物流进度更新事件
 *
 * @author luohq
 * @date 2022-11-27 18:54
 */
@Value
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ExpressProcessModifiedEvent implements DomainEvent<String, BizId> {
    /**
     * 事件ID
     */
    @EqualsAndHashCode.Include
    private String eventId;
    /**
     * 事件创建时间
     */
    private LocalDateTime createTime;

    /**
     * 快递记录ID
     */
    private BizId expressProcessId;

    /**
     * 配送ID
     */
    private BizId expressDeliveryId;

    /**
     * 订单ID
     */
    private BizId orderId;

    /**
     * 更新时间
     */
    private LocalDateTime modifyTime;

    /**
     * 物流进度描述
     */
    private String processDesc;

    /**
     * 物流进度状态
     */
    private String processStatus;

    public ExpressProcessModifiedEvent(BizId expressProcessId, BizId expressDeliveryId, BizId orderId, LocalDateTime modifyTime, String processDesc, String processStatus) {
        this.expressProcessId = expressProcessId;
        this.expressDeliveryId = expressDeliveryId;
        this.orderId = orderId;
        this.modifyTime = modifyTime;
        this.processDesc = processDesc;
        this.processStatus = processStatus;
        this.eventId = IdGenUtils.nextTextId();
        this.createTime = LocalDateTime.now();
    }

    @Override
    public BizId getAggregateRootId() {
        return this.orderId;
    }
}
