package com.luo.d3s.light.jpa.ec.domain.base;

import com.luo.d3s.core.domain.model.light.EntityLight;
import org.springframework.data.domain.Persistable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * JPA基础实体
 *
 * @author luohq
 * @date 2023-07-25 15:42
 */
@MappedSuperclass
public abstract class BaseEntity implements Persistable<BizId>, EntityLight<BizId> {

    /**
     * @Transient标识的属性不会持久化到DB， 且isNew用于标识是否为新对象（即不是从DB中查询得到的）
     */
    @Transient
    private boolean isNew = true;

    /**
     * 实体ID
     */
    @EmbeddedId
    @NotNull
    protected BizId id;

    /**
     * 创建时间
     */
    protected LocalDateTime createTime;
    /**
     * 修改时间
     */
    protected LocalDateTime updateTime;


    @Override
    public boolean isNew() {
        return isNew;
    }

    @PrePersist
    @PostLoad
    void markNotNew() {
        //持久化后 或者 从DB中查询出，即标识该实体不是新对象
        this.isNew = false;
    }

    @Override
    public BizId getId() {
        return id;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }
}