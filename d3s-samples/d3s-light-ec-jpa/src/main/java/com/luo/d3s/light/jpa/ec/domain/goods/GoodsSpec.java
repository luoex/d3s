package com.luo.d3s.light.jpa.ec.domain.goods;

import com.luo.d3s.core.domain.model.ValueObject;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Size;
import java.time.LocalDate;

/**
 * 商品规格
 *
 * @author luohq
 * @date 2022-11-27 18:06
 */
@Embeddable
public class GoodsSpec implements ValueObject {

    /**
     * 生产日期
     */
    @NotNull
    @PastOrPresent
    private LocalDate manufactureDate;

    /**
     * 过期日期
     * TODO 验证逻辑 - expirationDate > manufactureDate
     */
    @NotNull
    private LocalDate expirationDate;

    /**
     * 商品重量
     */
    @Embedded
    @AttributeOverride(name = "weight", column = @Column(name = "goods_weight"))
    @AttributeOverride(name = "unit", column = @Column(name = "goods_weight_unit"))
    @NotNull
    private GoodsWeight goodsWeight;

    /**
     * 商品介绍
     */
    @NotBlank
    @Size(max = 1024)
    private String goodsDesc;

    private GoodsSpec() {
    }

    public GoodsSpec(LocalDate manufactureDate, LocalDate expirationDate, GoodsWeight goodsWeight, String goodsDesc) {
        this.manufactureDate = manufactureDate;
        this.expirationDate = expirationDate;
        this.goodsWeight = goodsWeight;
        this.goodsDesc = goodsDesc;
        this.validateSelf();
    }

    public LocalDate getManufactureDate() {
        return manufactureDate;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public GoodsWeight getGoodsWeight() {
        return goodsWeight;
    }

    public String getGoodsDesc() {
        return goodsDesc;
    }

    @Override
    public String toString() {
        return "GoodsSpec{" +
                "manufactureDate=" + manufactureDate +
                ", expirationDate=" + expirationDate +
                ", goodsWeight=" + goodsWeight +
                ", goodsDesc='" + goodsDesc + '\'' +
                '}';
    }

    public static GoodsSpecBuilder builder() {
        return new GoodsSpecBuilder();
    }
    public static final class GoodsSpecBuilder {
        private LocalDate manufactureDate;
        private LocalDate expirationDate;
        private GoodsWeight goodsWeight;
        private String goodsDesc;

        private GoodsSpecBuilder() {
        }

        public GoodsSpecBuilder manufactureDate(LocalDate manufactureDate) {
            this.manufactureDate = manufactureDate;
            return this;
        }

        public GoodsSpecBuilder expirationDate(LocalDate expirationDate) {
            this.expirationDate = expirationDate;
            return this;
        }

        public GoodsSpecBuilder goodsWeight(GoodsWeight goodsWeight) {
            this.goodsWeight = goodsWeight;
            return this;
        }

        public GoodsSpecBuilder goodsDesc(String goodsDesc) {
            this.goodsDesc = goodsDesc;
            return this;
        }

        public GoodsSpec build() {
            return new GoodsSpec(manufactureDate, expirationDate, goodsWeight, goodsDesc);
        }
    }
}
