package com.luo.d3s.light.jpa.ec.application.event;

import com.luo.d3s.core.domain.event.DomainEventHandler;
import com.luo.d3s.light.jpa.ec.application.dto.command.OrderDeliverCommand;
import com.luo.d3s.light.jpa.ec.application.service.OrderCommandService;
import com.luo.d3s.light.jpa.ec.domain.order.event.StockDeliveredEvent;
import org.springframework.stereotype.Component;

/**
 * 库存已发货事件处理器
 *
 * @author luohq
 * @date 2022-11-27 19:29
 */
@Component
public class StockDeliveredEventHandler implements DomainEventHandler<StockDeliveredEvent> {

    private OrderCommandService orderCommandService;

    public StockDeliveredEventHandler(OrderCommandService orderCommandService) {
        this.orderCommandService = orderCommandService;
    }

    @Override
    public void handle(StockDeliveredEvent domainEvent) {
        this.orderCommandService.deliverOrder(
                new OrderDeliverCommand(
                        domainEvent.getOrderId().getId(),
                        domainEvent.getExpressCode().getExpressCode(),
                        domainEvent.getDeliveryTime()));
    }
}
