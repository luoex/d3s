package com.luo.d3s.light.jpa.ec.domain.specification;

import com.luo.d3s.core.domain.service.specifcation.AbstractSpecification;
import com.luo.d3s.core.util.validation.Validates;
import com.luo.d3s.light.jpa.ec.domain.base.BizId;
import com.luo.d3s.light.jpa.ec.domain.goods.GoodsRepository;

import java.util.Collection;

/**
 * 不存在已上架商品验证规则
 *
 * @author luohq
 * @date 2023-01-05 10:31
 */
public class GoodsShelvedNotExistSpecification extends AbstractSpecification<Collection<BizId>> {

    private GoodsRepository goodsRepository;

    public GoodsShelvedNotExistSpecification(GoodsRepository goodsRepository) {
        this.goodsRepository = goodsRepository;
    }

    @Override
    public boolean isSatisfiedBy(Collection<BizId> goodsIds) {
        //存在已上架商品
        //Boolean existGoodsShelved = this.goodsRepository.existsByIdInAndGoodsStatus(goodsIds, GoodsStatus.SHELVED);
        Boolean existGoodsShelved = this.goodsRepository.existsShelvedGoodsByIdIn(goodsIds);
        Validates.isFalse(existGoodsShelved, "exist shelved goods");
        return true;
    }
}
