package com.luo.d3s.light.jpa.ec.domain.order.event;

import com.luo.d3s.core.domain.event.DomainEvent;
import com.luo.d3s.core.util.IdGenUtils;
import com.luo.d3s.light.jpa.ec.domain.base.BizId;
import com.luo.d3s.light.jpa.ec.domain.order.model.Order;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.Value;

import java.time.LocalDateTime;

/**
 * 订单已支付事件
 *
 * @author luohq
 * @date 2022-11-27 18:41
 */
@Value
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class OrderPaidEvent implements DomainEvent<String, BizId> {

    /**
     * 事件ID
     */
    @EqualsAndHashCode.Include
    private String eventId;
    /**
     * 事件创建时间
     */
    private LocalDateTime createTime;

    /**
     * 订单信息
     */
    private Order order;

    public OrderPaidEvent(Order order) {
        this.order = order;
        this.eventId = IdGenUtils.nextTextId();
        this.createTime = LocalDateTime.now();
    }

    @Override
    public BizId getAggregateRootId() {
        return order.getId();
    }
}
