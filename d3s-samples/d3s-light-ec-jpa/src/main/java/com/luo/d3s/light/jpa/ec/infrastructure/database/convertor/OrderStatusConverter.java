package com.luo.d3s.light.jpa.ec.infrastructure.database.convertor;

import com.luo.d3s.core.infrastructure.convertor.JpaVoEnumAttrConverter;
import com.luo.d3s.light.jpa.ec.domain.order.model.OrderStatus;

import javax.persistence.Converter;

/**
 * 订单状态 - JPA值对象枚举属性转换器
 *
 * @author luohq
 * @date 2023-07-27
 */
@Converter(autoApply = true)
public class OrderStatusConverter extends JpaVoEnumAttrConverter<OrderStatus, Integer> {
}