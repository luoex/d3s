package com.luo.d3s.light.jpa.ec.application.service;

import com.luo.d3s.core.application.dto.Response;
import com.luo.d3s.core.application.dto.SingleResponse;
import com.luo.d3s.core.application.service.CommandService;
import com.luo.d3s.light.jpa.ec.application.dto.command.CategoryCreateCommand;
import com.luo.d3s.light.jpa.ec.application.dto.command.CategoryModifyCommand;
import com.luo.d3s.light.jpa.ec.application.dto.vo.CategoryVo;

/**
 * 分类命令处理服务接口
 *
 * @author luohq
 * @date 2023-01-04 17:00
 */
public interface CategoryCommandService extends CommandService {

    /**
     * 创建分类
     *
     * @param categoryCreateCommand 创建分类命令
     * @return 响应结果（分类信息）
     */
    SingleResponse<CategoryVo> createCategory(CategoryCreateCommand categoryCreateCommand);

    /**
     * 修改分类
     *
     * @param categoryModifyCommand 修改分类命令
     * @return 响应结果
     */
    Response modifyCategory(CategoryModifyCommand categoryModifyCommand);

    /**
     * 移除分类
     *
     * @param categoryId 分类ID
     * @return 响应结果
     */
    Response removeCategory(Long categoryId);
}
