package com.luo.d3s.light.jpa.ec;

import com.luo.d3s.ext.component.scan.D3sComponentScan;
import com.luo.d3s.light.jpa.ec.domain.category.Category;
import com.luo.d3s.light.jpa.ec.domain.category.CategoryRepository;
import com.luo.d3s.light.jpa.ec.domain.goods.GoodsRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Spring Boot Starter
 *
 * @author luohq
 * @date 2022-12-22
 * @notice @D3sComponentScan需放在@SpringBootApplication上方，否则启动报错（无法扫描到领域对象）
 */
@D3sComponentScan
@SpringBootApplication
@EnableJpaRepositories//(basePackageClasses = {CategoryRepository.class, GoodsRepository.class})
@EntityScan//(basePackageClasses = {Category.class})
public class StartD3sLightJpa {
    public static void main(String[] args) {
        SpringApplication.run(StartD3sLightJpa.class, args);
    }
}