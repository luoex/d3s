package com.luo.d3s.light.jpa.ec.domain.category;

import com.luo.d3s.core.util.Optionals;
import com.luo.d3s.light.jpa.ec.application.dto.command.CategoryModifyCommand;
import com.luo.d3s.light.jpa.ec.domain.base.BaseEntity;
import com.luo.d3s.light.jpa.ec.domain.base.BizId;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * 商品分类
 *
 * @author luohq
 * @date 2023-01-04 13:20
 */
@Entity
public class Category extends BaseEntity {

    /**
     * 上级分类ID
     */
    @Embedded
    @AttributeOverride(name = "id", column = @Column(name = "parent_category_id"))
    private BizId parentCategoryId;

    /**
     * 分类名称
     */
    @Embedded
    @NotNull
    private CategoryName categoryName;

    /**
     * 分类描述
     */
    @Embedded
    private CategoryDesc categoryDesc;

    Category() {
    }

    /**
     * 创建分类信息
     *
     * @param id               分类ID
     * @param parentCategoryId 上级分类ID
     * @param categoryName     分类名称
     * @param categoryDesc     分类描述
     */
    Category(BizId id, BizId parentCategoryId, CategoryName categoryName, CategoryDesc categoryDesc) {
        this.id = id;
        this.parentCategoryId = parentCategoryId;
        this.categoryName = categoryName;
        this.categoryDesc = categoryDesc;
        this.createTime = LocalDateTime.now();
        this.updateTime = this.createTime;
        this.validateSelf();
    }

    /**
     * 修改基础信息
     *
     * @param parentCategoryId 上级分类ID
     * @param categoryName     分类名称
     * @param categoryDesc     分类描述
     */
    public void modifyBasicInfo(BizId parentCategoryId, CategoryName categoryName, CategoryDesc categoryDesc) {
        this.parentCategoryId = parentCategoryId;
        this.categoryName = categoryName;
        this.categoryDesc = categoryDesc;
        this.updateTime = LocalDateTime.now();
        this.validateSelf();
    }

    /**
     * 修改基础信息
     *
     * @param categoryModifyCommand 修改命令
     */
    public void modifyBasicInfo(CategoryModifyCommand categoryModifyCommand) {
        this.parentCategoryId = Optionals.defaultNull(categoryModifyCommand.getParentCategoryId(), BizId::fromValue);
        this.categoryName = new CategoryName(categoryModifyCommand.getCategoryName());
        this.categoryDesc = new CategoryDesc(categoryModifyCommand.getCategoryDesc());
        this.updateTime = LocalDateTime.now();
        this.validateSelf();
    }

    public BizId getParentCategoryId() {
        return parentCategoryId;
    }

    public CategoryName getCategoryName() {
        return categoryName;
    }

    public CategoryDesc getCategoryDesc() {
        return categoryDesc;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", parentCategoryId=" + parentCategoryId +
                ", categoryName=" + categoryName +
                ", categoryDesc=" + categoryDesc +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
