package com.luo.d3s.light.jpa.ec.application.service;

import com.luo.d3s.core.application.dto.MultiResponse;
import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.core.application.dto.SingleResponse;
import com.luo.d3s.core.application.service.QueryService;
import com.luo.d3s.light.jpa.ec.application.dto.query.CategoryPageQuery;
import com.luo.d3s.light.jpa.ec.application.dto.vo.CategoryTreeVo;
import com.luo.d3s.light.jpa.ec.application.dto.vo.CategoryVo;

/**
 * 分类查询服务接口类
 *
 * @author luohq
 * @date 2023-01-05 11:19
 */
public interface CategoryQueryService extends QueryService {

    /**
     * 分类缓存名称
     */
    String CATEGORY_TREE_CACHE_NAME = "category-tree";

    /**
     * 查询分类详情
     *
     * @param categoryId 分类ID
     * @return 分类详情
     */
    SingleResponse<CategoryVo> findCategory(Long categoryId);

    /**
     * 查询分类分页列表
     *
     * @param categoryPageQuery 分类分页查询参数
     * @return 分页查询结果
     */
    PageResponse<CategoryVo> findCategoryPage(CategoryPageQuery categoryPageQuery);

    /**
     * 查询分类树形列表
     *
     * @param categoryId 分类ID
     * @return 分类树形列表
     */
    MultiResponse<CategoryTreeVo> findCategoryTree(Long categoryId);
}
