package com.luo.d3s.light.jpa.ec.domain.order.model;

import com.luo.d3s.core.domain.model.ValueObjectEnum;

/**
 * 订单状态
 *
 * @author luohq
 * @date 2022-11-27 16:01
 */
public enum OrderStatus implements ValueObjectEnum<Integer> {
    //10 - 已创建
    CREATED(10, "已创建"),
    //20 - 已支付
    PAID(20, "已支付"),
    //30 - 已发货
    DELIVERED(30, "已发货"),
    //40 - 已完成
    COMPLETED(40, "已完成"),
    //50 - 已取消
    CANCELED(50, "已取消");

    /**
     * 状态编码
     */
    private Integer value;

    /**
     * 状态描述
     */
    private String desc;

    OrderStatus(Integer status, String desc) {
        this.value = status;
        this.desc = desc;
    }

    @Override
    public Integer getValue() {
        return this.value;
    }

    @Override
    public String getDesc() {
        return this.desc;
    }

    public static OrderStatus of(Integer value) {
        return ValueObjectEnum.of(value, OrderStatus.class);
    }
}
