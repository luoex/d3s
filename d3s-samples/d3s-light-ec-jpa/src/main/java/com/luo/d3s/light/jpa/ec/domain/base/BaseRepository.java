package com.luo.d3s.light.jpa.ec.domain.base;

import com.luo.d3s.core.domain.adpator.light.RepositoryLight;
import com.luo.d3s.core.domain.model.light.EntityLight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * JPA基础仓库
 *
 * @author luohq
 * @date 2023-07-26 10:28
 */
@NoRepositoryBean
public interface BaseRepository<T extends EntityLight, ID> extends JpaRepository<T, ID>, JpaSpecificationExecutor<T>, RepositoryLight<T> {

}