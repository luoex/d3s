package com.luo.d3s.light.jpa.ec.domain.goodstag;

import com.luo.d3s.light.jpa.ec.domain.base.BaseEntity;
import com.luo.d3s.light.jpa.ec.domain.base.BizId;

import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * 商品标签
 *
 * @author luohq
 * @date 2023-08-02
 */
@Entity
public class GoodsTag extends BaseEntity {
    /**
     *标签名称
     */
    @NotBlank
    @Size(max = 64)
    private String tagName;

    /**
     *标签描述
     */
    @Size(max = 512)
    private String tagDesc;

    GoodsTag() {
    }

    GoodsTag(BizId id, String tagName, String tagDesc) {
        this.id = id;
        this.tagName = tagName;
        this.tagDesc = tagDesc;
        this.createTime = LocalDateTime.now();
        this.updateTime = this.createTime;
        this.validateSelf();
    }

    public void modifyBasicInfo(String tagName, String tagDesc) {
        this.tagName = tagName;
        this.tagDesc = tagDesc;
        this.updateTime = LocalDateTime.now();
        this.validateSelf();
    }

    public String getTagName() {
        return tagName;
    }

    public String getTagDesc() {
        return tagDesc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GoodsTag goodsTag = (GoodsTag) o;
        return Objects.equals(id, goodsTag.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "GoodsTag{" +
                "id=" + id +
                ", tagName='" + tagName + '\'' +
                ", tagDesc='" + tagDesc + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
