package com.luo.d3s.light.jpa.ec.domain.goods;

import com.luo.d3s.light.jpa.ec.application.dto.command.GoodsCreateCommand;
import com.luo.d3s.light.jpa.ec.domain.base.BizId;
import com.luo.d3s.light.jpa.ec.domain.base.Price;

import java.util.Set;

/**
 * 商品创建工厂<br/>
 * 注：领域对象创建工厂，强调初始创建领域对象的操作（区别于技术层面的构造函数）
 *
 * @author luohq
 * @date 2022-12-28 10:02
 */
public class GoodsFactory {


    /**
     * 创建初始商品
     *
     * @param categoryId 商品分类ID
     * @param goodsName  商品名称
     * @param goodsSpec  商品规格
     * @param goodsPrice 商品价格
     * @param tagIds     商品标签ID集合
     * @return 初始商品
     */
    public static Goods createGoods(BizId categoryId, GoodsName goodsName,
                                    GoodsSpec goodsSpec, Price goodsPrice, Set<BizId> tagIds) {
        return new Goods(
                BizId.newBizId(),
                categoryId,
                goodsName,
                goodsSpec,
                goodsPrice,
                GoodsStatus.UNSHELVED,
                tagIds
        );
    }

    /**
     * 根据创建命令创建初始商品
     *
     * @param goodsCreateCommand 商品创建命令
     * @return 初始商品
     */
    public static Goods createGoods(GoodsCreateCommand goodsCreateCommand) {
        return new Goods(
                BizId.newBizId(),
                BizId.fromValue(goodsCreateCommand.getCategoryId()),
                new GoodsName(goodsCreateCommand.getGoodsName()),
                GoodsSpec.builder()
                        .manufactureDate(goodsCreateCommand.getManufactureDate())
                        .expirationDate(goodsCreateCommand.getExpirationDate())
                        .goodsWeight(GoodsWeight.builder()
                                .weight(goodsCreateCommand.getGoodsWeight())
                                .unit(WeightUnit.of(goodsCreateCommand.getGoodsWeightUnit()))
                                .build())
                        .goodsDesc(goodsCreateCommand.getGoodsDesc())
                        .build(),
                new Price(goodsCreateCommand.getGoodsPrice()),
                GoodsStatus.UNSHELVED,
                BizId.fromNullableValues(goodsCreateCommand.getTagIds())
        );
    }
}
