package com.luo.d3s.light.jpa.ec.interfaces.web;

import com.luo.d3s.core.application.dto.MultiResponse;
import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.core.application.dto.Response;
import com.luo.d3s.core.application.dto.SingleResponse;
import com.luo.d3s.light.jpa.ec.application.dto.command.CategoryCreateCommand;
import com.luo.d3s.light.jpa.ec.application.dto.command.CategoryModifyCommand;
import com.luo.d3s.light.jpa.ec.application.dto.query.CategoryPageQuery;
import com.luo.d3s.light.jpa.ec.application.dto.vo.CategoryTreeVo;
import com.luo.d3s.light.jpa.ec.application.dto.vo.CategoryVo;
import com.luo.d3s.light.jpa.ec.application.service.CategoryCommandService;
import com.luo.d3s.light.jpa.ec.application.service.CategoryQueryService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 分类Controller
 *
 * @author luohq
 * @date 2022-12-22 16:12
 */
@RestController
@RequestMapping("/api/v1/categories")
public class CategoryController {
    @Resource
    private CategoryQueryService categoryQueryService;

    @Resource
    private CategoryCommandService categoryCommandService;

    /**
     * 根据分类ID查询分类信息
     *
     * @param categoryId 分类ID
     * @return 分类信息
     */
    @GetMapping("/{categoryId}")
    public SingleResponse<CategoryVo> findCategory(@PathVariable Long categoryId) {
        return this.categoryQueryService.findCategory(categoryId);
    }

    /**
     * 根据分类ID查询分类树形列表
     *
     * @param categoryId 分类ID
     * @return 分类树形列表
     */
    @GetMapping("/tree/{categoryId}")
    public MultiResponse<CategoryTreeVo> findCategoryTree(@PathVariable(required = false) Long categoryId) {
        return this.categoryQueryService.findCategoryTree(categoryId);
    }

    /**
     * 查询分类树形列表（完整分类树）
     *
     * @return 分类树形列表
     */
    @GetMapping("/tree")
    public MultiResponse<CategoryTreeVo> findAllCategoryTree() {
        return this.categoryQueryService.findCategoryTree(null);
    }


    /**
     * 查询分类分页列表
     *
     * @param categoryPageQuery 分类分页查询参数
     * @return 分类分页查询结果
     */
    @GetMapping
    public PageResponse<CategoryVo> findCategoryPage(CategoryPageQuery categoryPageQuery) {
        return this.categoryQueryService.findCategoryPage(categoryPageQuery);
    }

    /**
     * 创建分类
     *
     * @param categoryCreateCommand 分类创建命令
     * @return 创建结果
     */
    @PostMapping
    public SingleResponse<CategoryVo> createCategory(@RequestBody CategoryCreateCommand categoryCreateCommand) {
        return this.categoryCommandService.createCategory(categoryCreateCommand);
    }

    /**
     * 修改分类
     *
     * @param categoryModifyCommand 分类修改命令
     * @return 修改结果
     */
    @PutMapping
    public Response modifyCategory(@RequestBody CategoryModifyCommand categoryModifyCommand) {
        return this.categoryCommandService.modifyCategory(categoryModifyCommand);
    }

    /**
     * 删除分类
     *
     * @param categoryId 分类ID
     * @return 删除结果
     */
    @DeleteMapping("/{categoryId}")
    public Response removeCategory(@PathVariable Long categoryId) {
        return this.categoryCommandService.removeCategory(categoryId);
    }

}
