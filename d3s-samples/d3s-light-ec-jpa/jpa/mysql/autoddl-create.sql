
    create table category (
       id bigint not null,
        create_time datetime(6),
        update_time datetime(6),
        category_desc varchar(255),
        category_name varchar(255),
        parent_category_id bigint,
        primary key (id)
    ) engine=InnoDB;

    create table goods (
       id bigint not null,
        create_time datetime(6),
        update_time datetime(6),
        category_id bigint,
        goods_name varchar(255),
        goods_price decimal(19,2),
        expiration_date date,
        goods_desc varchar(255),
        goods_weight_unit varchar(255),
        goods_weight decimal(19,2),
        manufacture_date date,
        goods_status integer not null,
        primary key (id)
    ) engine=InnoDB;

    create table goods_tag_binding (
       goods_id bigint not null,
        tag_id bigint
    ) engine=InnoDB;

    create table goods_tag (
       id bigint not null,
        create_time datetime(6),
        update_time datetime(6),
        tag_desc varchar(512),
        tag_name varchar(64),
        primary key (id)
    ) engine=InnoDB;

    create table order_goods (
       id bigint not null,
        create_time datetime(6),
        update_time datetime(6),
        goods_count integer,
        goods_id bigint,
        goods_price decimal(19,2),
        goods_sum_price decimal(19,2),
        order_id bigint,
        primary key (id)
    ) engine=InnoDB;

    create table orders (
       id bigint not null,
        create_time datetime(6),
        update_time datetime(6),
        cancel_time datetime(6),
        complete_time datetime(6),
        deliver_time datetime(6),
        express_code varchar(255),
        order_price decimal(19,2),
        order_status integer not null,
        pay_time datetime(6),
        receive_address varchar(255),
        primary key (id)
    ) engine=InnoDB;
