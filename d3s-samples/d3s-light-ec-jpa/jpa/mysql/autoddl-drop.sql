
    drop table if exists category;

    drop table if exists goods;

    drop table if exists goods_tag_binding;

    drop table if exists goods_tag;

    drop table if exists order_goods;

    drop table if exists orders;
