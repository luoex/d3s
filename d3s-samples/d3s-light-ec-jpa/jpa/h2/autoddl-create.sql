
    create table category (
       id bigint not null,
        create_time timestamp,
        update_time timestamp,
        category_desc varchar(255),
        category_name varchar(255),
        parent_category_id bigint,
        primary key (id)
    );

    create table goods (
       id bigint not null,
        create_time timestamp,
        update_time timestamp,
        category_id bigint,
        goods_name varchar(255),
        goods_price numeric(19,2),
        expiration_date date,
        goods_desc varchar(255),
        goods_weight_unit varchar(255),
        goods_weight numeric(19,2),
        manufacture_date date,
        goods_status integer not null,
        primary key (id)
    );

    create table goods_tag_binding (
       goods_id bigint not null,
        tag_id bigint
    );

    create table goods_tag (
       id bigint not null,
        create_time timestamp,
        update_time timestamp,
        tag_desc varchar(512),
        tag_name varchar(64),
        primary key (id)
    );

    create table order_goods (
       id bigint not null,
        create_time timestamp,
        update_time timestamp,
        goods_count integer,
        goods_id bigint,
        goods_price numeric(19,2),
        goods_sum_price numeric(19,2),
        order_id bigint,
        primary key (id)
    );

    create table orders (
       id bigint not null,
        create_time timestamp,
        update_time timestamp,
        cancel_time timestamp,
        complete_time timestamp,
        deliver_time timestamp,
        express_code varchar(255),
        order_price numeric(19,2),
        order_status integer not null,
        pay_time timestamp,
        receive_address varchar(255),
        primary key (id)
    );
