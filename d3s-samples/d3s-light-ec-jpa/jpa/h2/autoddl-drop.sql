
    drop table if exists category CASCADE ;

    drop table if exists goods CASCADE ;

    drop table if exists goods_tag_binding CASCADE ;

    drop table if exists goods_tag CASCADE ;

    drop table if exists order_goods CASCADE ;

    drop table if exists orders CASCADE ;
