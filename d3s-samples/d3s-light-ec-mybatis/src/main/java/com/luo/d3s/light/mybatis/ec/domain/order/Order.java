package com.luo.d3s.light.mybatis.ec.domain.order;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.luo.d3s.core.util.validation.Validates;
import com.luo.d3s.light.mybatis.ec.domain.base.BaseEntity;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Order数据对象
 *
 * @author luohq
 * @date 2022-12-21 9:42
 */
@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@TableName("orders")
public class Order extends BaseEntity {

    /**
     * 价格值
     */
    @NotNull
    @Positive
    private BigDecimal orderPrice;

    /**
     * 收货地址
     */
    @NotBlank
    @Length(min = 1, max = 256)
    private String receiveAddress;

    /**
     * 订单状态
     */
    @NotNull
    private OrderStatus orderStatus;

    /**
     * 支付时间
     */
    private LocalDateTime payTime;

    /**
     * 快递单号
     */
    @Length(min = 1, max = 128)
    private String expressCode;

    /**
     * 配送时间
     */
    private LocalDateTime deliverTime;

    /**
     * 完成时间
     */
    private LocalDateTime completeTime;

    /**
     * 取消时间
     */
    private LocalDateTime cancelTime;

    /**
     * 订单商品列表
     */
    @TableField(exist = false)
    @NotEmpty
    @Valid
    private List<OrderGoods> orderGoodsList;

    /**
     * 初始创建订单
     *
     * @param id             订单ID
     * @param orderPrice     订单价格
     * @param receiveAddress 收货地址
     * @param orderGoodsList 订单商品列表
     */
    public Order(Long id, BigDecimal orderPrice, String receiveAddress, List<OrderGoods> orderGoodsList) {
        super.id = id;
        super.createTime = LocalDateTime.now();
        this.orderPrice = orderPrice;
        this.receiveAddress = receiveAddress;
        this.orderGoodsList = orderGoodsList;
        this.orderStatus = OrderStatus.CREATED;
        super.validateSelf();
    }

    /**
     * 支付订单
     */
    public void payOrder() {
        Validates.isTrue(OrderStatus.CREATED.equals(this.getOrderStatus()), "order status is not CREATED");
        this.orderStatus = OrderStatus.PAID;
        this.payTime = super.updateTime = LocalDateTime.now();
        super.validateSelf();
    }

    /**
     * 发货订单
     *
     * @param expressCode 快递单号
     * @param deliverTime 发货时间
     */
    public void deliverOrder(String expressCode, LocalDateTime deliverTime) {
        Validates.isTrue(OrderStatus.PAID.equals(this.getOrderStatus()), "order status is not PAID");
        this.orderStatus = OrderStatus.DELIVERED;
        this.expressCode = expressCode;
        this.deliverTime = deliverTime;
        super.updateTime = LocalDateTime.now();
        super.validateSelf();
    }

    /**
     * 完成订单
     */
    public void completeOrder() {
        Validates.isTrue(OrderStatus.DELIVERED.equals(this.getOrderStatus()), "order status is not DELIVERED");
        this.orderStatus = OrderStatus.COMPLETED;
        this.completeTime = super.updateTime = LocalDateTime.now();
        super.validateSelf();
    }

    /**
     * 取消订单
     */
    public void cancelOrder() {
        this.orderStatus = OrderStatus.CANCELED;
        this.cancelTime = super.updateTime = LocalDateTime.now();
        super.validateSelf();
    }
}
