package com.luo.d3s.light.mybatis.ec.domain.order.acl;

import com.luo.d3s.core.domain.adpator.Acl;
import com.luo.d3s.light.mybatis.ec.domain.order.Order;

/**
 * 库存ACL
 *
 * @author luohq
 * @date 2022-11-27 18:37
 */
public interface StockAcl extends Acl {

    /**
     * 锁库存
     *
     * @param order 订单信息
     */
    void lockStock(Order order);

    /**
     * 扣减库存
     *
     * @param order 扣减库存
     */
    void deductStock(Order order);
}
