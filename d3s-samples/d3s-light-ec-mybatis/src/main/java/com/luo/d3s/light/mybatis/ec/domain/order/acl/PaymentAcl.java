package com.luo.d3s.light.mybatis.ec.domain.order.acl;

import com.luo.d3s.core.domain.adpator.Acl;
import com.luo.d3s.light.mybatis.ec.domain.order.Order;

/**
 * 支付ACL
 *
 * @author luohq
 * @date 2022-11-27 18:37
 */
public interface PaymentAcl extends Acl {

    /**
     * 支付订单
     *
     * @param order 订单信息
     */
    void payOrder(Order order);

    /**
     * 退款订单
     *
     * @param order 订单信息
     */
    void refundOrder(Order order);
}
