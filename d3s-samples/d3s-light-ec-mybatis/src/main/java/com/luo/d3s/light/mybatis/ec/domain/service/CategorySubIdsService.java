package com.luo.d3s.light.mybatis.ec.domain.service;

import com.luo.d3s.core.domain.service.DomainService;
import com.luo.d3s.light.mybatis.ec.domain.category.Category;
import com.luo.d3s.light.mybatis.ec.domain.category.CategoryRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * 根据上级分类ID查询子分类ID列表 - 领域服务
 *
 * @author luohq
 * @date 2023-01-05 10:42
 */
public class CategorySubIdsService implements DomainService {

    private CategoryRepository categoryRepository;

    public CategorySubIdsService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }


    /**
     * 根据上级分类ID查询子分类ID列表（包括上级分类）
     *
     * @param parentCategoryId 上级分类ID
     * @return 子分类ID列表（包括上级分类）
     */
    public List<Long> extractSubCategoryIds(Long parentCategoryId) {
        List<Long> subCategoryIdList = new ArrayList<>();
        return this.extractSubCategoryIds(parentCategoryId, subCategoryIdList);
    }

    /**
     * 内部处理 - 根据上级分类ID查询子分类ID列表（包括上级分类）
     *
     * @param parentCategoryId  上级分类ID
     * @param subCategoryIdList 所有及子分类ID列表
     * @return 子分类ID列表（包括上级分类）
     */
    private List<Long> extractSubCategoryIds(Long parentCategoryId, List<Long> subCategoryIdList) {
        //上级分类ID为空，则直接返回
        if (null == parentCategoryId) {
            return subCategoryIdList;
        }
        //记录当前非空分类ID
        subCategoryIdList.add(parentCategoryId);
        //查询上级分类对应的子分类列表
        List<Category> subCategoryList = this.categoryRepository.findByParentId(parentCategoryId);
        //递归处理子分类列表
        if (null != subCategoryList && !subCategoryList.isEmpty()) {
            subCategoryList.stream()
                    .map(Category::getId)
                    .forEach(categoryId -> this.extractSubCategoryIds(categoryId, subCategoryIdList));
        }
        //返回上级分类及所有子分类ID列表
        return subCategoryIdList;
    }
}
