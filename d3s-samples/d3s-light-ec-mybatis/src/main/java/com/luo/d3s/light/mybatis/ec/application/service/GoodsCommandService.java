package com.luo.d3s.light.mybatis.ec.application.service;

import com.luo.d3s.core.application.dto.Response;
import com.luo.d3s.core.application.dto.SingleResponse;
import com.luo.d3s.core.application.service.CommandService;
import com.luo.d3s.light.mybatis.ec.application.dto.command.GoodsCreateCommand;
import com.luo.d3s.light.mybatis.ec.application.dto.command.GoodsModifyCommand;
import com.luo.d3s.light.mybatis.ec.application.dto.vo.GoodsVo;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * 商品命令处理服务接口
 *
 * @author luohq
 * @date 2023-01-04 17:00
 */
@Validated
public interface GoodsCommandService extends CommandService {

    /**
     * 创建商品
     *
     * @param goodsCreateCommand 创建商品命令
     * @return 响应结果（商品信息）
     */
    SingleResponse<GoodsVo> createGoods(@Validated GoodsCreateCommand goodsCreateCommand);

    /**
     * 修改商品
     *
     * @param goodsModifyCommand 修改商品命令
     * @return 响应结果
     */
    Response modifyGoods(@Validated GoodsModifyCommand goodsModifyCommand);

    /**
     * 批量移除商品
     *
     * @param goodsIds 商品ID列表
     * @return 响应结果
     */
    Response removeGoods(@NotEmpty List<Long> goodsIds);

    /**
     * 批量上架商品
     *
     * @param goodsIds 商品ID列表
     * @return 响应结果
     */
    Response shelveGoods(@NotEmpty List<Long> goodsIds);

    /**
     * 批量下架商品
     *
     * @param goodsIds 商品ID列表
     * @return 响应结果
     */
    Response unshelveGoods(@NotEmpty List<Long> goodsIds);
}
