package com.luo.d3s.light.mybatis.ec.domain.order;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.core.infrastructure.convertor.MpPageConvertor;
import com.luo.d3s.light.mybatis.ec.application.dto.query.OrderPageQuery;
import com.luo.d3s.light.mybatis.ec.application.dto.vo.GoodsVo;
import com.luo.d3s.light.mybatis.ec.application.dto.vo.OrderVo;
import com.luo.d3s.light.mybatis.ec.domain.base.BaseRepository;
import org.apache.ibatis.annotations.Param;

import java.util.Collection;
import java.util.function.Function;

/**
 * 订单仓库
 *
 * @author luohq
 * @date 2022-12-21
 */
public interface OrderRepository extends BaseRepository<Order> {
    @Override
    default Order save(Order order) {
        /** ID已存在，则修改 */
        if (this.exists(order.getId())) {
            //修改订单信息
            this.updateById(order);
            //返回订单信息
            return order;
        }
        /** ID不存在，则新增 */
        //保存订单及订单商品信息
        this.insertOrderAndGoods(order);
        return order;
    }

    /**
     * 查询订单分页列表
     *
     * @param orderPageQuery 订单分页查询参数
     * @return page response
     */
    default PageResponse<OrderVo> findPage(OrderPageQuery orderPageQuery) {
        IPage<OrderVo> pageResult = this.selectOrderPage(MpPageConvertor.toPage(orderPageQuery), orderPageQuery);
        return MpPageConvertor.toPageResponse(pageResult, Function.identity());
    }

    /**
     * 根据订单ID查询订单及商品信息
     *
     * @param id 订单ID
     * @return 订单信息
     */
    @Override
    Order findById(Long id);

    /**
     * 根据订单ID查询订单及商品信息VO
     *
     * @param id 订单ID
     * @return 订单信息
     */
    OrderVo findVoById(Long id);

    /**
     * 移除订单和订单商品
     *
     * @param orderId 订单ID
     * @return 成功移除订单数量
     */
    @Override
    Integer removeById(Long orderId);

    /**
     * 批量批量移除订单和订单商品
     *
     * @param orderIds 订单ID集合
     * @return 成功移除订单数量
     */
    @Override
    Integer removeByIds(Collection<Long> orderIds);

    /**
     * 保存订单及订单商品信息
     *
     * @param order 订单及订单商品信息
     * @return 成功保存订单数量
     */
    @Deprecated
    Integer insertOrderAndGoods(Order order);

    /**
     * 查询订单分页列表
     *
     * @param page           mp分页参数
     * @param orderPageQuery 具体查询参数
     * @return 分页查询结果
     */
    @Deprecated
    IPage<OrderVo> selectOrderPage(IPage<GoodsVo> page, @Param("orderPageQuery") OrderPageQuery orderPageQuery);
}
