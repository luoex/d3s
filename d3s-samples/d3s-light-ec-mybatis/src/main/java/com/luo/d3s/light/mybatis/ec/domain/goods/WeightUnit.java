package com.luo.d3s.light.mybatis.ec.domain.goods;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.luo.d3s.core.domain.model.ValueObjectEnum;

/**
 * 重量单位
 *
 * @author luohq
 * @date 2023-01-04 16:24
 */
public enum WeightUnit implements ValueObjectEnum<String> {
    //吨
    T("t", "吨"),
    //公斤
    KG("kg", "公斤"),
    //克
    G("g", "克");

    /**
     * 单位
     */
    @EnumValue
    private String value;
    /**
     * 描述
     */
    private String desc;

    WeightUnit(String value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public String getDesc() {
        return desc;
    }

    public static WeightUnit of(String value) {
        return ValueObjectEnum.of(value, WeightUnit.class);
    }
}
