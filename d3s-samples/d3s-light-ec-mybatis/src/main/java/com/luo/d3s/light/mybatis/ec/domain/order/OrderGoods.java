package com.luo.d3s.light.mybatis.ec.domain.order;

import com.baomidou.mybatisplus.annotation.TableName;
import com.luo.d3s.core.util.IdGenUtils;
import com.luo.d3s.light.mybatis.ec.domain.base.BaseEntity;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 订单商品数据对象
 *
 * @author luohq
 * @date 2022-12-21 9:42
 */
@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@TableName("order_goods")
public class OrderGoods extends BaseEntity {
    /**
     * 商品ID
     */
    @NotNull
    private Long goodsId;

    /**
     * 订单ID
     */
    @NotNull
    private Long orderId;

    /**
     * 商品数量
     */
    @NotNull
    @Positive
    private Integer goodsCount;

    /**
     * 商品价值
     */
    @NotNull
    @Positive
    private BigDecimal goodsPrice;

    /**
     * 商品总价
     */
    @NotNull
    @Positive
    private BigDecimal goodsSumPrice;

    /**
     * 初始创建订单商品
     *
     * @param goodsId       商品ID
     * @param orderId       订单ID
     * @param goodsCount    商品数量
     * @param goodsPrice    商品价值
     * @param goodsSumPrice 商品总价
     */
    public OrderGoods(Long goodsId, Long orderId, Integer goodsCount, BigDecimal goodsPrice, BigDecimal goodsSumPrice) {
        //生成ID
        super.id = IdGenUtils.nextDigitalId();
        super.createTime = LocalDateTime.now();
        this.goodsId = goodsId;
        this.orderId = orderId;
        this.goodsCount = goodsCount;
        this.goodsPrice = goodsPrice;
        this.goodsSumPrice = goodsSumPrice;
        super.validateSelf();
    }

}
