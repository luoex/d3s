package com.luo.d3s.light.mybatis.ec.application.dto.event;

import com.luo.d3s.core.application.event.ApplicationEvent;
import com.luo.d3s.core.util.IdGenUtils;
import lombok.*;

import java.time.LocalDateTime;

/**
 * 物流已发货事件
 *
 * @author luohq
 * @date 2022-11-27 18:44
 */
@Getter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class StockDeliveredEvent implements ApplicationEvent<String, Long> {
    /**
     * 事件ID
     */
    @EqualsAndHashCode.Include
    private String eventId;
    /**
     * 事件创建时间
     */
    private LocalDateTime createTime;

    /**
     * 发货记录ID
     */
    private Long deliveryRecordId;

    /**
     * 订单ID
     */
    private Long orderId;

    /**
     * 快递单号
     */
    private String expressCode;

    /**
     * 发货时间
     */
    private LocalDateTime deliveryTime;

    public StockDeliveredEvent(Long deliveryRecordId, Long orderId, String expressCode, LocalDateTime deliveryTime) {
        this.deliveryRecordId = deliveryRecordId;
        this.orderId = orderId;
        this.expressCode = expressCode;
        this.deliveryTime = deliveryTime;
        this.eventId = IdGenUtils.nextTextId();
        this.createTime = LocalDateTime.now();
    }

    @Override
    public Long getAggregateRootId() {
        return this.orderId;
    }
}
