package com.luo.d3s.light.mybatis.ec.application.dto.command;

import com.luo.d3s.light.mybatis.ec.application.dto.vo.OrderGoodsVo;
import com.luo.d3s.core.application.dto.Command;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;
import java.util.List;

/**
 * 创建订单Command
 *
 * @author luohq
 * @date 2022-11-27 19:00
 */
@Data
public class OrderCreateCommand extends Command {
    /**
     * 商品项
     */
    @NotEmpty
    private List<OrderGoodsVo> orderGoods;
    /**
     * 订单总价
     */
    private BigDecimal orderPrice;
    /**
     * 收货地址
     */
    private String receiveAddress;
}
