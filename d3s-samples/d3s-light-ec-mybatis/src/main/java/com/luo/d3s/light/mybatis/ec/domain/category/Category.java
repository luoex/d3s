package com.luo.d3s.light.mybatis.ec.domain.category;

import com.baomidou.mybatisplus.annotation.TableName;
import com.luo.d3s.core.util.IdGenUtils;
import com.luo.d3s.light.mybatis.ec.domain.base.BaseEntity;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

/**
 * Category数据对象
 *
 * @author luohq
 * @date 2022-12-21 9:42
 */
@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@TableName("category")
public class Category extends BaseEntity {

    /**
     * 上级分类ID
     */
    private Long parentCategoryId;

    /**
     * 分类名称
     */
    @NotBlank
    @Length(min = 1, max = 64)
    private String categoryName;

    /**
     * 分类描述
     */
    @Length(min = 1, max = 512)
    private String categoryDesc;

    /**
     * 创建分类
     *
     * @param parentCategoryId 上级分类ID
     * @param categoryName     分类名称
     * @param categoryDesc     分类描述
     */
    public Category(Long parentCategoryId, String categoryName, String categoryDesc) {
        //生成ID
        super.id = IdGenUtils.nextDigitalId();
        this.parentCategoryId = parentCategoryId;
        this.categoryName = categoryName;
        this.categoryDesc = categoryDesc;
        super.createTime = LocalDateTime.now();
        super.validateSelf();
    }

    /**
     * 修改基础信息
     *
     * @param parentCategoryId 上级分类ID
     * @param categoryName     分类名称
     * @param categoryDesc     分类描述
     */
    public void modifyBasicInfo(Long parentCategoryId, String categoryName, String categoryDesc) {
        this.parentCategoryId = parentCategoryId;
        this.categoryName = categoryName;
        this.categoryDesc = categoryDesc;
        super.updateTime = LocalDateTime.now();
        super.validateSelf();
    }
}
