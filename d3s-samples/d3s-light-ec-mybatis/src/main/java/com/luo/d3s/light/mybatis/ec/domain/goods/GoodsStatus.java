package com.luo.d3s.light.mybatis.ec.domain.goods;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.luo.d3s.core.domain.model.ValueObjectEnum;

/**
 * 商品状态
 *
 * @author luohq
 * @date 2022-11-27 16:01
 */
public enum GoodsStatus implements ValueObjectEnum<Integer> {
    //10 - 已上架
    SHELVED(10, "已上架"),
    //20 - 已下架
    UNSHELVED(20, "已下架");

    /**
     * 状态编码
     */
    @EnumValue
    private Integer status;
    /**
     * 状态描述
     */
    private String desc;

    GoodsStatus(Integer status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    @Override
    public Integer getValue() {
        return status;
    }

    @Override
    public String getDesc() {
        return desc;
    }

    public static GoodsStatus of(Integer status) {
        return ValueObjectEnum.of(status, GoodsStatus.class);
    }
}
