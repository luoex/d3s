package com.luo.d3s.light.mybatis.ec.interfaces.web.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Jackson配置属性
 *
 * @author luohq
 * @version 1.0.0
 * @date 2022-05-17 10:36
 */
@ConfigurationProperties(prefix = WebJacksonProps.PREFIX)
@Data
public class WebJacksonProps {
    /**
     * 属性配置前缀
     */
    public static final String PREFIX = "web.jackson";
    /**
     * 默认日期格式定义
     */
    public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String DEFAULT_LOCAL_DATE_FORMAT = "yyyy-MM-dd";
    public static final String DEFAULT_LOCAL_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String DEFAULT_TIME_ZONE = "GMT+8";

    /**
     * 是否启用Osmium Jackson配置
     */
    private Boolean enabled = true;
    /**
     * 日期格式 - java.util.Date
     */
    private String dateFormat = DEFAULT_DATE_FORMAT;
    /**
     * 日期格式 - java.time.LocalDate
     */
    private String localDateFormat = DEFAULT_LOCAL_DATE_FORMAT;
    /**
     * 日期时间格式 - java.time.LocalDateTime
     */
    private String localDateTimeFormat = DEFAULT_LOCAL_DATE_TIME_FORMAT;
    /**
     * 时区
     */
    private String timeZone = DEFAULT_TIME_ZONE;

}
