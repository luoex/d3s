package com.luo.d3s.light.mybatis.ec.application.assembler;

import com.luo.d3s.light.mybatis.ec.application.dto.command.GoodsCreateCommand;
import com.luo.d3s.light.mybatis.ec.domain.goods.Goods;
import com.luo.d3s.light.mybatis.ec.domain.goods.GoodsStatus;
import com.luo.d3s.light.mybatis.ec.domain.goods.WeightUnit;

/**
 * Goods转换器
 *
 * @author luohq
 * @date 2023-01-04 17:10
 */
public interface GoodsAssembler {

    /**
     * 创建商品
     *
     * @param goodsCreateCommand 创建商品命令
     * @return 商品信息
     */
    static Goods createGoods(GoodsCreateCommand goodsCreateCommand) {
        return new Goods(
                goodsCreateCommand.getCategoryId(),
                goodsCreateCommand.getGoodsName(),
                goodsCreateCommand.getManufactureDate(), goodsCreateCommand.getExpirationDate(),
                goodsCreateCommand.getGoodsWeight(), WeightUnit.of(goodsCreateCommand.getGoodsWeightUnit()),
                goodsCreateCommand.getGoodsDesc(),
                goodsCreateCommand.getGoodsPrice(),
                GoodsStatus.UNSHELVED,
                goodsCreateCommand.getTagIds()
        );
    }
}
