package com.luo.d3s.light.mybatis.ec.application.dto.command;

import com.luo.d3s.core.util.validation.constraints.EnumValueRange;
import com.luo.d3s.light.mybatis.ec.domain.goods.WeightUnit;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

/**
 * 创建商品Command
 *
 * @author luohq
 * @date 2023-01-04 15:29
 */
@Data
public class GoodsCreateCommand {

    /**
     * 所属分类ID
     */
    @NotNull
    private Long categoryId;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 生产日期
     */
    private LocalDate manufactureDate;

    /**
     * 过期日期
     */
    private LocalDate expirationDate;

    /**
     * 商品重量
     */
    private BigDecimal goodsWeight;

    /**
     * 商品重量单位
     */
    @EnumValueRange(WeightUnit.class)
    private String goodsWeightUnit;

    /**
     * 商品介绍
     */
    private String goodsDesc;

    /**
     * 商品价格
     */
    private BigDecimal goodsPrice;

    /**
     * 商品标签ID集合
     */
    private Set<Long> tagIds;
}
