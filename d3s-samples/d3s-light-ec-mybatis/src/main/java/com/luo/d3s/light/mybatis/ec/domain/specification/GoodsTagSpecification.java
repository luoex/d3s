package com.luo.d3s.light.mybatis.ec.domain.specification;

import com.luo.d3s.core.domain.service.specifcation.AbstractSpecification;
import com.luo.d3s.core.util.validation.Validates;
import com.luo.d3s.light.mybatis.ec.domain.goodstag.GoodsTag;
import com.luo.d3s.light.mybatis.ec.domain.goodstag.GoodsTagRepository;

import java.util.Objects;

/**
 * 商品标签验证规则
 *
 * @author luohq
 * @date 2023-01-05 8:47
 */
public class GoodsTagSpecification extends AbstractSpecification<GoodsTag> {

    private GoodsTagRepository goodsTagRepository;

    public GoodsTagSpecification(GoodsTagRepository goodsTagRepository) {
        this.goodsTagRepository = goodsTagRepository;
    }

    @Override
    public boolean isSatisfiedBy(GoodsTag goodsTag) {
        //验证商品标签名称唯一性
        GoodsTag existGoodsTag = this.goodsTagRepository.findByTagName(goodsTag.getTagName());
        //是否存在标签名（标签名不存在 或者 存在的标签名不是当前记录）
        Boolean existGoodsTagName = Objects.nonNull(existGoodsTag) && !existGoodsTag.getId().equals(goodsTag.getId());
        Validates.isFalse(existGoodsTagName, "goodsTag tagName is repeated");
        return true;
    }
}
