package com.luo.d3s.light.mybatis.ec.application.service;

import com.luo.d3s.core.application.dto.Response;
import com.luo.d3s.core.application.dto.SingleResponse;
import com.luo.d3s.core.application.service.CommandService;
import com.luo.d3s.light.mybatis.ec.application.dto.command.GoodsTagCreateCommand;
import com.luo.d3s.light.mybatis.ec.application.dto.command.GoodsTagModifyCommand;
import com.luo.d3s.light.mybatis.ec.application.dto.vo.GoodsTagVo;

/**
 * 商品标签命令处理服务接口
 *
 * @author luohq
 * @date 2023-08-03
 */
public interface GoodsTagCommandService extends CommandService {

    /**
     * 创建商品标签
     *
     * @param goodsTagCreateCommand 创建商品标签命令
     * @return 响应结果（商品标签信息）
     */
    SingleResponse<GoodsTagVo> createGoodsTag(GoodsTagCreateCommand goodsTagCreateCommand);

    /**
     * 修改商品标签
     *
     * @param goodsTagModifyCommand 修改商品标签命令
     * @return 响应结果
     */
    Response modifyGoodsTag(GoodsTagModifyCommand goodsTagModifyCommand);

    /**
     * 移除商品标签
     *
     * @param goodsTagId 商品标签ID
     * @return 响应结果
     */
    Response removeGoodsTag(Long goodsTagId);
}
