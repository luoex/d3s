package com.luo.d3s.light.mybatis.ec.application.service.impl;

import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.core.application.dto.SingleResponse;
import com.luo.d3s.light.mybatis.ec.application.dto.query.GoodsPageQuery;
import com.luo.d3s.light.mybatis.ec.application.dto.vo.GoodsVo;
import com.luo.d3s.light.mybatis.ec.application.service.GoodsQueryService;
import com.luo.d3s.light.mybatis.ec.domain.goods.GoodsRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 商品查询服务实现类 - infrastructure实现（适用于CQRS中的Query处理）
 *
 * @author luohq
 * @date 2023-01-07 11:12
 */
@Service
public class GoodsQueryServiceImpl implements GoodsQueryService {

    @Resource
    private GoodsRepository goodsRepository;


    @Override
    public SingleResponse<GoodsVo> findGoods(Long goodsId) {
        GoodsVo goodsVo = this.goodsRepository.findGoodsWithCNameById(goodsId);
        return SingleResponse.of(goodsVo);
    }

    @Override
    public PageResponse<GoodsVo> findGoodsPage(GoodsPageQuery goodsPageQuery) {
        return this.goodsRepository.findGoodsWithCNamePage(goodsPageQuery);
    }
}
