package com.luo.d3s.light.mybatis.ec.application.service.impl;

import com.luo.d3s.core.application.assmebler.BaseAssembler;
import com.luo.d3s.core.application.assmebler.PageResponseAssembler;
import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.core.application.dto.SingleResponse;
import com.luo.d3s.light.mybatis.ec.application.dto.query.GoodsTagPageQuery;
import com.luo.d3s.light.mybatis.ec.application.dto.vo.GoodsTagVo;
import com.luo.d3s.light.mybatis.ec.application.service.GoodsTagQueryService;
import com.luo.d3s.light.mybatis.ec.domain.goodstag.GoodsTag;
import com.luo.d3s.light.mybatis.ec.domain.goodstag.GoodsTagRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 商品标签查询服务实现类
 *
 * @author luohq
 * @date 2023-08-03
 */
@Service
public class GoodsTagQueryServiceImpl implements GoodsTagQueryService {

    @Resource
    private GoodsTagRepository goodsTagRepository;

    @Override
    public SingleResponse<GoodsTagVo> findGoodsTag(Long goodsTagId) {
        GoodsTag goodsTag = this.goodsTagRepository.findById(goodsTagId);
        return SingleResponse.of(BaseAssembler.convert(goodsTag, GoodsTagVo.class));
    }

    @Override
    public PageResponse<GoodsTagVo> findGoodsTagPage(GoodsTagPageQuery goodsTagPageQuery) {
        PageResponse<GoodsTag> pageResp = this.goodsTagRepository.findPage(goodsTagPageQuery);
        return PageResponseAssembler.toPageResp(pageResp, goodsTag -> BaseAssembler.convert(goodsTag, GoodsTagVo.class));
    }
}
