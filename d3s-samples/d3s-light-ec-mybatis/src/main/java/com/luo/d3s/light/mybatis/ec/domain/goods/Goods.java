package com.luo.d3s.light.mybatis.ec.domain.goods;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.luo.d3s.core.util.IdGenUtils;
import com.luo.d3s.light.mybatis.ec.domain.base.BaseEntity;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

/**
 * Goods数据对象
 *
 * @author luohq
 * @date 2022-12-21 9:42
 */
@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@TableName("goods")
public class Goods extends BaseEntity {

    /**
     * 商品标签集合
     */
    @TableField(exist = false)
    private Set<Long> tags;

    /**
     * 所属分类ID
     */
    @NotNull
    @Positive
    private Long categoryId;

    /**
     * 商品名称
     */
    @NotBlank
    @Length(min = 1, max =  120)
    private String goodsName;

    /**
     * 生产日期
     */
    @NotNull
    @PastOrPresent
    private LocalDate manufactureDate;

    /**
     * 过期日期
     */
    @NotNull
    private LocalDate expirationDate;

    /**
     * 商品重量
     */
    @NotNull
    @PositiveOrZero
    private BigDecimal goodsWeight;

    /**
     * 商品重量单位
     */
    @NotNull
    private WeightUnit goodsWeightUnit;

    /**
     * 商品介绍
     */
    @NotBlank
    @Length(min = 1, max = 1024)
    private String goodsDesc;

    /**
     * 商品价格
     */
    private BigDecimal goodsPrice;

    /**
     * 商品状态(10已上架, 20已下架)
     */
    private GoodsStatus goodsStatus;

    /**
     * 创建商品
     *
     * @param categoryId      所属分类ID
     * @param goodsName       商品名称
     * @param manufactureDate 生产日期
     * @param expirationDate  过期日期
     * @param goodsWeight     商品重量
     * @param goodsWeightUnit 商品重量单位
     * @param goodsDesc       商品介绍
     * @param goodsPrice      商品价格
     * @param goodsStatus     商品状态(10已上架, 20已下架)
     * @param tags            商品标签ID集合
     */
    public Goods(Long categoryId,
                 String goodsName,
                 LocalDate manufactureDate, LocalDate expirationDate,
                 BigDecimal goodsWeight, WeightUnit goodsWeightUnit,
                 String goodsDesc,
                 BigDecimal goodsPrice,
                 GoodsStatus goodsStatus,
                 Set<Long> tags) {
        //生成ID
        super.id = IdGenUtils.nextDigitalId();
        super.createTime = LocalDateTime.now();
        this.categoryId = categoryId;
        this.goodsName = goodsName;
        this.manufactureDate = manufactureDate;
        this.expirationDate = expirationDate;
        this.goodsWeight = goodsWeight;
        this.goodsWeightUnit = goodsWeightUnit;
        this.goodsDesc = goodsDesc;
        this.goodsPrice = goodsPrice;
        this.goodsStatus = goodsStatus;
        this.tags = tags;
        super.validateSelf();
    }

    /**
     * 修改基础信息
     *
     * @param categoryId      所属分类ID
     * @param goodsName       商品名称
     * @param manufactureDate 生产日期
     * @param expirationDate  过期日期
     * @param goodsWeight     商品重量
     * @param goodsWeightUnit 商品重量单位
     * @param goodsDesc       商品介绍
     * @param goodsPrice      商品价格
     * @param tags            商品标签ID集合
     */
    public void modifyBasicInfo(Long categoryId,
                                String goodsName,
                                LocalDate manufactureDate, LocalDate expirationDate,
                                BigDecimal goodsWeight, WeightUnit goodsWeightUnit,
                                String goodsDesc,
                                BigDecimal goodsPrice,
                                Set<Long> tags) {
        super.updateTime = LocalDateTime.now();
        this.categoryId = categoryId;
        this.goodsName = goodsName;
        this.manufactureDate = manufactureDate;
        this.expirationDate = expirationDate;
        this.goodsWeight = goodsWeight;
        this.goodsWeightUnit = goodsWeightUnit;
        this.goodsDesc = goodsDesc;
        this.goodsPrice = goodsPrice;
        this.tags = tags;
        super.validateSelf();
    }

    /**
     * 修改商品标签
     *
     * @param tags 商品标签ID集合
     */
    public void modifyTags(Set<Long> tags) {
        this.tags = tags;
    }

    /**
     * 上架商品
     */
    public void shelve() {
        super.updateTime = LocalDateTime.now();
        this.goodsStatus = GoodsStatus.SHELVED;
    }

    /**
     * 下架商品
     */
    public void unshelve() {
        super.updateTime = LocalDateTime.now();
        this.goodsStatus = GoodsStatus.UNSHELVED;
    }
}
