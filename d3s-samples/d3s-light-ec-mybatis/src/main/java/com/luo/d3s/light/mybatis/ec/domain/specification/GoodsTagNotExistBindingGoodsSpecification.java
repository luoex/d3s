package com.luo.d3s.light.mybatis.ec.domain.specification;

import com.luo.d3s.core.domain.service.specifcation.AbstractSpecification;
import com.luo.d3s.core.util.validation.Validates;
import com.luo.d3s.light.mybatis.ec.domain.goods.GoodsRepository;

import java.util.Collection;
import java.util.Objects;

/**
 * 商品标签是否存在验证规则
 *
 * @author luohq
 * @date 2023-08-02
 */
public class GoodsTagNotExistBindingGoodsSpecification extends AbstractSpecification<Collection<Long>> {

    private GoodsRepository goodsRepository;

    public GoodsTagNotExistBindingGoodsSpecification(GoodsRepository goodsRepository) {
        this.goodsRepository = goodsRepository;
    }

    @Override
    public boolean isSatisfiedBy(Collection<Long> goodsTagIds) {
        //若标签ID集合为空，则直接验证通过
        if (Objects.isNull(goodsTagIds) || goodsTagIds.isEmpty()) {
            return true;
        }
        //验证是否存在已绑定标签的商品
        Boolean existGoods = this.goodsRepository.existsByTagIds(goodsTagIds);
        Validates.isFalse(existGoods, "exist goods in tags");
        return true;
    }
}
