package com.luo.d3s.light.mybatis.ec.application.service.impl;

import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.core.application.dto.SingleResponse;
import com.luo.d3s.light.mybatis.ec.application.dto.query.OrderPageQuery;
import com.luo.d3s.light.mybatis.ec.application.dto.vo.OrderVo;
import com.luo.d3s.light.mybatis.ec.application.service.OrderQueryService;
import com.luo.d3s.light.mybatis.ec.domain.order.OrderRepository;
import org.springframework.stereotype.Service;

/**
 * 订单查询服务实现类
 *
 * @author luohq
 * @date 2022-11-27 19:18
 */
@Service
public class OrderQueryServiceImpl implements OrderQueryService {
    private OrderRepository orderRepository;

    public OrderQueryServiceImpl(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public SingleResponse<OrderVo> findOrder(Long orderId) {
        OrderVo orderVo = this.orderRepository.findVoById(orderId);
        return SingleResponse.of(orderVo);
    }

    @Override
    public PageResponse<OrderVo> findOrderPage(OrderPageQuery orderPageQuery) {
        return this.orderRepository.findPage(orderPageQuery);
    }
}
