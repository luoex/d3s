package com.luo.d3s.light.mybatis.ec.domain.specification;

import com.luo.d3s.core.domain.service.specifcation.AbstractSpecification;
import com.luo.d3s.core.util.validation.Validates;
import com.luo.d3s.light.mybatis.ec.domain.category.CategoryRepository;

/**
 * 分类是否存在验证规则
 *
 * @author luohq
 * @date 2023-01-06 15:38
 */
public class CategoryExistSpecification extends AbstractSpecification<Long> {

    private CategoryRepository categoryRepository;

    public CategoryExistSpecification(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public boolean isSatisfiedBy(Long categoryId) {
        //验证分类ID是否存在
        Boolean existCategory = this.categoryRepository.existCategoryId(categoryId);
        Validates.isTrue(existCategory, "category id does not exist");
        return true;
    }
}
