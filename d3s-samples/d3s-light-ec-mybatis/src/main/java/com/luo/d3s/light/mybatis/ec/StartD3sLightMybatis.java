package com.luo.d3s.light.mybatis.ec;

import com.luo.d3s.ext.component.scan.D3sComponentScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring Boot Starter
 *
 * @author luohq
 * @date 2022-12-22
 * @notice @D3sComponentScan需放在@SpringBootApplication上方，否则启动报错（无法扫描到领域对象）
 */
@D3sComponentScan
@SpringBootApplication
public class StartD3sLightMybatis {
    public static void main(String[] args) {
        SpringApplication.run(StartD3sLightMybatis.class, args);
    }
}