package com.luo.d3s.light.mybatis.ec.application.service;

import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.core.application.dto.SingleResponse;
import com.luo.d3s.core.application.service.QueryService;
import com.luo.d3s.light.mybatis.ec.application.dto.vo.GoodsVo;
import com.luo.d3s.light.mybatis.ec.application.dto.query.GoodsPageQuery;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;

/**
 * 商品查询服务接口类
 *
 * @author luohq
 * @date 2023-01-05 11:19
 */
@Validated
public interface GoodsQueryService extends QueryService {

    /**
     * 查询商品详情
     *
     * @param goodsId 商品ID
     * @return 商品详情
     */
    SingleResponse<GoodsVo> findGoods(@NotNull Long goodsId);

    /**
     * 查询商品分页列表
     *
     * @param goodsPageQuery 商品分页查询参数
     * @return 分页查询结果
     */
    PageResponse<GoodsVo> findGoodsPage(GoodsPageQuery goodsPageQuery);

}
