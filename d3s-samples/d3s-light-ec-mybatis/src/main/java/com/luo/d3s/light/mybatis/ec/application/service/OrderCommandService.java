package com.luo.d3s.light.mybatis.ec.application.service;

import com.luo.d3s.core.application.dto.Response;
import com.luo.d3s.core.application.dto.SingleResponse;
import com.luo.d3s.core.application.service.CommandService;
import com.luo.d3s.light.mybatis.ec.application.dto.command.OrderCreateCommand;
import com.luo.d3s.light.mybatis.ec.application.dto.command.OrderDeliverCommand;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;

/**
 * 订单命令处理服务接口类
 *
 * @author luohq
 * @date 2022-11-27 19:09
 */
@Validated
public interface OrderCommandService extends CommandService {

    /**
     * 创建订单
     *
     * @param createOrderCommand 创建订单命令
     */

    /**
     * 创建订单
     *
     * @param orderCreateCommand 创建订单命令
     * @return 响应结果（订单ID）
     */
    SingleResponse<Long> createOrder(@Validated OrderCreateCommand orderCreateCommand);

    /**
     * 支付订单
     *
     * @param orderId 订单ID
     * @return 响应结果
     */
    Response payOrder(@NotNull Long orderId);

    /**
     * 发货订单
     *
     * @param orderDeliverCommand 发货订单命令
     * @return 响应结果
     */
    Response deliverOrder(@Validated OrderDeliverCommand orderDeliverCommand);


    /**
     * 完成订单
     *
     * @param orderId 订单ID
     * @return 响应结果
     */
    Response completeOrder(@NotNull Long orderId);

    /**
     * 取消订单
     *
     * @param orderId 订单ID
     * @return 响应结果
     */
    Response cancelOrder(@NotNull Long orderId);
}
