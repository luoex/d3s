package com.luo.d3s.light.mybatis.ec.application.assembler;

import com.luo.d3s.core.util.IdGenUtils;
import com.luo.d3s.light.mybatis.ec.application.dto.command.OrderCreateCommand;
import com.luo.d3s.light.mybatis.ec.domain.order.Order;
import com.luo.d3s.light.mybatis.ec.domain.order.OrderGoods;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Order转换器
 *
 * @author luohq
 * @date 2022-11-28 14:00
 */
public interface OrderAssembler {

    /**
     * 转换创建订单Command为Order信息
     *
     * @param orderCreateCommand 创建订单Command
     * @return Order信息
     */
    static Order createOrder(OrderCreateCommand orderCreateCommand) {
        //生成新的订单ID
        Long orderId = IdGenUtils.nextDigitalId();
        //创建订单商品列表
        List<OrderGoods> orderGoodsList = orderCreateCommand.getOrderGoods().stream()
                .map(orderGoodsDto -> new OrderGoods(
                        orderGoodsDto.getGoodsId(),
                        orderId,
                        orderGoodsDto.getGoodsCount(),
                        orderGoodsDto.getGoodsPrice(),
                        orderGoodsDto.getGoodsSumPrice())
                )
                .collect(Collectors.toList());
        //创建订单
        return new Order(orderId, orderCreateCommand.getOrderPrice(), orderCreateCommand.getReceiveAddress(), orderGoodsList);
    }
}
