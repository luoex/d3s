package com.luo.d3s.light.mybatis.ec.application.dto.command;

import lombok.Data;

/**
 * 创建商品标签Command
 *
 * @author luohq
 * @date 2023-01-04 15:29
 */
@Data
public class GoodsTagCreateCommand {
    /**
     * 商品标签名称
     */
    private String tagName;

    /**
     * 商品标签描述
     */
    private String tagDesc;
}
