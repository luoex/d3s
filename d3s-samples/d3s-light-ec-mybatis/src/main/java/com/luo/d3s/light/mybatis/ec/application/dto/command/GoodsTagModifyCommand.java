package com.luo.d3s.light.mybatis.ec.application.dto.command;

import lombok.Data;

/**
 * 修改商品标签Command
 *
 * @author luohq
 * @date 2023-08-03
 */
@Data
public class GoodsTagModifyCommand {
    /**
     * 主键ID
     */
    private Long id;

    /**
     * 商品标签名称
     */
    private String tagName;

    /**
     * 商品标签描述
     */
    private String tagDesc;
}
