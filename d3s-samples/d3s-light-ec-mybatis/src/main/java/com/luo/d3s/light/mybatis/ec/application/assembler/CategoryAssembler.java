package com.luo.d3s.light.mybatis.ec.application.assembler;

import com.luo.d3s.light.mybatis.ec.application.dto.command.CategoryCreateCommand;
import com.luo.d3s.light.mybatis.ec.domain.category.Category;

/**
 * Category转换器
 *
 * @author luohq
 * @date 2023-01-04 17:10
 */
public interface CategoryAssembler {

    /**
     * 创建分类
     *
     * @param categoryCreateCommand 分类创建命令
     * @return 初始分类
     */
    static Category createCategory(CategoryCreateCommand categoryCreateCommand) {
        return new Category(categoryCreateCommand.getParentCategoryId(), categoryCreateCommand.getCategoryName(), categoryCreateCommand.getCategoryDesc());
    }
}
