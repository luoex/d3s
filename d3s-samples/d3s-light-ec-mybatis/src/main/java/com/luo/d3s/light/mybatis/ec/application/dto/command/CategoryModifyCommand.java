package com.luo.d3s.light.mybatis.ec.application.dto.command;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 修改分类Command
 *
 * @author luohq
 * @date 2023-01-04 15:29
 */
@Data
public class CategoryModifyCommand {
    /**
     * 分类ID
     */
    @NotNull
    private Long id;
    /**
     * 上级分类ID
     */
    private Long parentCategoryId;
    /**
     * 分类名称
     */
    private String categoryName;
    /**
     * 分类描述
     */
    private String categoryDesc;
}
