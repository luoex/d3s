package com.luo.d3s.light.mybatis.ec.infrastructure.rpc;

import com.luo.d3s.light.mybatis.ec.domain.order.Order;
import com.luo.d3s.light.mybatis.ec.domain.order.acl.PaymentAcl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 支付ACL实现类
 *
 * @author luohq
 * @date 2022-12-21 9:44
 */
@Component
@Slf4j
public class PaymentAclImpl implements PaymentAcl {

    @Override
    public void payOrder(Order order) {
        log.info("[MOCK RPC] pay order, order:{}", order);
    }

    @Override
    public void refundOrder(Order order) {
        log.info("[MOCK RPC] refund order, order:{}", order);
    }
}
