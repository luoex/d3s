package com.luo.d3s.light.mybatis.ec.infrastructure.rpc.dto;

import lombok.Data;

/**
 * 锁库存参数
 *
 * @author luohq
 * @date 2022-12-21 13:28
 */
@Data
public class LockStockDto {

    private Long goodsId;

    private Integer lockCount;


}
