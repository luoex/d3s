package com.luo.d3s.light.mybatis.ec.interfaces.web;

import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.core.application.dto.Response;
import com.luo.d3s.core.application.dto.SingleResponse;
import com.luo.d3s.light.mybatis.ec.application.dto.vo.GoodsVo;
import com.luo.d3s.light.mybatis.ec.application.dto.command.GoodsCreateCommand;
import com.luo.d3s.light.mybatis.ec.application.dto.command.GoodsModifyCommand;
import com.luo.d3s.light.mybatis.ec.application.dto.query.GoodsPageQuery;
import com.luo.d3s.light.mybatis.ec.application.service.GoodsCommandService;
import com.luo.d3s.light.mybatis.ec.application.service.GoodsQueryService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 商品Controller
 *
 * @author luohq
 * @date 2023-01-07 12:12
 */
@RestController
@RequestMapping("/api/v1/goods")
public class GoodsController {
    @Resource
    private GoodsQueryService goodsQueryService;

    @Resource
    private GoodsCommandService goodsCommandService;

    /**
     * 根据商品ID查询商品信息
     *
     * @param goodsId 商品ID
     * @return 商品信息
     */
    @GetMapping("/{goodsId}")
    public SingleResponse<GoodsVo> findGoods(@PathVariable Long goodsId) {
        return this.goodsQueryService.findGoods(goodsId);
    }

    /**
     * 查询商品分页列表
     *
     * @param goodsPageQuery 商品分页查询参数
     * @return 商品分页查询结果
     */
    @GetMapping
    public PageResponse<GoodsVo> findGoodsPage(GoodsPageQuery goodsPageQuery) {
        return this.goodsQueryService.findGoodsPage(goodsPageQuery);
    }

    /**
     * 创建商品
     *
     * @param goodsCreateCommand 商品创建命令
     * @return 创建结果
     */
    @PostMapping
    public SingleResponse<GoodsVo> createGoods(@RequestBody GoodsCreateCommand goodsCreateCommand) {
        return this.goodsCommandService.createGoods(goodsCreateCommand);
    }

    /**
     * 修改商品
     *
     * @param goodsModifyCommand 商品修改命令
     * @return 修改结果
     */
    @PutMapping
    public Response modifyGoods(@RequestBody GoodsModifyCommand goodsModifyCommand) {
        return this.goodsCommandService.modifyGoods(goodsModifyCommand);
    }

    /**
     * 批量删除商品
     *
     * @param goodsIds 商品ID列表
     * @return 删除结果
     */
    @DeleteMapping("/{goodsIds}")
    public Response removeGoods(@PathVariable List<Long> goodsIds) {
        return this.goodsCommandService.removeGoods(goodsIds);
    }

    /**
     * 批量上架商品
     *
     * @param goodsIds 商品ID列表
     * @return 上架结果
     */
    @PutMapping("/shelve/{goodsIds}")
    public Response shelveGoods(@PathVariable List<Long> goodsIds) {
        return this.goodsCommandService.shelveGoods(goodsIds);
    }

    /**
     * 批量下架商品
     *
     * @param goodsIds 商品ID列表
     * @return 下架结果
     */
    @PutMapping("/unshelve/{goodsIds}")
    public Response unshelveGoods(@PathVariable List<Long> goodsIds) {
        return this.goodsCommandService.unshelveGoods(goodsIds);
    }

}
