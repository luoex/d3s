package com.luo.d3s.light.mybatis.ec.domain.goodstag;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.core.infrastructure.convertor.MpPageConvertor;
import com.luo.d3s.light.mybatis.ec.application.dto.query.GoodsTagPageQuery;
import com.luo.d3s.light.mybatis.ec.domain.base.BaseRepository;

import java.util.Collection;
import java.util.Objects;

/**
 * 商品标签仓库
 *
 * @author luohq
 * @date 2023-08-14
 */
public interface GoodsTagRepository extends BaseRepository<GoodsTag> {
    /**
     * 查询指定标签ID集合对应的标签数量
     *
     * @param ids 标签ID集合
     * @return 存在的标签数量
     */
    default Integer countByIdIn(Collection<Long> ids) {
        return this.selectCount(Wrappers.<GoodsTag>lambdaQuery()
                .in(GoodsTag::getId, ids)).intValue();
    }

    /**
     * 根据标签名称查询商品标签
     *
     * @param tagName 标签名称
     * @return 商品标签
     */
    default GoodsTag findByTagName(String tagName) {
        return this.selectOne(Wrappers.<GoodsTag>lambdaQuery()
                .eq(GoodsTag::getTagName, tagName));
    }

    /**
     * 查询商品标签分页列表
     *
     * @param goodsTagPageQuery 查询参数
     * @return 品标签分页列表
     */
    default PageResponse<GoodsTag> findPage(GoodsTagPageQuery goodsTagPageQuery) {
        Page pageResult = this.selectPage(MpPageConvertor.toPage(goodsTagPageQuery),
                Wrappers.<GoodsTag>lambdaQuery()
                        .like(StringUtils.isNotBlank(goodsTagPageQuery.getTagName()), GoodsTag::getTagName, goodsTagPageQuery.getTagName())
                        .ge(Objects.nonNull(goodsTagPageQuery.getCreateTimeStart()), GoodsTag::getCreateTime, goodsTagPageQuery.getCreateTimeStart())
                        .le(Objects.nonNull(goodsTagPageQuery.getCreateTimeEnd()), GoodsTag::getCreateTime, goodsTagPageQuery.getCreateTimeEnd())
        );
        return MpPageConvertor.toPageResponse(pageResult);

    }
}
