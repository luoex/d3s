package com.luo.d3s.light.mybatis.ec.application.event.handler;

import com.luo.d3s.core.application.event.ApplicationEventHandler;
import com.luo.d3s.light.mybatis.ec.application.dto.command.OrderDeliverCommand;
import com.luo.d3s.light.mybatis.ec.application.dto.event.StockDeliveredEvent;
import com.luo.d3s.light.mybatis.ec.application.service.OrderCommandService;
import org.springframework.stereotype.Component;

/**
 * 库存已发货事件处理器
 *
 * @author luohq
 * @date 2022-11-27 19:29
 */
@Component
public class StockDeliveredEventHandler implements ApplicationEventHandler<StockDeliveredEvent> {

    private OrderCommandService orderCommandService;

    public StockDeliveredEventHandler(OrderCommandService orderCommandService) {
        this.orderCommandService = orderCommandService;
    }

    @Override
    public void handle(StockDeliveredEvent domainEvent) {
        this.orderCommandService.deliverOrder(
                new OrderDeliverCommand(
                        domainEvent.getOrderId(),
                        domainEvent.getExpressCode(),
                        domainEvent.getDeliveryTime()));
    }
}
