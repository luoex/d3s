package com.luo.d3s.light.mybatis.ec.infrastructure.database.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.luo.d3s.light.mybatis.ec.domain.category.CategoryRepository;
import com.luo.d3s.light.mybatis.ec.domain.goods.GoodsRepository;
import com.luo.d3s.light.mybatis.ec.domain.goodstag.GoodsTagRepository;
import com.luo.d3s.light.mybatis.ec.domain.order.OrderRepository;
import com.luo.d3s.light.mybatis.ec.infrastructure.database.dao.GoodsTagBindingMapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Mybatis-Plus配置
 *
 * @author luohq
 * @date 2021-11-02
 */
@Configuration
@MapperScan(
        basePackageClasses = {CategoryRepository.class, GoodsRepository.class, GoodsTagRepository.class, OrderRepository.class, GoodsTagBindingMapper.class},
        markerInterface = BaseMapper.class)
public class MybatisPlusConfig {

    /**
     * 新的分页插件,
     * 一缓和二缓遵循mybatis的规则,
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        return interceptor;
    }
}