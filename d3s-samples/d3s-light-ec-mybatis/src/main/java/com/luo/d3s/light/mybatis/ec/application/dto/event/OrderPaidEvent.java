package com.luo.d3s.light.mybatis.ec.application.dto.event;

import com.luo.d3s.core.application.event.ApplicationEvent;
import com.luo.d3s.core.util.IdGenUtils;
import com.luo.d3s.light.mybatis.ec.domain.order.Order;
import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 订单已支付事件
 *
 * @author luohq
 * @date 2022-11-27 18:41
 */
@Getter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class OrderPaidEvent implements ApplicationEvent<String, Long> {

    /**
     * 事件ID
     */
    @EqualsAndHashCode.Include
    private String eventId;
    /**
     * 事件创建时间
     */
    private LocalDateTime createTime;

    /**
     * 订单ID
     */
    private Long orderId;
    /**
     * 订单金额
     */
    private BigDecimal orderPrice;
    /**
     * 订单创建时间
     */
    private LocalDateTime orderCreateTime;
    /**
     * 订单支付时间
     */
    private LocalDateTime orderPayTime;

    public OrderPaidEvent(Long orderId, BigDecimal orderPrice, LocalDateTime orderCreateTime, LocalDateTime orderPayTime) {
        this.orderId = orderId;
        this.orderPrice = orderPrice;
        this.orderCreateTime = orderCreateTime;
        this.orderPayTime = orderPayTime;
        this.eventId = IdGenUtils.nextTextId();
        this.createTime = LocalDateTime.now();
    }

    public OrderPaidEvent(Order order) {
        this(order.getId(), order.getOrderPrice(), order.getCreateTime(), order.getPayTime());
    }

    @Override
    public Long getAggregateRootId() {
        return orderId;
    }
}
