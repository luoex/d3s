package com.luo.d3s.light.mybatis.ec.application.event.handler;

import com.luo.d3s.core.application.event.ApplicationEventHandler;
import com.luo.d3s.light.mybatis.ec.application.dto.event.ExpressProcessModifiedEvent;
import com.luo.d3s.light.mybatis.ec.application.service.OrderCommandService;
import org.springframework.stereotype.Component;

/**
 * 物流进度事件处理器
 *
 * @author luohq
 * @date 2022-11-27 19:29
 */
@Component
public class ExpressProcessModifiedEventHandler implements ApplicationEventHandler<ExpressProcessModifiedEvent> {

    /**
     * 物流进度状态：已完成
     */
    private final String COMPLETE_STATUS = "Complete";

    private OrderCommandService orderCommandService;

    public ExpressProcessModifiedEventHandler(OrderCommandService orderCommandService) {
        this.orderCommandService = orderCommandService;
    }

    @Override
    public void handle(ExpressProcessModifiedEvent domainEvent) {
        if (COMPLETE_STATUS.equals(domainEvent.getProcessStatus())) {
            this.orderCommandService.completeOrder(domainEvent.getOrderId());
        }
    }
}
