package com.luo.d3s.light.mybatis.ec.domain.specification;

import com.luo.d3s.core.domain.service.specifcation.AbstractSpecification;
import com.luo.d3s.core.util.validation.Validates;
import com.luo.d3s.light.mybatis.ec.domain.goodstag.GoodsTagRepository;

import java.util.Collection;
import java.util.Objects;

/**
 * 商品标签是否存在验证规则
 *
 * @author luohq
 * @date 2023-08-02
 */
public class GoodsTagExistSpecification extends AbstractSpecification<Collection<Long>> {

    private GoodsTagRepository goodsTagRepository;

    public GoodsTagExistSpecification(GoodsTagRepository goodsTagRepository) {
        this.goodsTagRepository = goodsTagRepository;
    }

    @Override
    public boolean isSatisfiedBy(Collection<Long> goodsTagIds) {
        //若标签ID集合为空，则直接验证通过
        if (Objects.isNull(goodsTagIds) || goodsTagIds.isEmpty()) {
            return true;
        }
        //查询存在的标签数量
        Integer existGoodsTagCount = this.goodsTagRepository.countByIdIn(goodsTagIds);
        Validates.isTrue(existGoodsTagCount.equals(goodsTagIds.size()), "goods tags don't exist");
        return true;
    }
}
