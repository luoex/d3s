package com.luo.d3s.light.mybatis.ec.domain.base;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.luo.d3s.core.domain.model.light.EntityLight;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * 基础实体接类 - 实现EntityLight
 *
 * @author luohq
 * @date 2023-07-04 9:54
 */
@Getter
public class BaseEntity implements EntityLight<Long> {

    @TableId(type = IdType.NONE)
    @NotNull
    protected Long id;

    /**
     * 创建时间
     */
    protected LocalDateTime createTime;

    /**
     * 更新时间
     */
    protected LocalDateTime updateTime;
}
