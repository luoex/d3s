<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.luo.d3s.light.mybatis.ec.domain.order.OrderRepository">

    <!-- 订单 - 基础列 -->
    <sql id="ORDER_BASE_COLUMNS">
        ${alias}id as ${prefix}id,
        ${alias}order_price as ${prefix}order_price,
        ${alias}receive_address as ${prefix}receive_address,
        ${alias}order_status as ${prefix}order_status,
        ${alias}pay_time as ${prefix}pay_time,
        ${alias}express_code as ${prefix}express_code,
        ${alias}deliver_time as ${prefix}deliver_time,
        ${alias}complete_time as ${prefix}complete_time,
        ${alias}cancel_time as ${prefix}cancel_time,
        ${alias}create_time as ${prefix}create_time,
        ${alias}update_time as ${prefix}update_time
    </sql>
    <!-- 订单商品 - 基础列 -->
    <sql id="ORDER_GOODS_BASE_COLUMNS">
        ${alias}id as ${prefix}id,
        ${alias}order_id as ${prefix}order_id,
        ${alias}goods_id as ${prefix}goods_id,
        ${alias}goods_count as ${prefix}goods_count,
        ${alias}goods_price as ${prefix}goods_price,
        ${alias}goods_sum_price as ${prefix}goods_sum_price,
        ${alias}create_time as ${prefix}create_time,
        ${alias}update_time as ${prefix}update_time
    </sql>

    <!-- 订单结果映射 -->
    <resultMap id="ORDER_BASE_RESULT_MAP" type="com.luo.d3s.light.mybatis.ec.domain.order.Order">
        <id column="id" property="id"/>
        <result column="order_price" property="orderPrice"/>
        <result column="receive_address" property="receiveAddress"/>
        <result column="order_status" property="orderStatus"/>
        <result column="pay_time" property="payTime"/>
        <result column="express_code" property="expressCode"/>
        <result column="deliver_time" property="deliverTime"/>
        <result column="complete_time" property="completeTime"/>
        <result column="cancel_time" property="cancelTime"/>
        <result column="create_time" property="createTime"/>
        <result column="update_time" property="updateTime"/>
    </resultMap>
    <!-- 订单商品结果映射 -->
    <resultMap id="ORDER_GOODS_BASE_RESULT_MAP" type="com.luo.d3s.light.mybatis.ec.domain.order.OrderGoods">
        <id column="id" property="id"/>
        <result column="goods_id" property="goodsId"/>
        <result column="order_id" property="orderId"/>
        <result column="goods_count" property="goodsCount"/>
        <result column="goods_price" property="goodsPrice"/>
        <result column="goods_sum_price" property="goodsSumPrice"/>
        <result column="create_time" property="createTime"/>
        <result column="update_time" property="updateTime"/>
    </resultMap>

    <!-- 订单实体结果映射 -->
    <resultMap id="ORDER_ENTITY_RESULT_MAP" type="com.luo.d3s.light.mybatis.ec.domain.order.Order" extends="ORDER_BASE_RESULT_MAP">
        <collection property="orderGoodsList" ofType="com.luo.d3s.light.mybatis.ec.domain.order.OrderGoods" columnPrefix="og_"
                    resultMap="ORDER_GOODS_BASE_RESULT_MAP">
        </collection>
    </resultMap>

    <!-- 订单VO结果映射 -->
    <resultMap id="ORDER_VO_RESULT_MAP" type="com.luo.d3s.light.mybatis.ec.application.dto.vo.OrderVo"
               extends="ORDER_BASE_RESULT_MAP">
        <!-- 特殊处理原Entity中的枚举类型，此处DTO转换为简单属性类型转换 -->
        <result column="order_status" property="orderStatus"/>
        <collection property="orderGoodsList" ofType="com.luo.d3s.light.mybatis.ec.application.dto.vo.OrderGoodsVo"
                    columnPrefix="og_" resultMap="ORDER_GOODS_BASE_RESULT_MAP">
        </collection>
    </resultMap>

    <!-- 查询订单及订单商品 -->
    <sql id="SELECT_ORDER_WITH_GOODS">
        select
        <include refid="ORDER_BASE_COLUMNS">
            <property name="alias" value="o."/>
            <property name="prefix" value=""/>
        </include>
        ,
        <include refid="ORDER_GOODS_BASE_COLUMNS">
            <property name="alias" value="og."/>
            <property name="prefix" value="og_"/>
        </include>
        from orders o
        left join order_goods og on og.order_id = o.id
    </sql>

    <!-- 查询订单及商品 -->
    <select id="findById" resultMap="ORDER_ENTITY_RESULT_MAP">
        <include refid="SELECT_ORDER_WITH_GOODS"/>
        <where>
            o.id = #{id}
        </where>
    </select>

    <!-- 查询订单及商品VO -->
    <select id="findVoById" resultMap="ORDER_VO_RESULT_MAP">
        <include refid="SELECT_ORDER_WITH_GOODS"/>
        <where>
            o.id = #{id}
        </where>
    </select>

    <!-- 保存订单及订单商品 -->
    <insert id="insertOrderAndGoods" parameterType="com.luo.d3s.light.mybatis.ec.domain.order.Order">
        insert into orders
        (
        id,
        order_price,
        receive_address,
        order_status,
        pay_time,
        express_code,
        deliver_time,
        complete_time,
        cancel_time,
        create_time,
        update_time
        )
        values
        (
        #{id},
        #{orderPrice},
        #{receiveAddress},
        #{orderStatus},
        #{payTime},
        #{expressCode},
        #{deliverTime},
        #{completeTime},
        #{cancelTime},
        #{createTime},
        #{updateTime}
        );
        insert into order_goods
        (
        id,
        order_id,
        goods_id,
        goods_count,
        goods_price,
        goods_sum_price,
        create_time,
        update_time
        )
        values
        <foreach collection="orderGoodsList" item="orderGoods" open="" close=";" separator=",">
            (
            #{orderGoods.id},
            #{orderGoods.orderId},
            #{orderGoods.goodsId},
            #{orderGoods.goodsCount},
            #{orderGoods.goodsPrice},
            #{orderGoods.goodsSumPrice},
            #{orderGoods.createTime},
            #{orderGoods.updateTime}
            )
        </foreach>
    </insert>

    <!-- 删除订单及订单商品 -->
    <delete id="removeById">
        delete from orders where id = #{orderId};
        delete from order_goods where order_id = #{orderId};
    </delete>

    <!-- 批量移除订单及订单商品 -->
    <delete id="removeByIds">
        delete from orders where id in
        <foreach collection="collection" separator="," open="(" close=")" item="orderId">
            #{orderId}
        </foreach>
        ;
        delete from order_goods where order_id in
        <foreach collection="collection" separator="," open="(" close=")" item="orderId">
            #{orderId}
        </foreach>
        ;
    </delete>

    <!-- 查询订单分页列表 -->
    <select id="selectOrderPage" resultMap="ORDER_VO_RESULT_MAP">
        select
        <include refid="ORDER_BASE_COLUMNS">
            <property name="alias" value=""/>
            <property name="prefix" value=""/>
        </include>
        from orders
        <where>
            <if test="orderPageQuery.createTimeStart != null">
                and create_time >= #{orderPageQuery.createTimeStart}
            </if>
            <if test="orderPageQuery.createTimeEnd != null">
                and create_time &lt;= #{orderPageQuery.createTimeEnd}
            </if>
        </where>
    </select>

</mapper>