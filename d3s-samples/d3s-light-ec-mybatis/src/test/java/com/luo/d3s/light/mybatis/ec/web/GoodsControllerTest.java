package com.luo.d3s.light.mybatis.ec.web;

import com.luo.d3s.core.application.dto.PageQuery;
import com.luo.d3s.core.exception.ValidateException;
import com.luo.d3s.ext.test.junit5.params.JsonFileSource;
import com.luo.d3s.ext.test.matcher.MatcherFactory;
import com.luo.d3s.light.mybatis.ec.base.BaseSpringTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * 商品Controller - 单元测试
 *
 * @author luohq
 * @date 2023-01-07 14:50
 */
public class GoodsControllerTest extends BaseSpringTest {

    /**
     * Junit5 Json参数文件路径
     */
    private static final String JSON_SOURCE_PATH = "classpath:jsonSource/goods.json";
    /** 数据常量 */
    private static final String SEARCH_GOODS_ID = "1";
    private static final String REMOVE_GOODS_IDS = "4,5";
    private static final String SHELVE_GOODS_IDS = "6,7";

    @Test
    void findGoodsTest() throws Exception {
        //get pathParam参数
        this.mockMvc.perform(get("/api/v1/goods/{goodsId}", SEARCH_GOODS_ID))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_SUCCESS).value(true));
    }

    @Test
    void findGoodsPageTest() throws Exception {
        //get pathParam参数
        this.mockMvc.perform(get("/api/v1/goods")
                        .queryParam(PARAM_PAGE_INDEX, DEFAULT_PAGE_INDEX)
                        .queryParam(PARAM_PAGE_SIZE, DEFAULT_PAGE_SIZE)
                        .queryParam(PARAM_ORDER_BY, DEFAULT_ORDER_BY)
                        .queryParam(PARAM_ORDER_DIRECTION, PageQuery.DESC)
                        .queryParam("goodsName", "商品1")
                        .queryParam("categoryId", "1")
                        .queryParam("startGoodsPrice", "1")
                        .queryParam("endGoodsPrice", "10000.00")
                        .queryParam("goodsStatus", "10")
                        .queryParam("beforeExpirationDate", "2030-01-01")
                        .queryParam("createTimeStart", "2023-01-01 00:00:00")
                        .queryParam("createTimeEnd", "2030-01-01 12:33:10")
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_SUCCESS).value(true))
                .andExpect(jsonPath(JSON_PATH_TOTAL_COUNT).value(MatcherFactory.greaterThan(0, "totalCount should be gt 0")));
    }

    @ParameterizedTest
    @JsonFileSource(jsonKey = "$.goodsCreateCommand", resource = JSON_SOURCE_PATH)
    void createGoodsTest(String createGoodsCommandJson) throws Exception {
        this.mockMvc.perform(post("/api/v1/goods")
                        //application/json请求体
                        .contentType(MediaType.APPLICATION_JSON)
                        //具体请求Json参数
                        .content(createGoodsCommandJson))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_SUCCESS).value(true))
                .andExpect(jsonPath(JSON_PATH_DATA + ".id").isNotEmpty());
    }


    @ParameterizedTest
    @JsonFileSource(jsonKey = "$.goodsCreateCommandFailure", resource = JSON_SOURCE_PATH)
    void createGoodsFailureTest(String createGoodsCommandJson) throws Exception {
        this.mockMvc.perform(post("/api/v1/goods")
                        //application/json请求体
                        .contentType(MediaType.APPLICATION_JSON)
                        //具体请求Json参数
                        .content(createGoodsCommandJson))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_SUCCESS).value(false))
                .andExpect(jsonPath(JSON_PATH_ERR_CODE).value(ValidateException.DEFAULT_ERR_CODE))
                .andExpect(jsonPath(JSON_PATH_ERR_MESSAGE).isNotEmpty());
    }

    @ParameterizedTest
    @JsonFileSource(jsonKey = "$.goodsModifyCommand", resource = JSON_SOURCE_PATH)
    void modifyGoodsTest(String modifyGoodsCommandJson) throws Exception {
        this.mockMvc.perform(put("/api/v1/goods")
                        //application/json请求体
                        .contentType(MediaType.APPLICATION_JSON)
                        //具体请求Json参数
                        .content(modifyGoodsCommandJson))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_SUCCESS).value(true));
    }

    @ParameterizedTest
    @JsonFileSource(jsonKey = "$.goodsModifyCommandFailure", resource = JSON_SOURCE_PATH)
    void modifyGoodsFailureTest(String modifyGoodsCommandJson) throws Exception {
        this.mockMvc.perform(put("/api/v1/goods")
                        //application/json请求体
                        .contentType(MediaType.APPLICATION_JSON)
                        //具体请求Json参数
                        .content(modifyGoodsCommandJson))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_SUCCESS).value(false))
                .andExpect(jsonPath(JSON_PATH_ERR_CODE).value(ValidateException.DEFAULT_ERR_CODE))
                .andExpect(jsonPath(JSON_PATH_ERR_MESSAGE).isNotEmpty());
    }

    @Test
    void removeGoodsTest() throws Exception {
        this.mockMvc.perform(delete("/api/v1/goods/{goodsIds}", REMOVE_GOODS_IDS)
                        //application/json请求体
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_SUCCESS).value(true));
    }

    @ParameterizedTest
    @ValueSource(strings = {"1", "1,11",  "1,2"})
    void removeGoodsFailureTest(String goodsIds) throws Exception {
        this.mockMvc.perform(delete("/api/v1/goods/{goodsIds}", goodsIds)
                        //application/json请求体
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_SUCCESS).value(false))
                .andExpect(jsonPath(JSON_PATH_ERR_CODE).value(ValidateException.DEFAULT_ERR_CODE))
                .andExpect(jsonPath(JSON_PATH_ERR_MESSAGE).isNotEmpty());
    }

    @Test
    void shelveGoodsTest() throws Exception {
        this.mockMvc.perform(put("/api/v1/goods/shelve/{goodsIds}", SHELVE_GOODS_IDS)
                        //application/json请求体
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_SUCCESS).value(true));
    }

    @Test
    void unshelveGoodsTest() throws Exception {
        this.mockMvc.perform(put("/api/v1/goods/unshelve/{goodsIds}", SHELVE_GOODS_IDS)
                        //application/json请求体
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_SUCCESS).value(true));
    }
}
