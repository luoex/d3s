package com.luo.d3s.light.mybatis.ec.web;

import com.luo.d3s.core.application.dto.PageQuery;
import com.luo.d3s.core.exception.ValidateException;
import com.luo.d3s.ext.test.junit5.params.JsonFileSource;
import com.luo.d3s.ext.test.matcher.MatcherFactory;
import com.luo.d3s.light.mybatis.ec.base.BaseSpringTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * 订单Controller - 单元测试
 *
 * @author luohq
 * @date 2023-07-03
 */
public class OrderControllerTest extends BaseSpringTest {

    /**
     * Junit5 Json参数文件路径
     */
    private static final String JSON_SOURCE_PATH = "classpath:jsonSource/order.json";
    /** 数据常量 */
    private static final String SEARCH_ORDER_ID = "1";
    private static final String PAY_ORDER_ID = "2";

    @Test
    void findOrderTest() throws Exception {
        //get pathParam参数
        this.mockMvc.perform(get("/api/v1/orders/{orderId}", SEARCH_ORDER_ID))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_SUCCESS).value(true));
    }

    @Test
    void findOrderPageTest() throws Exception {
        this.mockMvc.perform(get("/api/v1/orders")
                        .queryParam(PARAM_PAGE_INDEX, DEFAULT_PAGE_INDEX)
                        .queryParam(PARAM_PAGE_SIZE, DEFAULT_PAGE_SIZE)
                        .queryParam(PARAM_ORDER_BY, DEFAULT_ORDER_BY)
                        .queryParam(PARAM_ORDER_DIRECTION, PageQuery.DESC)
                        .queryParam("createTimeStart", "2023-01-01 00:00:00")
                        .queryParam("createTimeEnd", "2030-01-01 12:33:10")
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_SUCCESS).value(true))
                .andExpect(jsonPath(JSON_PATH_TOTAL_COUNT).value(MatcherFactory.greaterThan(0, "totalCount should be gt 0")));
    }

    @ParameterizedTest
    @JsonFileSource(jsonKey = "$.orderCreateCommand", resource = JSON_SOURCE_PATH)
    void createOrderTest(String createOrderCommandJson) throws Exception {
        this.mockMvc.perform(post("/api/v1/orders")
                        //application/json请求体
                        .contentType(MediaType.APPLICATION_JSON)
                        //具体请求Json参数
                        .content(createOrderCommandJson))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_SUCCESS).value(true))
                .andExpect(jsonPath(JSON_PATH_DATA).isNotEmpty());
    }

    @Test
    void payOrderTest() throws Exception {
        this.mockMvc.perform(post("/api/v1/orders/pay/{orderId}", PAY_ORDER_ID))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_SUCCESS).value(true));
    }

    @Test
    void payOrderFailureTest() throws Exception {
        this.mockMvc.perform(post("/api/v1/orders/pay/{orderId}", SEARCH_ORDER_ID))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_SUCCESS).value(false))
                .andExpect(jsonPath(JSON_PATH_ERR_CODE).value(ValidateException.DEFAULT_ERR_CODE));
    }

}
