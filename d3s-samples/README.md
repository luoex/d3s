# D3S电商示例 
本案例源自TSD方法学电商案例，前期采用事件风暴建模，  
示例代码选取电商示例中的核心限界上下文`电子商务`进行实现。

## 一、示例模块说明
| 模块                                             | 说明                                                                                                                                                                                                                                     |
|:-----------------------------------------------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [anti-ec-mvc](./anti-ec-mvc)                   | DDD反模式示例 <br/><ul><li>使用SpringMvc常见的开发模型</li><li>贫血模型</li><li>事务脚本</li></ul>                                                                                                                                                                                                                 |
| [d3s-complete-ec](./d3s-complete-ec)           | D3S 完全遵循DDD的电商示例<br/><ul><li>严格4层架构（maven多模块隔离）</li><li>严格Entity、ValueObject模型</li><li>Always-Valid Domain Model</li><li>DomainEvent</li><li>ACL</li><li>CQRS</li><li>Mybatis-plus仓库实现</li></ul>                                                                                             |
| [d3s-light-ec-mybatis](./d3s-light-ec-mybatis) | D3S 借鉴DDD但又不完全受限于DDD的轻量化电商示例（妥协后的4层架构/Mybatis-Plus），<br/>旨在提供能快速上手且有效降低代码量的DDD实现。<br/><ul><li>宽松4层架构</li><li>Entity与DataObject融合、简化值对象为简单属性（且原属于值对象的领域行为可合并到实体、领域服务中）</li><li>Mybatis-plus仓库实现</li><li>Always-Valid Domain Model</li><li>ApplicationEvent</li><li>ACL</li><li>CQRS</li></ul> |
| [d3s-light-ec-jpa](./d3s-light-ec-jpa)         | D3S 借鉴DDD但又不完全受限于DDD的轻量化电商示例（妥协后的4层架构/Spring Data JPA），<br/>旨在提供一种能充分发挥DDD模型的优势、又能相对减少一定低代码量的DDD实现。<br/><ul><li>宽松4层架构</li><li>保留领域模型完整性（实体、值对象、对象间关联、领域事件等）</li><li>使用Spring Data JPA完成ORM仓库实现</li><li>Always-Valid Domain Model</li><li>DomainEvent</li><li>ACL</li><li>CQRS</li></ul>   |
| [d3s-share](./d3s-share)                       | 共享内核示例                                                                                                                                                                                                                                 |
| [ddd-demo.sql](./ddd-demo.sql)                 | 电商示例Mysql数据库定义 |

## 二、示例DDD建模说明
### 1） DDD事件风暴建模
![event-storm](../doc/imgs/event-storm.png)  

### 2）DDD战略设计
#### 2.1） 限界上下文
![bounded-context](../doc/imgs/bounded-context.png)  

#### 2.2） 六边形架构
![ec-hex](../doc/imgs/ec-hex.png)  
![eda-hex](../doc/imgs/eda-hex.png)  
![shipping-hex](../doc/imgs/shipping-hex.png)  

### 3）DDD战术设计
#### 3.1） DDD分层架构设计
![d3s-layer](../doc/imgs/d3s-layer.jpg)  

#### 3.2） 类图设计
![ec-class](../doc/imgs/ec-class.jpg)  