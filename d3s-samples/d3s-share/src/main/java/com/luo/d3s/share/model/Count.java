package com.luo.d3s.share.model;

import com.luo.d3s.core.domain.model.ValueObject;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.Value;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

/**
 * 数量
 *
 * @author luohq
 * @date 2022-11-27 18:00
 */
@Value
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class Count implements ValueObject {

    /**
     * 数量值
     */
    @NotNull
    @Positive
    private Integer value;

    Count(Integer value) {
        this.value = value;
        this.validateSelf();
    }

    public static Count of(Integer count) {
        return new Count(count);
    }
}
