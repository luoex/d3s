package com.luo.d3s.share.model;

import com.luo.d3s.core.domain.model.Identity;
import com.luo.d3s.core.util.validation.Validates;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.Value;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 业务ID
 *
 * @author luohq
 * @date 2022-11-27 16:19
 */
@Value
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class BizId implements Identity {

    /**
     * ID值
     */
    @NotNull
    @Positive
    private Long value;

    BizId(Long value) {
        this.value = value;
        this.validateSelf();
    }

    /**
     * 批量转换实体ID为具体值列表
     *
     * @param bizIds 实体ID列表
     * @return 具体值列表
     */
    public static List<Long> toValues(Collection<BizId> bizIds) {
        Validates.notEmpty(bizIds, "BizId list is empty");
        return bizIds.stream()
                .map(BizId::getValue)
                .collect(Collectors.toList());
    }

    /**
     * 根据ID值转换BizId
     *
     * @param id id长整形值
     * @return 业务ID
     */
    public static BizId of(Long id) {
        return new BizId(id);
    }

    /**
     * 批量转换具体值为实体ID列表
     *
     * @param values 具体值列表
     * @return 实体ID列表
     */
    public static List<BizId> of(Collection<Long> values) {
        Validates.notEmpty(values, "BizId value list is empty");
        return values.stream()
                .map(BizId::new)
                .collect(Collectors.toList());
    }

    /**
     * 批量转换具体值为实体ID列表（若values为空则返回空列表）
     *
     * @param values 具体值列表
     * @return 实体ID列表
     */
    public static Set<BizId> ofNullable(Collection<Long> values) {
        if (Objects.isNull(values) || values.isEmpty()) {
            return new HashSet<>();
        }
        return values.stream()
                .map(BizId::new)
                .collect(Collectors.toSet());
    }

    @Override
    public String toString() {
        return String.valueOf(this.value);
    }
}
