package com.luo.d3s.share.model;

import com.luo.d3s.core.domain.model.ValueObject;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.Value;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

/**
 * 订单金额
 *
 * @author luohq
 * @date 2022-11-27 15:56
 */
@Value
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class Price implements ValueObject {

    /**
     * 价格值
     */
    @NotNull
    @Positive
    private BigDecimal value;

    Price(BigDecimal value) {
        this.value = value;
        this.validateSelf();
    }

    public static Price of(BigDecimal price) {
        return new Price(price);
    }
}
