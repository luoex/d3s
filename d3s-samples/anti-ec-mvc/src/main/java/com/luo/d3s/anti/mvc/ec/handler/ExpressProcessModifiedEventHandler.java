package com.luo.d3s.anti.mvc.ec.handler;

import com.luo.d3s.anti.mvc.ec.model.event.ExpressProcessModifiedEvent;
import com.luo.d3s.anti.mvc.ec.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 物流进度事件处理器
 *
 * @author luohq
 * @date 2022-11-27 19:29
 */
@Component
@Slf4j
public class ExpressProcessModifiedEventHandler {

    /**
     * 物流进度状态：已完成
     */
    private final String COMPLETE_STATUS = "Complete";

    @Resource
    private OrderService orderService;


    /**
     * 物流进度事件监听器
     *
     * @param expressProcessModifiedEvent 物流进度事件
     */
    @RabbitListener(queues = {"${spring.rabbitmq.events.express-process.queue.name}"})
    public void handle(ExpressProcessModifiedEvent expressProcessModifiedEvent) {
        log.info("[RabbitMq RECV] Express Process: {}", expressProcessModifiedEvent);
        if (COMPLETE_STATUS.equals(expressProcessModifiedEvent.getProcessStatus())) {
            this.orderService.completeOrder(expressProcessModifiedEvent.getOrderId());
        }
    }
}
