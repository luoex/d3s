package com.luo.d3s.anti.mvc.ec.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

/**
 * 订单商品数据对象
 *
 * @author luohq
 * @date 2022-12-21 9:42
 */
@Data
@ToString(callSuper = true)
@TableName("order_goods")
public class OrderGoods extends BaseEntity {

    private Long goodsId;

    private Long orderId;

    private Integer goodsCount;

    private BigDecimal goodsPrice;

    private BigDecimal goodsSumPrice;
}
