package com.luo.d3s.anti.mvc.ec.service;

import com.luo.d3s.anti.mvc.ec.model.dto.CategoryCreateDto;
import com.luo.d3s.anti.mvc.ec.model.dto.CategoryModifyDto;
import com.luo.d3s.anti.mvc.ec.model.dto.CategoryPageQueryDto;
import com.luo.d3s.anti.mvc.ec.model.vo.CategoryTreeVo;
import com.luo.d3s.anti.mvc.ec.model.vo.CategoryVo;
import com.luo.d3s.core.application.dto.MultiResponse;
import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.core.application.dto.Response;
import com.luo.d3s.core.application.dto.SingleResponse;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;

/**
 * 分类服务接口
 *
 * @author luohq
 * @date 2023-01-04 17:00
 */
public interface CategoryService {
    /**
     * 分类缓存名称
     */
    String CATEGORY_TREE_CACHE_NAME = "category-tree";

    /**
     * 查询分类详情
     *
     * @param categoryId 分类ID
     * @return 分类详情
     */
    SingleResponse<CategoryVo> findCategory(Long categoryId);

    /**
     * 查询分类分页列表
     *
     * @param categoryPageQueryDto 分类分页查询参数
     * @return 分页查询结果
     */
    PageResponse<CategoryVo> findCategoryPage(CategoryPageQueryDto categoryPageQueryDto);

    /**
     * 查询分类树形列表
     *
     * @param categoryId 分类ID
     * @return 分类树形列表
     */
    MultiResponse<CategoryTreeVo> findCategoryTree(Long categoryId);

    /**
     * 创建分类
     *
     * @param categoryCreateDto 创建分类命令
     * @return 响应结果（分类信息）
     */
    SingleResponse<CategoryVo> createCategory(CategoryCreateDto categoryCreateDto);

    /**
     * 修改分类
     *
     * @param categoryModifyDto 修改分类命令
     * @return 响应结果
     */
    Response modifyCategory(@Validated CategoryModifyDto categoryModifyDto);

    /**
     * 移除分类
     *
     * @param categoryId 分类ID
     * @return 响应结果
     */
    Response removeCategory(@NotNull Long categoryId);
}
