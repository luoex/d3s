package com.luo.d3s.anti.mvc.ec.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 商品标签
 *
 * @author luohq
 * @date 2023-08-02
 */
@Data
@TableName("goods_tag")
public class GoodsTag extends BaseEntity {
    /**
     * 标签名称
     */
    private String tagName;

    /**
     * 标签描述
     */
    private String tagDesc;
}
