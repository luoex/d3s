package com.luo.d3s.anti.mvc.ec.controller;

import com.luo.d3s.anti.mvc.ec.model.dto.OrderCreateDto;
import com.luo.d3s.anti.mvc.ec.model.dto.OrderPageQueryDto;
import com.luo.d3s.anti.mvc.ec.model.vo.OrderVo;
import com.luo.d3s.anti.mvc.ec.service.OrderService;
import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.core.application.dto.Response;
import com.luo.d3s.core.application.dto.SingleResponse;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

/**
 * 订单Controller
 *
 * @author luohq
 * @date 2022-12-22 16:12
 */
@Validated
@RestController
@RequestMapping("/api/v1/orders")
public class OrderController {

    @Resource
    private OrderService orderService;

    /**
     * 根据订单ID查询订单信息
     *
     * @param orderId 订单ID
     * @return 订单信息
     */
    @GetMapping(value = "/{orderId}")
    public SingleResponse<OrderVo> findOrder(@PathVariable @NotNull Long orderId) {
        return this.orderService.findOrder(orderId);
    }

    /**
     * 查询订单分页列表
     *
     * @param orderPageQueryDto 订单分页查询参数
     * @return 订单分页查询结果
     */
    @GetMapping
    public PageResponse<OrderVo> findOrderPage(OrderPageQueryDto orderPageQueryDto) {
        return this.orderService.findOrderPage(orderPageQueryDto);
    }

    /**
     * 创建订单
     *
     * @param orderCreateDto 订单创建命令
     * @return 创建结果
     */
    @PostMapping
    public SingleResponse<Long> createOrder(@RequestBody @Validated OrderCreateDto orderCreateDto) {
        return this.orderService.createOrder(orderCreateDto);
    }

    /**
     * 支付订单
     *
     * @param orderId 订单ID
     * @return 支付结果
     */
    @PostMapping("/pay/{orderId}")
    public Response payOrder(@PathVariable @NotNull Long orderId) {
        return this.orderService.payOrder(orderId);
    }

}
