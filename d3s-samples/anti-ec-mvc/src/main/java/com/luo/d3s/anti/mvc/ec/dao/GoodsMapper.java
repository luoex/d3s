package com.luo.d3s.anti.mvc.ec.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.luo.d3s.anti.mvc.ec.model.dto.GoodsPageQueryDto;
import com.luo.d3s.anti.mvc.ec.entity.Goods;
import com.luo.d3s.anti.mvc.ec.enums.GoodsStatus;
import com.luo.d3s.anti.mvc.ec.model.vo.GoodsVo;
import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.core.infrastructure.convertor.MpPageConvertor;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.function.Function;

/**
 * 商品信息 Mapper 接口
 *
 * @author luohq
 * @date 2022-12-21
 */
public interface GoodsMapper extends BaseMapper<Goods> {

    /**
     * 分类下是否存在商品
     *
     * @param categoryIds 分类ID列表
     * @return 是否存在
     */
    default Boolean existGoodsInCategories(List<Long> categoryIds) {
        //验证分类ID集合是否存在商品
        return this.exists(Wrappers.<Goods>lambdaQuery()
                .in(Goods::getCategoryId, categoryIds)
        );
    }

    /**
     * 是否存在上架状态的商品
     *
     * @param goodsIds 商品ID列表
     * @return 是否存在
     */
    default Boolean existGoodsShelved(List<Long> goodsIds) {
        //验证是否存在上架状态的商品
        return this.exists(Wrappers.<Goods>lambdaQuery()
                .in(Goods::getId, goodsIds)
                .eq(Goods::getGoodsStatus, GoodsStatus.SHELVED)
        );
    }

    /**
     * 批量更新商品状态
     *
     * @param goodsIds    商品ID列表
     * @param goodsStatus 商品状态
     */
    default void batchModifyGoodsStatus(List<Long> goodsIds, GoodsStatus goodsStatus) {
        this.update(null, Wrappers.<Goods>lambdaUpdate()
                .set(Goods::getGoodsStatus, goodsStatus)
                .in(Goods::getId,goodsIds)
        );
    }

    /**
     * 根据ID查询商品详情
     *
     * @param id 商品ID
     * @return 商品详情
     */
    GoodsVo findGoodsWithCNameById(@Param("id") Long id);

    /**
     * 查询商品分页列表
     *
     * @param goodsPageQueryDto 具体查询参数
     * @return 分页查询结果
     */
    default PageResponse<GoodsVo> findGoodsWithCNamePage(GoodsPageQueryDto goodsPageQueryDto) {
        IPage<GoodsVo> goodsPage = this.findGoodsWithCNamePage(MpPageConvertor.toPage(goodsPageQueryDto), goodsPageQueryDto);
        return MpPageConvertor.toPageResponse(goodsPage, Function.identity());
    }

    /**
     * 查询商品分页列表
     *
     * @param page           mp分页参数
     * @param goodsPageQueryDto 具体查询参数
     * @return 分页查询结果
     */
    IPage<GoodsVo> findGoodsWithCNamePage(IPage<GoodsVo> page, @Param("goodsPageQueryDto") GoodsPageQueryDto goodsPageQueryDto);
}


