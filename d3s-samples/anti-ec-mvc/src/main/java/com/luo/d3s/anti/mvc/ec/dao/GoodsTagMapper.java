package com.luo.d3s.anti.mvc.ec.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.luo.d3s.anti.mvc.ec.model.dto.GoodsTagPageQueryDto;
import com.luo.d3s.anti.mvc.ec.entity.GoodsTag;
import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.core.infrastructure.convertor.MpPageConvertor;

import java.util.Collection;
import java.util.Objects;

/**
 * 商品标签仓库
 *
 * @author luohq
 * @date 2023-08-14
 */
public interface GoodsTagMapper extends BaseMapper<GoodsTag> {
    /**
     * 查询指定标签ID集合对应的标签数量
     *
     * @param ids 标签ID集合
     * @return 存在的标签数量
     */
    default Integer countByIdIn(Collection<Long> ids) {
        return this.selectCount(Wrappers.<GoodsTag>lambdaQuery()
                .in(GoodsTag::getId, ids)).intValue();
    }

    /**
     * 根据标签名称查询商品标签
     *
     * @param tagName 标签名称
     * @return 商品标签
     */
    default GoodsTag findByTagName(String tagName) {
        return this.selectOne(Wrappers.<GoodsTag>lambdaQuery()
                .eq(GoodsTag::getTagName, tagName));
    }

    /**
     * 查询商品标签分页列表
     *
     * @param goodsTagPageQueryDto 查询参数
     * @return 品标签分页列表
     */
    default PageResponse<GoodsTag> findPage(GoodsTagPageQueryDto goodsTagPageQueryDto) {
        Page pageResult = this.selectPage(MpPageConvertor.toPage(goodsTagPageQueryDto),
                Wrappers.<GoodsTag>lambdaQuery()
                        .like(StringUtils.isNotBlank(goodsTagPageQueryDto.getTagName()), GoodsTag::getTagName, goodsTagPageQueryDto.getTagName())
                        .ge(Objects.nonNull(goodsTagPageQueryDto.getCreateTimeStart()), GoodsTag::getCreateTime, goodsTagPageQueryDto.getCreateTimeStart())
                        .le(Objects.nonNull(goodsTagPageQueryDto.getCreateTimeEnd()), GoodsTag::getCreateTime, goodsTagPageQueryDto.getCreateTimeEnd())
        );
        return MpPageConvertor.toPageResponse(pageResult);

    }
}
