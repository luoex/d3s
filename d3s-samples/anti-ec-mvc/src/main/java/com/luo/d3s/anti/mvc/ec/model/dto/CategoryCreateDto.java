package com.luo.d3s.anti.mvc.ec.model.dto;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * 创建分类Command
 *
 * @author luohq
 * @date 2023-01-04 15:29
 */
@Data
public class CategoryCreateDto {
    /**
     * 上级分类ID
     */
    private Long parentCategoryId;
    /**
     * 分类名称
     */
    @NotBlank
    @Length(min = 1, max = 64)
    private String categoryName;
    /**
     * 分类描述
     */
    @Length(min = 1, max = 512)
    private String categoryDesc;
}
