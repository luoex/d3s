package com.luo.d3s.anti.mvc.ec.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.luo.d3s.anti.mvc.ec.model.dto.CategoryPageQueryDto;
import com.luo.d3s.anti.mvc.ec.entity.Category;
import com.luo.d3s.anti.mvc.ec.model.vo.CategoryTreeVo;
import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.core.infrastructure.convertor.MpPageConvertor;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Objects;
import java.util.function.Function;

/**
 * 分类信息 Mapper 接口
 *
 * @author luohq
 * @date 2022-12-21
 */
public interface CategoryMapper extends BaseMapper<Category> {

    /**
     * 是否已存在分类名称（若excludeCategoryId非空，则排除excludeCategoryId所对应的分类后再做比较）
     *
     * @param categoryName      分类名称
     * @param excludeCategoryId 不包含的分类ID
     * @return 是否存在
     */
    default Boolean existCategoryName(String categoryName, Long excludeCategoryId) {
        //验证分类名称是否存在
        return this.exists(Wrappers.<Category>lambdaQuery()
                .eq(Category::getCategoryName, categoryName)
                .ne(Objects.nonNull(excludeCategoryId), Category::getId, excludeCategoryId)
        );
    }

    /**
     * 是否存在分类ID
     *
     * @param categoryId 分类ID
     * @return 是否存在
     */
    default Boolean existCategoryId(Long categoryId) {
        //验证分类ID是否存在
        return this.exists(Wrappers.<Category>lambdaQuery()
                .eq(Category::getId, categoryId)
        );
    }

    /**
     * 根据上级分类ID查询子分类列表
     *
     * @param parentCategoryId 上级分类ID
     * @return 根据上级分类ID查询子分类列表
     */
    default List<Category> findByParentId(Long parentCategoryId) {
        //根据上级分类ID查询子分类列表
        return this.selectList(Wrappers.<Category>lambdaQuery()
                .eq(Category::getParentCategoryId, parentCategoryId));

    }

    /**
     * find page
     *
     * @param categoryPageQueryDto page query param
     * @return page response
     */
    default PageResponse<Category> findPage(CategoryPageQueryDto categoryPageQueryDto) {
        Page<Category> pageResult = this.selectPage(
                MpPageConvertor.toPage(categoryPageQueryDto),
                Wrappers.<Category>lambdaQuery()
                        .ge(Objects.nonNull(categoryPageQueryDto.getCreateTimeStart()), Category::getCreateTime, categoryPageQueryDto.getCreateTimeStart())
                        .le(Objects.nonNull(categoryPageQueryDto.getCreateTimeEnd()), Category::getCreateTime, categoryPageQueryDto.getCreateTimeEnd()));
        return MpPageConvertor.toPageResponse(pageResult, Function.identity());
    }

    /**
     * 根据分类ID查询分类TreeVo列表，若分类ID为空则查询根分类TreeVo列表
     *
     * @param categoryId 分类ID
     * @return 分类TreeVo列表
     */
    List<CategoryTreeVo> findRootCategoryTreeVoList(@Param("categoryId") Long categoryId);

    /**
     * 根据上级分类ID查询子分类TreeVo列表
     *
     * @param parentCategoryId 上级分类ID
     * @return 子分类TreeVo列表
     */
    List<CategoryTreeVo> findSubCategoryTreeVoList(@Param("parentCategoryId") Long parentCategoryId);


}
