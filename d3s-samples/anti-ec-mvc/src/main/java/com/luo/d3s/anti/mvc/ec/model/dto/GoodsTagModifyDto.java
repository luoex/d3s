package com.luo.d3s.anti.mvc.ec.model.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 修改商品标签Command
 *
 * @author luohq
 * @date 2023-08-03
 */
@Data
public class GoodsTagModifyDto {
    /**
     * 主键ID
     */
    @NotNull
    private Long id;

    /**
     * 商品标签名称
     */
    @NotBlank
    @Size(max = 64)
    private String tagName;

    /**
     * 商品标签描述
     */
    @Size(max = 512)
    private String tagDesc;
}
