package com.luo.d3s.anti.mvc.ec.model.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 订单商品VO
 *
 * @author luohq
 * @date 2022-11-27 19:05
 */
@Data
public class OrderGoodsVo {

    /**
     * 商品ID
     */
    @NotNull
    private Long goodsId;

    /**
     * 商品数量
     */
    @NotNull
    private Integer goodsCount;

    /**
     * 商品单价
     */
    @NotNull
    private BigDecimal goodsPrice;

    /**
     * 商品总价
     */
    @NotNull
    private BigDecimal goodsSumPrice;
}
