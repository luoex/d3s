package com.luo.d3s.anti.mvc.ec.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.luo.d3s.anti.mvc.ec.entity.OrderGoods;

/**
 * 订单商品 Mapper 接口
 *
 * @author luohq
 * @date 2022-12-21
 */
public interface OrderGoodsMapper extends BaseMapper<OrderGoods> {

}
