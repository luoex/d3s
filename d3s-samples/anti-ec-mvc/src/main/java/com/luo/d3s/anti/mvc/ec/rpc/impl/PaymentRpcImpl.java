package com.luo.d3s.anti.mvc.ec.rpc.impl;

import com.luo.d3s.anti.mvc.ec.entity.Order;
import com.luo.d3s.anti.mvc.ec.rpc.PaymentRpc;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 支付RPC实现类
 *
 * @author luohq
 * @date 2023-11-08 15:54
 */
@Component
@Slf4j
public class PaymentRpcImpl implements PaymentRpc {
    @Override
    public void payOrder(Order order) {
        log.info("[MOCK RPC] pay order, order:{}", order);
    }

    @Override
    public void refundOrder(Order order) {
        log.info("[MOCK RPC] refund order, order:{}", order);
    }
}
