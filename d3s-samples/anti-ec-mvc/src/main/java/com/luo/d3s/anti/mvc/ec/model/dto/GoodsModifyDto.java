package com.luo.d3s.anti.mvc.ec.model.dto;

import com.luo.d3s.anti.mvc.ec.enums.WeightUnit;
import com.luo.d3s.core.util.validation.constraints.EnumValueRange;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

/**
 * 修改商品Command
 *
 * @author luohq
 * @date 2023-01-04 15:29
 */
@Data
public class GoodsModifyDto {

    /**
     * 主键ID
     */
    @NotNull
    private Long id;

    /**
     * 所属分类ID
     */
    @NotNull
    @Positive
    private Long categoryId;

    /**
     * 商品名称
     */
    @NotBlank
    @Length(min = 1, max =  120)
    private String goodsName;

    /**
     * 生产日期
     */
    @NotNull
    @PastOrPresent
    private LocalDate manufactureDate;

    /**
     * 过期日期
     */
    @NotNull
    private LocalDate expirationDate;

    /**
     * 商品重量
     */
    @NotNull
    @PositiveOrZero
    private BigDecimal goodsWeight;

    /**
     * 商品重量单位
     */
    @NotNull
    @EnumValueRange(WeightUnit.class)
    private String goodsWeightUnit;

    /**
     * 商品介绍
     */
    @NotBlank
    @Length(min = 1, max = 1024)
    private String goodsDesc;

    /**
     * 商品价格
     */
    private BigDecimal goodsPrice;

    /**
     * 商品标签ID集合
     */
    private Set<Long> tagIds;
}
