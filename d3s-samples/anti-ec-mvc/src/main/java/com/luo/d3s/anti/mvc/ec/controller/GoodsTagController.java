package com.luo.d3s.anti.mvc.ec.controller;

import com.luo.d3s.anti.mvc.ec.model.dto.GoodsTagCreateDto;
import com.luo.d3s.anti.mvc.ec.model.dto.GoodsTagModifyDto;
import com.luo.d3s.anti.mvc.ec.model.dto.GoodsTagPageQueryDto;
import com.luo.d3s.anti.mvc.ec.model.vo.GoodsTagVo;
import com.luo.d3s.anti.mvc.ec.service.GoodsTagService;
import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.core.application.dto.Response;
import com.luo.d3s.core.application.dto.SingleResponse;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

/**
 * 商品标签Controller
 *
 * @author luohq
 * @date 2023-08-03
 */
@Validated
@RestController
@RequestMapping("/api/v1/goods_tags")
public class GoodsTagController {

    @Resource
    private GoodsTagService goodsTagService;

    /**
     * 根据商品标签ID查询商品标签信息
     *
     * @param goodsTagId 商品标签ID
     * @return 商品标签信息
     */
    @GetMapping("/{goodsTagId}")
    public SingleResponse<GoodsTagVo> findGoodsTag(@PathVariable @NotNull Long goodsTagId) {
        return this.goodsTagService.findGoodsTag(goodsTagId);
    }

    /**
     * 查询商品标签分页列表
     *
     * @param goodsTagPageQueryDto 商品标签分页查询参数
     * @return 商品标签分页查询结果
     */
    @GetMapping
    public PageResponse<GoodsTagVo> findGoodsTagPage(GoodsTagPageQueryDto goodsTagPageQueryDto) {
        return this.goodsTagService.findGoodsTagPage(goodsTagPageQueryDto);
    }

    /**
     * 创建商品标签
     *
     * @param goodsTagCreateDto 商品标签创建命令
     * @return 创建结果
     */
    @PostMapping
    public SingleResponse<GoodsTagVo> createGoodsTag(@RequestBody @Validated GoodsTagCreateDto goodsTagCreateDto) {
        return this.goodsTagService.createGoodsTag(goodsTagCreateDto);
    }

    /**
     * 修改商品标签
     *
     * @param goodsTagModifyDto 商品标签修改命令
     * @return 修改结果
     */
    @PutMapping
    public Response modifyGoodsTag(@RequestBody @Validated GoodsTagModifyDto goodsTagModifyDto) {
        return this.goodsTagService.modifyGoodsTag(goodsTagModifyDto);
    }

    /**
     * 删除商品标签
     *
     * @param goodsTagId 商品标签ID
     * @return 删除结果
     */
    @DeleteMapping("/{goodsTagId}")
    public Response removeGoodsTag(@PathVariable @NotNull Long goodsTagId) {
        return this.goodsTagService.removeGoodsTag(goodsTagId);
    }

}
