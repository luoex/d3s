package com.luo.d3s.anti.mvc.ec.model.event;

import com.luo.d3s.core.application.event.ApplicationEvent;
import com.luo.d3s.core.util.IdGenUtils;
import lombok.*;

import java.time.LocalDateTime;

/**
 * 物流进度更新事件
 *
 * @author luohq
 * @date 2022-11-27 18:54
 */
@Getter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ExpressProcessModifiedEvent implements ApplicationEvent<String, Long> {
    /**
     * 事件ID
     */
    @EqualsAndHashCode.Include
    private String eventId;
    /**
     * 事件创建时间
     */
    private LocalDateTime createTime;

    /**
     * 快递记录ID
     */
    private Long expressProcessId;

    /**
     * 配送ID
     */
    private Long expressDeliveryId;

    /**
     * 订单ID
     */
    private Long orderId;

    /**
     * 更新时间
     */
    private LocalDateTime modifyTime;

    /**
     * 物流进度描述
     */
    private String processDesc;

    /**
     * 物流进度状态
     */
    private String processStatus;

    public ExpressProcessModifiedEvent(Long expressProcessId, Long expressDeliveryId, Long orderId, LocalDateTime modifyTime, String processDesc, String processStatus) {
        this.expressProcessId = expressProcessId;
        this.expressDeliveryId = expressDeliveryId;
        this.orderId = orderId;
        this.modifyTime = modifyTime;
        this.processDesc = processDesc;
        this.processStatus = processStatus;
        this.eventId = IdGenUtils.nextTextId();
        this.createTime = LocalDateTime.now();
    }

    @Override
    public Long getAggregateRootId() {
        return this.orderId;
    }
}
