package com.luo.d3s.anti.mvc.ec.model.vo;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * 商品标签VO
 *
 * @author luohq
 * @date 2023-08-02
 */
@Data
public class GoodsTagVo {

    /**
     * 商品ID
     */
    private Long id;

    /**
     *标签名称
     */
    private String tagName;

    /**
     *标签描述
     */
    private String tagDesc;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;
}
