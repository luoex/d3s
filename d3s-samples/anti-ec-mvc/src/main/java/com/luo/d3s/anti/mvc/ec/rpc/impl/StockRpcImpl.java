package com.luo.d3s.anti.mvc.ec.rpc.impl;

import com.luo.d3s.anti.mvc.ec.entity.Order;
import com.luo.d3s.anti.mvc.ec.model.dto.LockStockDto;
import com.luo.d3s.anti.mvc.ec.rpc.StockRpc;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

/**
 * 库存RPC实现类
 *
 * @author luohq
 * @date 2023-11-08 15:54
 */
@Component
@Slf4j
public class StockRpcImpl implements StockRpc {
    @Override
    public void lockStock(Order order) {
        log.info("[MOCK RPC] lock stock, order: {}", order);
        //MOCK LockStockDto
        List<LockStockDto> lockStockDtoList = Collections.emptyList();
        log.info("[MOCK RPC] lock stock, param: {}", lockStockDtoList);
        //TODO RPC
        //this.feignClient.lockStock(lockStockDtoList);
    }

    @Override
    public void deductStock(Order order) {
        log.info("[MOCK RPC] deduct stock, order: {}", order);
    }
}
