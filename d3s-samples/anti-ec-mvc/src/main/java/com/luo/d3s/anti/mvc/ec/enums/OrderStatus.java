package com.luo.d3s.anti.mvc.ec.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.luo.d3s.core.domain.model.ValueObjectEnum;

/**
 * 订单状态
 *
 * @author luohq
 * @date 2022-11-27 16:01
 */
public enum OrderStatus implements ValueObjectEnum<Integer> {
    //10 - 已创建
    CREATED(10, "已创建"),
    //20 - 已支付
    PAID(20, "已支付"),
    //30 - 已发货
    DELIVERED(30, "已发货"),
    //40 - 已完成
    COMPLETED(40, "已完成"),
    //50 - 已取消
    CANCELED(50, "已取消");

    /**
     * 状态编码
     */
    @EnumValue
    private Integer status;

    /**
     * 状态描述
     */
    private String desc;

    OrderStatus(Integer status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    @Override
    public Integer getValue() {
        return this.status;
    }

    @Override
    public String getDesc() {
        return this.desc;
    }

    public static OrderStatus of(Integer status) {
        return ValueObjectEnum.of(status, OrderStatus.class);
    }
}
