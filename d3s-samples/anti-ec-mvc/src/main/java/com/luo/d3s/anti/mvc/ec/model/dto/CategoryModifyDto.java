package com.luo.d3s.anti.mvc.ec.model.dto;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 修改分类Command
 *
 * @author luohq
 * @date 2023-01-04 15:29
 */
@Data
public class CategoryModifyDto {
    /**
     * 分类ID
     */
    @NotNull
    private Long id;
    /**
     * 上级分类ID
     */
    private Long parentCategoryId;
    /**
     * 分类名称
     */
    @NotBlank
    @Length(min = 1, max = 64)
    private String categoryName;
    /**
     * 分类描述
     */
    @Length(min = 1, max = 512)
    private String categoryDesc;
}
