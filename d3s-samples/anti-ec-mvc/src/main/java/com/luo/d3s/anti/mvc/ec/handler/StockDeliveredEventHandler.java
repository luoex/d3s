package com.luo.d3s.anti.mvc.ec.handler;

import com.luo.d3s.anti.mvc.ec.model.dto.OrderDeliverDto;
import com.luo.d3s.anti.mvc.ec.model.event.StockDeliveredEvent;
import com.luo.d3s.anti.mvc.ec.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 库存已发货事件处理器
 *
 * @author luohq
 * @date 2022-11-27 19:29
 */
@Component
@Slf4j
public class StockDeliveredEventHandler {

    @Resource
    private OrderService orderService;

    /**
     * 库存已发货事件监听器
     *
     * @param stockDeliveredEvent 库存已发货事件
     */
    @RabbitListener(queues = {"${spring.rabbitmq.events.stock-delivery.queue.name}"})
    public void handle(StockDeliveredEvent stockDeliveredEvent) {
        log.info("[RabbitMq RECV] Stock Delivery: {}", stockDeliveredEvent);
        this.orderService.deliverOrder(
                new OrderDeliverDto(
                        stockDeliveredEvent.getOrderId(),
                        stockDeliveredEvent.getExpressCode(),
                        stockDeliveredEvent.getDeliveryTime()));
    }
}
