package com.luo.d3s.anti.mvc.ec.controller;

import com.luo.d3s.anti.mvc.ec.model.dto.GoodsCreateDto;
import com.luo.d3s.anti.mvc.ec.model.dto.GoodsModifyDto;
import com.luo.d3s.anti.mvc.ec.model.dto.GoodsPageQueryDto;
import com.luo.d3s.anti.mvc.ec.model.vo.GoodsVo;
import com.luo.d3s.anti.mvc.ec.service.GoodsService;
import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.core.application.dto.Response;
import com.luo.d3s.core.application.dto.SingleResponse;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 商品Controller
 *
 * @author luohq
 * @date 2023-01-07 12:12
 */
@Validated
@RestController
@RequestMapping("/api/v1/goods")
public class GoodsController {

    @Resource
    private GoodsService goodsService;

    /**
     * 根据商品ID查询商品信息
     *
     * @param goodsId 商品ID
     * @return 商品信息
     */
    @GetMapping("/{goodsId}")
    public SingleResponse<GoodsVo> findGoods(@PathVariable @NotNull Long goodsId) {
        return this.goodsService.findGoods(goodsId);
    }

    /**
     * 查询商品分页列表
     *
     * @param goodsPageQueryDto 商品分页查询参数
     * @return 商品分页查询结果
     */
    @GetMapping
    public PageResponse<GoodsVo> findGoodsPage(GoodsPageQueryDto goodsPageQueryDto) {
        return this.goodsService.findGoodsPage(goodsPageQueryDto);
    }

    /**
     * 创建商品
     *
     * @param goodsCreateDto 商品创建命令
     * @return 创建结果
     */
    @PostMapping
    public SingleResponse<GoodsVo> createGoods(@RequestBody @Validated GoodsCreateDto goodsCreateDto) {
        return this.goodsService.createGoods(goodsCreateDto);
    }

    /**
     * 修改商品
     *
     * @param goodsModifyDto 商品修改命令
     * @return 修改结果
     */
    @PutMapping
    public Response modifyGoods(@RequestBody @Validated GoodsModifyDto goodsModifyDto) {
        return this.goodsService.modifyGoods(goodsModifyDto);
    }

    /**
     * 批量删除商品
     *
     * @param goodsIds 商品ID列表
     * @return 删除结果
     */
    @DeleteMapping("/{goodsIds}")
    public Response removeGoods(@PathVariable @NotEmpty List<Long> goodsIds) {
        return this.goodsService.removeGoods(goodsIds);
    }

    /**
     * 批量上架商品
     *
     * @param goodsIds 商品ID列表
     * @return 上架结果
     */
    @PutMapping("/shelve/{goodsIds}")
    public Response shelveGoods(@PathVariable @NotEmpty List<Long> goodsIds) {
        return this.goodsService.shelveGoods(goodsIds);
    }

    /**
     * 批量下架商品
     *
     * @param goodsIds 商品ID列表
     * @return 下架结果
     */
    @PutMapping("/unshelve/{goodsIds}")
    public Response unshelveGoods(@PathVariable @NotEmpty List<Long> goodsIds) {
        return this.goodsService.unshelveGoods(goodsIds);
    }

}
