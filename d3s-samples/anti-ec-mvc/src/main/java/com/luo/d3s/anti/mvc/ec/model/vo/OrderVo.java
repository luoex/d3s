package com.luo.d3s.anti.mvc.ec.model.vo;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 订单VO
 *
 * @author luohq
 * @date 2022-11-27 19:11
 */
@Data
public class OrderVo {
    /**
     * 商品ID
     */
    private Long id;

    /**
     * 订单总价
     */
    private BigDecimal orderPrice;

    /**
     * 收货地址
     */
    private String receiveAddress;

    /**
     * 订单状态
     */
    private Integer orderStatus;

    /**
     * 订单创建时间
     */
    private LocalDateTime createTime;

    /**
     * 支付时间
     */
    private LocalDateTime payTime;

    /**
     * 快递单号
     */
    @NotBlank
    @Length(min = 1, max = 128)
    private String expressCode;

    /**
     * 配送时间
     */
    private LocalDateTime deliverTime;

    /**
     * 完成时间
     */
    private LocalDateTime completeTime;

    /**
     * 取消时间
     */
    private LocalDateTime cancelTime;


    /**
     * 订单修改时间
     */
    private LocalDateTime updateTime;

    /**
     * 订单商品列表
     */
    private List<OrderGoodsVo> orderGoodsList;
}
