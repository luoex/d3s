package com.luo.d3s.anti.mvc.ec.service;

import com.luo.d3s.anti.mvc.ec.model.dto.OrderCreateDto;
import com.luo.d3s.anti.mvc.ec.model.dto.OrderDeliverDto;
import com.luo.d3s.anti.mvc.ec.model.dto.OrderPageQueryDto;
import com.luo.d3s.anti.mvc.ec.model.vo.OrderVo;
import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.core.application.dto.Response;
import com.luo.d3s.core.application.dto.SingleResponse;

;

/**
 * 订单服务接口类
 *
 * @author luohq
 * @date 2022-11-27 19:09
 */
public interface OrderService {

    /**
     * 查询订单详情
     *
     * @param orderId 订单ID
     * @return 订单详情
     */
    SingleResponse<OrderVo> findOrder(Long orderId);

    /**
     * 查询订单分页列表
     *
     * @param orderPageQueryDto 订单分页查询参数
     * @return 分页查询结果
     */
    PageResponse<OrderVo> findOrderPage(OrderPageQueryDto orderPageQueryDto);

    /**
     * 创建订单
     *
     * @param orderCreateDto 创建订单命令
     * @return 响应结果（订单ID）
     */
    SingleResponse<Long> createOrder(OrderCreateDto orderCreateDto);

    /**
     * 支付订单
     *
     * @param orderId 订单ID
     * @return 响应结果
     */
    Response payOrder(Long orderId);

    /**
     * 发货订单
     *
     * @param orderDeliverDto 发货订单命令
     * @return 响应结果
     */
    Response deliverOrder(OrderDeliverDto orderDeliverDto);


    /**
     * 完成订单
     *
     * @param orderId 订单ID
     * @return 响应结果
     */
    Response completeOrder(Long orderId);

    /**
     * 取消订单
     *
     * @param orderId 订单ID
     * @return 响应结果
     */
    Response cancelOrder(Long orderId);
}
