package com.luo.d3s.anti.mvc.ec.service;

import com.luo.d3s.anti.mvc.ec.model.dto.GoodsCreateDto;
import com.luo.d3s.anti.mvc.ec.model.dto.GoodsModifyDto;
import com.luo.d3s.anti.mvc.ec.model.dto.GoodsPageQueryDto;
import com.luo.d3s.anti.mvc.ec.model.vo.GoodsVo;
import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.core.application.dto.Response;
import com.luo.d3s.core.application.dto.SingleResponse;

import java.util.List;

/**
 * 商品服务接口
 *
 * @author luohq
 * @date 2023-01-04 17:00
 */
public interface GoodsService {

    /**
     * 查询商品详情
     *
     * @param goodsId 商品ID
     * @return 商品详情
     */
    SingleResponse<GoodsVo> findGoods(Long goodsId);

    /**
     * 查询商品分页列表
     *
     * @param goodsPageQueryDto 商品分页查询参数
     * @return 分页查询结果
     */
    PageResponse<GoodsVo> findGoodsPage(GoodsPageQueryDto goodsPageQueryDto);

    /**
     * 创建商品
     *
     * @param goodsCreateDto 创建商品命令
     * @return 响应结果（商品信息）
     */
    SingleResponse<GoodsVo> createGoods(GoodsCreateDto goodsCreateDto);

    /**
     * 修改商品
     *
     * @param goodsModifyDto 修改商品命令
     * @return 响应结果
     */
    Response modifyGoods(GoodsModifyDto goodsModifyDto);

    /**
     * 批量移除商品
     *
     * @param goodsIds 商品ID列表
     * @return 响应结果
     */
    Response removeGoods(List<Long> goodsIds);

    /**
     * 批量上架商品
     *
     * @param goodsIds 商品ID列表
     * @return 响应结果
     */
    Response shelveGoods(List<Long> goodsIds);

    /**
     * 批量下架商品
     *
     * @param goodsIds 商品ID列表
     * @return 响应结果
     */
    Response unshelveGoods(List<Long> goodsIds);
}
