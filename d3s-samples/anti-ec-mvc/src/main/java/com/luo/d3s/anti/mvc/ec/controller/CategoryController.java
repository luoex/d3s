package com.luo.d3s.anti.mvc.ec.controller;

import com.luo.d3s.anti.mvc.ec.model.dto.CategoryCreateDto;
import com.luo.d3s.anti.mvc.ec.model.dto.CategoryModifyDto;
import com.luo.d3s.anti.mvc.ec.model.dto.CategoryPageQueryDto;
import com.luo.d3s.anti.mvc.ec.model.vo.CategoryTreeVo;
import com.luo.d3s.anti.mvc.ec.model.vo.CategoryVo;
import com.luo.d3s.anti.mvc.ec.service.CategoryService;
import com.luo.d3s.core.application.dto.MultiResponse;
import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.core.application.dto.Response;
import com.luo.d3s.core.application.dto.SingleResponse;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

/**
 * 分类Controller
 *
 * @author luohq
 * @date 2022-12-22 16:12
 */
@Validated
@RestController
@RequestMapping("/api/v1/categories")
public class CategoryController {

    @Resource
    private CategoryService categoryService;

    /**
     * 根据分类ID查询分类信息
     *
     * @param categoryId 分类ID
     * @return 分类信息
     */
    @GetMapping("/{categoryId}")
    public SingleResponse<CategoryVo> findCategory(@PathVariable @NotNull Long categoryId) {
        return this.categoryService.findCategory(categoryId);
    }

    /**
     * 根据分类ID查询分类树形列表
     *
     * @param categoryId 分类ID
     * @return 分类树形列表
     */
    @GetMapping("/tree/{categoryId}")
    public MultiResponse<CategoryTreeVo> findCategoryTree(@PathVariable(required = false) Long categoryId) {
        return this.categoryService.findCategoryTree(categoryId);
    }

    /**
     * 查询分类树形列表（完整分类树）
     *
     * @return 分类树形列表
     */
    @GetMapping("/tree")
    public MultiResponse<CategoryTreeVo> findAllCategoryTree() {
        return this.categoryService.findCategoryTree(null);
    }


    /**
     * 查询分类分页列表
     *
     * @param categoryPageQueryDto 分类分页查询参数
     * @return 分类分页查询结果
     */
    @GetMapping
    public PageResponse<CategoryVo> findCategoryPage(CategoryPageQueryDto categoryPageQueryDto) {
        return this.categoryService.findCategoryPage(categoryPageQueryDto);
    }

    /**
     * 创建分类
     *
     * @param categoryCreateDto 分类创建命令
     * @return 创建结果
     */
    @PostMapping
    public SingleResponse<CategoryVo> createCategory(@RequestBody @Validated CategoryCreateDto categoryCreateDto) {
        return this.categoryService.createCategory(categoryCreateDto);
    }

    /**
     * 修改分类
     *
     * @param categoryModifyDto 分类修改命令
     * @return 修改结果
     */
    @PutMapping
    public Response modifyCategory(@RequestBody @Validated CategoryModifyDto categoryModifyDto) {
        return this.categoryService.modifyCategory(categoryModifyDto);
    }

    /**
     * 删除分类
     *
     * @param categoryId 分类ID
     * @return 删除结果
     */
    @DeleteMapping("/{categoryId}")
    public Response removeCategory(@PathVariable @NotNull Long categoryId) {
        return this.categoryService.removeCategory(categoryId);
    }

}
