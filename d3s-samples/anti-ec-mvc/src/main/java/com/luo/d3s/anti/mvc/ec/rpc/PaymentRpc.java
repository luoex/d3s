package com.luo.d3s.anti.mvc.ec.rpc;

import com.luo.d3s.anti.mvc.ec.entity.Order;
import com.luo.d3s.core.domain.adpator.Acl;

/**
 * 支付RPC接口
 *
 * @author luohq
 * @date 2022-11-27 18:37
 */
public interface PaymentRpc extends Acl {

    /**
     * 支付订单
     *
     * @param order 订单信息
     */
    void payOrder(Order order);

    /**
     * 退款订单
     *
     * @param order 订单信息
     */
    void refundOrder(Order order);
}
