package com.luo.d3s.anti.mvc.ec.service;

import com.luo.d3s.anti.mvc.ec.model.dto.GoodsTagCreateDto;
import com.luo.d3s.anti.mvc.ec.model.dto.GoodsTagModifyDto;
import com.luo.d3s.anti.mvc.ec.model.dto.GoodsTagPageQueryDto;
import com.luo.d3s.anti.mvc.ec.model.vo.GoodsTagVo;
import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.core.application.dto.Response;
import com.luo.d3s.core.application.dto.SingleResponse;

/**
 * 商品标签服务接口
 *
 * @author luohq
 * @date 2023-08-03
 */
public interface GoodsTagService {


    /**
     * 查询商品标签详情
     *
     * @param goodsTagId 商品标签ID
     * @return 商品标签详情
     */
    SingleResponse<GoodsTagVo> findGoodsTag(Long goodsTagId);

    /**
     * 查询商品标签分页列表
     *
     * @param goodsTagPageQueryDto 商品标签分页查询参数
     * @return 分页查询结果
     */
    PageResponse<GoodsTagVo> findGoodsTagPage(GoodsTagPageQueryDto goodsTagPageQueryDto);

    /**
     * 创建商品标签
     *
     * @param goodsTagCreateDto 创建商品标签命令
     * @return 响应结果（商品标签信息）
     */
    SingleResponse<GoodsTagVo> createGoodsTag(GoodsTagCreateDto goodsTagCreateDto);

    /**
     * 修改商品标签
     *
     * @param goodsTagModifyDto 修改商品标签命令
     * @return 响应结果
     */
    Response modifyGoodsTag(GoodsTagModifyDto goodsTagModifyDto);

    /**
     * 移除商品标签
     *
     * @param goodsTagId 商品标签ID
     * @return 响应结果
     */
    Response removeGoodsTag(Long goodsTagId);
}
