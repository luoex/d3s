package com.luo.d3s.anti.mvc.ec.model.dto;

import com.luo.d3s.core.application.dto.Command;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * 订单发货Command
 *
 * @author luohq
 * @date 2022-11-27 19:00
 */
@Data
@AllArgsConstructor
public class OrderDeliverDto extends Command {
    /**
     * 订单ID
     */
    @NotNull
    private Long orderId;
    /**
     * 快递单号
     */
    @NotBlank
    private String expressCode;
    /**
     * 发货时间
     */
    @NotNull
    private LocalDateTime deliveryTime;
}

