package com.luo.d3s.anti.mvc.ec.model.dto;

import com.luo.d3s.anti.mvc.ec.model.vo.OrderGoodsVo;
import com.luo.d3s.core.application.dto.Command;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.util.List;

/**
 * 创建订单Command
 *
 * @author luohq
 * @date 2022-11-27 19:00
 */
@Data
public class OrderCreateDto extends Command {
    /**
     * 商品项
     */
    @Valid
    @NotEmpty
    private List<OrderGoodsVo> orderGoods;
    /**
     * 订单总价
     */
    @NotNull
    @Positive
    private BigDecimal orderPrice;
    /**
     * 收货地址
     */
    @NotBlank
    @Length(min = 1, max = 256)
    private String receiveAddress;
}
