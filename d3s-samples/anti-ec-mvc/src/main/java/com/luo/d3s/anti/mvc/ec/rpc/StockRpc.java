package com.luo.d3s.anti.mvc.ec.rpc;

import com.luo.d3s.anti.mvc.ec.entity.Order;
import com.luo.d3s.core.domain.adpator.Acl;

/**
 * 库存RPC接口
 *
 * @author luohq
 * @date 2022-11-27 18:37
 */
public interface StockRpc extends Acl {

    /**
     * 锁库存
     *
     * @param order 订单信息
     */
    void lockStock(Order order);

    /**
     * 扣减库存
     *
     * @param order 扣减库存
     */
    void deductStock(Order order);
}
