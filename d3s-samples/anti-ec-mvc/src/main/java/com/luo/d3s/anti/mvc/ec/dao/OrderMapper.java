package com.luo.d3s.anti.mvc.ec.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.luo.d3s.anti.mvc.ec.model.dto.OrderPageQueryDto;
import com.luo.d3s.anti.mvc.ec.entity.Order;
import com.luo.d3s.anti.mvc.ec.model.vo.GoodsVo;
import com.luo.d3s.anti.mvc.ec.model.vo.OrderVo;
import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.core.infrastructure.convertor.MpPageConvertor;
import org.apache.ibatis.annotations.Param;

import java.util.function.Function;

/**
 * 订单信息 Mapper 接口
 *
 * @author luohq
 * @date 2022-12-21
 */
public interface OrderMapper extends BaseMapper<Order> {
    /**
     * 查询订单分页列表
     *
     * @param orderPageQueryDto 订单分页查询参数
     * @return page response
     */
    default PageResponse<OrderVo> findPage(OrderPageQueryDto orderPageQueryDto) {
        IPage<OrderVo> pageResult = this.selectOrderPage(MpPageConvertor.toPage(orderPageQueryDto), orderPageQueryDto);
        return MpPageConvertor.toPageResponse(pageResult, Function.identity());
    }

    /**
     * 根据订单ID查询订单及商品信息VO
     *
     * @param id 订单ID
     * @return 订单信息
     */
    OrderVo findVoById(Long id);

    /**
     * 查询订单分页列表
     *
     * @param page           mp分页参数
     * @param orderPageQueryDto 具体查询参数
     * @return 分页查询结果
     */
    @Deprecated
    IPage<OrderVo> selectOrderPage(IPage<GoodsVo> page, @Param("orderPageQueryDto") OrderPageQueryDto orderPageQueryDto);
}
