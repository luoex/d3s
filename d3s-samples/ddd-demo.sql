-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category`  (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT '主键ID',
  `parent_category_id` bigint(20) NULL DEFAULT NULL COMMENT '上级分类ID',
  `category_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '分类名称',
  `category_desc` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '分类描述',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for goods
-- ----------------------------
DROP TABLE IF EXISTS `goods`;
CREATE TABLE `goods`  (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT '主键ID',
  `category_id` bigint(20) NOT NULL COMMENT '所属分类ID',
  `goods_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '商品名称',
  `manufacture_date` date NOT NULL COMMENT '生成日期',
  `expiration_date` date NOT NULL COMMENT '过期日期',
  `goods_weight` decimal(10, 2) NOT NULL COMMENT '商品重量',
  `goods_weight_unit` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '重量单位',
  `goods_desc` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '商品描述',
  `goods_price` decimal(20, 2) NOT NULL COMMENT '商品价格',
  `goods_status` int(11) NOT NULL COMMENT '商品状态(10已上架, 20已下架)',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for goods_tag
-- ----------------------------
DROP TABLE IF EXISTS goods_tag;
CREATE TABLE goods_tag (
	id bigint(20) NOT NULL COMMENT '主键ID',
	tag_name varchar(64) NOT NULL COMMENT '标签名称',
	tag_desc varchar(512) DEFAULT NULL COMMENT '标签描述',
	create_time datetime NOT NULL COMMENT '创建时间',
	update_time datetime DEFAULT NULL COMMENT '修改时间',
	PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB;

-- ----------------------------
-- Table structure for goods_tag_binding
-- ----------------------------
DROP TABLE IF EXISTS goods_tag_binding;
CREATE TABLE goods_tag_binding (
	goods_id bigint(20) NOT NULL COMMENT '商品ID',
	tag_id bigint(20) NOT NULL COMMENT '商品标签ID',
	PRIMARY KEY (`goods_id`, `tag_id`) USING BTREE
) ENGINE = InnoDB;



-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`  (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT '订单ID',
  `order_price` decimal(10, 2) NOT NULL COMMENT '订单价格',
  `receive_address` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '收货地址',
  `order_status` int(11) NOT NULL COMMENT '订单状态(10已创建, 20已支付, 30已发货, 40已完成, 50已取消)',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `pay_time` datetime NULL DEFAULT NULL COMMENT '支付时间',
  `express_code` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '快递单号',
  `deliver_time` datetime NULL DEFAULT NULL COMMENT '发货时间',
  `complete_time` datetime NULL DEFAULT NULL COMMENT '完成时间',
  `cancel_time` datetime NULL DEFAULT NULL COMMENT '取消时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for order_goods
-- ----------------------------
DROP TABLE IF EXISTS `order_goods`;
CREATE TABLE `order_goods`  (
  `id` bigint(20) NOT NULL COMMENT '主键ID',
  `goods_id` bigint(20) NOT NULL COMMENT '商品ID',
  `order_id` bigint(20) NOT NULL COMMENT '订单ID',
  `goods_count` int(11) NOT NULL COMMENT '商品数量',
  `goods_price` decimal(10, 2) NOT NULL COMMENT '商品单价',
  `goods_sum_price` decimal(10, 2) NOT NULL COMMENT '商品总价',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for domain_event
-- ----------------------------
DROP TABLE IF EXISTS `domain_event`;
CREATE TABLE `domain_event`  (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键ID',
  `aggregate_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '聚合根ID',
  `json_type` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '事件Json类型（对应Java Class名称）',
  `json_content` varchar(4096) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '事件Json内容',
  `status` tinyint(4) NOT NULL COMMENT '事件状态（10已创建，20已发送）',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for domain_event_record
-- ----------------------------
DROP TABLE IF EXISTS `domain_event_record`;
CREATE TABLE `domain_event_record`  (
  `event_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '事件ID',
  `record_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '记录时间',
  PRIMARY KEY (`event_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for shedlock
-- ----------------------------
DROP TABLE IF EXISTS `shedlock`;
CREATE TABLE `shedlock`  (
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `lock_until` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  `locked_at` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `locked_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;




-- 初始订单
INSERT INTO orders
(id, order_price, receive_address, order_status, create_time,           pay_time,               express_code, deliver_time,          complete_time, cancel_time, update_time) VALUES
(1,  20.00,       '辽宁沈阳',        30,          '2023-01-23 11:30:59',  '2023-01-23 14:16:14',  '111',        '2023-01-23 14:16:48', NULL,          NULL,        NULL),
(2,  30.00,       '辽宁沈阳',        10,          '2023-01-23 13:10:09',   NULL,                   NULL,         NULL,                  NULL,          NULL,        NULL),
(3,  30.00,       '辽宁沈阳',        10,          '2023-01-23 13:10:09',   NULL,                   NULL,         NULL,                  NULL,          NULL,        NULL),
(4,  30.00,       '辽宁沈阳',        10,          '2023-01-23 13:10:09',   NULL,                   NULL,         NULL,                  NULL,          NULL,        NULL),
(5,  30.00,       '辽宁沈阳',        10,          '2023-01-23 13:10:09',   NULL,                   NULL,         NULL,                  NULL,          NULL,        NULL);
-- 初始订单商品
INSERT INTO order_goods
(id, goods_id, order_id, goods_count, goods_price, goods_sum_price, create_time,           update_time) VALUES
(1,  1,        1,        2,           10.00,       20.00,           '2023-01-23 11:30:59', '2023-01-23 11:30:59'),
(2,  2,        2,        3,           10.00,       30.00,           '2023-01-23 11:30:59', '2023-01-23 11:30:59'),
(3,  1,        3,        1,           10.00,       10.00,           '2023-01-23 11:30:59', '2023-01-23 11:30:59'),
(4,  1,        4,        1,           10.00,       10.00,           '2023-01-23 11:30:59', '2023-01-23 11:30:59'),
(5,  1,        5,        1,           10.00,       10.00,           '2023-01-23 11:30:59', '2023-01-23 11:30:59');


-- 初始分类数据
INSERT INTO category
(id,   parent_category_id, category_name, category_desc,      create_time,                update_time) VALUES
( 1,   null,               '分类1',       '查询测试专用数据',    '2023-01-01T15:58:04.105',   '2023-01-07T15:58:04.105'),
( 11,   1,                 '分类1-1',     '分类1-1描述',        '2023-01-02T15:58:04.105',   '2023-01-07T15:58:04.105'),
( 12,   1,                 '分类1-2',     '分类1-2描述',        '2023-01-03T15:58:04.105',   '2023-01-07T15:58:04.105'),
( 2,   null,               '分类2',       '分类2描述',          '2023-01-04T15:58:04.105',   '2023-01-07T15:58:04.105'),
( 21,   2,                  '分类2-1',    '分类2-1描述',        '2023-05-03T15:58:04.105',   '2023-01-07T15:58:04.105'),
( 3,   null,               '分类3',       'modify测试专用数据',  '2023-01-05T15:58:04.105',   '2023-01-07T15:58:04.105'),
( 4,   null,               '分类4',       'remove测试专用数据',  '2023-01-06T15:58:04.105',   '2023-01-07T15:58:04.105'),
( 41,   4,                 '分类4-1',     'remove测试专用数据',  '2023-01-07T15:58:04.105',   '2023-01-07T15:58:04.105'),
( 42,   4,                 '分类4-2',     'remove测试专用数据',  '2023-01-08T15:58:04.105',   '2023-01-07T15:58:04.105');

-- 初始商品数据
INSERT INTO goods
( id, category_id, goods_name,   manufacture_date,  expiration_date, goods_weight, goods_weight_unit, goods_desc,             goods_price, goods_status, create_time,               update_time ) VALUES
( 1,  1,           '商品1',      '2023-01-01',       '2023-11-01',    2,            'kg',              '商品1描述',              220.5,       10,          '2023-01-01T16:30:01.095', '2023-01-01T16:30:01.095'),
( 2,  2,           '商品2',      '2022-12-01',       '2023-05-01',    200,          'g',               '商品2描述',              220.5,       10,          '2023-01-02T16:30:01.095', '2023-01-02T16:30:01.095'),
( 3,  11,          '商品3',      '2022-12-01',       '2023-05-01',    200,          'g',               '商品3描述',              220.5,       10,          '2023-01-02T16:30:01.095', '2023-01-02T16:30:01.095'),
( 4,  1,           '商品4',      '2022-12-01',       '2023-05-01',    200,          'g',               'remove测试专用 - 未上架', 220.5,       20,           '2023-01-02T16:30:01.095', '2023-01-02T16:30:01.095'),
( 5,  1,           '商品5',      '2022-12-01',       '2023-05-01',    200,          'g',               'remove测试专用 - 未上架', 220.5,       20,           '2023-01-02T16:30:01.095', '2023-01-02T16:30:01.095'),
( 6,  1,           '商品6',      '2022-12-01',       '2023-05-01',    200,          'g',               '上下架测试专用',          220.5,       20,           '2023-01-02T16:30:01.095', '2023-01-02T16:30:01.095'),
( 7,  1,           '商品7',      '2022-12-01',       '2023-05-01',    200,          'g',               '上下架测试专用',          220.5,       20,           '2023-01-02T16:30:01.095', '2023-01-02T16:30:01.095');

-- 初始商品标签数据
INSERT INTO goods_tag
(id,  tag_name,      tag_desc,           create_time,                update_time) VALUES
( 1,  '标签1',       '标签描述',          '2023-01-01T15:58:04.105',   '2023-01-07T15:58:04.105'),
( 2,  '标签2',       '标签描述',          '2023-01-02T15:58:04.105',   '2023-01-07T15:58:04.105'),
( 3,  '标签3',       '标签描述',          '2023-01-03T15:58:04.105',   '2023-01-07T15:58:04.105'),
( 4,  '标签4',       '标签描述',          '2023-01-04T15:58:04.105',   '2023-01-07T15:58:04.105'),
( 5,  '标签5',       '标签描述',          '2023-05-03T15:58:04.105',   '2023-01-07T15:58:04.105'),
( 6,  '标签6',       '标签描述',          '2023-01-05T15:58:04.105',   '2023-01-07T15:58:04.105'),
( 7,  '标签7',       '标签描述',          '2023-01-06T15:58:04.105',   '2023-01-07T15:58:04.105'),
( 8,  '标签8',       '标签描述',          '2023-01-07T15:58:04.105',   '2023-01-07T15:58:04.105'),
( 9,  '标签9',       '标签描述',          '2023-01-08T15:58:04.105',   '2023-01-07T15:58:04.105'),
( 10, '标签10',      '标签描述',          '2023-01-08T15:58:04.105',   '2023-01-07T15:58:04.105'),
( 11, '标签11',      '标签描述',          '2023-01-08T15:58:04.105',   '2023-01-07T15:58:04.105');

-- 初始商品标签绑定数据
INSERT INTO goods_tag_binding
(goods_id,    tag_id) VALUES
( 1,          1),
( 1,          2),
( 1,          3),
( 2,          3),
( 2,          4),
( 2,          5);