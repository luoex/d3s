package com.luo.d3s.ext.component.query;

import com.luo.d3s.core.application.dto.SingleResponse;
import com.luo.d3s.core.application.service.QueryService;
import com.luo.d3s.ext.component.query.param.Query3;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 查询处理服务1
 *
 * @author luohq
 * @date 2023-01-27 13:52
 */
public class QueryService3 implements QueryService {

    private static final Logger log = LoggerFactory.getLogger(QueryService3.class);

    public SingleResponse handleQuery3Async(Query3 query3) {
        log.info("handle query3: {}", query3);
        return SingleResponse.of(query3);
    }
}
