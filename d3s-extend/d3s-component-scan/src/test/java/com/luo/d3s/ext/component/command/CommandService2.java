package com.luo.d3s.ext.component.command;

import com.luo.d3s.core.application.dto.Response;
import com.luo.d3s.core.application.service.CommandService;
import com.luo.d3s.ext.component.command.param.Command2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;

/**
 * 命令处理服务2
 *
 * @author luohq
 * @date 2023-01-27 13:52
 */
public class CommandService2 implements CommandService {

    private static final Logger log = LoggerFactory.getLogger(CommandService2.class);

    public Response handleCommand2(Command2 command2) {
        log.info("handle command2: {}", command2);
        return Response.buildSuccess();
    }

    @Async
    public Response handleCommand2Async(Command2 command2) {
        log.info("handle command2 async: {}", command2);
        return Response.buildSuccess();
    }
}
