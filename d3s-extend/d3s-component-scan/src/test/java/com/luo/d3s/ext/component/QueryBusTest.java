package com.luo.d3s.ext.component;

import com.luo.d3s.core.application.dto.SingleResponse;
import com.luo.d3s.ext.component.base.BaseSpringTest;
import com.luo.d3s.ext.component.bus.QueryBus;
import com.luo.d3s.ext.component.query.QueryComboService;
import com.luo.d3s.ext.component.query.QueryService1;
import com.luo.d3s.ext.component.query.QueryService2;
import com.luo.d3s.ext.component.query.QueryService3;
import com.luo.d3s.ext.component.query.param.Query1;
import com.luo.d3s.ext.component.query.param.Query2;
import com.luo.d3s.ext.component.query.param.Query3;
import com.luo.d3s.ext.test.utils.InvokeCountStat;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * QueryBus测试
 *
 * @author luohq
 * @date 2023-01-27 14:00
 */
public class QueryBusTest extends BaseSpringTest {

    @BeforeEach
    void beforeEach() {
        InvokeCountStat.resetInvokeCount();
    }
    
    @Test
    void testQueryBusDispatchAsync() {
        Query1 query1 = new Query1(1L, "I'm query1");
        //QueryService1.handleQuery1Async方法使用@Async标记，故此处返回null
        SingleResponse<Query1> resp = QueryBus.dispatch(query1);
        Assertions.assertNull(resp);

        super.sleep5Secs();

        Assertions.assertEquals(1, InvokeCountStat.invokeCount(QueryService1.class));
    }

    @Test
    void testQueryBusDispatchSync() {
        Long id = 3L;
        String content = "I'm query3";
        Query3 query3 = new Query3(id, content);
        SingleResponse<Query3> resp = QueryBus.dispatch(query3);
        Assertions.assertNotNull(resp);
        Assertions.assertEquals(id, resp.getData().getId());
        Assertions.assertEquals(content, resp.getData().getContent());

        super.sleep5Secs();

        Assertions.assertEquals(1, InvokeCountStat.invokeCount(QueryService3.class));
    }

    @Test
    void testQueryBusDispatchBatch() {
        Query2 query2 = new Query2(2L, "I'm query2");
        QueryBus.dispatchBatch(query2);

        super.sleep5Secs();

        Assertions.assertEquals(2, InvokeCountStat.invokeCount(QueryService2.class));
        Assertions.assertEquals(2, InvokeCountStat.invokeCount(QueryComboService.class));
    }
}
