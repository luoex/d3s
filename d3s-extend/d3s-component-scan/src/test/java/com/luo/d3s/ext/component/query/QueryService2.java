package com.luo.d3s.ext.component.query;

import com.luo.d3s.core.application.dto.SingleResponse;
import com.luo.d3s.core.application.service.QueryService;
import com.luo.d3s.ext.component.query.param.Query2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;

/**
 * 查询处理服务2
 *
 * @author luohq
 * @date 2023-01-27 13:52
 */
public class QueryService2 implements QueryService {

    private static final Logger log = LoggerFactory.getLogger(QueryService2.class);

    public SingleResponse handleQuery2(Query2 query2) {
        log.info("handle query2: {}", query2);
        return SingleResponse.of(query2);
    }

    @Async
    public SingleResponse handleQuery2Async(Query2 query2) {
        log.info("handle query2 async: {}", query2);
        return SingleResponse.of(query2);
    }
}
