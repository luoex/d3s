package com.luo.d3s.ext.component.query.param;

import com.luo.d3s.core.application.dto.Query;

import java.util.Objects;

/**
 * 查询2
 *
 * @author luohq
 * @date 2023-01-27 13:48
 */
public class Query2 extends Query {

    private Long id;

    private String content;

    public Query2() {
    }

    public Query2(Long id, String content) {
        this.id = id;
        this.content = content;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Query2 command1 = (Query2) o;
        return Objects.equals(id, command1.id) && Objects.equals(content, command1.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, content);
    }

    @Override
    public String toString() {
        return "Query2{" +
                "id=" + id +
                ", content='" + content + '\'' +
                '}';
    }
}
