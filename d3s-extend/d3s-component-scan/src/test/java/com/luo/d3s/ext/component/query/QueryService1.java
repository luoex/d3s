package com.luo.d3s.ext.component.query;

import com.luo.d3s.core.application.dto.SingleResponse;
import com.luo.d3s.core.application.service.QueryService;
import com.luo.d3s.ext.component.query.param.Query1;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;

/**
 * 查询处理服务1
 *
 * @author luohq
 * @date 2023-01-27 13:52
 */
public class QueryService1 implements QueryService {

    private static final Logger log = LoggerFactory.getLogger(QueryService1.class);

    @Async //异步处理
    public SingleResponse handleQuery1Async(Query1 query1) {
        log.info("handle query1 async: {}", query1);
        return SingleResponse.of(query1);
    }
}
