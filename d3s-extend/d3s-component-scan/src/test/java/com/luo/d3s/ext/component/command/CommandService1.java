package com.luo.d3s.ext.component.command;

import com.luo.d3s.core.application.dto.Response;
import com.luo.d3s.core.application.service.CommandService;
import com.luo.d3s.ext.component.command.param.Command1;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 命令处理服务1
 *
 * @author luohq
 * @date 2023-01-27 13:52
 */
public class CommandService1 implements CommandService {

    private static final Logger log = LoggerFactory.getLogger(CommandService1.class);

    public Response handleCommand1(Command1 command1) {
        log.info("handle command1: {}", command1);
        return Response.buildSuccess();
    }
}
