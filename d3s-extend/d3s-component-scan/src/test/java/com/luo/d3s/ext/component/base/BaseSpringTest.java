package com.luo.d3s.ext.component.base;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;


/**
 * 基础测试类 - 集成Spring基础测试配置
 *
 * @author luohq
 * @date 2021-12-11 11:19
 */
@SpringBootTest(classes = BaseConfiguration.class)
@ActiveProfiles("test")
public class BaseSpringTest {


    protected void sleep(Long mills) {
        try {
            Thread.sleep(mills);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    protected void sleep5Secs() {
        this.sleep(5000L);
    }

}
