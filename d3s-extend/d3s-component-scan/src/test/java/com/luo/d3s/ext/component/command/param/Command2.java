package com.luo.d3s.ext.component.command.param;

import com.luo.d3s.core.application.dto.Command;

import java.util.Objects;

/**
 * 命令2
 *
 * @author luohq
 * @date 2023-01-27 13:48
 */
public class Command2 extends Command {

    private Long id;

    private String content;

    public Command2() {
    }

    public Command2(Long id, String content) {
        this.id = id;
        this.content = content;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Command2 command1 = (Command2) o;
        return Objects.equals(id, command1.id) && Objects.equals(content, command1.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, content);
    }

    @Override
    public String toString() {
        return "Command2{" +
                "id=" + id +
                ", content='" + content + '\'' +
                '}';
    }
}
