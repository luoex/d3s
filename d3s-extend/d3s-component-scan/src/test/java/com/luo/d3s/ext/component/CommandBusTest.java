package com.luo.d3s.ext.component;

import com.luo.d3s.core.application.dto.Response;
import com.luo.d3s.ext.component.base.BaseSpringTest;
import com.luo.d3s.ext.component.bus.CommandBus;
import com.luo.d3s.ext.component.command.CommandComboService;
import com.luo.d3s.ext.component.command.CommandService1;
import com.luo.d3s.ext.component.command.CommandService2;
import com.luo.d3s.ext.component.command.param.Command1;
import com.luo.d3s.ext.component.command.param.Command2;
import com.luo.d3s.ext.test.utils.InvokeCountStat;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * CommandBus测试
 *
 * @author luohq
 * @date 2023-01-27 14:00
 */
public class CommandBusTest extends BaseSpringTest {
    private AtomicInteger repeatIndexCounter = new AtomicInteger(0);

    @BeforeEach
    void beforeEach() {
        InvokeCountStat.resetInvokeCount();
        //@RepeatedTest方法仅执行1次@BeforeEach方法
        repeatIndexCounter.set(0);
    }

    @Test
    void testCommandBusDispatch() {
        Command1 command1 = new Command1(1L, "I'm command1");
        Response resp = CommandBus.dispatch(command1);
        Assertions.assertTrue(resp.isSuccess());

        //判断调用是否成功
        Assertions.assertEquals(1, InvokeCountStat.invokeCount(CommandService1.class));
    }

    @RepeatedTest(3)
    void testCommandBusDispatchRepeat() {
        //重复计数+1
        Integer repeatIndex = repeatIndexCounter.incrementAndGet();

        Command1 command1 = new Command1(1L, "I'm command1");
        Response resp = CommandBus.dispatch(command1);
        Assertions.assertTrue(resp.isSuccess());

        //判断多次调用是否成功
        Assertions.assertEquals(1 * repeatIndex, InvokeCountStat.invokeCount(CommandService1.class));
    }

    @Test
    void testCommandBusDispatchBatch() {
        Command2 command2 = new Command2(2L, "I'm command2");
        CommandBus.dispatchBatch(command2);

        sleep5Secs();

        //判断调用是否成功
        Assertions.assertEquals(2, InvokeCountStat.invokeCount(CommandService2.class));
        Assertions.assertEquals(2, InvokeCountStat.invokeCount(CommandComboService.class));

    }

    @RepeatedTest(3)
    void testCommandBusDispatchBatchRepeat() {
        //重复计数+1
        Integer repeatIndex = repeatIndexCounter.incrementAndGet();

        Command2 command2 = new Command2(2L, "I'm command2");
        CommandBus.dispatchBatch(command2);

        sleep5Secs();

        //判断调用是否成功
        Assertions.assertEquals(2 * repeatIndex, InvokeCountStat.invokeCount(CommandService2.class));
        Assertions.assertEquals(2 * repeatIndex, InvokeCountStat.invokeCount(CommandComboService.class));
    }
}
