package com.luo.d3s.ext.component.scan;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * DDD组件扫描配置类
 *
 * @author luohq
 * @date 2023-01-18 9:41
 */
@Configuration
@ComponentScan(basePackages = {"com.luo.d3s.ext.component"})
public class D3sComponentScanConfiguration {

}
