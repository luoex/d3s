package com.luo.d3s.ext.component.bus;

import com.luo.d3s.core.application.dto.Query;
import com.luo.d3s.core.application.service.QueryService;
import com.luo.d3s.ext.component.bus.base.BaseServiceMethodBatchBus;
import com.luo.d3s.ext.component.bus.base.BaseServiceMethodSingletonBus;

/**
 * 查询分发器
 *
 * @author luohq
 * @date 2023-01-24 16:38
 */
public class QueryBus {

    /**
     * 单例调用分派实现类
     */
    private static BaseServiceMethodSingletonBus<QueryService, Query> singletonBus = new BaseServiceMethodSingletonBus(QueryService.class, Query.class);
    /**
     * 批量调用分派实现类
     */
    private static BaseServiceMethodBatchBus<QueryService, Query> batchBus = new BaseServiceMethodBatchBus(QueryService.class, Query.class);

    /**
     * 根据查询调用（同步）对应的查询服务实现方法<br/>
     * 注：仅调用唯一处理方法（不支持多实现方法），且支持返回调用结果（若被调用方法标记为@Async则返回null）
     *
     * @param query 查询
     * @param <R>   返回结果类型
     * @return query处理方法返回结果
     */
    public static <R> R dispatch(Query query) {
        return singletonBus.dispatch(query);
    }

    /**
     * 根据查询调用（同步）对应的查询服务实现方法<br/>
     * 注：支持调用多实现方法，不支持返回调用结果
     *
     * @param query 查询
     */
    public static void dispatchBatch(Query query) {
        batchBus.dispatch(query);
    }
}
