package com.luo.d3s.ext.component.bus;

import com.luo.d3s.core.application.dto.Command;
import com.luo.d3s.core.application.service.CommandService;
import com.luo.d3s.ext.component.bus.base.BaseServiceMethodBatchBus;
import com.luo.d3s.ext.component.bus.base.BaseServiceMethodSingletonBus;

/**
 * 命令分发器
 *
 * @author luohq
 * @date 2023-01-24 16:38
 */
public class CommandBus {

    /**
     * 单例调用分派实现类
     */
    private static BaseServiceMethodSingletonBus<CommandService, Command> singletonBus = new BaseServiceMethodSingletonBus<>(CommandService.class, Command.class);
    /**
     * 批量调用分派实现类
     */
    private static BaseServiceMethodBatchBus<CommandService, Command> batchBus = new BaseServiceMethodBatchBus<>(CommandService.class, Command.class);

    /**
     * 根据命令调用（同步）对应的命令服务实现方法<br/>
     * 注：仅调用唯一处理方法（不支持多实现方法），且支持返回调用结果（若被调用方法标记为@Async则返回null）
     *
     * @param command 命令
     * @param <R>     返回结果类型
     * @return command处理方法返回结果
     */
    public static <R> R dispatch(Command command) {
        return singletonBus.dispatch(command);
    }


    /**
     * 根据命令调用（同步）对应的命令服务实现方法<br/>
     * 注：支持调用多实现方法，不支持返回调用结果
     *
     * @param command 命令
     */
    public static void dispatchBatch(Command command) {
        batchBus.dispatch(command);
    }


}
