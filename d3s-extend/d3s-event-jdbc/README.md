# 领域事件 - Jdbc扩展实现

## 实现说明
- 采用Spring Jdbc(JdbcTemplate) 实现ddd-core中定义的Event相关接口：  
  - [DomainEventRepository.java](../../d3s-core/src/main/java/com/luo/d3s/core/domain/event/DomainEventRepository.java)
  - [DomainEventRecordRepository.java](../../d3s-core/src/main/java/com/luo/d3s/core/domain/event/DomainEventRecordRepository.java)
  - [DomainEventHandler.java](../../d3s-core/src/main/java/com/luo/d3s/core/domain/event/DomainEventHandler.java)
  - [DomainEventPublisher.java](../../d3s-core/src/main/java/com/luo/d3s/core/domain/event/DomainEventPublisher.java)
- 由于采用底层Spring Jdbc实现，所以Mybatis、Jpa等皆兼容本扩展实现
- 采用存储转发的机制
  - 应用层发送领域事件时，在应用层DB事务内首先将事件持久化
  - DB事务成功提交后立即触发事件发送
  - 若事件发送失败，后续会通过定时任务查询待发送事件并触发事件的补充发送，直至事件发送成功
  - 定时任务实现采用Spring Scheduling，同时配合Shedlock Jdbc保证同一时刻仅有一个服务实例执行定时任务
- 接收事件时记录事件ID，保证接收事件的幂等性
- 支持领域事件相关存储的定时清理


## 配置说明
具体定时发送与清理相关配置说明如下：  
```yaml
# 领域事件 - jdbc扩展 - 相关配置
d3s:
  events:
    # 定时发送配置
    publish:
      # 补偿定时发送时间间隔（单位：毫秒，默认3秒）
      compensate-interval-seconds: 3
      # 补偿发送查询事件的延迟时间（单位：毫秒，默认5秒）
      compensate-delay-seconds: 5
    # 定时清理配置
    clean:
      # 是否开启定时清理任务（默认开启）
      enabled: true
      # 定时清理任务执行时间Cron表达式（默认每天凌晨1点）
      cron: 0 0 1 * * ?
      # 领域事件发送记录 - 保留天数（默认30天）
      event-retention-days: 30
      # 领域事件接收记录 - 保留天数（默认30天）
      record-retention-days: 30
```

## Sql定义说明
底层存储Sql定义参见：[events.sql](src/main/resources/events.sql)  
```sql
-- domain_event definition
CREATE TABLE `domain_event` (
  `id` char(32) NOT NULL COMMENT '主键ID',
  `aggregate_id` varchar(64) DEFAULT NULL COMMENT '聚合根ID',
  `json_type` varchar(256) NOT NULL COMMENT '事件Json类型（对应Java Class名称）',
  `json_content` varchar(4096) NOT NULL COMMENT '事件Json内容',
  `status` tinyint(4) NOT NULL COMMENT '事件状态（10已创建，20已发送）',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间'
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='领域事件信息';

-- domain_event_record definition
CREATE TABLE `domain_event_record` (
  `event_id` char(32) NOT NULL COMMENT '事件ID',
  `record_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '记录时间',
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='领域事件接收记录';

-- shedlock definition
CREATE TABLE shedlock (
    name VARCHAR(64) NOT NULL,
    lock_until TIMESTAMP(3) NOT NULL,
    locked_at TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    locked_by VARCHAR(255) NOT NULL,
    PRIMARY KEY (name)
);

```

## 集成说明
>注：  
>具体集成示例可参见：[ddd-samples/ddd-ec](../ddd-samples/ddd-ec)  
>推荐在application层集成d3s-event-jdbc依赖。

### 发布事件
在infrastructure层需实现DomainEventPublisher：
```java
@Component
public class DomainEventPublisherImpl implements DomainEventPublisher {
    

    public void publish(DomainEvent event) {
        //TODO 此处需实现真正的事件发送，如通过RabbitMq等进行发布
    }
}
```
然后在application层调用EventBus.publish发布事件：
```java
//发送订单已支付事件
EventBus.publish(new OrderPaidEvent(order));
```


### 接收事件

在application层实现DomainEventHandler：  
>注：  
>本扩展使用AOP拦截DomainEventHandler.handle实现方法，增强幂等处理，  
>具体AOP实现参见：[DomainEventHandlerAspect.java](src/main/java/com/luo/d3s/ext/event/jdbc/DomainEventHandlerAspect.java)
```java
@Component
public class StockDeliveryDomainEventHandler implements DomainEventHandler<StockDeliveryEvent> {

    private OrderCommandService orderCommandService;

    public StockDeliveryDomainEventHandler(DomainEventRecordRepository domainEventRecordRepository) {
        super(domainEventRecordRepository);
    }

    @Override
    public void handle(StockDeliveryEvent domainEvent) {
        //TODO 此处可根据DomainEvent调用ApplicationService触发业务逻辑
    }
}
```

在interfaces层实现实际依赖的MQ监听器，将监听到的消息转换为领域事件后，调用EventBus.dispatchBatch发布事件到application层的DomainEventHandler，例如：
```java
@Component
@Slf4j
public class DomainEventListener {

  /**
   * 库存已发货事件监听器
   *
   * @param stockDeliveredEvent 库存已发货事件
   */
  @RabbitListener(queues = {"${spring.rabbitmq.events.stock-delivery.queue.name}"})
  public void on(StockDeliveredEvent stockDeliveredEvent) {
    log.info("[RabbitMq RECV] Stock Delivery: {}", stockDeliveredEvent);
    //分发事件（又或者 直接注入对应的DomainEventHandler进行调用）
    EventBus.dispatchBatch(stockDeliveredEvent);
  }
  
}
 
```