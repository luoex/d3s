package com.luo.d3s.ext.event.jdbc.dataobject;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * 领域事件接收记录数据对象
 *
 * @author luohq
 * @date 2022-12-26 8:45
 */
public class DomainEventRecordDo {

    private String eventId;
    private LocalDateTime recordTime;

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public LocalDateTime getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(LocalDateTime recordTime) {
        this.recordTime = recordTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DomainEventRecordDo that = (DomainEventRecordDo) o;
        return Objects.equals(eventId, that.eventId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(eventId);
    }

    @Override
    public String toString() {
        return "DomainEventRecordDo{" +
                "eventId='" + eventId + '\'' +
                ", recordTime=" + recordTime +
                '}';
    }
}
