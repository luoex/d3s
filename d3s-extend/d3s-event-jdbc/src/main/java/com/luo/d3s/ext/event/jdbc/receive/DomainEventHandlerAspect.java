package com.luo.d3s.ext.event.jdbc.receive;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 领域事件处理器AOP增强 - 幂等实现
 *
 * @author luohq
 * @date 2022-12-26 8:21
 */
@Component
@Aspect
public class DomainEventHandlerAspect {

    @Resource
    private DomainEventIdempotentTxHandler domainEventIdempotentTxHandler;

    /**
     * DomainEventHandler.handle切面处理<br/>
     * 注：此处需保证 事件接收记录入库 和 业务DB处理操作 在同一个事务中，
     * 直接在此方法上添加@Transactional注解并未生效，故单独提取DomainEventIdempotentTxHandler服务处理事务。
     *
     * @param joinPoint 连接点
     * @throws Throwable 抛出异常类型
     */
    @Around("execution(public void com.luo.d3s.core.domain.event.DomainEventHandler+.handle(com.luo.d3s.core.domain.event.DomainEvent+))")
    public void aroundHandle(ProceedingJoinPoint joinPoint) throws Throwable {
        this.domainEventIdempotentTxHandler.proceedWithTx(joinPoint);
    }

}
