package com.luo.d3s.ext.event.jdbc.publish;

import com.luo.d3s.core.domain.event.DomainEvent;
import com.luo.d3s.core.domain.event.DomainEventPublisher;
import com.luo.d3s.core.domain.event.DomainEventType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 内部领域事件发布器
 *
 * @author luohq
 * @date 2023-01-24 15:31
 */
@Component
public class InternalDomainEventPublisher implements DomainEventPublisher {

    private static final Logger log = LoggerFactory.getLogger(InternalDomainEventPublisher.class);

    /**
     * Spring内部事件发布器
     */
    @Resource
    private ApplicationEventPublisher publisher;

    public void publish(DomainEvent domainEvent) {
        log.debug("Publish internal event: {}", domainEvent);
        this.publisher.publishEvent(domainEvent);
    }

    @Override
    public DomainEventType type() {
        return DomainEventType.INTERNAL;
    }
}
