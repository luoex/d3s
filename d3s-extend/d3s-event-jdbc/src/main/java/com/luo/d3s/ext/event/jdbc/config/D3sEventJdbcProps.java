package com.luo.d3s.ext.event.jdbc.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 事件扩展配置属性
 *
 * @author luohq
 * @date 2023-02-01 8:18
 */
@ConfigurationProperties(prefix = D3sEventJdbcProps.PREFIX)
public class D3sEventJdbcProps {
    public static final String PREFIX = "d3s.events";

    /**
     *  定时发送配置
     */
    private PublishProps publish = new PublishProps();

    /**
     * 定时清理配置
     */
    private CleanProps clean = new CleanProps();

    public PublishProps getPublish() {
        return publish;
    }

    public void setPublish(PublishProps publish) {
        this.publish = publish;
    }

    public CleanProps getClean() {
        return clean;
    }

    public void setClean(CleanProps clean) {
        this.clean = clean;
    }

    @Override
    public String toString() {
        return "D3sEventJdbcProps{" +
                "publish=" + publish +
                ", clean=" + clean +
                '}';
    }

    /**
     * 定时发送配置
     */
    public static class PublishProps {

        /**
         * 补偿定时发送时间间隔（单位：毫秒，默认3秒）
         */
        private Long compensateIntervalSeconds = 3L;

        /**
         * 补偿发送查询事件的延迟时间（单位：毫秒，默认5秒）
         */
        private Long compensateDelaySeconds = 5L;

        ///**
        // * 待发送事件查询限制数量SQL（默认Mysql：LIMIT 100，若使用oracle可替换为：AND ROWNUM <= 10）
        // */
        //private String limitSql = "LIMIT 100";


        public Long getCompensateIntervalSeconds() {
            return compensateIntervalSeconds;
        }

        public void setCompensateIntervalSeconds(Long compensateIntervalSeconds) {
            this.compensateIntervalSeconds = compensateIntervalSeconds;
        }

        public Long getCompensateDelaySeconds() {
            return compensateDelaySeconds;
        }

        public void setCompensateDelaySeconds(Long compensateDelaySeconds) {
            this.compensateDelaySeconds = compensateDelaySeconds;
        }

        @Override
        public String toString() {
            return "PublishProps{" +
                    "compensateIntervalSeconds=" + compensateIntervalSeconds +
                    ", compensateDelaySeconds=" + compensateDelaySeconds +
                    '}';
        }
    }

    /**
     * 定时清理配置
     */
    public static class CleanProps {
        /**
         * 是否开启定时清理任务（默认开启）
         */
        private Boolean enabled = true;
        /**
         * 定时清理任务执行时间Cron表达式（默认每天凌晨1点）
         */
        private String cron = "0 0 1 * * ?";
        /**
         * 领域事件发送记录 - 保留天数（默认30天）
         */
        private Integer eventRetentionDays = 30;
        /**
         *  领域事件接收记录 - 保留天数（默认30天）
         */
        private Integer recordRetentionDays = 30;

        public Boolean getEnabled() {
            return enabled;
        }

        public void setEnabled(Boolean enabled) {
            this.enabled = enabled;
        }

        public String getCron() {
            return cron;
        }

        public void setCron(String cron) {
            this.cron = cron;
        }

        public Integer getEventRetentionDays() {
            return eventRetentionDays;
        }

        public void setEventRetentionDays(Integer eventRetentionDays) {
            this.eventRetentionDays = eventRetentionDays;
        }

        public Integer getRecordRetentionDays() {
            return recordRetentionDays;
        }

        public void setRecordRetentionDays(Integer recordRetentionDays) {
            this.recordRetentionDays = recordRetentionDays;
        }

        @Override
        public String toString() {
            return "CleanProps{" +
                    "enabled=" + enabled +
                    ", cron='" + cron + '\'' +
                    ", eventRetentionDays=" + eventRetentionDays +
                    ", recordRetentionDays=" + recordRetentionDays +
                    '}';
        }
    }
}
