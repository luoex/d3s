package com.luo.d3s.ext.event.jdbc.convertor;

import com.luo.d3s.core.domain.event.DomainEvent;
import com.luo.d3s.ext.event.jdbc.dataobject.DomainEventRecordDo;

import java.time.LocalDateTime;


/**
 * 领域事件记录转换器
 *
 * @author luohq
 * @date 2022-12-21 10:30
 */
public class DomainEventRecordConvertor {

    public static DomainEventRecordDo toRecordDo(DomainEvent domainEvent) {
        DomainEventRecordDo domainEventRecordDo = new DomainEventRecordDo();
        domainEventRecordDo.setEventId(String.valueOf(domainEvent.getEventId()));
        domainEventRecordDo.setRecordTime(LocalDateTime.now());
        return domainEventRecordDo;
    }
}
