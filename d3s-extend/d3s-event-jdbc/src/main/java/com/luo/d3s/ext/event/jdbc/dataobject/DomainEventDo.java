package com.luo.d3s.ext.event.jdbc.dataobject;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * 领域事件数据对象
 *
 * @author luohq
 * @date 2022-12-22 13:11
 */
public class DomainEventDo {
    private String id;
    private LocalDateTime createTime;
    private String aggregateId;

    private String jsonType;

    private String jsonContent;

    private Integer status;
    private LocalDateTime updateTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public String getAggregateId() {
        return aggregateId;
    }

    public void setAggregateId(String aggregateId) {
        this.aggregateId = aggregateId;
    }

    public String getJsonType() {
        return jsonType;
    }

    public void setJsonType(String jsonType) {
        this.jsonType = jsonType;
    }

    public String getJsonContent() {
        return jsonContent;
    }

    public void setJsonContent(String jsonContent) {
        this.jsonContent = jsonContent;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DomainEventDo domainEventDo = (DomainEventDo) o;
        return id.equals(domainEventDo.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "DomainEventDo{" +
                "id='" + id + '\'' +
                ", createTime=" + createTime +
                ", aggregateId='" + aggregateId + '\'' +
                ", jsonType='" + jsonType + '\'' +
                ", jsonContent='" + jsonContent + '\'' +
                ", status=" + status +
                ", updateTime=" + updateTime +
                '}';
    }
}
