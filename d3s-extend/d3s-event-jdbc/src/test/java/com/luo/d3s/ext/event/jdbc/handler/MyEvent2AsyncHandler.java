package com.luo.d3s.ext.event.jdbc.handler;

import com.luo.d3s.core.domain.event.DomainEventHandler;
import com.luo.d3s.ext.event.jdbc.event.MyEvent2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;

/**
 * 事件2异步处理器
 *
 * @author luohq
 * @date 2023-01-24 19:53
 */
public class MyEvent2AsyncHandler implements DomainEventHandler<MyEvent2> {
    private static final Logger log = LoggerFactory.getLogger(MyEvent2AsyncHandler.class);

    @Async
    @Override
    public void handle(MyEvent2 domainEvent) {
        log.info("handle async event2 types:{}, {}", domainEvent.getTypes(), domainEvent);
    }
}
