package com.luo.d3s.ext.event.jdbc.handler;

import com.luo.d3s.core.domain.event.DomainEventHandler;
import com.luo.d3s.ext.event.jdbc.event.MyEvent2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 事件2处理器
 *
 * @author luohq
 * @date 2023-01-24 19:53
 */
public class MyEvent2Handler implements DomainEventHandler<MyEvent2> {
    private static final Logger log = LoggerFactory.getLogger(MyEvent2Handler.class);

    @Override
    public void handle(MyEvent2 domainEvent) {
        log.info("handle event2 types:{}, {}", domainEvent.getTypes(), domainEvent);
    }
}
