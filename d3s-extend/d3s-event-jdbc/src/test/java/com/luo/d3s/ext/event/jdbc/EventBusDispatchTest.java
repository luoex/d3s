package com.luo.d3s.ext.event.jdbc;

import com.luo.d3s.ext.event.jdbc.base.BaseSpringTest;
import com.luo.d3s.ext.event.jdbc.event.MyEvent1;
import com.luo.d3s.ext.event.jdbc.event.MyEvent2;
import com.luo.d3s.ext.event.jdbc.event.MyEvent3;
import com.luo.d3s.ext.event.jdbc.handler.*;
import com.luo.d3s.ext.test.utils.InvokeCountStat;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * EventBus分发测试
 *
 * @author luohq
 * @date 2023-01-27 14:00
 */
public class EventBusDispatchTest extends BaseSpringTest {

    @BeforeEach
    void beforeEach() {
        InvokeCountStat.resetInvokeCount();
    }

    @Test
    void testEventBusDispatch() {
        MyEvent1 event1 = new MyEvent1(1L, "I'm event1");
        EventBus.dispatchBatch(event1);

        MyEvent2 event2 = new MyEvent2(2L, "I'm event2");
        EventBus.dispatchBatch(event2);

        MyEvent3 event3 = new MyEvent3(3L, "I'm event3");
        EventBus.dispatchBatch(event3);

        super.sleep5Secs();

        Assertions.assertEquals(1, InvokeCountStat.invokeCount(MyEvent1Handler.class));
        Assertions.assertEquals(1, InvokeCountStat.invokeCount(MyEvent1AsyncHandler.class));
        Assertions.assertEquals(1, InvokeCountStat.invokeCount(MyEvent2Handler.class));
        Assertions.assertEquals(1, InvokeCountStat.invokeCount(MyEvent2AsyncHandler.class));
        Assertions.assertEquals(1, InvokeCountStat.invokeCount(MyEvent3Handler.class));
        Assertions.assertEquals(1, InvokeCountStat.invokeCount(MyEvent3AsyncHandler.class));
    }
}
