package com.luo.d3s.ext.event.jdbc.handler;

import com.luo.d3s.core.domain.event.DomainEventHandler;
import com.luo.d3s.ext.event.jdbc.event.MyEvent1;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 事件1处理器
 *
 * @author luohq
 * @date 2023-01-24 19:53
 */
public class MyEvent1Handler implements DomainEventHandler<MyEvent1> {
    private static final Logger log = LoggerFactory.getLogger(MyEvent1Handler.class);

    @Override
    public void handle(MyEvent1 domainEvent) {
        log.info("handle event1 types:{}, {}", domainEvent.getTypes(), domainEvent);
    }
}
