package com.luo.d3s.ext.event.jdbc;

import com.luo.d3s.ext.component.bus.DomainRegistry;
import com.luo.d3s.ext.event.jdbc.base.BaseSpringTest;
import com.luo.d3s.ext.event.jdbc.event.MyEventRollback;
import com.luo.d3s.ext.event.jdbc.handler.MyEventRollbackHandler;
import com.luo.d3s.ext.test.utils.InvokeCountStat;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

/**
 * 事件消费处理 - 幂等性 - 测试
 *
 * @author luohq
 * @date 2023-02-01 17:10
 */
public class EventHandlerIdempotentTest extends BaseSpringTest {
    private static final Logger log = LoggerFactory.getLogger(EventHandlerIdempotentTest.class);

    @BeforeEach
    void beforeEach() {
        InvokeCountStat.resetInvokeCount();
    }

    @Test
    void publishEvent1() throws Throwable {
        MyEventRollback myEventRollback = new MyEventRollback(1L, "I'm event1");
        log.info("publish myEventRollback types:{}, {}", myEventRollback.getTypes(), myEventRollback);

        IntStream.range(0, 6).forEach(index -> {
            try {
                EventBus.dispatchBatch(myEventRollback);
            } catch (Throwable ex) {
               log.error("EventBus.dispatchBatch ex: {}", ex.getMessage());
            }
        });


        TimeUnit.SECONDS.sleep(5L);
        Assertions.assertEquals(1, DomainRegistry.getBean(MyEventRollbackHandler.class).getCounterSuccess());
    }

}
