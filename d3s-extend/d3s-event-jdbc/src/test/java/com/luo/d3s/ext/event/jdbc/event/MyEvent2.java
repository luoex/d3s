package com.luo.d3s.ext.event.jdbc.event;

import com.luo.d3s.core.domain.event.DomainEvent;
import com.luo.d3s.core.domain.event.DomainEventType;
import com.luo.d3s.core.util.IdGenUtils;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * 事件2
 *
 * @author luohq
 * @date 2023-01-24 19:46
 */
public class MyEvent2 implements DomainEvent<String, Long> {


    private String eventId;

    private Long aggregateRootId;

    private LocalDateTime createTime;

    private String content;

    public MyEvent2() {
    }

    public MyEvent2(Long aggregateRootId, String content) {
        this.aggregateRootId = aggregateRootId;
        this.content = content;
        this.eventId = IdGenUtils.nextTextId();
        this.createTime = LocalDateTime.now();
    }

    @Override
    public String getEventId() {
        return eventId;
    }

    public Long getAggregateRootId() {
        return aggregateRootId;
    }

    @Override
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public String getContent() {
        return content;
    }


    @Override
    public DomainEventType[] getTypes() {
        return new DomainEventType[]{DomainEventType.INTERNAL};
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MyEvent2 myEvent1 = (MyEvent2) o;
        return Objects.equals(eventId, myEvent1.eventId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(eventId);
    }

    @Override
    public String toString() {
        return "MyEvent2{" +
                "eventId='" + eventId + '\'' +
                ", aggregateRootId=" + aggregateRootId +
                ", createTime=" + createTime +
                ", content='" + content + '\'' +
                '}';
    }
}
