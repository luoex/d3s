package com.luo.d3s.ext.event.jdbc.external;

import com.luo.d3s.core.domain.event.DomainEvent;
import com.luo.d3s.core.domain.event.DomainEventPublisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 领域事件发布器实现类
 *
 * @author luohq
 * @date 2023-01-24 19:57
 */
@Component
public class DomainEventPublisherImpl implements DomainEventPublisher {

    private static final Logger log = LoggerFactory.getLogger(DomainEventPublisherImpl.class);

    @Override
    public void publish(DomainEvent event) {
        log.info("publish external event types:{}, {}", event.getTypes(), event);
        //MOCK
        //EventBus.dispatch(event);
    }
}
