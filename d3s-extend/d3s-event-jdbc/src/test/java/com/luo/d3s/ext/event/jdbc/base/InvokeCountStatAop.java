package com.luo.d3s.ext.event.jdbc.base;

import com.luo.d3s.ext.test.utils.InvokeCountStat;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 * 调用计数AOP
 *
 * @author luohq
 * @date 2023-01-27 14:53
 */
@Component
@Aspect
public class InvokeCountStatAop {

    @AfterReturning("execution(public void com.luo.d3s.core.domain.event.DomainEventHandler+.handle(com.luo.d3s.core.domain.event.DomainEvent+))")
    public void afterHandle(JoinPoint joinPoint) throws Throwable {
        //目标类调用次数+1
        InvokeCountStat.invoke(joinPoint.getTarget().getClass());
    }
}
