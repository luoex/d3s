package com.luo.d3s.ext.event.jdbc.handler;

import com.luo.d3s.core.domain.event.DomainEventHandler;
import com.luo.d3s.ext.event.jdbc.event.MyEvent3;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 事件3处理器
 *
 * @author luohq
 * @date 2023-01-24 19:53
 */
public class MyEvent3Handler implements DomainEventHandler<MyEvent3> {
    private static final Logger log = LoggerFactory.getLogger(MyEvent3Handler.class);

    @Override
    public void handle(MyEvent3 domainEvent) {
        log.info("handle event3 types:{}, {}", domainEvent.getTypes(), domainEvent);
    }
}
