package com.luo.d3s.ext.event.jdbc;

import com.luo.d3s.ext.event.jdbc.base.BaseSpringTest;
import com.luo.d3s.ext.event.jdbc.event.MyEvent1;
import com.luo.d3s.ext.event.jdbc.event.MyEvent2;
import com.luo.d3s.ext.event.jdbc.event.MyEvent3;
import com.luo.d3s.ext.event.jdbc.handler.*;
import com.luo.d3s.ext.event.jdbc.publish.DomainEventTxPublisher;
import com.luo.d3s.ext.test.utils.InvokeCountStat;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

/**
 * 领域事件Tx发布器测试
 *
 * @author luohq
 * @date 2023-01-24 19:59
 */
public class DomainEventTxPublisherTest extends BaseSpringTest {
    private static final Logger log = LoggerFactory.getLogger(DomainEventTxPublisherTest.class);

    @Resource
    private DomainEventTxPublisher domainEventTxPublisher;

    @BeforeEach
    void beforeEach() {
        InvokeCountStat.resetInvokeCount();
    }

    @Test
    void publishEvent1() {
        //发布external事件
        MyEvent1 myEvent1 = new MyEvent1(1L, "I'm event1");
        log.info("publish event1 types:{}, {}", myEvent1.getTypes(), myEvent1);
        this.domainEventTxPublisher.saveAndSend(myEvent1);

        super.sleep5Secs();

        Assertions.assertEquals(0, InvokeCountStat.invokeCount(MyEvent1Handler.class));
        Assertions.assertEquals(0, InvokeCountStat.invokeCount(MyEvent1AsyncHandler.class));
    }

    @Test
    void publishEvent2() {
        //发布internal事件
        MyEvent2 myEvent2 = new MyEvent2(2L, "I'm event2");
        log.info("publish event2 types:{}, {}", myEvent2.getTypes(), myEvent2);
        this.domainEventTxPublisher.saveAndSend(myEvent2);

        super.sleep5Secs();

        Assertions.assertEquals(1, InvokeCountStat.invokeCount(MyEvent2Handler.class));
        Assertions.assertEquals(1, InvokeCountStat.invokeCount(MyEvent2AsyncHandler.class));
    }


    @Test
    void publishEvent3() {
        //同时发布internal, external事件
        MyEvent3 myEvent3 = new MyEvent3(3L, "I'm event3");
        log.info("publish event3 types:{}, {}", myEvent3.getTypes(), myEvent3);
        this.domainEventTxPublisher.saveAndSend(myEvent3);

        super.sleep5Secs();

        Assertions.assertEquals(1, InvokeCountStat.invokeCount(MyEvent3Handler.class));
        Assertions.assertEquals(1, InvokeCountStat.invokeCount(MyEvent3AsyncHandler.class));
    }

}
