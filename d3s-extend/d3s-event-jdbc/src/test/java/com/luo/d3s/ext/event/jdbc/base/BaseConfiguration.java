package com.luo.d3s.ext.event.jdbc.base;

import com.luo.d3s.ext.component.scan.D3sComponentScan;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * 基础配置
 *
 * @author luohq
 * @date 2023-01-24 20:09
 */
@EnableAutoConfiguration
@D3sComponentScan(scanBasePackages = {"com.luo.d3s.ext.event.jdbc"})
@EnableAsync
public class BaseConfiguration {
}
