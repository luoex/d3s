-- domain_event definition
DROP TABLE IF EXISTS domain_event;
CREATE TABLE domain_event (
	id char(32) NOT NULL COMMENT '主键ID',
	aggregate_id varchar(64) DEFAULT NULL COMMENT '聚合根ID',
	json_type varchar(256) NOT NULL COMMENT '事件Json类型（对应Java Class名称）',
	json_content varchar(4096) NOT NULL COMMENT '事件Json内容',
	status tinyint(4) NOT NULL COMMENT '事件状态（10已创建，20已发送）',
	create_time datetime NOT NULL COMMENT '创建时间',
	update_time datetime DEFAULT NULL COMMENT '更新时间',
	primary key(id)
) ENGINE = InnoDB;

-- domain_event_record definition
DROP TABLE IF EXISTS domain_event_record;
CREATE TABLE domain_event_record (
	event_id char(32) NOT NULL COMMENT '事件ID',
	record_time datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '记录时间',
	primary key(event_id)
) ENGINE = InnoDB;

-- shedlock definition
DROP TABLE IF EXISTS shedlock;
CREATE TABLE shedlock (
	name VARCHAR(64) NOT NULL,
	lock_until TIMESTAMP(3) NOT NULL,
	locked_at TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
	locked_by VARCHAR(255) NOT NULL,
	PRIMARY KEY (name)
);