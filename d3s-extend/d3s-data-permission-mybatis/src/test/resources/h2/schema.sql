DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
	`id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
	`dept_id` bigint(20) NULL DEFAULT NULL COMMENT '部门ID',
	`user_name` varchar(30) NOT NULL COMMENT '用户账号',
	`nick_name` varchar(30) NOT NULL COMMENT '用户昵称',
	`user_type` varchar(2) NULL DEFAULT '00' COMMENT '用户类型（00系统用户）',
	`email` varchar(50) NULL DEFAULT '' COMMENT '用户邮箱',
	`phonenumber` varchar(11) NULL DEFAULT '' COMMENT '手机号码',
	`sex` char(1) NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
	`avatar` varchar(100) NULL DEFAULT '' COMMENT '头像地址',
	`password` varchar(100) NULL DEFAULT '' COMMENT '密码',
	`status` char(1) NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
	`del_flag` char(1) NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	`login_ip` varchar(128) NULL DEFAULT '' COMMENT '最后登录IP',
	`login_date` datetime(0) NULL DEFAULT NULL COMMENT '最后登录时间',
	`create_by` varchar(64) NULL DEFAULT '' COMMENT '创建者',
	`create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
	`update_by` varchar(64) NULL DEFAULT '' COMMENT '更新者',
	`update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
	`remark` varchar(500) NULL DEFAULT NULL COMMENT '备注',
	PRIMARY KEY (`id`)
) ENGINE = InnoDB ROW_FORMAT = Dynamic;


DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
	`id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
	`parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父部门id',
	`ancestors` varchar(50) NULL DEFAULT '' COMMENT '祖级列表',
	`dept_name` varchar(30) NULL DEFAULT '' COMMENT '部门名称',
	`order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
	`leader` varchar(20) NULL DEFAULT NULL COMMENT '负责人',
	`phone` varchar(11) NULL DEFAULT NULL COMMENT '联系电话',
	`email` varchar(50) NULL DEFAULT NULL COMMENT '邮箱',
	`status` char(1) NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
	`del_flag` char(1) NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	`create_by` varchar(64) NULL DEFAULT '' COMMENT '创建者',
	`create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
	`update_by` varchar(64) NULL DEFAULT '' COMMENT '更新者',
	`update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB ROW_FORMAT = Dynamic;


DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
    `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
	`user_id` bigint(20) NOT NULL COMMENT '用户ID',
	`role_id` bigint(20) NOT NULL COMMENT '角色ID',
	PRIMARY KEY (`id`)
) ENGINE = InnoDB ROW_FORMAT = Dynamic;


DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
	`id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
	`role_name` varchar(30) NOT NULL COMMENT '角色名称',
	`role_key` varchar(100) NOT NULL COMMENT '角色权限字符串',
	`role_sort` int(4) NOT NULL COMMENT '显示顺序',
	`data_scope` char(1) NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
	`menu_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '菜单树选择项是否关联显示',
	`dept_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '部门树选择项是否关联显示',
	`status` char(1) NOT NULL COMMENT '角色状态（0正常 1停用）',
	`del_flag` char(1) NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	`create_by` varchar(64) NULL DEFAULT '' COMMENT '创建者',
	`create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
	`update_by` varchar(64) NULL DEFAULT '' COMMENT '更新者',
	`update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
	`remark` varchar(500) NULL DEFAULT NULL COMMENT '备注',
	PRIMARY KEY (`id`)
) ENGINE = InnoDB ROW_FORMAT = Dynamic;


DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB ROW_FORMAT = Dynamic;
