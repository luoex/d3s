INSERT INTO `sys_user`
(`id`, `dept_id`, `user_name`, `nick_name`, `user_type`, `email`,       `phonenumber`, `sex`,  `avatar`, `password`,                                                     `status`, `del_flag`, `login_ip`,     `login_date`,           `create_by`, `create_time`,          `update_by`, `update_time`, `remark`) VALUES
(1,     103,      'admin',      '管理员',     '00',        'ry@163.com', '15888888888', '1',     '',      '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2',   '0',     '0',        '127.0.0.1',    '2023-06-23 20:06:06',  'admin',     '2023-06-23 20:06:06',   '',          NULL,         '管理员'),
(2,     105,      'user1',      '普通用户1',  '00',        'ry@qq.com',   '15666666666', '1',    '',      '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2',   '0',      '0',        '127.0.0.1',    '2023-06-23 20:06:06',  'admin',     '2023-06-23 20:06:06',   '',          NULL,         '普通用户1'),
(3,     108,      'user2',      '普通用户2',  '00',        'ry@qq.com',   '15666666666', '1',    '',      '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2',   '0',      '0',        '127.0.0.1',    '2023-06-23 20:06:06',  'admin',     '2023-06-23 20:06:06',   '',          NULL,         '普通用户2'),
(4,     108,      'user3',      '普通用户3',  '00',        'ry@qq.com',   '15666666666', '1',    '',      '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2',   '0',      '0',        '127.0.0.1',    '2023-06-23 20:06:06',  'admin',     '2023-06-23 20:06:06',   '',          NULL,         '普通用户3'),
(5,     108,      'user4',      '普通用户4',  '00',        'ry@qq.com',   '15666666666', '1',    '',      '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2',   '0',      '0',        '127.0.0.1',    '2023-06-23 20:06:06',  'admin',     '2023-06-23 20:06:06',   '',          NULL,         '普通用户4'),
(6,     108,      'user5',      '普通用户5',  '00',        'ry@qq.com',   '15666666666', '1',    '',      '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2',   '0',      '0',        '127.0.0.1',    '2023-06-23 20:06:06',  'admin',     '2023-06-23 20:06:06',   '',          NULL,         '普通用户5'),
(7,     108,      'user6',      '普通用户6',  '00',        'ry@qq.com',   '15666666666', '1',    '',      '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2',   '0',      '0',        '127.0.0.1',    '2023-06-23 20:06:06',  'admin',     '2023-06-23 20:06:06',   '',          NULL,         '普通用户6'),
(8,     108,      'user7',      '普通用户7',  '00',        'ry@qq.com',   '15666666666', '0',    '',      '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2',   '0',      '0',        '127.0.0.1',    '2023-06-23 20:06:06',  'admin',     '2023-06-23 20:06:06',   '',          NULL,         '普通用户7'),
(9,     108,      'user8',      '普通用户8',  '00',        'ry@qq.com',   '15666666666', '0',    '',      '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2',   '0',      '0',        '127.0.0.1',    '2023-06-23 20:06:06',  'admin',     '2023-06-23 20:06:06',   '',          NULL,         '普通用户8'),
(10,    109,      'user9',      '普通用户9',  '00',        'ry@qq.com',   '15666666666', '0',    '',      '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2',   '0',      '0',        '127.0.0.1',    '2023-06-23 20:06:06',  'admin',     '2023-06-23 20:06:06',   '',          NULL,         '普通用户9'),
(11,    109,      'user10',     '普通用户10', '00',        'ry@qq.com',   '15666666666', '0',    '',      '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2',   '0',      '0',        '127.0.0.1',    '2023-06-23 20:06:06',  'admin',     '2023-06-23 20:06:06',   '',          NULL,         '普通用户10'),
(12,    109,      'user11',     '普通用户11', '00',        'ry@qq.com',   '15666666666', '0',    '',      '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2',   '0',      '0',        '127.0.0.1',    '2023-06-23 20:06:06',  'admin',     '2023-06-23 20:06:06',   '',          NULL,         '普通用户11'),
(13,    109,      'user12',     '普通用户12', '00',        'ry@qq.com',   '15666666666', '0',    '',      '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2',   '0',      '0',        '127.0.0.1',    '2023-06-23 20:06:06',  'admin',     '2023-06-23 20:06:06',   '',          NULL,         '普通用户12');


INSERT INTO
`sys_dept`
(`id`, `parent_id`, `ancestors`, `dept_name`, `order_num`, `leader`, `phone`,       `email`,    `status`, `del_flag`, `create_by`, `create_time`,           `update_by`, `update_time`) VALUES
(100,   0,          '0',         'Bliss依科技', 0,          '罗小爬',  '15888888888', 'ry@qq.com', '0',      '0',       'admin',      '2023-06-23 20:06:06',   '',           NULL),
(101,   100,        '0,100',     '深圳总公司',   1,          '罗小爬',  '15888888888', 'ry@qq.com', '0',      '0',       'admin',      '2023-06-23 20:06:06',   '',           NULL),
(102,   100,        '0,100',     '长沙分公司',   2,          '罗小爬',  '15888888888', 'ry@qq.com', '0',      '0',       'admin',      '2023-06-23 20:06:06',   '',           NULL),
(103,   101,        '0,100,101', '研发部门',     1,          '罗小爬',  '15888888888', 'ry@qq.com', '0',      '0',       'admin',      '2023-06-23 20:06:06',   '',           NULL),
(104,   101,        '0,100,101', '市场部门',     2,          '罗小爬',  '15888888888', 'ry@qq.com', '0',      '0',       'admin',      '2023-06-23 20:06:06',   '',           NULL),
(105,   101,        '0,100,101', '测试部门',     3,          '罗小爬',  '15888888888', 'ry@qq.com', '0',      '0',       'admin',      '2023-06-23 20:06:06',   '',           NULL),
(106,   101,        '0,100,101', '财务部门',     4,          '罗小爬',  '15888888888', 'ry@qq.com', '0',      '0',       'admin',      '2023-06-23 20:06:06',   '',           NULL),
(107,   101,        '0,100,101', '运维部门',     5,          '罗小爬',  '15888888888', 'ry@qq.com', '0',      '0',       'admin',      '2023-06-23 20:06:06',   '',           NULL),
(108,   102,        '0,100,102', '市场部门',     1,          '罗小爬',  '15888888888', 'ry@qq.com', '0',      '0',       'admin',      '2023-06-23 20:06:06',   '',           NULL),
(109,   102,        '0,100,102', '财务部门',     2,          '罗小爬',  '15888888888', 'ry@qq.com', '0',      '0',       'admin',      '2023-06-23 20:06:06',   '',           NULL);


INSERT INTO `sys_user_role`(`id`, `user_id`, `role_id`)
VALUES
(1, 1, 1),
(2, 2, 2);


INSERT INTO `sys_role`
(`id`, `role_name`, `role_key`, `role_sort`, `data_scope`, `menu_check_strictly`, `dept_check_strictly`, `status`, `del_flag`, `create_by`, `create_time`,         `update_by`, `update_time`, `remark`) VALUES
(1,    '超级管理员',  'admin',     1,          '1',           1,                     1,                    '0',       '0',       'admin',     '2023-06-23 20:06:06',  '',          NULL,       '超级管理员'),
(2,    '普通角色',    'common',    2,          '2',          1,                      1,                    '0',       '0',       'admin',     '2023-06-23 20:06:06',  '',          NULL,       '普通角色');


INSERT INTO `sys_role_dept`(`id`, `role_id`, `dept_id`)
VALUES
(1, 2, 100),
(2, 2, 101),
(3, 2, 105);







