package com.luo.d3s.ext.dp.sample;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.luo.d3s.ext.data.permission.config.DataPermissionProps;
import com.luo.d3s.ext.data.permission.context.DpUserContextHolder;
import com.luo.d3s.ext.data.permission.dto.context.DpPropDto;
import com.luo.d3s.ext.data.permission.dto.context.UserPropDto;
import com.luo.d3s.ext.data.permission.dto.context.UserSupplierDto;
import com.luo.d3s.ext.data.permission.dto.context.base.BaseDpDto;
import com.luo.d3s.ext.data.permission.enums.DpOpEnum;
import com.luo.d3s.ext.data.permission.enums.DpTypes;
import com.luo.d3s.ext.data.permission.exception.DataPermissionException;
import com.luo.d3s.ext.dp.sample.config.TestConfig;
import com.luo.d3s.ext.dp.sample.dao.SysUserMapper;
import com.luo.d3s.ext.dp.sample.entity.SysUser;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.condition.DisabledIf;
import org.junit.jupiter.api.condition.EnabledIf;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mybatis.spring.MyBatisSystemException;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;
import java.util.*;

/**
 * 数据权限测试
 *
 * @author luohq
 * @date 2023-06-23 19:48
 */
@Slf4j
@SpringBootTest
@ContextConfiguration(classes = {TestConfig.class})
@TestMethodOrder(MethodOrderer.MethodName.class)
public class SyUserMapperTest {

    @Resource
    SysUserMapper sysUserMapper;

    @Resource
    private DataPermissionProps dpProps;

    public static final Long USER_ID_WITH_DP_USER = 2L;
    public static final Long USER_ID_WITHOUT_DP_USER = 1L;

    @BeforeEach
    void initDpUserContext() {
        this.mockUser();
    }

    private void mockUser() {
        Collection<BaseDpDto> dpCollection = Arrays.asList(
                DpPropDto.ofType(DpTypes.USER),
                DpPropDto.ofType(DpTypes.DEPT_AND_CHILD),
                DpPropDto.ofTypeAndManageIds(DpTypes.DEPT_CUSTOM, Arrays.asList(100L, 101L, 105L), null)
        );
        //子部门未设置，则默认为{0}
        UserPropDto user = UserPropDto.ofUserDeptAndDataPermissions(USER_ID_WITH_DP_USER, 105L, dpCollection);
        //设置数据权限用户上下文
        DpUserContextHolder.setContext(user);
    }

    private void mockUser_withDP_ADMIN() {
        Collection<BaseDpDto> dpCollection = Arrays.asList(
                DpPropDto.ofType(DpTypes.ADMIN)
        );
        //子部门未设置，则默认为{0}
        UserPropDto user = UserPropDto.ofUserDeptAndDataPermissions(USER_ID_WITH_DP_USER, 105L, dpCollection);
        //设置数据权限用户上下文
        DpUserContextHolder.setContext(user);
    }

    private void mockUser_withoutAnyDP() {
        Collection<BaseDpDto> dpCollection = Collections.emptySet();

        //子部门未设置，则默认为{0}
        UserPropDto user = UserPropDto.ofUserDeptAndDataPermissions(USER_ID_WITH_DP_USER, 105L, dpCollection);
        //设置数据权限用户上下文
        DpUserContextHolder.setContext(user);
    }


    private void mockUser_withDP_DEPT_AND_CHILD() {
        Collection<BaseDpDto> dpCollection = Arrays.asList(
                DpPropDto.ofType(DpTypes.USER),
                DpPropDto.ofType(DpTypes.DEPT_AND_CHILD),
                DpPropDto.ofTypeAndManageIds(DpTypes.DEPT_CUSTOM, Arrays.asList(100L, 101L, 105L), null)
        );
        //设置用户子部门
        Collection<Long> deptAndChildDeptIds = Arrays.asList(101L, 102L, 103L, 104L, 105L, 106L, 107L);
        UserPropDto user = UserPropDto.ofAll(USER_ID_WITH_DP_USER, 105L, deptAndChildDeptIds, dpCollection);
        //设置数据权限用户上下文
        DpUserContextHolder.setContext(user);
    }

    void mockUser_withSupplier() {
        Collection<BaseDpDto> dpCollection = Arrays.asList(
                DpPropDto.ofType(DpTypes.USER),
                DpPropDto.ofType(DpTypes.DEPT_AND_CHILD),
                DpPropDto.ofTypeAndManageIds(DpTypes.DEPT_CUSTOM, Arrays.asList(100L, 101L, 105L), null)
        );
        UserPropDto user = UserPropDto.ofUserDeptAndDataPermissions(USER_ID_WITH_DP_USER, 105L, dpCollection);
        UserSupplierDto userSupplier = UserSupplierDto.fromProp(user);
        userSupplier.setDeptAndChildDeptIdsSupplier(() -> {
            String userId = userSupplier.getUserId();
            return Arrays.asList(101L, 102L, 103L, 104L, 105L, 106L, 107L);
        });

        //设置数据权限用户上下文
        DpUserContextHolder.setContext(user);
    }

    private void mockUser_withOperations_SELECT() {
        Collection<BaseDpDto> dpCollection = Arrays.asList(
                DpPropDto.ofTypeAndOperations(DpTypes.USER, Arrays.asList(DpOpEnum.SELECT)),
                DpPropDto.ofTypeAndOperations(DpTypes.DEPT_AND_CHILD, Arrays.asList(DpOpEnum.SELECT)),
                DpPropDto.ofAll(DpTypes.DEPT_CUSTOM, Arrays.asList(DpOpEnum.SELECT), Arrays.asList(100L, 101L, 105L), null)
        );
        //子部门未设置，则默认为{0}
        UserPropDto user = UserPropDto.ofUserDeptAndDataPermissions(USER_ID_WITH_DP_USER, 105L, dpCollection);
        //设置数据权限用户上下文
        DpUserContextHolder.setContext(user);
    }

    /**
     * 无数据权限时，是否抛出异常
     *
     * @return 是否抛出异常
     */
    boolean throwExceptionWhenNoDataPermission() {
        return this.dpProps.getThrowExceptionWhenNoDataPermission();
    }

    @RepeatedTest(2)
    void testSysUserMapper0100_findById_withDP_USER() {
        //仅允许查询自己的用户信息
        SysUser sysUser = this.sysUserMapper.findById(USER_ID_WITH_DP_USER);
        log.info("[WITH DP USER] sysUserMapper.findById: {}", sysUser);
        Assertions.assertNotNull(sysUser);
    }

    @RepeatedTest(2)
    void testSysUserMapper0110_findById_withoutDP_USER() {
        //仅允许查询自己的用户信息
        SysUser sysUser = this.sysUserMapper.findById(USER_ID_WITHOUT_DP_USER);
        log.info("[WITHOUT DP USER] sysUserMapper.findById: {}", sysUser);
        Assertions.assertNull(sysUser);
    }


    @RepeatedTest(2)
    void testSysUserMapper0200_selectById_withDP_USER() {
        //仅允许查询自己的用户信息
        SysUser sysUser = this.sysUserMapper.selectById(USER_ID_WITH_DP_USER);
        log.info("[WITH DP USER] sysUserMapper.selectById: {}", sysUser);
        Assertions.assertNotNull(sysUser);
    }

    @RepeatedTest(2)
    void testSysUserMapper0210_selectById_withoutDP_USER() {
        //不允许查询别人的用户信息
        SysUser sysUser = this.sysUserMapper.selectById(USER_ID_WITHOUT_DP_USER);
        log.info("[WITHOUT DP USER] sysUserMapper.selectById: {}", sysUser);
        Assertions.assertNull(sysUser);
    }

    @Test
    @DisabledIf("throwExceptionWhenNoDataPermission")
    void testSysUserMapper0220_selectById_withoutAnyDP() {
        this.mockUser_withoutAnyDP();

        //不允许查询任何的用户信息
        SysUser sysUser = this.sysUserMapper.selectById(USER_ID_WITH_DP_USER);
        log.info("[WITHOUT ANY DP] sysUserMapper.selectById: {}", sysUser);
        Assertions.assertNull(sysUser);
    }

    @Test
    @EnabledIf("throwExceptionWhenNoDataPermission")
    void testSysUserMapper0220_selectById_withoutAnyDP_throwEx() {
        this.mockUser_withoutAnyDP();

        //不允许查询任何的用户信息
        MyBatisSystemException myBatisSystemException = Assertions.assertThrows(
                MyBatisSystemException.class,
                () -> {
                    //不允许查询任何的用户信息
                    SysUser sysUser = this.sysUserMapper.selectById(USER_ID_WITH_DP_USER);
                });
        DataPermissionException dataPermissionException = this.extractDpException(myBatisSystemException);
        log.error("[WITHOUT ANY DP] sysUserMapper.selectById: {}", dataPermissionException.getMessage(), dataPermissionException);
    }

    @Test
    void testSysUserMapper0300_updateById_withDP_USER() {
        //仅允许更新自己的用户信息
        SysUser sysUser = new SysUser();
        sysUser.setId(USER_ID_WITH_DP_USER);
        sysUser.setUserName("user_haha");
        Integer retCount = this.sysUserMapper.updateById(sysUser);
        log.info("[WITH DP USER] sysUserMapper.updateById result: {}", retCount);
        Assertions.assertEquals(1, retCount);
    }


    @Test
    @DisabledIf("throwExceptionWhenNoDataPermission")
    void testSysUserMapper0305_updateById_withOP_SELECT() {
        this.mockUser_withOperations_SELECT();

        //不允许更新操作
        SysUser sysUser = new SysUser();
        sysUser.setId(USER_ID_WITH_DP_USER);
        sysUser.setUserName("user_haha");
        Integer retCount = this.sysUserMapper.updateById(sysUser);
        log.info("[WITH OP SELECT] sysUserMapper.updateById result: {}", retCount);
        Assertions.assertEquals(0, retCount);
    }

    @Test
    @EnabledIf("throwExceptionWhenNoDataPermission")
    void testSysUserMapper0305_updateById_withOP_SELECT_throwEx() {
        this.mockUser_withOperations_SELECT();

        MyBatisSystemException myBatisSystemException = Assertions.assertThrows(
                MyBatisSystemException.class,
                () -> {
                    //不允许更新操作
                    SysUser sysUser = new SysUser();
                    sysUser.setId(USER_ID_WITH_DP_USER);
                    sysUser.setUserName("user_haha");
                    Integer retCount = this.sysUserMapper.updateById(sysUser);
                });
        DataPermissionException dataPermissionException = this.extractDpException(myBatisSystemException);
        log.error("[WITH OP SELECT] sysUserMapper.updateById: {}", dataPermissionException.getMessage(), dataPermissionException);
    }

    @Test
    void testSysUserMapper0310_updateById_withoutDP_USER() {
        //不允许更新别人的用户信息
        SysUser sysUser = new SysUser();
        sysUser.setId(USER_ID_WITHOUT_DP_USER);
        sysUser.setUserName("user_haha");
        Integer retCount = this.sysUserMapper.updateById(sysUser);
        log.info("[WITHOUT DP USER] sysUserMapper.updateById result: {}", retCount);
        Assertions.assertEquals(0, retCount);
    }

    @Disabled
    @Test
    void testSysUserMapper0400_deleteById_withDP_USER() {
        //仅允许删除自己的用户信息
        Integer retCount = this.sysUserMapper.deleteById(USER_ID_WITH_DP_USER);
        log.info("[WITH DP USER] sysUserMapper.deleteById result: {}", retCount);
        Assertions.assertEquals(1, retCount);
    }

    @Test
    void testSysUserMapper0410_deleteById_withoutDP_USER() {
        //不允许删除其他用户的用户信息
        Integer retCount = this.sysUserMapper.deleteById(USER_ID_WITHOUT_DP_USER);
        log.info("[WITHOUT DP USER] sysUserMapper.deleteById result: {}", retCount);
        Assertions.assertEquals(0, retCount);
    }
    
    @Test
    @DisabledIf("throwExceptionWhenNoDataPermission")
    void testSysUserMapper0415_deleteById_withoutOP_SELECT() {
        this.mockUser_withOperations_SELECT();
        
        //不允许删除操作
        Integer retCount = this.sysUserMapper.deleteById(USER_ID_WITHOUT_DP_USER);
        log.info("[WITH OP SELECT] sysUserMapper.deleteById result: {}", retCount);
        Assertions.assertEquals(0, retCount);
    }

    @Test
    @EnabledIf("throwExceptionWhenNoDataPermission")
    void testSysUserMapper0415_deleteById_withoutOP_SELECT_throwEx() {
        this.mockUser_withOperations_SELECT();

        MyBatisSystemException myBatisSystemException = Assertions.assertThrows(
                MyBatisSystemException.class, 
                () -> {
                    //不允许删除操作
                    Integer retCount = this.sysUserMapper.deleteById(USER_ID_WITHOUT_DP_USER);
        });
        DataPermissionException dataPermissionException = this.extractDpException(myBatisSystemException);
        log.error("[WITH OP SELECT] sysUserMapper.deleteById: {}", dataPermissionException.getMessage(), dataPermissionException);
    }
    
    @Test
    void testSysUserMapper0500_deleteBatchIds_withoutDP_USER() {
        //不允许批量删除其他用户的用户信息
        Integer retCount = this.sysUserMapper.deleteBatchIds(Arrays.asList(USER_ID_WITHOUT_DP_USER));
        log.info("[WITHOUT DP USER] sysUserMapper.deleteBatchIds result: {}", retCount);
        Assertions.assertEquals(0, retCount);
    }

    @Test
    @DisabledIf("throwExceptionWhenNoDataPermission")
    void testSysUserMapper0525_deleteBatchIds_withoutAnyDP() {
        this.mockUser_withoutAnyDP();

        //不允许批量删除任何用户的用户信息
        Integer retCount = this.sysUserMapper.deleteBatchIds(Arrays.asList(USER_ID_WITHOUT_DP_USER));
        log.info("[WITHOUT ANY DP] sysUserMapper.deleteBatchIds result: {}", retCount);
        Assertions.assertEquals(0, retCount);
    }
    @Test
    @EnabledIf("throwExceptionWhenNoDataPermission")
    void testSysUserMapper0525_deleteBatchIds_withoutAnyDP_throwEx() {
        this.mockUser_withoutAnyDP();

        //没有数据权限
        MyBatisSystemException myBatisSystemException = Assertions.assertThrows(
                MyBatisSystemException.class,
                () -> {
                    //不允许批量删除任何用户的用户信息
                    Integer retCount = this.sysUserMapper.deleteBatchIds(Arrays.asList(USER_ID_WITHOUT_DP_USER));
                });
        DataPermissionException dataPermissionException = this.extractDpException(myBatisSystemException);
        log.error("[WITHOUT ANY DP] sysUserMapper.deleteBatchIds: {}", dataPermissionException.getMessage(), dataPermissionException);
    }

    @Test
    void testSysUserMapper0600_selectPage_withDP_USER() {
        //仅允许查询自己的用户信息
        Page pageParam = Page.of(1, 10);
        OrderItem orderItem = OrderItem.desc("create_time");
        pageParam.addOrder(orderItem);
        Page pageResult = this.sysUserMapper.selectPage(pageParam, Wrappers.<SysUser>lambdaQuery()
                .eq(SysUser::getStatus, 0));
        log.info("[WITH DP USER] sysUserMapper.selectPage: total={}, pages={}, records={}", pageResult.getTotal(), pageResult.getPages(), pageResult.getRecords());
        Assertions.assertEquals(1, pageResult.getTotal());
        Assertions.assertEquals(1, pageResult.getRecords().size());
    }

    @Test
    @DisabledIf("throwExceptionWhenNoDataPermission")
    void testSysUserMapper0610_selectPage_withoutAnyDP() {
        this.mockUser_withoutAnyDP();

        //不允许查询任何的用户信息
        Page pageParam = Page.of(1, 10);
        OrderItem orderItem = OrderItem.desc("create_time");
        pageParam.addOrder(orderItem);
        Page pageResult = this.sysUserMapper.selectPage(pageParam, Wrappers.<SysUser>lambdaQuery()
                .eq(SysUser::getStatus, 0));
        log.info("[WITHOUT ANY DP] sysUserMapper.selectPage: total={}, pages={}, records={}", pageResult.getTotal(), pageResult.getPages(), pageResult.getRecords());
        Assertions.assertEquals(0, pageResult.getTotal());
        Assertions.assertEquals(0, pageResult.getRecords().size());
    }

    @Test
    @EnabledIf("throwExceptionWhenNoDataPermission")
    void testSysUserMapper0610_selectPage_withoutAnyDP_throwEx() {
        this.mockUser_withoutAnyDP();

        //没有数据权限
        MyBatisSystemException myBatisSystemException = Assertions.assertThrows(
                MyBatisSystemException.class,
                () -> {
                    //不允许查询任何的用户信息
                    Page pageParam = Page.of(1, 10);
                    OrderItem orderItem = OrderItem.desc("create_time");
                    pageParam.addOrder(orderItem);
                    Page pageResult = this.sysUserMapper.selectPage(pageParam, Wrappers.<SysUser>lambdaQuery()
                            .eq(SysUser::getStatus, 0));
                });
        DataPermissionException dataPermissionException = this.extractDpException(myBatisSystemException);
        log.error("[WITHOUT ANY DP] sysUserMapper.selectPage: {}", dataPermissionException.getMessage(), dataPermissionException);
    }

    @Test
    void testSysUserMapper0620_selectPage_withDP_ADMIN() {
        //重置用户为超级管理员
        this.mockUser_withDP_ADMIN();

        //允许查询全部的用户信息
        Page pageParam = Page.of(1, 10);
        OrderItem orderItem = OrderItem.desc("create_time");
        pageParam.addOrder(orderItem);
        Page pageResult = this.sysUserMapper.selectPage(pageParam, Wrappers.<SysUser>lambdaQuery()
                .eq(SysUser::getStatus, 0));
        log.info("[WITH DP ADMIN] sysUserMapper.selectPage: total={}, pages={}, records={}", pageResult.getTotal(), pageResult.getPages(), pageResult.getRecords());
        Assertions.assertEquals(13, pageResult.getTotal());
        Assertions.assertEquals(10, pageResult.getRecords().size());
    }

    @Test
    void testSysUserMapper0630_selectPage_withDP_USER__DEPT_AND_CHILD() {
        //重置用户，该用户设置了部门及子部门
        this.mockUser_withDP_DEPT_AND_CHILD();

        //仅允许查询自己及子部门的用户信息
        Page pageParam = Page.of(1, 10);
        OrderItem orderItem = OrderItem.desc("create_time");
        pageParam.addOrder(orderItem);
        Page pageResult = this.sysUserMapper.selectPage(pageParam, Wrappers.<SysUser>lambdaQuery()
                .eq(SysUser::getStatus, 0));
        log.info("[WITH DP USER, DEPT_AND_CHILD] sysUserMapper.selectPage: total={}, pages={}, records={}", pageResult.getTotal(), pageResult.getPages(), pageResult.getRecords());
        Assertions.assertEquals(2, pageResult.getTotal());
        Assertions.assertEquals(2, pageResult.getRecords().size());
    }

    @Test
    void testSysUserMapper0700_findPageWithPageHelper_withDP_USER() {
        //仅允许查询自己的用户信息
        PageInfo<SysUser> pageInfo = this.sysUserMapper.findPageWithPageHelper(1, 10);
        log.info("[WITH DP USER] sysUserMapper.findPageWithPageHelper: total={}, pages={}, list={}", pageInfo.getTotal(), pageInfo.getPages(), pageInfo.getList());
        Assertions.assertEquals(1, pageInfo.getTotal());
        Assertions.assertEquals(1, pageInfo.getList().size());
    }

    @Test
    @DisabledIf("throwExceptionWhenNoDataPermission")
    void testSysUserMapper0710_findPageWithPageHelper_withoutAndyDP() {
        this.mockUser_withoutAnyDP();

        //不允许查询任何的用户信息
        PageInfo<SysUser> pageInfo = this.sysUserMapper.findPageWithPageHelper(1, 10);
        log.info("[WITHOUT ANY DP] sysUserMapper.findPageWithPageHelper: total={}, pages={}, list={}", pageInfo.getTotal(), pageInfo.getPages(), pageInfo.getList());
        Assertions.assertEquals(0, pageInfo.getTotal());
        Assertions.assertEquals(0, pageInfo.getList().size());
    }

    @Test
    @EnabledIf("throwExceptionWhenNoDataPermission")
    void testSysUserMapper0710_findPageWithPageHelper_withoutAndyDP_throwEx() {
        this.mockUser_withoutAnyDP();

        //没有数据权限
        MyBatisSystemException myBatisSystemException = Assertions.assertThrows(
                MyBatisSystemException.class,
                () -> {
                    //不允许查询任何的用户信息
                    PageInfo<SysUser> pageInfo = this.sysUserMapper.findPageWithPageHelper(1, 10);
                });
        DataPermissionException dataPermissionException = this.extractDpException(myBatisSystemException);
        log.error("[WITHOUT ANY DP] sysUserMapper.findPageWithPageHelper: {}", dataPermissionException.getMessage(), dataPermissionException);
    }

    @Test
    void testSysUserMapper0720_findPageWithPageHelper_withDP_ADMIN() {
        //重置用户为超级管理员
        this.mockUser_withDP_ADMIN();

        //允许查询全部的用户信息
        PageInfo<SysUser> pageInfo = this.sysUserMapper.findPageWithPageHelper(1, 10);
        log.info("[WITH DP ADMIN] sysUserMapper.findPageWithPageHelper: total={}, pages={}, list={}", pageInfo.getTotal(), pageInfo.getPages(), pageInfo.getList());
        Assertions.assertEquals(13, pageInfo.getTotal());
        Assertions.assertEquals(10, pageInfo.getList().size());
    }

    @Test
    void testSysUserMapper0730_findPageWithPageHelper_withDP_USER__DEPT_AND_CHILD() {
        //重置用户，该用户设置了部门及子部门
        this.mockUser_withDP_DEPT_AND_CHILD();

        //仅允许查询自己及子部门的用户信息
        PageInfo<SysUser> pageInfo = this.sysUserMapper.findPageWithPageHelper(1, 10);
        log.info("[WITH DP USER, DEPT_AND_CHILD] sysUserMapper.findPageWithPageHelper: total={}, pages={}, list={}", pageInfo.getTotal(), pageInfo.getPages(), pageInfo.getList());
        Assertions.assertEquals(2, pageInfo.getTotal());
        Assertions.assertEquals(2, pageInfo.getList().size());
    }

    @Test
    void testSysUserMapper0800_findPageWithPageHelper_dynamicSql_withDP_USER() {
        //仅允许查询自己的用户信息
        String userName = "user";
        //开启分页
        PageHelper.startPage(1, 10);
        List<SysUser> userList = this.sysUserMapper.findPageWithPageHelper_dynamicSql(userName);
        PageInfo<SysUser> pageInfo = new PageInfo<>(userList);
        log.info("[WITH DP USER] sysUserMapper.findPageWithPageHelper_dynamicSql: total={}, pages={}, list={}", pageInfo.getTotal(), pageInfo.getPages(), pageInfo.getList());
        Assertions.assertEquals(1, pageInfo.getTotal());
        Assertions.assertEquals(1, pageInfo.getList().size());
    }

    @Test
    @DisabledIf("throwExceptionWhenNoDataPermission")
    void testSysUserMapper0810_findPageWithPageHelper_dynamicSql_withoutAnyDP() {
        this.mockUser_withoutAnyDP();

        //仅允许查询自己的用户信息
        String userName = "user";
        //开启分页
        PageHelper.startPage(1, 10);
        List<SysUser> userList = this.sysUserMapper.findPageWithPageHelper_dynamicSql(userName);
        PageInfo<SysUser> pageInfo = new PageInfo<>(userList);
        log.info("[WITHOUT ANY DP] sysUserMapper.findPageWithPageHelper_dynamicSql: total={}, pages={}, list={}", pageInfo.getTotal(), pageInfo.getPages(), pageInfo.getList());
        Assertions.assertEquals(0, pageInfo.getTotal());
        Assertions.assertEquals(0, pageInfo.getList().size());
    }

    @Test
    @EnabledIf("throwExceptionWhenNoDataPermission")
    void testSysUserMapper0810_findPageWithPageHelper_dynamicSql_withoutAnyDP_throwEx() {
        this.mockUser_withoutAnyDP();

        //没有数据权限
        MyBatisSystemException myBatisSystemException = Assertions.assertThrows(
                MyBatisSystemException.class,
                () -> {
                    //仅允许查询自己的用户信息
                    String userName = "user";
                    //开启分页
                    PageHelper.startPage(1, 10);
                    List<SysUser> userList = this.sysUserMapper.findPageWithPageHelper_dynamicSql(userName);
                });
        DataPermissionException dataPermissionException = this.extractDpException(myBatisSystemException);
        log.error("[WITHOUT ANY DP] sysUserMapper.findPageWithPageHelper_dynamicSql: {}", dataPermissionException.getMessage(), dataPermissionException);
    }

    @Test
    void testSysUserMapper0820_findPageWithPageHelper_dynamicSql_withDP_ADMIN() {
        //重置用户为超级管理员
        this.mockUser_withDP_ADMIN();

        //仅允许查询自己的用户信息
        String userName = "user";
        //开启分页
        PageHelper.startPage(1, 10);
        List<SysUser> userList = this.sysUserMapper.findPageWithPageHelper_dynamicSql(userName);
        PageInfo<SysUser> pageInfo = new PageInfo<>(userList);
        log.info("[WITH DP ADMIN] sysUserMapper.findPageWithPageHelper_dynamicSql: total={}, pages={}, list={}", pageInfo.getTotal(), pageInfo.getPages(), pageInfo.getList());
        Assertions.assertEquals(12, pageInfo.getTotal());
        Assertions.assertEquals(10, pageInfo.getList().size());
    }

    @Test
    void testSysUserMapper0830_findPageWithPageHelper_dynamicSql_withDP_USER__DEPT_AND_CHILD() {
        //重置用户，该用户设置了部门及子部门
        this.mockUser_withDP_DEPT_AND_CHILD();

        //仅允许查询自己的用户信息
        String userName = "user";
        //开启分页
        PageHelper.startPage(1, 10);
        List<SysUser> userList = this.sysUserMapper.findPageWithPageHelper_dynamicSql(userName);
        PageInfo<SysUser> pageInfo = new PageInfo<>(userList);
        log.info("[WITH DP USER, DEPT_AND_CHILD] sysUserMapper.findPageWithPageHelper_dynamicSql: total={}, pages={}, list={}", pageInfo.getTotal(), pageInfo.getPages(), pageInfo.getList());
        Assertions.assertEquals(1, pageInfo.getTotal());
        Assertions.assertEquals(1, pageInfo.getList().size());
    }


    @Test
    void testSysUserMapper0900_findList_dpConditionPlaceholder_withDP_USER() {
        //仅允许查询自己的用户信息
        List<SysUser> sysUsers = this.sysUserMapper.findList_dpConditionPlaceholder("0", "1");
        log.info("[WITH DP USER] sysUserMapper.findList_dpConditionPlaceholder: total={},sysUsers={}", sysUsers.size(), sysUsers);
        Assertions.assertEquals(1, sysUsers.size());
        Assertions.assertEquals(USER_ID_WITH_DP_USER, sysUsers.get(0).getId());
    }

    @Test
    void testSysUserMapper0905_findList_dpConditionPlaceholder_withoutDP_USER() {
        //当前用户和性别参数不匹配，故查询结果为空
        List<SysUser> sysUsers = this.sysUserMapper.findList_dpConditionPlaceholder("0", "0");
        log.info("[WITH DP USER] sysUserMapper.findList_dpConditionPlaceholder: total={},sysUsers={}", sysUsers.size(), sysUsers);
        Assertions.assertEquals(0, sysUsers.size());
    }

    @Test
    @DisabledIf("throwExceptionWhenNoDataPermission")
    void testSysUserMapper0910_findList_dpConditionPlaceholder_withoutAnyDP() {
        this.mockUser_withoutAnyDP();

        //不允许查询任何的用户信息
        List<SysUser> sysUsers = this.sysUserMapper.findList_dpConditionPlaceholder("0", "1");
        log.info("[WITHOUT ANY DP] sysUserMapper.findList_dpConditionPlaceholder: total={},sysUsers={}", sysUsers.size(), sysUsers);
        Assertions.assertEquals(0, sysUsers.size());
    }

    @Test
    @EnabledIf("throwExceptionWhenNoDataPermission")
    void testSysUserMapper0910_findList_dpConditionPlaceholder_withoutAnyDP_throwEx() {
        this.mockUser_withoutAnyDP();

        //没有数据权限
        MyBatisSystemException myBatisSystemException = Assertions.assertThrows(
                MyBatisSystemException.class,
                () -> {
                    //不允许查询任何的用户信息
                    List<SysUser> sysUsers = this.sysUserMapper.findList_dpConditionPlaceholder("0", "1");
                });
        DataPermissionException dataPermissionException = this.extractDpException(myBatisSystemException);
        log.error("[WITHOUT ANY DP] sysUserMapper.findList_dpConditionPlaceholder: {}", dataPermissionException.getMessage(), dataPermissionException);
    }

    @ParameterizedTest
    @CsvSource(value = {
            "1,7",
            "0,6"
    })
    void testSysUserMapper0920_findList_dpConditionPlaceholder_withDP_ADMIN(String sex, Integer userCount) {
        this.mockUser_withDP_ADMIN();

        //许查询任何的用户信息
        List<SysUser> sysUsers = this.sysUserMapper.findList_dpConditionPlaceholder("0", sex);
        log.info("[WITH DP ADMIN] sysUserMapper.findList_dpConditionPlaceholder: total={},sysUsers={}", sysUsers.size(), sysUsers);
        Assertions.assertEquals(userCount, sysUsers.size());
    }

    @Test
    void testSysUserMapper1000_insert_throwEx() {
        this.mockUser_withOperations_SELECT();

        MyBatisSystemException myBatisSystemException = Assertions.assertThrows(
                MyBatisSystemException.class,
                () -> {
                    SysUser sysUser = new SysUser();
                    sysUser.setUserName("haha");
                    sysUser.setNickName("haha");
                    sysUser.setSex("1");
                    sysUser.setPhonenumber("123456789");
                    sysUser.setEmail("luo@email.com");
                    int retCount = this.sysUserMapper.insert(sysUser);
                });
        DataPermissionException dataPermissionException = this.extractDpException(myBatisSystemException);
        log.error("[WITHOUT OP INSERT] sysUserMapper.insert: {}", dataPermissionException.getMessage(), dataPermissionException);
    }

    /**
     * 从MyBatisSystemException提取DataPermissionException
     *
     * @param myBatisSystemException Mybatis系统异常
     * @return 数据权限异常
     */
    private DataPermissionException extractDpException(MyBatisSystemException myBatisSystemException) {
        return Optional.ofNullable(myBatisSystemException)
                //org.apache.ibatis.exceptions.PersistenceException
                .map(MyBatisSystemException::getCause)
                //DataPermissionException
                .map(Throwable::getCause)
                .filter(ex -> ex instanceof DataPermissionException)
                .map(DataPermissionException.class::cast)
                .orElse(null);
    }
}
