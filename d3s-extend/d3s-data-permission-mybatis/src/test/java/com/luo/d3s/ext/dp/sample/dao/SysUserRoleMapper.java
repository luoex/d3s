package com.luo.d3s.ext.dp.sample.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.luo.d3s.ext.dp.sample.entity.SysUserRole;

/**
 * 用户角色Mapper
 *
 * @author luohq
 * @date 2023-06-25
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {
}
