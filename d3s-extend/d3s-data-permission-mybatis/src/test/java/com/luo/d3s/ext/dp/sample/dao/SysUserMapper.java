package com.luo.d3s.ext.dp.sample.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.luo.d3s.ext.data.permission.anno.DataPermission;
import com.luo.d3s.ext.data.permission.enums.DpTypes;
import com.luo.d3s.ext.dp.sample.entity.SysUser;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 用户Mapper
 *
 * @author luohq
 * @date 2023-06-25
 */
@DataPermission(methodName = "selectById", userAlias = "", userIdColumn = "id", supportTypes = {DpTypes.USER})
@DataPermission(methodName = "selectPage", userAlias = "", userIdColumn = "id", deptAlias = "", deptIdColumn = "dept_id", supportTypes = {DpTypes.USER, DpTypes.DEPT_AND_CHILD})
@DataPermission(methodName = "updateById", userAlias = "", userIdColumn = "id", supportTypes = {DpTypes.USER})
@DataPermission(methodName = "deleteById", userAlias = "", userIdColumn = "id", supportTypes = {DpTypes.USER})
@DataPermission(methodName = "deleteBatchIds", userAlias = "", userIdColumn = "id", supportTypes = {DpTypes.USER})
@DataPermission(methodName = "selectList", userAlias = "", userIdColumn = "id", deptAlias = "", deptIdColumn = "dept_id", supportTypes = {DpTypes.USER, DpTypes.DEPT_AND_CHILD})
@DataPermission(methodName = "insert")
public interface SysUserMapper extends BaseMapper<SysUser> {

    /**
     * 根据ID查询用户
     *
     * @param id 用户ID
     * @return 用户
     */
    @DataPermission(userAlias = "", userIdColumn = "id", supportTypes = {DpTypes.USER})
    SysUser findById(Long id);

    /**
     * Mapper接口中的默认方法（default）不走Mybatis拦截器，仅默认方法中调用的具体方法才会走拦截器，
     * 如该方法中的this.selectList()方法，拦截器拦截到的也是selectList()方法，
     * 可通过在Mapper接口类上声明@DataPermission(methodName = "selectList", ...)来指定拦截的具体方法
     * @param pageNum  分页页码
     * @param pageSize 分页大小
     * @return 分页结果
     */
    default PageInfo<SysUser> findPageWithPageHelper(Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<SysUser> sysUsers = this.selectList(Wrappers.emptyWrapper());
        return new PageInfo(sysUsers);
    }

    /**
     * PageHelper - 动态SQL查询
     *
     * @param userName 用户名
     * @return 用户列表
     */
    @DataPermission(userAlias = "", userIdColumn = "id", deptAlias = "", deptIdColumn = "dept_id", supportTypes = {DpTypes.USER, DpTypes.DEPT_AND_CHILD})
    @Select("select * from sys_user where user_name like concat('%', #{userName}, '%')")
    List<SysUser> findPageWithPageHelper_dynamicSql(String userName);

    /**
     * 查询用户列表（Mapper.xml中使用了数据权限SQL条件占位符{DATA_PERMISSION_CONDITION}）
     *
     * @param status 帐号状态（0正常 1停用）
     * @param sex    用户性别（0男 1女 2未知）
     * @return 用户列表
     */
    @DataPermission(userAlias = "", userIdColumn = "id", deptAlias = "", deptIdColumn = "dept_id", supportTypes = {DpTypes.USER, DpTypes.DEPT_AND_CHILD})
    List<SysUser> findList_dpConditionPlaceholder(@Param("status") String status, @Param("sex") String sex);

}
