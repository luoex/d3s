package com.luo.d3s.ext.dp.sample.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.luo.d3s.ext.dp.sample.dao.BizMapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

/**
 * 测试配置
 *
 * @author luohq
 * @date 2023-06-23 19:47
 */
@EnableAutoConfiguration
@MapperScan(basePackageClasses = {BizMapper.class})
public class TestConfig {


    /**
     * 新的分页插件,
     * 一缓和二缓遵循mybatis的规则,
     */
    @Bean
    //默认order为Ordered.LOWEST_PRECEDENCE=Integer.MAX_VALUE,
    //而我们自定义的DataPermissionInterceptor即默认为Ordered.LOWEST_PRECEDENCE，
    //Mybatis插件顺序为后注册先执行，先注册后执行，
    //先注册Mybatis-plus的插件，然后再注册我们自定义的插件，
    //如此保证我们的插件在Mybatis-Plus插件之前执行
    //在Mybatis-Plus中可参见MybatisPlusAutoConfiguration.interceptors属性
    @Order(Ordered.HIGHEST_PRECEDENCE)
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        return interceptor;
    }
}
