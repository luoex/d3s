/*
 * 版本1.0，2021年9月
 * 许可证地址：http://osscoe.neusoft.com:8000/oscoe/oscoelicense/-/blob/master/LICENSE.md
 * 贡献者列表及排名：http://osscoe.neusoft.com:8000/oscoe/ ->项目目录->仓库->贡献者
 *
 * 使用、复制和分发遵守的条款和条件:
 * 1）您可以免费用于学习交流或东软商业用途，禁止用于个人商业用途；
 * 2）分发作品时，您必须向作品或衍生作品的接收者提供本许可证的副本；
 * 3）您必须对修改过的文件做出明显的标记，说明您更改了文件；
 * 4）您必须尊重其他开发者的贡献，在修改、制作、使用、售卖、转让作品时保留原贡献者的信息；
 * 5）您如果售卖、转让作品的接收方为非许可方授权实体，必须以目标形式提供，不可泄露源形式文件，经过许可方审批时除外；
 * 6）您如果以产品形式对外售卖、对外提供服务需要经过许可方单独授权，并由此产生的附加工作量需要双方协商解决；
 * 7）如果作品包括Notice文本文件，则您分发的衍生作品必须包括该Notice文件中包含的归属说明的可读副本；
 * 8）您提交的任何贡献的版权默认归属许可方所有；
 * 9）您的贡献提交只能在东软范围内，任何形式的外部传播需经许可方审批；
 * 10）您提交给许可方任何贡献，均应遵守本许可的条款和条件，没有任何附加条款或条件;
 * 11）其他条件及条款详见OSSCoE许可证原版。
 * 条款和条件结束
 */
package com.luo.d3s.ext.dp.sample.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 基础实体类
 *
 * @author luohq
 * @date 2023-06-23
 */
@Data
public class BaseEntity {
    /**
     * 主键
     */
    @TableId
    private Long id;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改人
     */
    private String updateBy;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;
}