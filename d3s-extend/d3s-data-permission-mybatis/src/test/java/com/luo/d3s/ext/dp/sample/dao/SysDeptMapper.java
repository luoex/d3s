package com.luo.d3s.ext.dp.sample.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.luo.d3s.ext.dp.sample.entity.SysDept;

/**
 * 部门Mapper
 *
 * @author luohq
 * @date 2023-06-25
 */
public interface SysDeptMapper extends BaseMapper<SysDept> {
}
