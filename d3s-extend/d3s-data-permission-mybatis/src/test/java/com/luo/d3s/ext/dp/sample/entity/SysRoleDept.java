package com.luo.d3s.ext.dp.sample.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 角色和部门关联 sys_role_dept
 *
 * @author luohq
 * @date 2023-06-23
 */
@Data
@TableName("sys_role_dept")
public class SysRoleDept {
    /**
     * 主键
     */
    @TableId
    private Long id;

    /**
     * 角色ID
     */
    private Long roleId;

    /**
     * 部门ID
     */
    private Long deptId;
}