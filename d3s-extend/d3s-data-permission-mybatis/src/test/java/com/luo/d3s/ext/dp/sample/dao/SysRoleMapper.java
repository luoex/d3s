package com.luo.d3s.ext.dp.sample.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.luo.d3s.ext.dp.sample.entity.SysRole;

/**
 * 角色Mapper
 *
 * @author luohq
 * @date 2023-06-25
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {
}
