package com.luo.d3s.ext.dp.sample.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.luo.d3s.ext.data.permission.anno.DataPermission;

import java.util.List;

/**
 * 业务Mapper
 *
 * @author luohq
 * @date 2023-06-18 16:19
 */
@DataPermission(methodName = "selectById")
@DataPermission(methodName = "deleteById")
public interface BizMapper extends BaseMapper {

    @DataPermission
    Object findById(String id);

    @DataPermission
    List<Object> findList();
}
