package com.luo.d3s.ext.data.permission.config;

import com.luo.d3s.ext.data.permission.interceptor.DataPermissionInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 数据权限配置
 *
 * @author luohq
 * @date 2023-06-25 11:37
 */
@Configuration
@EnableConfigurationProperties({DataPermissionProps.class})
@ConditionalOnMissingClass("com.github.pagehelper.autoconfigure.PageHelperAutoConfiguration")
public class DataPermissionAutoConfiguration {
    private static final Logger log = LoggerFactory.getLogger(DataPermissionAutoConfiguration.class);

    private DataPermissionProps dataPermissionProps;

    public DataPermissionAutoConfiguration(DataPermissionProps dataPermissionProps) {
        this.dataPermissionProps = dataPermissionProps;
    }

    @Bean
    public DataPermissionInterceptor dataPermissionInterceptor() {
        log.info("[DATA_PERMISSION] init DataPermissionInterceptor");
        return new DataPermissionInterceptor(this.dataPermissionProps);
    }
}
