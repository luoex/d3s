package com.luo.d3s.ext.data.permission.context;

import com.luo.d3s.ext.data.permission.dto.context.base.BaseUserDto;
import org.springframework.util.Assert;

/**
 * 数据权限用户信息上线文容器
 *
 * @author luohq
 * @date 2023-06-20 11:10
 */
public class DpUserContextHolder {

    /**
     * 用户ThreadLocal
     */
    private static final ThreadLocal<BaseUserDto> contextHolder = new ThreadLocal<>();


    /**
     * 内部构造函数
     */
    private DpUserContextHolder() {
    }

    /**
     * 清空用户上下文
     */
    public static void clearContext() {
        contextHolder.remove();
    }

    /**
     * 获取当前用户上下文
     *
     * @return 数据权限用户信息
     */
    public static BaseUserDto getContext() {
        return contextHolder.get();
    }

    /**
     * 设置当前用户上下文
     *
     * @param dpUserDto 数据权限用户信息
     */
    public static void setContext(BaseUserDto dpUserDto) {
        Assert.notNull(dpUserDto, "Only non-null DpUser instances are permitted");
        contextHolder.set(dpUserDto);
    }
}
