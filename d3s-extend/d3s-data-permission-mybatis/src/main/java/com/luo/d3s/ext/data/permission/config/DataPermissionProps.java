package com.luo.d3s.ext.data.permission.config;

import com.luo.d3s.ext.data.permission.dto.parse.SqlParseParamDto;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * 数据权限配置属性
 *
 * @author luohq
 * @date 2023-06-21 10:13
 */
@ConfigurationProperties(prefix = DataPermissionProps.PREFIX)
public class DataPermissionProps {

    public static final String PREFIX = "d3s.data-permission";

    /**
     * 用户权限限制条件
     */
    private String conditionForUser = "{userAlias}.{userIdColumn} = {userId}";
    /**
     * 自定义用户权限限制条件
     */
    private String conditionForUserCustom = "{userAlias}.{userIdColumn} in ({manageUserIds})";
    /**
     * 部门权限限制条件
     */
    private String conditionForDept = "{deptAlias}.{deptIdColumn} = {deptId}";
    /**
     * 部门及子部门权限限制条件，例如：
     * <ul>
     *     <li>{deptAlias}.{deptIdColumn} in (select dept_id from sys_dept where dept_id = {deptId} or find_in_set({deptId}, ancestors))</li>
     *     <li>{deptAlias}.{deptIdColumn} in (select dept_id from sys_dept where dept_id = {deptId} or ancestors like '%,{deptId},%')</li>
     *     <li>{deptAlias}.{deptIdColumn} in ({deptAndChildDeptIds})</li>
     * </ul>
     */
    private String conditionForDeptAndChild = "{deptAlias}.{deptIdColumn} in ({deptAndChildDeptIds})";
    /**
     * 自定义部门权限限制条件
     */
    private String conditionForDeptCustom = "{deptAlias}.{deptIdColumn} in ({manageDeptIds})";

    /**
     * 自定义其他权限类型对应的限制条件<br/>
     * 例如：
     * <pre>
     *     d3s.data-permission.conditions-for-other:
     *       # 格式：{数据权限名称}: {限制条件SQL}
     *       biz-user-permission: "{userAlias}.{userIdColumn} = {userId}"
     *       biz-dept-permission: "{deptAlias}.{deptIdColumn} = {deptId}"
     * </pre>
     */
    private Map<String, String> conditionsForOther = new HashMap<>();
    /**
     * 默认无效ID
     */
    private String defaultInvalidId = "0";

    /**
     * 没有数据权限时是否抛出DataPermissionException
     * <ol>
     *     <li>true则表示没有数据权限时抛出DataPermissionException</li>
     *     <li>false则表示没有数据权限时拼接false条件，仅INSERT命令类型会抛出DataPermissionException</li>
     * </ol>
     */
    private Boolean throwExceptionWhenNoDataPermission = true;

    public String getConditionForUser() {
        return conditionForUser;
    }

    public void setConditionForUser(String conditionForUser) {
        this.conditionForUser = conditionForUser;
    }

    public String getConditionForUserCustom() {
        return conditionForUserCustom;
    }

    public void setConditionForUserCustom(String conditionForUserCustom) {
        this.conditionForUserCustom = conditionForUserCustom;
    }

    public String getConditionForDept() {
        return conditionForDept;
    }

    public void setConditionForDept(String conditionForDept) {
        this.conditionForDept = conditionForDept;
    }

    public String getConditionForDeptAndChild() {
        return conditionForDeptAndChild;
    }

    public void setConditionForDeptAndChild(String conditionForDeptAndChild) {
        this.conditionForDeptAndChild = conditionForDeptAndChild;
    }

    public String getConditionForDeptCustom() {
        return conditionForDeptCustom;
    }

    public void setConditionForDeptCustom(String conditionForDeptCustom) {
        this.conditionForDeptCustom = conditionForDeptCustom;
    }

    public Map<String, String> getConditionsForOther() {
        return conditionsForOther;
    }

    public void setConditionsForOther(Map<String, String> conditionsForOther) {
        this.conditionsForOther = conditionsForOther;
    }

    public String getDefaultInvalidId() {
        return defaultInvalidId;
    }

    public void setDefaultInvalidId(String defaultInvalidId) {
        this.defaultInvalidId = defaultInvalidId;
        //覆盖默认无效ID值
        SqlParseParamDto.DEFAULT_INVALID_ID = this.defaultInvalidId;
    }

    public Boolean getThrowExceptionWhenNoDataPermission() {
        return throwExceptionWhenNoDataPermission;
    }

    public void setThrowExceptionWhenNoDataPermission(Boolean throwExceptionWhenNoDataPermission) {
        this.throwExceptionWhenNoDataPermission = throwExceptionWhenNoDataPermission;
    }

    @Override
    public String toString() {
        return "DataPermissionProps{" +
                "conditionForUser='" + conditionForUser + '\'' +
                ", conditionForUserCustom='" + conditionForUserCustom + '\'' +
                ", conditionForDept='" + conditionForDept + '\'' +
                ", conditionForDeptAndChild='" + conditionForDeptAndChild + '\'' +
                ", conditionForDeptCustom='" + conditionForDeptCustom + '\'' +
                ", conditionsForOther=" + conditionsForOther +
                ", defaultInvalidId='" + defaultInvalidId + '\'' +
                ", throwExceptionWhenNoDataPermission=" + throwExceptionWhenNoDataPermission +
                '}';
    }
}
