package com.luo.d3s.ext.data.permission.enums;

import java.util.stream.Stream;

/**
 * 数据权限 - 操作 - 枚举
 *
 * @author luohq
 * @date 2023-06-30
 */
public enum DpOpEnum {
    //全部操作
    ALL("1", "ALL"),
    //查询操作
    SELECT("2", "SELECT"),
    //修改操作
    UPDATE("3", "UPDATE"),
    //插入操作
    INSERT("4", "INSERT"),
    //删除操作
    DELETE("5", "DELETE");


    private String code;
    private String sqlCommandType;


    DpOpEnum(String code, String sqlCommandType) {
        this.code = code;
        this.sqlCommandType = sqlCommandType;
    }


    public String getCode() {
        return code;
    }

    public String getSqlCommandType() {
        return sqlCommandType;
    }

    public static DpOpEnum ofCode(String code) {
        return Stream.of(DpOpEnum.values())
                .filter(curEnum -> curEnum.getCode().equals(code))
                .findFirst()
                .orElse(null);
    }
}
