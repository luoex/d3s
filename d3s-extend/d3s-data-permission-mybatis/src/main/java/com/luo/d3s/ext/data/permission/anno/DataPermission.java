package com.luo.d3s.ext.data.permission.anno;


import java.lang.annotation.*;

/**
 * 数据权限注解
 *
 * @author luohq
 * @date 2023-06-18
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Repeatable(DataPermission.DataPermissions.class)
public @interface DataPermission {
    /**
     * 部门表的别名
     */
    String deptAlias() default "";

    /**
     * 部门表的ID列名
     */
    String deptIdColumn() default "";

    /**
     * 用户表的别名
     */
    String userAlias() default "";

    /**
     * 用户表的ID列名
     */
    String userIdColumn() default "";

    /**
     * 支持的数据权限类型（默认支持所有类型）
     */
    String[] supportTypes() default {};

    /**
     * 当用户数据权限集合为空时，默认是否允许查看全部数据（默认false，即不允许）
     */
    boolean defaultAllowAll() default false;

    /**
     * 方法名（仅注解在类或接口上时需设置此方法名，而注解在方法上时无需设置此方法名）
     */
    String methodName() default "";



    /**
     * 数据权限组合注解
     */
    @Target({ElementType.TYPE, ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    @interface DataPermissions {

        /**
         * 数据权限集合
         */
        DataPermission[] value();
    }

}
