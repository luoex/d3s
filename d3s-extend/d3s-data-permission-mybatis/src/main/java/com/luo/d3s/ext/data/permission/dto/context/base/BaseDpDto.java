package com.luo.d3s.ext.data.permission.dto.context.base;

import com.luo.d3s.ext.data.permission.enums.DpOpEnum;

import java.util.Collection;

/**
 * 数据权限DTO
 *
 * @author luohq
 * @date 2023-06-20 10:37
 */
public interface BaseDpDto {
    /**
     * 获取数据权限类型
     *
     * @return 数据权限类型
     */
    String getType();

    /**
     * 获取数据权限支持的操作集合，若操作集合为空则表示默认支持所有操作
     *
     * @return 数据权限支持的操作集合
     */
    Collection<DpOpEnum> getOperations();

    /**
     * 获取当前用户管控部门ID集合
     *
     * @return 用户管控部门ID集合
     */
    Collection<String> getManageDeptIds();

    /**
     * 获取当前用户管控人员ID集合
     *
     * @return 用户管控人员ID集合
     */
    Collection<String> getManageUserIds();
}
