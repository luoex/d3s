package com.luo.d3s.ext.data.permission.dto.context;

import com.luo.d3s.ext.data.permission.dto.context.base.BaseDpDto;
import com.luo.d3s.ext.data.permission.enums.DpOpEnum;
import com.luo.d3s.ext.data.permission.util.DpUtils;

import java.util.Collection;
import java.util.Collections;

/**
 * 数据权限DTO
 *
 * @author luohq
 * @date 2023-06-20 10:37
 */
public class DpPropDto implements BaseDpDto {

    /**
     * 数据权限类型
     */
    private String type;

    /**
     * 当前数据权限支持的操作集合
     */
    private Collection<DpOpEnum> operations;

    /**
     * 用户管控的部门或组织（例如用户或者用户角色上关联的管控组织部门ID集合）
     */
    private Collection<String> manageDeptIds;

    /**
     * 用户管控的人员（例如用户或者用户角色上关联的管控的人员ID集合）
     */
    private Collection<String> manageUserIds;


    public DpPropDto() {
    }

    public DpPropDto(String type, Collection<DpOpEnum> operations, Collection<?> manageDeptIds, Collection<?> manageUserIds) {
        this.setType(type);
        this.setOperations(operations);
        this.setManageDeptIds(manageDeptIds);
        this.setManageUserIds(manageUserIds);
    }

    public static DpPropDto ofType(String type) {
        return new DpPropDto(type, null, null, null);
    }

    public static DpPropDto ofTypeAndOperations(String type, Collection<DpOpEnum> operations) {
        return new DpPropDto(type, operations, null, null);
    }

    public static DpPropDto ofTypeAndManageIds(String type, Collection<?> manageDeptIds, Collection<?> manageUserIds) {
        return new DpPropDto(type, null, manageDeptIds, manageUserIds);
    }

    public static DpPropDto ofAll(String type, Collection<DpOpEnum> operations, Collection<?> manageDeptIds, Collection<?> manageUserIds) {
        return new DpPropDto(type, operations, manageDeptIds, manageUserIds);
    }

    @Override
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public Collection<DpOpEnum> getOperations() {
        return operations;
    }

    public void setOperations(Collection<DpOpEnum> operations) {
        this.operations = DpUtils.withNullDefault(operations, Collections.emptySet());
    }

    @Override
    public Collection<String> getManageDeptIds() {
        return manageDeptIds;
    }

    public void setManageDeptIds(Collection<?> manageDeptIds) {
        this.manageDeptIds = DpUtils.convertObjToStrCollection(manageDeptIds);
    }

    @Override
    public Collection<String> getManageUserIds() {
        return manageUserIds;
    }

    public void setManageUserIds(Collection<?> manageUserIds) {
        this.manageUserIds = DpUtils.convertObjToStrCollection(manageUserIds);
    }

    @Override
    public String toString() {
        return "DpPropDto{" +
                "type=" + type +
                ", operations=" + operations +
                ", manageDeptIds=" + manageDeptIds +
                ", manageUserIds=" + manageUserIds +
                '}';
    }
}
