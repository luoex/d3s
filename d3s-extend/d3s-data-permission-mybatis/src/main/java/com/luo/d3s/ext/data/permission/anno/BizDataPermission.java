package com.luo.d3s.ext.data.permission.anno;

import java.lang.annotation.*;

/**
 * 业务数据权限注解
 *
 * @author luohq
 * @date 2023-06-18
 */
@Target({ElementType.TYPE, ElementType.METHOD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Repeatable(BizDataPermission.BizDataPermissions.class)
@DataPermission
public @interface BizDataPermission {
    /**
     * 部门表的别名
     */
    String deptAlias() default "d";

    /**
     * 部门表的ID列名
     */
    String deptIdColumn() default "id";

    /**
     * 用户表的别名
     */
    String userAlias() default "u";

    /**
     * 用户表的ID列名
     */
    String userIdColumn() default "id";

    /**
     * 方法名（仅注解在类或接口上时需设置此方法名，而注解在方法上时无需设置此方法名）
     */
    String methodName() default "";

    /**
     * 数据权限组合注解
     */
    @Target({ElementType.TYPE, ElementType.METHOD, ElementType.ANNOTATION_TYPE})
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    @interface BizDataPermissions {

        /**
         * 数据权限集合
         */
        BizDataPermission[] value();
    }

}
