package com.luo.d3s.ext.data.permission.exception;

import com.luo.d3s.ext.data.permission.dto.context.base.BaseUserDto;

/**
 * 数据权限异常
 *
 * @author luohq
 * @date 2023-07-01
 */
public class DataPermissionException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    /**
     * Mapper方法ID
     */
    private String mapperMethodId;

    /**
     * SQL命令类型
     */
    private String sqlCommandType;

    /**
     * 用户及数据权限信息
     */
    private BaseUserDto dpUser;

    public DataPermissionException(String mapperMethodId, String sqlCommandType, BaseUserDto dpUser) {
        super(String.format("No Data Permission - %s - SqlCommandType: %s - DpUser: %s", mapperMethodId, sqlCommandType, String.valueOf(dpUser)));
        this.mapperMethodId = mapperMethodId;
        this.sqlCommandType = sqlCommandType;
        this.dpUser = dpUser;
    }

    public String getMapperMethodId() {
        return mapperMethodId;
    }

    public String getSqlCommandType() {
        return sqlCommandType;
    }

    public BaseUserDto getDpUser() {
        return dpUser;
    }

    @Override
    public String toString() {
        return super.getMessage();
    }
}
