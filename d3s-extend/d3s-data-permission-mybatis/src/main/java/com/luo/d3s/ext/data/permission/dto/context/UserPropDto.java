package com.luo.d3s.ext.data.permission.dto.context;

import com.luo.d3s.ext.data.permission.dto.context.base.BaseDpDto;
import com.luo.d3s.ext.data.permission.dto.context.base.BaseUserDto;
import com.luo.d3s.ext.data.permission.util.DpUtils;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

/**
 * 用户数据权限DTO
 *
 * @author luohq
 * @date 2023-06-20 10:37
 */
public class UserPropDto implements BaseUserDto {

    /**
     * 用户ID（通常对应当前登录用户ID）
     */
    private String userId;

    /**
     * 用户所在部门或所属组织（通常对应登录用户的组织部门ID）
     */
    private String deptId;

    /**
     * 前用户所属的部门及子部门ID集合
     */
    private Collection<String> deptAndChildDeptIds;

    /**
     * 数据权限集合
     */
    private Collection<BaseDpDto> dataPermissions;

    /**
     * 额外的参数
     */
    private Map<String, String> additionalParams;

    public UserPropDto() {
    }


    public UserPropDto(Object userId, Object deptId, Collection<?> deptAndChildDeptIds, Collection<BaseDpDto> dataPermissions) {
        this.setUserId(userId);
        this.setDeptId(deptId);
        this.setDeptAndChildDeptIds(deptAndChildDeptIds);
        this.setDataPermissions(dataPermissions);
    }

    public static UserPropDto ofUserAndDept(Object userId, Object deptId) {
        return new UserPropDto(userId, deptId, null, Collections.emptySet());
    }

    public static UserPropDto ofUserDeptAndDataPermissions(Object userId, Object deptId, Collection<BaseDpDto> dataPermissions) {
        return new UserPropDto(userId, deptId, null, dataPermissions);
    }

    public static UserPropDto ofAll(Object userId, Object deptId, Collection<?> deptAndChildDeptIds, Collection<BaseDpDto> dataPermissions) {
        return new UserPropDto(userId, deptId, deptAndChildDeptIds, dataPermissions);
    }

    @Override
    public String getUserId() {
        return userId;
    }

    public void setUserId(Object userId) {
        this.userId = DpUtils.convertObjToStr(userId);
    }

    @Override
    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(Object deptId) {
        this.deptId = DpUtils.convertObjToStr(deptId);
    }

    @Override
    public Collection<String> getDeptAndChildDeptIds() {
        return deptAndChildDeptIds;
    }

    public void setDeptAndChildDeptIds(Collection<?> deptAndChildDeptIds) {
        this.deptAndChildDeptIds = DpUtils.convertObjToStrCollection(deptAndChildDeptIds);
    }

    @Override
    public Collection<BaseDpDto> getDataPermissions() {
        return dataPermissions;
    }

    public void setDataPermissions(Collection<BaseDpDto> dataPermissions) {
        this.dataPermissions = dataPermissions;
    }


    @Override
    public Map<String, String> getAdditionalParams() {
        return additionalParams;
    }

    public void setAdditionalParams(Map<String, String> additionalParams) {
        this.additionalParams = additionalParams;
    }

    @Override
    public String toString() {
        return "UserPropDto{" +
                "userId='" + userId + '\'' +
                ", deptId='" + deptId + '\'' +
                ", deptAndChildDeptIds=" + deptAndChildDeptIds +
                ", dataPermissions=" + dataPermissions +
                ", additionalParams=" + additionalParams +
                '}';
    }
}
