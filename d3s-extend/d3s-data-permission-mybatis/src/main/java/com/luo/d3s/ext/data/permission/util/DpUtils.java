package com.luo.d3s.ext.data.permission.util;

import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * 数据权限工具类
 *
 * @author luohq
 * @date 2023-06-22 20:09
 */
public class DpUtils {

    private DpUtils() {
    }

    /**
     * 转换任意对象集合为字符串集合
     *
     * @param objCollection 对象集合
     * @return 字符串集合
     */
    public static Collection<String> convertObjToStrCollection(Collection<?> objCollection) {
        if (Objects.nonNull(objCollection) && !objCollection.isEmpty()) {
            return objCollection.stream().map(String::valueOf).collect(Collectors.toSet());
        } else {
            return Collections.emptySet();
        }
    }

    /**
     * 转换对象集合提供器为字符串集合提供器
     *
     * @param objCollectionSupplier 对象集合提供器
     * @return 字符串集合提供器
     */
    public static Supplier<Collection<String>> convertObjToStringCollectionSupplier(Supplier<Collection<?>> objCollectionSupplier) {
        if (Objects.isNull(objCollectionSupplier)) {
            return () -> Collections.emptySet();
        }
        return () -> convertObjToStrCollection(objCollectionSupplier.get());
    }

    /**
     * 转换对象为字符串
     *
     * @param obj 对象
     * @return 字符串
     */
    public static String convertObjToStr(Object obj) {
        return Optional.ofNullable(obj).map(String::valueOf).orElse(null);
    }

    /**
     * 转换对象提供器为字符串提供器
     *
     * @param objSupplier 对象提供器
     * @return 字符串提供器
     */
    public static Supplier<String> convertObjToStringSupplier(Supplier<Object> objSupplier) {
        if (Objects.isNull(objSupplier)) {
            return () -> null;
        }
        return () -> convertObjToStr(objSupplier.get());
    }

    /**
     * 获取Supplier的值，若supplier为空则直接返回null
     *
     * @param supplier 提供器
     * @param <T>
     * @return 返回值
     */
    public static <T> T getSupplierValue(Supplier<T> supplier) {
        return getSupplierValue(supplier, null);
    }

    /**
     * 获取Supplier的值，若supplier为空则直接返回默认值
     *
     * @param supplier     提供器
     * @param defaultValue 默认值
     * @param <T>
     * @return 返回值
     */
    public static <T> T getSupplierValue(Supplier<T> supplier, T defaultValue) {
        return Optional.ofNullable(supplier).map(Supplier::get).orElse(defaultValue);
    }

    /**
     * 为空则设置默认值
     *
     * @param val        原始值
     * @param defaultVal 默认值
     * @param <T>
     * @return 最终值
     */
    public static <T> T withNullDefault(T val, T defaultVal) {
        return Optional.ofNullable(val).orElse(defaultVal);
    }

    /**
     * 设置目标对象的指定字段值
     *
     * @param objTarget  目标对象
     * @param fieldName  字段名称
     * @param fieldValue 字段值
     */
    public static void setFieldValue(Object objTarget, String fieldName, Object fieldValue) {
        Field field = ReflectionUtils.findField(objTarget.getClass(), fieldName);
        ReflectionUtils.makeAccessible(field);
        ReflectionUtils.setField(field, objTarget, fieldValue);
    }

    /**
     * 判断对象数组是否为空
     *
     * @param objArray 对象数组
     * @return 是否为空
     */
    public static Boolean isEmptyArray(Object[] objArray) {
        return null == objArray || 0 == objArray.length;
    }

    /**
     * 判断集合是否为空
     *
     * @param collection 集合
     * @return 是否为空
     */
    public static Boolean isEmptyCollection(Collection<?> collection) {
        return null == collection || collection.isEmpty();
    }
}

