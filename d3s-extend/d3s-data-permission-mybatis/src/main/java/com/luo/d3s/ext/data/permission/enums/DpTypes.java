package com.luo.d3s.ext.data.permission.enums;

/**
 * 数据权限类型
 *
 * @author luohq
 * @date 2023-06-20 10:15
 */
public interface DpTypes {
    /**
     * 管理员数据权限（全部数据）
     */
    String ADMIN = "ADMIN";

    /**
     * 用户自身数据权限
     */
    String USER = "USER";

    /**
     * 自定义用户数据权限
     */
    String USER_CUSTOM = "USER_CUSTOM";

    /**
     * 部门数据权限
     */
    String DEPT = "DEPT";

    /**
     * 部门及子部门数据权限
     */
    String DEPT_AND_CHILD = "DEPT_AND_CHILD";

    /**
     * 自定义部门数据权限
     */
    String DEPT_CUSTOM = "DEPT_CUSTOM";
}
