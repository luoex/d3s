package com.luo.d3s.ext.data.permission.dto.context.base;

import java.util.Collection;
import java.util.Map;

/**
 * 用户及数据权限集合DTO
 *
 * @author luohq
 * @date 2023-06-20 10:37
 */
public interface BaseUserDto {
    /**
     * 获取当前用户ID
     *
     * @return 当前用户ID
     */
    String getUserId();

    /**
     * 获取当前用户所属部门ID
     *
     * @return
     */
    String getDeptId();

    /**
     * 获取当前用户所属的部门及子部门ID集合
     *
     * @return 前用户所属的部门及子部门ID集合
     */
    Collection<String> getDeptAndChildDeptIds();

    /**
     * 获取当前用户数据权限集合
     *
     * @return 当前用户数据权限集合
     */
    Collection<BaseDpDto> getDataPermissions();

    /**
     * 获取其他附加的参数集合
     *
     * @return 附加参数集合
     */
    Map<String, String> getAdditionalParams();
}
