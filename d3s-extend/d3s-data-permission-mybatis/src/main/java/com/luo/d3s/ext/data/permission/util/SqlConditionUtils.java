package com.luo.d3s.ext.data.permission.util;

/**
 * Sql条件拼接工具
 *
 * @author luohq
 * @date 2023-06-30 10:48
 */
public class SqlConditionUtils {
    /**
     * 或 - 分隔符
     */
    public static final String OR_SEPARATOR = " or ";
    /**
     * 或 - 条件格式
     */
    public static final String OR_CONDITION_FORMAT = " or (%s)";
    /**
     * 与 - 分隔符
     */
    public static final String AND_SEPARATOR = " and ";
    /**
     * 与 - 条件格式
     */
    public static final String AND_CONDITION_FORMAT = " and (%s)";
    /**
     * 双括号 - 条件格式
     */
    public static final String BRACKET_CONDITION_FORMAT = " (%s) ";
    /**
     * where - 关键字
     */
    public static final String WHERE = " where ";
    /**
     * where - 条件格式
     */
    public static final String WHERE_CONDITION_FORMAT = " where %s";

    /**
     * 允许全部数据权限对应的sql条件: true
     */
    public static final String ALLOW_ALL_CONDITION = Boolean.TRUE.toString();
    /**
     * 拒绝全部数据权限对应的sql条件：false
     */
    public static final String DENY_ALL_CONDITION = Boolean.FALSE.toString();


    private SqlConditionUtils() {
    }

    /**
     * 附加OR条件
     *
     * @param originCondition 原条件
     * @param appendCondition 附加条件
     * @return 附加OR条件后的条件
     */
    public static String appendOrCondition(String originCondition, String appendCondition) {
        return new StringBuilder(originCondition)
                .append(formatOrConditionWithParams(appendCondition))
                .toString();
    }

    /**
     * 附加AND条件
     *
     * @param originCondition 原条件
     * @param appendCondition 附加条件
     * @return 附加AND条件后的条件
     */
    public static String appendAndCondition(String originCondition, String appendCondition) {
        return new StringBuilder(originCondition)
                .append(formatAndConditionWithParams(appendCondition))
                .toString();
    }

    /**
     * 附加AND条件
     * <ol>
     *     <li>原sql中包含where则在where后附加and appendCondition</li>
     *     <li>原sql中不包含where则在sql后直接附加where appendCondition</li>
     * </ol>
     *
     * @param originCondition 原条件
     * @param appendCondition 附加条件
     * @return 附加AND条件后的条件
     */
    public static String appendWhereAndCondition(String originCondition, String appendCondition) {
        //原sql中包含where，则在where后附加and appendCondition
        if (sqlContainsWhere(originCondition)) {
            return appendAndCondition(originCondition, appendCondition);
        }
        //原sql中不包含where，则在sql后直接附加where appendCondition
        return appendWhereCondition(originCondition, appendCondition);
    }

    /**
     * 附加WHERE条件
     *
     * @param originCondition 原条件
     * @param appendCondition 附加条件
     * @return 附加WHERE条件后的条件
     */
    public static String appendWhereCondition(String originCondition, String appendCondition) {
        return new StringBuilder(originCondition)
                .append(formatWhereConditionWithParams(appendCondition))
                .toString();
    }

    /**
     * 格式化字符串
     *
     * @param format 格式
     * @param params 参数
     * @return 格式化后的字符串
     */
    public static String formatWithParams(String format, String... params) {
        return String.format(format, params);
    }

    /**
     * 格式化and条件
     *
     * @param params 参数
     * @return 格式化后的字符串
     */
    public static String formatAndConditionWithParams(String... params) {
        return String.format(AND_CONDITION_FORMAT, params);
    }

    /**
     * 格式化or条件
     *
     * @param params 参数
     * @return 格式化后的字符串
     */
    public static String formatOrConditionWithParams(String... params) {
        return String.format(OR_CONDITION_FORMAT, params);
    }

    /**
     * 格式化where条件
     *
     * @param params 参数
     * @return 格式化后的字符串
     */
    public static String formatWhereConditionWithParams(String... params) {
        return String.format(WHERE_CONDITION_FORMAT, params);
    }

    /**
     * 格式化(...)条件
     *
     * @param params 参数
     * @return 格式化后的字符串
     */
    public static String formatBracketConditionWithParams(String... params) {
        return String.format(BRACKET_CONDITION_FORMAT, params);
    }

    /**
     * SQL是否包含WHERE
     *
     * @param sql SQL
     * @return 是否包含WHERE
     */
    public static Boolean sqlContainsWhere(String sql) {
        return sql.toLowerCase().contains(WHERE);
    }

    /**
     * 是否为允许全部条件
     *
     * @param condition 条件
     * @return 是否为允许全部条件
     */
    public static Boolean isAllowAllCondition(String condition) {
        return ALLOW_ALL_CONDITION.equalsIgnoreCase(condition);
    }

    /**
     * 是否为拒绝全部条件
     *
     * @param condition 条件
     * @return 是否为拒绝全部条件
     */
    public static Boolean isDenyAllCondition(String condition) {
        return DENY_ALL_CONDITION.equalsIgnoreCase(condition);
    }
}
