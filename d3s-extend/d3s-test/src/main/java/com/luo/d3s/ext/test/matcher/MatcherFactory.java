package com.luo.d3s.ext.test.matcher;

import org.hamcrest.CustomMatcher;
import org.hamcrest.Matcher;

import java.util.function.Function;

/**
 * Matcher工厂
 *
 * @author luohq
 * @date 2023-01-09 16:43
 */
public class MatcherFactory {

    /**
     * 创建GreaterThan匹配器
     *
     * @param min        最小值(大于此最小值)
     * @param errMessage 错误提示信息
     * @return GreaterThanMatcher
     */
    public static GreaterThanMatcher greaterThan(Number min, String errMessage) {
        return new GreaterThanMatcher(min, errMessage);
    }

    /**
     * 自定义匹配器
     *
     * @param errMessage 错误提示信息
     * @param matchFunc  匹配逻辑
     * @return CustomMatcher
     */
    public static Matcher match(String errMessage, Function<Object, Boolean> matchFunc) {
        return new CustomMatcher(errMessage) {
            @Override
            public boolean matches(Object actual) {
                return matchFunc.apply(actual);
            }
        };
    }


}
