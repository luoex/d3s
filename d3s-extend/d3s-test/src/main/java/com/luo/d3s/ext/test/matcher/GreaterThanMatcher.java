package com.luo.d3s.ext.test.matcher;

import com.luo.d3s.core.util.validation.Validates;
import org.hamcrest.CustomMatcher;

/**
 * 大于数值Matcher
 *
 * @author luohq
 * @date 2023-01-09
 */
public class GreaterThanMatcher extends CustomMatcher<Number> {
    private Number min;

    public GreaterThanMatcher(Number min, String description) {
        super(description);
        this.min = min;
        Validates.notNull(min);
    }

    @Override
    public boolean matches(Object actual) {
        double doubleValue = ((Number) actual).doubleValue();
        return this.min.doubleValue() < doubleValue;
    }
}