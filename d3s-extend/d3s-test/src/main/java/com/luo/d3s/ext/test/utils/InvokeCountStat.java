package com.luo.d3s.ext.test.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 调用次数统计类
 *
 * @author luohq
 * @date 2023-01-27 14:35
 */
public class InvokeCountStat {

    private static Map<Class, AtomicInteger> class2InvokeCountMap = new HashMap<>();

    /**
     * clazz类调用计数+1
     *
     * @param clazz 被调用类
     */
    public static synchronized void invoke(Class clazz) {
        AtomicInteger invokeCount = class2InvokeCountMap.computeIfAbsent(clazz, classInner -> new AtomicInteger(0));
        invokeCount.incrementAndGet();
    }

    /**
     * 获取clazz类被调用次数
     *
     * @param clazz 被调用类
     * @return 调用次数
     */
    public static synchronized Integer invokeCount(Class clazz) {
        return Optional.ofNullable(class2InvokeCountMap.get(clazz)).map(AtomicInteger::get).orElse(0);
    }

    /**
     * 重置调用计数（清零）
     */
    public static synchronized void resetInvokeCount() {
        class2InvokeCountMap.clear();
    }
}
