package com.luo.d3s.ext.test.mock;

import com.github.fridujo.rabbitmq.mock.MockConnectionFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Bean;

/**
 * RabbitMq Broker（服务端）Mock配置
 * <ul>
 *     <li>使用MockConnectionFactory代替原ConnectionFactory</li>
 *     <li>可在单元测试类上通过注解导入配置：@Import({RabbitMqBrokerMockConfiguration.class})</li>
 * </ul>
 *
 * @author luohq
 * @date 2023-01-09 14:16
 * @see <a href="https://github.com/fridujo/rabbitmq-mock">https://github.com/fridujo/rabbitmq-mock</a>
 */
public class RabbitMqBrokerMockConfiguration {

    @Bean
    ConnectionFactory connectionFactory() {
        return new CachingConnectionFactory(new MockConnectionFactory());
    }

    @Bean
    RabbitAdmin rabbitAdmin(ConnectionFactory connectionFactory) {
        return new RabbitAdmin(connectionFactory);
    }

    @Bean
    RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        return new RabbitTemplate(connectionFactory);
    }
}
