# Test扩展

## Junit5扩展
- 支持@JsonFileSource

## Mock扩展
- 支持Spring MockMvc基类
- 支持RabbitMq Broker Mock
- 支持Redis Server Mock

> 注：  
> Jdbc Mock推荐集成H2  
> Ldap Mock推荐集成Spring Ldap Embedded