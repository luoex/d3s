package com.luo.d3s.ext.aspectj.domain;

import com.luo.d3s.core.domain.model.DomainModelValidate;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 领域模型构造函数验证（多参数） - AOP增强切面
 *
 * @author luohq
 * @date 2023-01-29 09:57
 */
@Aspect
public class D3sDomainConstructorValidateAspect {

    private Logger log = LoggerFactory.getLogger(D3sDomainConstructorValidateAspect.class);

    public static final String OR_SEPARATOR = " || ";
    public static final String ENTITY_CONSTRUCTOR_WITH_ARGS_POINTCUT = "execution(com.luo.d3s.core.domain.model.Entity+.new(Object+,..))";
    public static final String VALUE_OBJECT_CONSTRUCTOR_WITH_ARGS_POINTCUT = "execution(com.luo.d3s.core.domain.model.ValueObject+.new(Object+,..))";
    public static final String DOMAIN_CONSTRUCTOR_POINTCUT = ENTITY_CONSTRUCTOR_WITH_ARGS_POINTCUT
            + OR_SEPARATOR + VALUE_OBJECT_CONSTRUCTOR_WITH_ARGS_POINTCUT;

    @After(DOMAIN_CONSTRUCTOR_POINTCUT)
    public void constructorValidate(JoinPoint point) throws Throwable {
        Object target = point.getTarget();
        Class targetClass = target.getClass();
        if (target instanceof DomainModelValidate) {
            log.debug("Validate Domain Model: {}", targetClass);
            DomainModelValidate.class.cast(target).validateSelf();
        }
    }

}