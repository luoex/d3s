# D3s AOP扩展

使用Spring Aop扩展日志打印、Web异常处理。  

## 一、全局配置说明
```yaml
# D3s AOP扩展配置
# 注：如下配置即为默认配置，可根据需要进行调整
d3s:
  aop:
    # 异常处理配置
    exception:
      # 异常处理全局开关
      enabled: true
      # web异常处理全局开关
      enable-web-exception-handle: true
      # 是否在响应结果中展示验证错误提示信息
      enable-validation-message: true
      # 默认的验证异常错误码
      default-validation-error-code: VALIDATE_ERROR
    # 日志配置
    log:
      # 日志处理全局开关
      enabled: true
      # 日志级别
      log-level: INFO
      # 是否打印参数
      show-params: true
      # 是否打印结果
      show-result: true
      # 是否打印类名称，true则ClassName.methodName...，false则methodName...
      show-class-name: false
      # 对象转换字符串的转换方式（支持TO_STRING, JSON，默认TO_STRING，即String.valueOf()）
      object-to-string-type: TO_STRING
      # 是否打印执行时间
      show-execution-time: true
      # 执行时间单位
      execution-time-unit: MILLIS
      # 是否启用CommandService日志
      enable-command-service-log: true
      # 是否启用QueryService日志
      enable-query-service-log: true
      #  是否启用DomainEventHandler日志
      enable-domain-event-handler-log: true
      # 是否启用Web日志
      enable-web-log: true

```
## 二、日志处理
### 2.1 支持@D3sLog注解进行类、方法级别日志设置

```java

import com.luo.d3s.ext.aop.config.ObjectToStringType;
import com.luo.d3s.ext.aop.log.anno.D3sLog;

//禁用类级别日志
@D3sLog(enabled = false)
public class CategoryController {
  ...
}

//类级别日志设置
@D3sLog(showParams = true, showResult = false, showExecutionTime = false, objectToStringType = ObjectToStringType.JSON)
public class CategoryController {
  ...
}

---

public class GoodsController {

  //禁用方法级别日志
  @D3sLog(enabled = false)
  public Response createGoods(GoodsCreateCommand command) {
      ...
  }
}

@D3sLog(showParams = true, showResult = false, showExecutionTime = false)
public class GoodsController {

  //方法级别日志
  @D3sLog(showParams = true, showResult = true, showExecutionTime = true, objectToStringType = ObjectToStringType.JSON)
  public Response createGoods(GoodsCreateCommand command) {
      ...
  }
}

```

## 2.2 日志格式
```bash
2023-02-01 15:58:05.725 [main] INFO  [c.l.d.ec.inf.web.CategoryController] - PUT /api/v1/categories Started with params: {categoryModifyCommand=CategoryModifyCommand(id=3, parentCategoryId=null, categoryName=分类3, categoryDesc=修改分类1描述 - 分类名称不变)}
2023-02-01 15:58:05.735 [main] INFO  [c.l.d.e.a.s.i.CategoryCommandServiceImpl] - modifyCategory Started with params: {categoryModifyCommand=CategoryModifyCommand(id=3, parentCategoryId=null, categoryName=分类3, categoryDesc=修改分类1描述 - 分类名称不变)}
2023-02-01 15:58:05.989 [main] INFO  [c.l.d.e.a.s.i.CategoryCommandServiceImpl] - modifyCategory Finished in 254 millis with result: Response{success=true, errCode='null', errMessage='null'}
2023-02-01 15:58:07.252 [main] INFO  [c.l.d.ec.inf.web.CategoryController] - PUT /api/v1/categories Finished in 1526 millis with result: Response{success=true, errCode='null', errMessage='null'}
```

## 三、异常处理
### 3.1 支持@D3sException注解进行类、方法级别异常处理设置

```java


import com.luo.d3s.ext.aop.exception.anno.D3sException;

//启用类级别异常处理
@D3sException
public class CategoryController {
  ...
}

//禁用类级别异常处理
@D3sException(enabled = false)
public class CategoryController {
  ...
}
---

public class GoodsController {

    //启用方法级别异常处理
    @D3sException
    public Response createGoods(GoodsCreateCommand command) {
      ...
    }
}
public class GoodsController {

    //禁用方法级别异常处理
    @D3sException(enabled = false)
    public Response createGoods(GoodsCreateCommand command) {
      ...
    }
}

@D3sException(enabled = true)
public class GoodsController {

    //禁用方法级别异常处理
    @D3sException(enabled = false)
    public Response createGoods(GoodsCreateCommand command) {
      ...
    }
}

```

## 3.2 支持自定义异常处理器D3sExceptionHandler
>注：默认实现参见 D3sDefaultExceptionHandler

```java
import org.springframework.stereotype.Component;

//通过自定义D3sExceptionHandler处理异常
@Component
public class CustomExceptionHandlerImpl implements D3sExceptionHandler {

    /**
     * 处理异常
     *
     * @param method    执行方法
     * @param throwable 异常
     * @return 返回结果
     */
    Object handleException(Method method, Throwable throwable) {
        ...
    }
}
```

## 3.3 异常处理日志格式
```bash
2023-02-01 15:58:07.982 [main] INFO  [c.l.d.ec.inf.web.CategoryController] - POST /api/v1/categories Started with params: {categoryCreateCommand=CategoryCreateCommand(parentCategoryId=null, categoryName=分类1, categoryDesc=失败原因：categoryName重复)}
2023-02-01 15:58:07.982 [main] INFO  [c.l.d.e.a.s.i.CategoryCommandServiceImpl] - createCategory Started with params: {categoryCreateCommand=CategoryCreateCommand(parentCategoryId=null, categoryName=分类1, categoryDesc=失败原因：categoryName重复)}
2023-02-01 15:58:07.985 [main] ERROR [c.l.d.e.a.e.a.D3sWebExceptionHandlerAspect] - Handle Exception At CategoryController.POST /api/v1/categories - ValidateException{errCode='VALIDATE_ERROR',errMessage='category name is repeated'}
2023-02-01 15:58:07.985 [main] INFO  [c.l.d.ec.inf.web.CategoryController] - POST /api/v1/categories Finished in 3 millis with result: SingleResponse{success=false, data=null, errCode='VALIDATE_ERROR', errMessage='category name is repeated'}
```


