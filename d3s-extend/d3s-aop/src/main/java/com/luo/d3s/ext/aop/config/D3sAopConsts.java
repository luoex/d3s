package com.luo.d3s.ext.aop.config;

import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * D3s AOP常量
 *
 * @author luohq
 * @date 2023-01-29 12:29
 */
public class D3sAopConsts {

    /**
     * 符号常量
     */
    public static final String DOT = ".";
    public static final String SEPARATOR_COMMA = ", ";
    public static final String SEPARATOR_COLON = ": ";
    public static final String SEPARATOR_AND = " && ";
    public static final String SEPARATOR_OR = " || ";
    public static final String SEPARATOR_NOT = "!";


    /**
     * 切点表达式
     */
    public static final String APPLICATION_SERVICE_POINTCUT = "execution(public * com.luo.d3s.core.application.service.ApplicationService+.*(..))";
    public static final String LOG_ANNO_CLASS_POINTCUT = "(@within(com.luo.d3s.ext.aop.log.anno.D3sLog) && execution(public * *.*(..)))";
    public static final String LOG_ANNO_METHOD_POINTCUT = "@annotation(com.luo.d3s.ext.aop.log.anno.D3sLog)";
    public static final String PUBLIC_METHOD_POINTCUT = "execution(public * *.*(..))";
    public static final String COMMAND_SERVICE_POINTCUT = "execution(public * com.luo.d3s.core.application.service.CommandService+.*(com.luo.d3s.core.application.dto.Command+))";
    public static final String QUERY_SERVICE_POINTCUT = "execution(public * com.luo.d3s.core.application.service.QueryService+.*(com.luo.d3s.core.application.dto.Query+))";
    public static final String DOMAIN_EVENT_HANDLER_POINTCUT = "execution(public void com.luo.d3s.core.domain.event.DomainEventHandler+.handle(com.luo.d3s.core.domain.event.DomainEvent+))";
    public static final String CONTROLLER_POINTCUT = "@within(org.springframework.stereotype.Controller)";
    public static final String REST_CONTROLLER_POINTCUT = "@within(org.springframework.web.bind.annotation.RestController)";
    public static final String WEB_MVC_POINT = D3sAopConsts.CONTROLLER_POINTCUT + D3sAopConsts.SEPARATOR_OR + D3sAopConsts.REST_CONTROLLER_POINTCUT;
    public static final String WEB_MVC_EXCLUDE_POINT = "!(" + WEB_MVC_POINT + ")";
    public static final String EXCEPTION_ANNO_CLASS_POINTCUT = "(@within(com.luo.d3s.ext.aop.exception.anno.D3sException) && execution(public * *.*(..)))";
    public static final String EXCEPTION_ANNO_METHOD_POINTCUT = "@annotation(com.luo.d3s.ext.aop.exception.anno.D3sException)";


    public static String orPointcut(String... pointcutExpression) {
        return Stream.of(pointcutExpression).collect(Collectors.joining(SEPARATOR_OR));
    }

    public static String andPointcut(String... pointcutExpression) {
        return Stream.of(pointcutExpression).collect(Collectors.joining(SEPARATOR_AND));
    }


    public static String negativePointcut(String pointcutExpression) {
        return new StringBuffer("!(").append(pointcutExpression).append(")").toString();
    }
}
