package com.luo.d3s.ext.aop.log.aspect;

import com.luo.d3s.ext.aop.config.D3sAopConsts;
import com.luo.d3s.ext.aop.config.D3sAopProps;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;

/**
 * Service日志AOP增强切面
 *
 * @author luohq
 * @date 2023-01-29 09:57
 */
@Aspect
public class D3sLogServiceAspect extends D3sBaseLogAspect {

    private Logger log = LoggerFactory.getLogger(D3sLogServiceAspect.class);

    public static final String LOG_POINTCUT = "(" + D3sAopConsts.APPLICATION_SERVICE_POINTCUT
            + D3sAopConsts.SEPARATOR_OR + D3sAopConsts.DOMAIN_EVENT_HANDLER_POINTCUT
            + D3sAopConsts.SEPARATOR_OR + D3sAopConsts.LOG_ANNO_CLASS_POINTCUT
            + D3sAopConsts.SEPARATOR_OR + D3sAopConsts.LOG_ANNO_METHOD_POINTCUT + ") && "
            + D3sAopConsts.WEB_MVC_EXCLUDE_POINT;

    private D3sAopProps d3sAopProps;

    public D3sLogServiceAspect(D3sAopProps d3sAopProps) {
        this.d3sAopProps = d3sAopProps;
    }

    @PostConstruct
    void postConstruct() {
        log.info("D3s AOP EXT - SERVICE LOG - Started - Pointcut: {}, configProps: {}", LOG_POINTCUT, this.d3sAopProps.getLog());
    }


    @Around(LOG_POINTCUT)
    public Object proceedServiceWithLog(ProceedingJoinPoint point) throws Throwable {
        return this.proceedWithLog(point, this.d3sAopProps.getLog());
    }
}