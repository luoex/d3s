package com.luo.d3s.ext.aop.config;

/**
 * 对象转换字符串类型<br/>
 * 注：打印日志时，需将参数、返回结果转换成字符串
 *
 * @author luohq
 * @date 2023-01-04 16:24
 */
public enum ObjectToStringType {
    //使用String.valueOf()进行转换（性能好）
    TO_STRING,
    //使用JsonUtils.toJson()进行转换（性能差，可读性好，需注意排除不可Json序列化的对象）
    JSON;
}
