package com.luo.d3s.ext.aop.exception.anno;

import java.lang.annotation.*;

/**
 * 异常梳理AOP增强配置注解
 *
 * @author luohq
 * @date 2023-01-29 09:57
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
@Inherited
public @interface D3sException {
    /**
     * 异常处理开关
     *
     * @return 是否启用
     */
    boolean enabled() default true;

}