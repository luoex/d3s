package com.luo.d3s.ext.aop.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.logging.LogLevel;

import java.time.temporal.ChronoUnit;

/**
 * D3s AOP 配置属性
 *
 * @author luohq
 * @date 2023-01-29 09:45
 */
@ConfigurationProperties(prefix = D3sAopProps.PREFIX)
public class D3sAopProps {

    public static final String PREFIX = "d3s.aop";

    private ExceptionProps exception = new ExceptionProps();

    private LogProps log = new LogProps();

    public ExceptionProps getException() {
        return exception;
    }

    public void setException(ExceptionProps exception) {
        this.exception = exception;
    }

    public LogProps getLog() {
        return log;
    }

    public void setLog(LogProps log) {
        this.log = log;
    }

    /**
     * 异常配置属性
     */
    public static class ExceptionProps {
        /**
         * 异常处理全局开关
         */
        private Boolean enabled = true;
        /**
         * web异常处理全局开关
         */
        private Boolean enableWebExceptionHandle= true;

        /**
         * 是否在响应结果中展示验证错误提示信息
         */
        private Boolean enableValidationMessage = true;

        /**
         * 默认的验证异常错误码
         */
        private String defaultValidationErrorCode = "VALIDATE_ERROR";

        public Boolean getEnabled() {
            return enabled;
        }

        public void setEnabled(Boolean enabled) {
            this.enabled = enabled;
        }

        public Boolean getEnableWebExceptionHandle() {
            return enableWebExceptionHandle;
        }

        public void setEnableWebExceptionHandle(Boolean enableWebExceptionHandle) {
            this.enableWebExceptionHandle = enableWebExceptionHandle;
        }

        public Boolean getEnableValidationMessage() {
            return enableValidationMessage;
        }

        public void setEnableValidationMessage(Boolean enableValidationMessage) {
            this.enableValidationMessage = enableValidationMessage;
        }

        public String getDefaultValidationErrorCode() {
            return defaultValidationErrorCode;
        }

        public void setDefaultValidationErrorCode(String defaultValidationErrorCode) {
            this.defaultValidationErrorCode = defaultValidationErrorCode;
        }

        @Override
        public String toString() {
            return "ExceptionProps{" +
                    "enabled=" + enabled +
                    ", enableWebExceptionHandle=" + enableWebExceptionHandle +
                    ", enableValidationMessage=" + enableValidationMessage +
                    ", defaultValidationErrorCode='" + defaultValidationErrorCode + '\'' +
                    '}';
        }
    }

    /**
     * 日志配置属性
     */
    public static class LogProps {
        /**
         * 日志处理全局开关
         */
        private Boolean enabled = true;

        /**
         * 是否启用CommandService日志
         */
        private Boolean enableCommandServiceLog = true;

        /**
         * 是否启用QueryService日志
         */
        private Boolean enableQueryServiceLog = true;

        /**
         * 是否启用ApplicationService日志
         */
        private Boolean enableApplicationServiceLog = true;

        /**
         * 是否启用DomainEventHandler日志
         */
        private Boolean enableDomainEventHandlerLog = true;

        /**
         * 是否启用Web日志
         */
        private Boolean enableWebLog = true;

        /**
         * 日志级别
         */
        private LogLevel logLevel = LogLevel.INFO;

        /**
         * 是否打印参数
         */
        private Boolean showParams = true;

        /**
         * 是否打印结果
         */
        private Boolean showResult = true;

        /**
         * 是否打印类名称，true则ClassName.methodName...，false则methodName...
         */
        private Boolean showClassName = false;

        /**
         * 对象转换字符串的转换方式（支持TO_STRING, JSON，默认TO_STRING，即String.valueOf()）
         */
        private ObjectToStringType objectToStringType = ObjectToStringType.TO_STRING;

        /**
         * 是否打印执行时间
         */
        private Boolean showExecutionTime = true;

        /**
         * 执行时间单位
         */
        private ChronoUnit executionTimeUnit = ChronoUnit.MILLIS;

        public Boolean getEnabled() {
            return enabled;
        }

        public void setEnabled(Boolean enabled) {
            this.enabled = enabled;
        }

        public Boolean getEnableCommandServiceLog() {
            return enableCommandServiceLog;
        }

        public void setEnableCommandServiceLog(Boolean enableCommandServiceLog) {
            this.enableCommandServiceLog = enableCommandServiceLog;
        }

        public Boolean getEnableQueryServiceLog() {
            return enableQueryServiceLog;
        }

        public void setEnableQueryServiceLog(Boolean enableQueryServiceLog) {
            this.enableQueryServiceLog = enableQueryServiceLog;
        }

        public Boolean getEnableApplicationServiceLog() {
            return enableApplicationServiceLog;
        }

        public void setEnableApplicationServiceLog(Boolean enableApplicationServiceLog) {
            this.enableApplicationServiceLog = enableApplicationServiceLog;
        }

        public Boolean getEnableDomainEventHandlerLog() {
            return enableDomainEventHandlerLog;
        }

        public void setEnableDomainEventHandlerLog(Boolean enableDomainEventHandlerLog) {
            this.enableDomainEventHandlerLog = enableDomainEventHandlerLog;
        }

        public Boolean getEnableWebLog() {
            return enableWebLog;
        }

        public void setEnableWebLog(Boolean enableWebLog) {
            this.enableWebLog = enableWebLog;
        }

        public LogLevel getLogLevel() {
            return logLevel;
        }

        public void setLogLevel(LogLevel logLevel) {
            this.logLevel = logLevel;
        }

        public Boolean getShowParams() {
            return showParams;
        }

        public void setShowParams(Boolean showParams) {
            this.showParams = showParams;
        }

        public Boolean getShowResult() {
            return showResult;
        }

        public void setShowResult(Boolean showResult) {
            this.showResult = showResult;
        }

        public Boolean getShowClassName() {
            return showClassName;
        }

        public void setShowClassName(Boolean showClassName) {
            this.showClassName = showClassName;
        }

        public ObjectToStringType getObjectToStringType() {
            return objectToStringType;
        }

        public void setObjectToStringType(ObjectToStringType objectToStringType) {
            this.objectToStringType = objectToStringType;
        }

        public Boolean getShowExecutionTime() {
            return showExecutionTime;
        }

        public void setShowExecutionTime(Boolean showExecutionTime) {
            this.showExecutionTime = showExecutionTime;
        }

        public ChronoUnit getExecutionTimeUnit() {
            return executionTimeUnit;
        }

        public void setExecutionTimeUnit(ChronoUnit executionTimeUnit) {
            this.executionTimeUnit = executionTimeUnit;
        }

        @Override
        public String toString() {
            return "LogProps{" +
                    "enabled=" + enabled +
                    ", enableCommandServiceLog=" + enableCommandServiceLog +
                    ", enableQueryServiceLog=" + enableQueryServiceLog +
                    ", enableApplicationServiceLog=" + enableApplicationServiceLog +
                    ", enableDomainEventHandlerLog=" + enableDomainEventHandlerLog +
                    ", enableWebLog=" + enableWebLog +
                    ", logLevel=" + logLevel +
                    ", showParams=" + showParams +
                    ", showResult=" + showResult +
                    ", showClassName=" + showClassName +
                    ", objectToStringType=" + objectToStringType +
                    ", showExecutionTime=" + showExecutionTime +
                    ", executionTimeUnit=" + executionTimeUnit +
                    '}';
        }
    }
}
