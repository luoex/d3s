package com.luo.d3s.ext.aop.log.anno;

import com.luo.d3s.ext.aop.config.ObjectToStringType;
import org.springframework.boot.logging.LogLevel;

import java.lang.annotation.*;
import java.time.temporal.ChronoUnit;

/**
 * 日志AOP增强配置注解
 *
 * @author luohq
 * @date 2023-01-29 09:57
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
@Inherited
public @interface D3sLog {
    /**
     * 日志处理全局开关
     *
     * @return 是否启用
     */
    boolean enabled() default true;

    /**
     * 日志级别
     *
     * @return 日志级别
     */
    LogLevel logLevel() default LogLevel.INFO;

    /**
     * 是否打印参数
     *
     * @return 是否打印参数
     */
    boolean showParams() default true;

    /**
     * 是否打印结果
     *
     * @return 是否打印结果
     */
    boolean showResult() default true;

    /**
     * 对象转换字符串的转换方式（支持TO_STRING, JSON，默认TO_STRING，即String.valueOf()）
     *
     * @return 对象转换字符串的转换方式
     */
    ObjectToStringType objectToStringType() default ObjectToStringType.TO_STRING;

    /**
     * 是否打印执行时间
     *
     * @return 是否打印执行时间
     */
    boolean showExecutionTime() default true;

    /**
     * 执行时间单位
     *
     * @return 执行时间单位
     */
    ChronoUnit executionTimeUnit() default ChronoUnit.MILLIS;

}