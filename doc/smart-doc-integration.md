# 集成smart-doc

## maven依赖
```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    
    <properties>
        <smart.doc.version>2.6.1</smart.doc.version>
        <smart.doc.goal>openapi</smart.doc.goal>
    </properties>

    <build>
        <pluginManagement>
            <plugins>
                <!-- smart-doc插件定义 -->
                <plugin>
                    <groupId>com.github.shalousun</groupId>
                    <artifactId>smart-doc-maven-plugin</artifactId>
                    <version>${smart.doc.version}</version>
                    <configuration>
                        <!--指定生成文档的使用的配置文件,配置文件放在自己的项目中-->
                        <configFile>./src/main/resources/smart-doc.json</configFile>
                        <!--smart-doc实现自动分析依赖树加载第三方依赖的源码，如果一些框架依赖库加载不到导致报错，这时请使用excludes排除掉-->
                        <excludes>
                            <!--格式为：groupId:artifactId;参考如下-->
                            <!--也可以支持正则式如：com.alibaba:.* -->
                            <exclude>com.alibaba:.*</exclude>
                            <exclude>cn.hutool:hutool-core</exclude>
                        </excludes>
                    </configuration>
                    <executions>
                        <execution>
                            <!--如果不需要在执行编译时启动smart-doc，则将phase注释掉-->
                            <phase>compile</phase>
                            <goals>
                                <!--smart-doc提供了html、openapi、markdown等goal，可按需配置-->
                                <goal>${smart.doc.goal}</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
            </plugins>
        </pluginManagement>

        <plugins>
            <!-- smart-doc插件定义 -->
            <plugin>
                <groupId>com.github.shalousun</groupId>
                <artifactId>smart-doc-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </build>    
</project>
```
## smart-doc.json配置
在src/main/resources下添加smart-doc.json，示例配置如下:
```json
{
  "projectName": "D3S - Smartdoc - API",
  "serverUrl": "http://localhost:8080",
  "pathPrefix": "/",
  "outPath": "./target/smart-doc",
  "allInOne": true,
  "showAuthor": true,
  "groups": [
    {
      "name": "用户管理",
      "apis": "com.luo.demo.api.controller.user.*"
    },
    {
      "name": "角色管理",
      "apis": "com.luo.demo.api.controller.role.*"
    }
  ],
  "revisionLogs": [
    {
      "version": "1.0",
      "revisionTime": "2022-01-17 16:30",
      "status": "create",
      "author": "luohq",
      "remarks": "Smartdoc OAS3集成Springdoc"
    }
  ]
}
```
> 注：  
> outPath为生成接口文档位置，上述示例配置放到了target/smart-doc下，亦可将文档生成至源代码路径，如: ./src/main/resources/smart-doc，   
> groups、revisionLogs若不需要可移除，  
> 完整配置说明可参见：[https://smart-doc-group.github.io/#/zh-cn/diy/config](https://smart-doc-group.github.io/#/zh-cn/diy/config)。

## smart-doc-maven-plugin相关命令
如上集成方式可直接使用`mvn clean package`即可生成smart-doc相关接口文档，  
更多smart-doc相关命令使用参考如下：
```bash
# =========== REST API文档 =============
# 生成html
mvn -Dfile.encoding=UTF-8 smart-doc:html
# 生成markdown
mvn -Dfile.encoding=UTF-8 smart-doc:markdown
# 生成adoc
mvn -Dfile.encoding=UTF-8 smart-doc:adoc
# 生成postman json数据
mvn -Dfile.encoding=UTF-8 smart-doc:postman
# 生成 Open Api 3.0+,Since smart-doc-maven-plugin 1.1.5
mvn -Dfile.encoding=UTF-8 smart-doc:openapi
# 生成文档推送到Torna平台
mvn -Dfile.encoding=UTF-8 smart-doc:torna-rest

# =========== Apache Dubbo RPC文档 =============
# 生成html
mvn -Dfile.encoding=UTF-8 smart-doc:rpc-html
# 生成markdown
mvn -Dfile.encoding=UTF-8 smart-doc:rpc-markdown
# 生成adoc
mvn -Dfile.encoding=UTF-8 smart-doc:rpc-adoc
# 生成dubbo接口文档推送到torna
mvn -Dfile.encoding=UTF-8 smart-doc:torna-rpc
```

## 推送到Torna
修改src/main/resources/smart-doc.json配置，添加appToken、openUrl属性：
```json
{
  "projectName": "D3S smart-doc API",
  "serverUrl": "http://localhost:8080",
  "pathPrefix": "/",
  "outPath": "./src/main/resources/static/doc",
  "allInOne": true,
  //appToken对应下图Tonar管理端中的token
  "appToken": "c16931fa6590483fb7a4e85340fcbfef",
  //openUrl对应下图Tonar管理端中的请求路径
  "openUrl": "http://localhost:7700/api"
}
```
![tonar-app-token.png](imgs/tonar-app-token.png)

执行如下命令：
```bash
# 生成接口文档并推送到Torna平台
mvn -Dfile.encoding=UTF-8 smart-doc:torna-rest
```


## 通过Maven Profile简化构建
在pom中添加不同profile，用于区分smart-doc-maven-plugin构建目标，后续切换不同Profile即可执行对应的目标：
```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    
    <properties>
        <smart.doc.version>2.6.1</smart.doc.version>
        <smart.doc.goal>openapi</smart.doc.goal>
    </properties>

    <build>
        <pluginManagement>
            <plugins>
                <!-- smart-doc插件定义 -->
                <plugin>
                    <groupId>com.github.shalousun</groupId>
                    <artifactId>smart-doc-maven-plugin</artifactId>
                    <version>${smart.doc.version}</version>
                    <configuration>
                        <!--指定生成文档的使用的配置文件,配置文件放在自己的项目中-->
                        <configFile>./src/main/resources/smart-doc.json</configFile>
                        <!--smart-doc实现自动分析依赖树加载第三方依赖的源码，如果一些框架依赖库加载不到导致报错，这时请使用excludes排除掉-->
                        <excludes>
                            <!--格式为：groupId:artifactId;参考如下-->
                            <!--也可以支持正则式如：com.alibaba:.* -->
                            <exclude>com.alibaba:.*</exclude>
                            <exclude>cn.hutool:hutool-core</exclude>
                        </excludes>
                    </configuration>
                    <executions>
                        <execution>
                            <!--如果不需要在执行编译时启动smart-doc，则将phase注释掉-->
                            <phase>compile</phase>
                            <goals>
                                <!--smart-doc提供了html、openapi、markdown等goal，可按需配置-->
                                <goal>${smart.doc.goal}</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>

    <profiles>
        <!-- 生成openapi.json -->
        <profile>
            <id>smart-doc-openapi</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <properties>
                <smart.doc.goal>openapi</smart.doc.goal>
            </properties>
        </profile>

        <!-- 同步到Torna -->
        <profile>
            <id>smart-doc-torna-rest</id>
            <properties>
                <smart.doc.goal>torna-rest</smart.doc.goal>
            </properties>
        </profile>

        <!-- 生成Html接口文档 -->
        <profile>
            <id>smart-doc-html</id>
            <properties>
                <smart.doc.goal>html</smart.doc.goal>
            </properties>
        </profile>

        <!-- 生成Markdown接口文档 -->
        <profile>
            <id>smart-doc-markdown</id>
            <properties>
                <smart.doc.goal>markdown</smart.doc.goal>
            </properties>
        </profile>
    </profiles>

</project>

```

之后即可通过切换mvn profile生成不同形式的接口文档:
```bash
# 生成openapi.json
mvn clean package -P smart-doc-openapi
# 生成html接口文档
mvn clean package -P smart-doc-html
# 生成markdown接口文档
mvn clean package -P smart-doc-markdown
# 推送到Torna
mvn clean package -P smart-doc-torna-rest
```
