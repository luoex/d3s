import org.springframework.util.Assert;

/**
 * Jpa工具
 *
 * @author luohq
 * @version 1.0.0
 * @date 2022-07-14 08:21
 */
public class JpaUtil {

    public static final String PERCENT = "%";

    public static String like(String value) {
        Assert.notNull(value, "value should not null!");
        return new StringBuilder(PERCENT)
                .append(value)
                .append(PERCENT)
                .toString();
    }

    public static String contains(String value) {
        return like(value);
    }

    public static String startsWith(String value) {
        Assert.notNull(value, "value should not null!");
        return new StringBuilder(value)
                .append(PERCENT)
                .toString();
    }

    public static String endsWith(String value) {
        Assert.notNull(value, "value should not null!");
        return new StringBuilder(PERCENT)
                .append(value)
                .toString();
    }


}
