## TOOD
- jpa sql提取到orm.xml，orm.xml修改为goods.xml, category.xml
- jpa nativeQuery Pageable排序不生效
- jpa Pageable total数量验证

- d3s-core/domain/light - light抽象定义
- BaseAssembler支持ValueObjectEnum拷贝
- 区分应用事件（跨进程）、领域事件（不跨进程） - 待测试
- data-permission-mybatis - 管控组织及其自组织、支持扩展权限类型、自定义扩展逻辑
- 精简版
  - 多属性组合验证 - javax
  - Domain融合实体（无值对象定义）
  - Mapper or JPA Interfaces作为repository 
    - Mybatis实现repository、xml（resultMap适配实体）
- d3s-extend/d3s-web - jackson配置、序列化null处理、枚举值序列化处理
- d3s-extend/d3s-auth - oauth2-serer/jwt-simple、feign interceptor client_credentials

---

- rpc feign
- domain mock test
- 单元测试待提取
- Validate验证（DTO）
  - 统一移到Entity中？
- @CommandHandler, @QueryHandler, @EventHandler



