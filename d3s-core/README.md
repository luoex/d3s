# D3s Core

## DDD核心
- Application层服务接口、DTO定义
  - ApplicationService、CommandService、QueryService
  - DTO、Command、Query
  - Response、SingleResponse、MultiResponse
  - PageQuery、PageResponse、PageAssembler
  - ApplicationEvent、ApplicationEventHandler
- Domain层接口定义 
  - AggregateRoot、Entity、ValueObject、Identity、DomainModelValidate
  - DomainService、Specification
  - Repository、CrudRepository、PageQueryRepository
  - Acl
- Exception定义
  - BaseException、BizException、SysException、ValidateException
- Utils定义
  - JsonUtils、Validates、Optionals、UuidGenerator

## 相关依赖
- jackson
- java validation、hibernate-validator
- javatuples