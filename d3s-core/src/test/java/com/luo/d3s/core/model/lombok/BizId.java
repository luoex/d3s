package com.luo.d3s.core.model.lombok;

import com.luo.d3s.core.domain.model.Identity;
import lombok.*;

/**
 * 业务ID
 *
 * @author luohq
 * @date 2022-10-20 16:19
 */
@Getter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BizId implements Identity {

    @NonNull
    Long id;
}
