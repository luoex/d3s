package com.luo.d3s.core.model.lombok;

import com.luo.d3s.core.util.validation.Validates;
import com.luo.d3s.core.domain.model.ValueObject;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 业务名称
 *
 * @author luohq
 * @date 2022-10-20 16:39
 */
@Getter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
public class BizName implements ValueObject {

    String name;

    public BizName(String name) {
        Validates.notBlank(name);
        this.name = name;
    }

}
