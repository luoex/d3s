package com.luo.d3s.core.serialize;

/**
 * 我的名字
 *
 * @author luohq
 * @date 2023-05-05 16:42
 */
public class MyName {

    private String name;

    /**
     * Jackson反序列化需Domain对象有默认无参数构造函数，考虑DDD无参构造函数没有意义，故调整无参构造函数为private，仅供序列化框架使用
     */
    private MyName() {
    }

    public MyName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "MyName{" +
                "name='" + name + '\'' +
                '}';
    }
}
