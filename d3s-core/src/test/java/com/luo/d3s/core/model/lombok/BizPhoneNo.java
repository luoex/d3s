package com.luo.d3s.core.model.lombok;

import com.luo.d3s.core.util.validation.Validates;
import com.luo.d3s.core.domain.model.ValueObject;
import lombok.*;

import java.util.regex.Pattern;

/**
 * 业务电话号码
 *
 * @author luohq
 * @date 2022-10-20 16:55
 */
@Getter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
public class BizPhoneNo implements ValueObject {

    @NonNull
    String phoneNo;

    /**
     * 有效性正则
     */
    private static final Pattern VALID_PATTERN = Pattern.compile("^((13[0-9])|(14[0,1,4-9])|(15[0-3,5-9])|(16[2,5,6,7])|(17[0-8])|(18[0-9])|(19[0-3,5-9]))\\d{8}$");

    /**
     * Constructor.
     *
     * @param phoneNo 手机号
     */
    public BizPhoneNo(String phoneNo) {
        Validates.notEmpty(phoneNo, "phone no can not empty");
        Validates.isTrue(VALID_PATTERN.matcher(phoneNo).matches(), "phone no format is invalid");
        this.phoneNo = phoneNo;
    }
}
