package com.luo.d3s.core;

import com.luo.d3s.core.model.pojo.BizEntity;
import com.luo.d3s.core.model.pojo.BizId;
import com.luo.d3s.core.model.pojo.BizPhoneNo;
import com.luo.d3s.core.model.pojo.BizName;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Model测试
 *
 * @author luohq
 * @date 2022-10-20 16:32
 */
public class ModelTest {

    @Test
    void testEntity() {
        BizEntity entity1 = new BizEntity(new BizId(1L), new BizName("luo"), new BizPhoneNo("18888888888"));
        BizEntity entity2 = new BizEntity(new BizId(2L), new BizName("liu"), new BizPhoneNo("18888888888"));
        BizEntity entity3 = new BizEntity(new BizId(1L), new BizName("luo"), new BizPhoneNo("18888887777"));

        System.out.println("entity1: " + entity1);
        System.out.println("entity2: " + entity2);
        System.out.println("entity3: " + entity3);
        Assertions.assertFalse(entity1.equals(entity2));
        Assertions.assertTrue(entity1.equals(entity3));
        Assertions.assertFalse(entity1.getId().equals(entity2.getId()));
        Assertions.assertTrue(entity1.getId().equals(entity3.getId()));
    }

}
