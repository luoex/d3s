package com.luo.d3s.core.model.jsr380;

import com.luo.d3s.core.domain.model.ValueObject;

import javax.validation.constraints.NotBlank;
import java.util.Objects;

/**
 * 业务名称
 *
 * @author luohq
 * @date 2022-10-20 16:39
 */
public class BizName implements ValueObject {

    @NotBlank
    private String name;

    public BizName(String name) {
        this.name = name;
        this.validateSelf();
    }

    public BizName() {
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BizName bizName = (BizName) o;
        return name.equals(bizName.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return this.name;
    }
}
