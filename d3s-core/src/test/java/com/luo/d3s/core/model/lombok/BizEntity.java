package com.luo.d3s.core.model.lombok;

import com.luo.d3s.core.domain.model.Entity;
import lombok.*;

/**
 * 业务实体类
 *
 * @author luohq
 * @date 2022-10-20 16:26
 */
@Getter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BizEntity implements Entity<BizId> {

    @NonNull
    @EqualsAndHashCode.Include
    BizId id;

    @NonNull
    BizName name;

    @NonNull
    BizPhoneNo phoneNo;

}
