package com.luo.d3s.core;

import com.luo.d3s.core.util.Optionals;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Optional工具测试
 *
 * @author luohq
 * @date 2023-01-06 11:29
 */
@Slf4j
public class OptionalsTest {

    @Test
    void testOptionals() {
        class TempObj {
            private String value;

            public TempObj(String value) {
                this.value = value;
            }

            public String getValue() {
                return this.value;
            }
        }

        TempObj tempObj = new TempObj("hello");
        String strVal = Optionals.defaultNull(tempObj, TempObj::getValue);
        log.info("value: {}", strVal);
        Assertions.assertFalse(Optionals.isNull(tempObj, TempObj::getValue));

        tempObj = null;
        strVal = Optionals.defaultNull(tempObj, TempObj::getValue);
        log.info("value: {}", strVal);
        Assertions.assertTrue(Optionals.isNull(tempObj, TempObj::getValue));

        tempObj = new TempObj(null);
        strVal = Optionals.defaultNull(tempObj, TempObj::getValue);
        log.info("value: {}", strVal);
        Assertions.assertTrue(Optionals.isNull(tempObj, TempObj::getValue));

    }
}
