package com.luo.d3s.core.model.jsr380;

import com.luo.d3s.core.domain.model.ValueObject;
import com.luo.d3s.core.util.validation.constraints.PhoneNo;

import javax.validation.constraints.NotEmpty;
import java.util.Objects;

/**
 * 业务电话号码
 *
 * @author luohq
 * @date 2022-10-20 16:55
 */
public class BizPhoneNo implements ValueObject {

    @NotEmpty
    @PhoneNo
    private String phoneNo;

    public BizPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
        this.validateSelf();
    }

    public BizPhoneNo() {
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BizPhoneNo that = (BizPhoneNo) o;
        return phoneNo.equals(that.phoneNo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(phoneNo);
    }

    @Override
    public String toString() {
        return phoneNo;
    }
}
