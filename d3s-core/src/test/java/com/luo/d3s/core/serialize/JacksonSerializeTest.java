package com.luo.d3s.core.serialize;

import com.luo.d3s.core.util.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

/**
 * Jackson序列化测试
 *
 * @author luohq
 * @date 2023-05-05 16:42
 */
@Slf4j
public class JacksonSerializeTest {

    @Test
    void testJackson() {
        MyName myName = new MyName("myName");
        MyEntity myEntity = new MyEntity(myName, 33);
        String json = JsonUtils.toJson(myEntity);
        log.info("myEntityJson: {}", json);
        /** Jackson反序列化需Domain对象有默认无参数构造函数，考虑DDD无参构造函数没有意义，故调整无参构造函数为private，仅供序列化框架使用 */
        myEntity = JsonUtils.fromJson(json, MyEntity.class);
        log.info("myEntity: {}", myEntity);
    }

    @Test
    void testConstructorReflect() throws Throwable {
        //通过private无参数构造函数创建对象
        Constructor<MyName> privateNoArgConstructor = MyName.class.getDeclaredConstructor();
        privateNoArgConstructor.setAccessible(true);
        MyName myName = privateNoArgConstructor.newInstance();
        log.info("myName After private NoArgConstructor: {}", myName);

        //修改private属性
        Field nameField = MyName.class.getDeclaredField("name");
        nameField.setAccessible(true);
        nameField.set(myName, "myName");
        log.info("myName After set field: {}", myName);

        //通过public多参数构造函数创建对象
        Constructor<MyEntity> publicAllArgConstructor = MyEntity.class.getDeclaredConstructor(MyName.class, Integer.class);
        MyEntity myEntity = publicAllArgConstructor.newInstance(myName, 33);
        log.info("myEntity: {}", myEntity);
    }


}
