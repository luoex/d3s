package com.luo.d3s.core;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.impl.LaissezFaireSubTypeValidator;
import com.luo.d3s.core.model.pojo.BizEntity;
import com.luo.d3s.core.model.pojo.BizId;
import com.luo.d3s.core.model.pojo.BizPhoneNo;
import com.luo.d3s.core.model.pojo.BizName;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

/**
 * Jackson单元测试
 *
 * @author luohq
 * @date 2022-12-27 10:33
 */
@Slf4j
public class JacksonTest {

    @Test
    void testObjectMapperWithType() throws JsonProcessingException {
        ObjectMapper objectMapperWithType = new ObjectMapper();

        //PropertyAccessor.ALL包含GETTER, SETTER, CREATOR, FIELD, IS_GETTER
        //JsonAutoDetect.Visibility.ANY即包含private, protected, public
        //即ObjectMapper可以对[private|protected|public]修饰的[getter|setter|creator|field|isGetter]进行序列化/反序列化
        //注：如下配置支持直接通过field序列化，否则默认需提供getter方法
        objectMapperWithType.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);

        //必须设置，否则无法将JSON转化为对象，会转化成Map类型
        //设置序列化Json中包含对象类型
        //无类型验证 + 仅附加非final类 + WRAPPER_ARRAY形式，即["com.luo.d3s.core.model.pojo.BizEntity", {...}]
        objectMapperWithType.activateDefaultTyping(LaissezFaireSubTypeValidator.instance, ObjectMapper.DefaultTyping.NON_FINAL);


        BizEntity entity = new BizEntity(new BizId(1L), new BizName("luo"), new BizPhoneNo("18888888888"));
        String json = objectMapperWithType.writeValueAsString(entity);
        log.info("entityJson: {}", json);
        /*
        [
            "com.luo.d3s.core.model.pojo.BizEntity",
            {
                "id": [
                    "com.luo.d3s.core.model.pojo.BizId",
                    {
                        "id": 1
                    }
                ],
                "name": [
                    "com.luo.d3s.core.model.pojo.BizName",
                    {
                        "name": "luo"
                    }
                ],
                "phoneNo": [
                    "com.luo.d3s.core.model.pojo.BizPhoneNo",
                    {
                        "phoneNo": "18888888888"
                    }
                ]
            }
        ]
        */

        entity = objectMapperWithType.readValue(json, BizEntity.class);
        log.info("entity: {}", entity);
    }

    @Test
    void testObjectMapper() throws JsonProcessingException {
        ObjectMapper objectMapperWithType = new ObjectMapper();

        //PropertyAccessor.ALL包含GETTER, SETTER, CREATOR, FIELD, IS_GETTER
        //JsonAutoDetect.Visibility.ANY即包含private, protected, public
        //即ObjectMapper可以对[private|protected|public]修饰的[getter|setter|creator|field|isGetter]进行序列化/反序列化
        //注：如下配置支持直接通过field序列化，否则默认需提供getter方法
        //objectMapperWithType.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);


        BizEntity entity = new BizEntity(new BizId(1L), new BizName("luo"), new BizPhoneNo("18888888888"));
        String json = objectMapperWithType.writeValueAsString(entity);
        log.info("entityJson: {}", json);
        /*
        {
            "id": {
                "id": 1
            },
            "name": {
                "name": "luo"
            },
            "phoneNo": {
                "phoneNo": "18888888888"
            }
        }
        */

        entity = objectMapperWithType.readValue(json, BizEntity.class);
        log.info("entity: {}", entity);
    }


}
