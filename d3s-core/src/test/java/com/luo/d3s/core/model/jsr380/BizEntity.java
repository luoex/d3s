package com.luo.d3s.core.model.jsr380;

import com.luo.d3s.core.domain.model.Entity;

import javax.validation.constraints.NotNull;
import java.util.Objects;

/**
 * 业务实体类
 *
 * @author luohq
 * @date 2022-10-20 16:26
 */
public class BizEntity implements Entity<BizId> {

    /*
     * @Valid支持嵌套对象验证，若此只需在Entity中调用validateSelf方法即可（同时自动验证ValueObject）,
     * 不推荐在Entity中@Valid，
     * 推荐在ValueObject中也调用validateSelf方法，若此即支持Entity及ValueObject整体验证，又支持单独声明ValueObject的验证
     */
    //@Valid
    @NotNull
    private BizId id;

    //@Valid
    @NotNull
    private BizName name;

    //@Valid
    @NotNull
    private BizPhoneNo phoneNo;

    public BizEntity(BizId id, BizName name, BizPhoneNo phoneNo) {
        this.id = id;
        this.name = name;
        this.phoneNo = phoneNo;
        this.validateSelf();
    }

    //若不提供默认无参数构造函数，则需放开上面构造函数上的 @JsonCreator+@JsonProperty 或者 @ConstructorProperties（二选一即可），
    //否则不支持Jackson反序列化
    public BizEntity() {
    }

    @Override
    public BizId getId() {
        return this.id;
    }

    public BizName getName() {
        return name;
    }


    public BizPhoneNo getPhoneNo() {
        return phoneNo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BizEntity bizEntity = (BizEntity) o;
        return id.equals(bizEntity.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "BizEntity{" +
                "id=" + id +
                ", name=" + name +
                ", phoneNo=" + phoneNo +
                '}';
    }
}
