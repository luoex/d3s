package com.luo.d3s.core.model.pojo;

import com.luo.d3s.core.util.validation.Validates;
import com.luo.d3s.core.domain.model.ValueObject;

import java.util.Objects;

/**
 * 业务名称
 *
 * @author luohq
 * @date 2022-10-20 16:39
 */
public class BizName implements ValueObject {

    private String name;

    //@JsonCreator
    //public BizName(@JsonProperty("name") String name) {
    //@ConstructorProperties({"name"})
    public BizName(String name) {
        Validates.notBlank(name, "biz name can not null");
        this.name = name;
    }

    public BizName() {
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BizName bizName = (BizName) o;
        return name.equals(bizName.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return this.name;
    }
}
