package com.luo.d3s.core.serialize;

/**
 * 我的实体
 *
 * @author luohq
 * @date 2023-05-05 16:42
 */
public class MyEntity {

    private MyName myName;

    private Integer age;

    /**
     * Jackson反序列化需Domain对象有默认无参数构造函数，考虑DDD无参构造函数没有意义，故调整无参构造函数为private，仅供序列化框架使用
     */
    private MyEntity() {
    }

    public MyEntity(MyName myName, Integer age) {
        this.myName = myName;
        this.age = age;
    }

    public MyName getMyName() {
        return myName;
    }

    public Integer getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "MyEntity{" +
                "myName=" + myName +
                ", age=" + age +
                '}';
    }
}
