package com.luo.d3s.core.model.pojo;

import com.luo.d3s.core.util.validation.Validates;
import com.luo.d3s.core.domain.model.ValueObject;

import java.util.Objects;
import java.util.regex.Pattern;

/**
 * 业务电话号码
 *
 * @author luohq
 * @date 2022-10-20 16:55
 */
public class BizPhoneNo implements ValueObject {

    private String phoneNo;

    /**
     * 有效性正则
     */
    private static final Pattern VALID_PATTERN = Pattern.compile("^((13[0-9])|(14[0,1,4-9])|(15[0-3,5-9])|(16[2,5,6,7])|(17[0-8])|(18[0-9])|(19[0-3,5-9]))\\d{8}$");

    /**
     * Constructor.
     *
     * @param phoneNo
     */
    //@JsonCreator
    //public BizPhoneNo(@JsonProperty("phoneNo") String phoneNo) {
    //@ConstructorProperties({"phoneNo"})
    public BizPhoneNo(String phoneNo) {
        Validates.notEmpty(phoneNo, "phone no can not empty");
        Validates.isTrue(VALID_PATTERN.matcher(phoneNo).matches(), "phone no format is invalid");
        this.phoneNo = phoneNo;
    }

    public BizPhoneNo() {
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BizPhoneNo that = (BizPhoneNo) o;
        return phoneNo.equals(that.phoneNo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(phoneNo);
    }

    @Override
    public String toString() {
        return phoneNo;
    }
}
