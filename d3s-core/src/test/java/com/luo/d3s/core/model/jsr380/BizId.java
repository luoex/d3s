package com.luo.d3s.core.model.jsr380;

import com.luo.d3s.core.domain.model.Identity;

import javax.validation.constraints.NotNull;
import java.util.Objects;

/**
 * 业务ID
 *
 * @author luohq
 * @date 2022-10-20 16:19
 */
public class BizId implements Identity {

    @NotNull
    private Long id;

    public BizId(Long id) {
        this.id = id;
        this.validateSelf();
    }

    public BizId() {
    }

    public Long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BizId bizId = (BizId) o;
        return Objects.equals(id, bizId.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return String.valueOf(id);
    }
}
