package com.luo.d3s.core;

import com.luo.d3s.core.model.jsr380.BizEntity;
import com.luo.d3s.core.model.jsr380.BizId;
import com.luo.d3s.core.model.jsr380.BizName;
import com.luo.d3s.core.model.jsr380.BizPhoneNo;
import com.luo.d3s.core.exception.ValidateException;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Javax Validation测试
 *
 * @author luohq
 * @date 2023-01-08 17:51
 */
@Slf4j
public class ValidationTest {

    @Test
    void testValidation() {

        ValidateException validateException = Assertions.assertThrows(ValidateException.class, () -> {
            BizId bizId = null;
            BizName bizName = null;
            BizPhoneNo bizPhoneNo = null;
            BizEntity bizEntity = new BizEntity(bizId, bizName, bizPhoneNo);
        });
        //phoneNo:不能为null, id:不能为null, name:不能为null
        printValidateException(validateException);

        validateException = Assertions.assertThrows(ValidateException.class, () -> {
            BizId bizId = new BizId(null);
            BizName bizName = new BizName(null);
            BizPhoneNo bizPhoneNo = new BizPhoneNo(null);
            BizEntity bizEntity = new BizEntity(bizId, bizName, bizPhoneNo);
        });
        //Entity使用@Valid，ValueObject不调用validateSelf - name:BizName.name should not be blank, phoneNo:BizPhoneNo.phoneNo should not be null, id:BizId.id should not be null
        //Entity不使用@Valid，ValueObject调用validateSelf - id:BizId.id should not be null - FailFast（第一个Vo抛出异常即结束，无法继续验证其他对象）
        printValidateException(validateException);

        validateException = Assertions.assertThrows(ValidateException.class, () -> {
            BizId bizId = new BizId(1L);
            BizName bizName = new BizName("Tom");
            BizPhoneNo bizPhoneNo = new BizPhoneNo("188");
            BizEntity bizEntity = new BizEntity(bizId, bizName, bizPhoneNo);
        });
        //phoneNo:电话号码格式不正确
        printValidateException(validateException);


        Assertions.assertDoesNotThrow(() -> {
            BizId bizId = new BizId(1L);
            BizName bizName = new BizName("Tom");
            BizPhoneNo bizPhoneNo = new BizPhoneNo("18888888888");
            BizEntity bizEntity = new BizEntity(bizId, bizName, bizPhoneNo);
        });

    }

    private void printValidateException(ValidateException validateException) {
        log.info("[ValidateException] errCode={}, errMessage={}", validateException.getErrCode(), validateException.getMessage());
    }
}
