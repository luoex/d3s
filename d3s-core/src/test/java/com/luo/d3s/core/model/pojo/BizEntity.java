package com.luo.d3s.core.model.pojo;

import com.luo.d3s.core.util.validation.Validates;
import com.luo.d3s.core.domain.model.Entity;

import java.util.Objects;

/**
 * 业务实体类
 *
 * @author luohq
 * @date 2022-10-20 16:26
 */
public class BizEntity implements Entity<BizId> {

    private BizId id;
    private BizName name;

    private BizPhoneNo phoneNo;

    //@JsonCreator
    //public BizEntity(@JsonProperty("id") BizId id, @JsonProperty("name") BizName name, @JsonProperty("phoneNo") BizPhoneNo phoneNo) {
    //@ConstructorProperties({"id", "name", "phoneNo"})
    public BizEntity(BizId id, BizName name, BizPhoneNo phoneNo) {
        Validates.notNull(id);
        Validates.notNull(name);
        Validates.notNull(phoneNo);
        this.id = id;
        this.name = name;
        this.phoneNo = phoneNo;
    }

    //若不提供默认无参数构造函数，则需放开上面构造函数上的 @JsonCreator+@JsonProperty 或者 @ConstructorProperties（二选一即可），
    //否则不支持Jackson反序列化
    public BizEntity() {
    }

    @Override
    public BizId getId() {
        return this.id;
    }

    public BizName getName() {
        return name;
    }


    public BizPhoneNo getPhoneNo() {
        return phoneNo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BizEntity bizEntity = (BizEntity) o;
        return id.equals(bizEntity.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "BizEntity{" +
                "id=" + id +
                ", name=" + name +
                ", phoneNo=" + phoneNo +
                '}';
    }
}
