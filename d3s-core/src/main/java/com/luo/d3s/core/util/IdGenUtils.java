package com.luo.d3s.core.util;

import com.luo.d3s.core.domain.service.IdGenerator;
import com.luo.d3s.core.util.snowflake.SnowflakeSequence;

import java.util.UUID;

/**
 * Id生成工具
 * <ol>
 *     <li>digitalId: 默认雪花ID</li>
 *     <li>textId: 默认UUID（32位）</li>
 * </ol>
 *
 * @author luohq
 * @date 2023-07-05 12:26
 */
public class IdGenUtils {
    /**
     * 默认的雪花ID生成器
     */
    private static final SnowflakeSequence DEFAULT_SNOWFLAKE_SEQUENCE = new SnowflakeSequence();

    /**
     * 默认的ID生成器
     */
    private static IdGenerator idGenerator = new IdGenerator() {
        @Override
        public Long nextDigitalId() {
            return DEFAULT_SNOWFLAKE_SEQUENCE.nextId();
        }

        @Override
        public String nextTextId() {
            return UUID.randomUUID().toString().replace("-", "");
        }
    };

    /**
     * 替换业务自定义的ID生成器
     *
     * @param idGenerator ID生成器
     */
    public static void setIdGenerator(IdGenerator idGenerator) {
        IdGenUtils.idGenerator = idGenerator;
    }

    public static Long nextDigitalId() {
        return idGenerator.nextDigitalId();
    }

    public static String nextTextId() {
        return idGenerator.nextTextId();
    }
}
