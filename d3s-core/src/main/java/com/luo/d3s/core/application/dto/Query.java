package com.luo.d3s.core.application.dto;

/**
 * Query request from Client.
 *
 * @author luohq
 * @date 2022-11-27
 */
public abstract class Query extends DTO {

    private static final long serialVersionUID = 1L;

}
