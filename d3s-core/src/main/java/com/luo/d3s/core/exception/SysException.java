package com.luo.d3s.core.exception;

/**
 * System Exception is unexpected Exception, retry might work again
 *
 * @author luohq
 * @date 2022-11-26
 */
public class SysException extends BaseException {

    private static final long serialVersionUID = 1L;

    public static final String DEFAULT_ERR_CODE = "SYS_ERROR";

    public SysException(String errMessage) {
        super(DEFAULT_ERR_CODE, errMessage);
    }

    public SysException(String errCode, String errMessage) {
        super(errCode, errMessage);
    }

    public SysException(String errMessage, Throwable e) {
        super(DEFAULT_ERR_CODE, errMessage, e);
    }

    public SysException(String errorCode, String errMessage, Throwable e) {
        super(errorCode, errMessage, e);
    }

    @Override
    public String toString() {
        return "SysException{" +
                "errCode='" + this.getErrCode() + "\'," +
                "errMessage='" + this.getMessage() + "\'" +
                '}';
    }
}
