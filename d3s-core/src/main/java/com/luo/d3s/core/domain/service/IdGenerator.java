package com.luo.d3s.core.domain.service;

/**
 * Id生成器接口
 *
 * @author luohq
 * @date 2023-07-05
 */
public interface IdGenerator extends DomainService {

    /**
     * 生成数字ID
     *
     * @return 数字ID
     */
    Long nextDigitalId();

    /**
     * 生成文本ID
     *
     * @return 文本ID
     */
    String nextTextId();
}
