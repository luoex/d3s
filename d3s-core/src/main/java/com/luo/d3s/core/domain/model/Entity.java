package com.luo.d3s.core.domain.model;

import java.io.Serializable;

/**
 * An entity
 *
 * @author luohq
 * @date 2022-10-20 15:46
 * @equals/hashCode compare by identity, not by attributes.
 * @param <ID> ID Type
 */
public interface Entity<ID extends Identity> extends DomainModelValidate, Serializable {

    long serialVersionUID = 1L;

    /**
     * get identifier
     *
     * @return identifier
     */
    ID getId();
}