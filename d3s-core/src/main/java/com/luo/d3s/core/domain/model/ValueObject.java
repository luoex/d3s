package com.luo.d3s.core.domain.model;

import java.io.Serializable;

/**
 * A value object
 *
 * @author luohq
 * @date 2022-10-20 15:46
 * @equals/hashCode compare by the values of their attributes, they don't have an identity.
 */
public interface ValueObject extends DomainModelValidate, Serializable {
    long serialVersionUID = 1L;
}
