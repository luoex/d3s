package com.luo.d3s.core.domain.adpator.light;

import com.luo.d3s.core.domain.model.light.EntityLight;

/**
 * Repository For Light Implementation
 *
 * @author luohq
 * @date 2023-07-07
 * @param <T> Aggregate Root Type (Support EntityLight Or AggregateRootLight)
 */
public interface RepositoryLight<T extends EntityLight> {
}
