package com.luo.d3s.core.domain.event;

import com.luo.d3s.core.domain.model.ValueObject;
import com.luo.d3s.core.exception.ValidateException;

import java.util.stream.Stream;

/**
 * 领域事件类型
 *
 * @author luohq
 * @date 2023-01-04 16:24
 */
public enum DomainEventType implements ValueObject {
    //10 - 内部事件
    INTERNAL(10, "内部事件"),
    //20 - 外部事件
    EXTERNAL(20, "外部事件");

    /**
     * 事件类型值
     */
    private Integer value;

    /**
     * 事件类型描述
     */
    private String desc;

    DomainEventType(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Integer getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }

    public static DomainEventType of(Integer value) {
        return Stream.of(values())
                .filter(curStatus -> curStatus.value.equals(value))
                .findFirst()
                .orElseThrow(() -> new ValidateException("DomainEventType.value is illegal"));
    }
}
