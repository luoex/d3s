package com.luo.d3s.core.domain.model.light;

import com.luo.d3s.core.domain.model.DomainModelValidate;

import java.io.Serializable;

/**
 * An entity For Light Implementation
 *
 * @param <ID> ID Type (Base Java Type)
 * @author luohq
 * @date 2023-07-07
 * @equals/hashCode compare by identity, not by attributes.
 */
public interface EntityLight<ID extends Serializable> extends DomainModelValidate, Serializable {

    long serialVersionUID = 1L;

    /**
     * get identifier
     *
     * @return identifier
     */
    ID getId();
}