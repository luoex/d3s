package com.luo.d3s.core.domain.adpator;

import com.luo.d3s.core.application.dto.PageQuery;
import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.core.domain.model.AggregateRoot;

/**
 * PageQuery Repository
 *
 * @author luohq
 * @date 2022-11-27
 * @param <T> Aggregate Root Type
 */
public interface PageQueryRepository<T extends AggregateRoot> extends Repository<T> {

    /**
     * find page
     *
     * @param pageQuery page query param
     * @return page response
     */
    PageResponse<T> findPage(PageQuery pageQuery);
}
