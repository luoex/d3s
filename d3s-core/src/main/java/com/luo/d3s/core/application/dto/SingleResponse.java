package com.luo.d3s.core.application.dto;

/**
 * Response with single record to return
 * <p/>
 * @author luohq
 * @date 2022-11-27
 */
public class SingleResponse<T> extends Response {

    private static final long serialVersionUID = 1L;

    /**
     * 数据结果
     */
    private T data;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public static SingleResponse buildSuccess() {
        SingleResponse response = new SingleResponse();
        response.setSuccess(true);
        return response;
    }

    public static SingleResponse buildFailure(String errCode, String errMessage) {
        SingleResponse response = new SingleResponse();
        response.setSuccess(false);
        response.setErrCode(errCode);
        response.setErrMessage(errMessage);
        return response;
    }

    public static <T> SingleResponse<T> of(T data) {
        SingleResponse<T> response = new SingleResponse<>();
        response.setSuccess(true);
        response.setData(data);
        return response;
    }

    @Override
    public String toString() {
        return "SingleResponse{" +
                "success=" + super.isSuccess() +
                ", data=" + data +
                ", errCode='" + super.getErrCode() + '\'' +
                ", errMessage='" + super.getErrMessage() + '\'' +
                '}';
    }
}
