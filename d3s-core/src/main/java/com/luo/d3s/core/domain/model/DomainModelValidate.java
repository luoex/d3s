package com.luo.d3s.core.domain.model;

import com.luo.d3s.core.util.validation.Validates;

/**
 * Always-Valid Domain Model
 *
 * @author luohq
 * @date 2023-01-31 13:17
 */
public interface DomainModelValidate {

    /**
     * 领域对象验证（支持Jsr380 Validation注解）<br/>
     * 注：
     * <ol>
     *     <li>建议在Domain Model多参数构造函数最末尾调用此验证逻辑，以保证Always-Valid Domain Model</li>
     *     <li>亦可使用Aspectj统一对Domain Model多参数构造函数进行织入</li>
     * </ol>
     */
    default void validateSelf() {
        Validates.validateEntity(this);
    }
}
