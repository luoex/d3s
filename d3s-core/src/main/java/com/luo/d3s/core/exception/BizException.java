package com.luo.d3s.core.exception;

/**
 * BizException is known Exception, no need retry
 *
 * @author luohq
 * @date 2022-11-26
 */
public class BizException extends BaseException {

    private static final long serialVersionUID = 1L;

    public static final String DEFAULT_ERR_CODE = "BIZ_ERROR";

    public BizException(String errMessage) {
        super(DEFAULT_ERR_CODE, errMessage);
    }

    public BizException(String errCode, String errMessage) {
        super(errCode, errMessage);
    }

    public BizException(String errMessage, Throwable e) {
        super(DEFAULT_ERR_CODE, errMessage, e);
    }

    public BizException(String errorCode, String errMessage, Throwable e) {
        super(errorCode, errMessage, e);
    }

    @Override
    public String toString() {
        return "BizException{" +
                "errCode='" + this.getErrCode() + "\'," +
                "errMessage='" + this.getMessage() + "\'" +
                '}';
    }
}