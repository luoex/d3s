package com.luo.d3s.core.exception;

/**
 * ValidateException is known Exception, no need retry
 *
 * @author luohq
 * @date 2022-11-26
 */
public class ValidateException extends BaseException {

    private static final long serialVersionUID = 1L;

    public static final String DEFAULT_ERR_CODE = "VALIDATE_ERROR";

    public ValidateException(String errMessage) {
        super(DEFAULT_ERR_CODE, errMessage);
    }

    public ValidateException(String errCode, String errMessage) {
        super(errCode, errMessage);
    }

    public ValidateException(String errMessage, Throwable e) {
        super(DEFAULT_ERR_CODE, errMessage, e);
    }

    public ValidateException(String errorCode, String errMessage, Throwable e) {
        super(errorCode, errMessage, e);
    }

    @Override
    public String toString() {
        return "ValidateException{" +
                "errCode='" + this.getErrCode() + "\'," +
                "errMessage='" + this.getMessage() + "\'" +
                '}';
    }

}