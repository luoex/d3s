package com.luo.d3s.core.application.dto;

/**
 * Page Query Param
 *
 * @author luohq
 * @date 2022-11-27
 */
public abstract class PageQuery extends Query {
    private static final long serialVersionUID = 1L;

    public static final String ASC = "ASC";

    public static final String DESC = "DESC";

    private static final int DEFAULT_PAGE_SIZE = 10;

    /**
     * 每页记录数量（默认10）
     */
    private int pageSize = DEFAULT_PAGE_SIZE;

    /**
     * 页码（从1开始）
     */
    private int pageIndex = 1;

    /**
     * 排序属性
     */
    private String orderBy;

    /**
     * 排序顺序(DESC, ASC)
     */
    private String orderDirection = DESC;

    /**
     * 分组属性（可选）
     */
    private String groupBy;

    /**
     * 是否需要返回总数（可选, true需要, false不需要）
     */
    private boolean needTotalCount = true;

    public int getPageIndex() {
        if (pageIndex < 1) {
            return 1;
        }
        return pageIndex;
    }

    public PageQuery setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
        return this;
    }

    public int getPageSize() {
        return pageSize;
    }

    public PageQuery setPageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public int getOffset() {
        return (getPageIndex() - 1) * getPageSize();
    }

    public String getOrderBy() {
        return orderBy;
    }

    public PageQuery setOrderBy(String orderBy) {
        this.orderBy = orderBy;
        return this;
    }

    public String getOrderDirection() {
        return orderDirection;
    }

    public PageQuery setOrderDirection(String orderDirection) {
        if (ASC.equalsIgnoreCase(orderDirection) || DESC.equalsIgnoreCase(orderDirection)) {
            this.orderDirection = orderDirection;
        }
        return this;
    }

    public String getGroupBy() {
        return groupBy;
    }

    public void setGroupBy(String groupBy) {
        this.groupBy = groupBy;
    }

    public boolean isNeedTotalCount() {
        return needTotalCount;
    }

    public void setNeedTotalCount(boolean needTotalCount) {
        this.needTotalCount = needTotalCount;
    }

    @Override
    public String toString() {
        return "PageQuery{" +
                "pageSize=" + pageSize +
                ", pageIndex=" + pageIndex +
                ", orderBy='" + orderBy + '\'' +
                ", orderDirection='" + orderDirection + '\'' +
                ", groupBy='" + groupBy + '\'' +
                ", needTotalCount=" + needTotalCount +
                '}';
    }
}
