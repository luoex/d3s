package com.luo.d3s.core.domain.service.specifcation;


/**
 * Abstract base implementation of composite {@link Specification} with default
 * implementations for {@code and}, {@code or} and {@code not}.
 *
 * @author luohq
 * @date 2022-12-29
 */
public abstract class AbstractSpecification<T> implements Specification<T> {

    public Specification<T> and(final Specification<T> specification) {
        return new AndSpecification<T>(this, specification);
    }

    public Specification<T> or(final Specification<T> specification) {
        return new OrSpecification<T>(this, specification);
    }

    public Specification<T> not(final Specification<T> specification) {
        return new NotSpecification<T>(specification);
    }
}
