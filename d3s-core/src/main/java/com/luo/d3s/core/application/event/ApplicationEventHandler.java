package com.luo.d3s.core.application.event;


import com.luo.d3s.core.domain.event.DomainEventHandler;

/**
 * Application Event Handler
 *
 * @param <T> Application Event Type
 * @author luohq
 * @date 2022-11-27 10:46
 */
public interface ApplicationEventHandler<T extends ApplicationEvent> extends DomainEventHandler<T> {

}
