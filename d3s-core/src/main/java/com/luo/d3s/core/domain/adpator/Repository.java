package com.luo.d3s.core.domain.adpator;

import com.luo.d3s.core.domain.model.AggregateRoot;

/**
 * Repository
 *
 * @author luohq
 * @date 2022-11-27
 * @param <T> Aggregate Root Type
 */
public interface Repository <T extends AggregateRoot> {
}
