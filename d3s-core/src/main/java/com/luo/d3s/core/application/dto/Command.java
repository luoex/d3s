package com.luo.d3s.core.application.dto;

/**
 * Command request from Client.
 *
 * @author luohq
 * @date 2022-11-27
 */
public abstract class Command extends DTO {

    private static final long serialVersionUID = 1L;

}
