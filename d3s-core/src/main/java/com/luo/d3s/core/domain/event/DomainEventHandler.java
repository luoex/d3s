package com.luo.d3s.core.domain.event;


/**
 * Domain Event Handler
 *
 * @param <T> Domain Event Type
 * @author luohq
 * @date 2022-11-27 10:46
 */
public interface DomainEventHandler<T extends DomainEvent> {

    /**
     * handle domain event
     *
     * @param domainEvent Domain Event
     */
    void handle(T domainEvent);
}
