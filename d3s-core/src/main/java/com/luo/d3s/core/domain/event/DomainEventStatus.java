package com.luo.d3s.core.domain.event;

import java.util.stream.Stream;

/**
 * 领域事件状态
 *
 * @author luohq
 * @date 2022-12-22
 */
public enum DomainEventStatus {
    CREATED(10, "已创建"),
    PUBLISHED(20, "已发送");

    private Integer status;
    private String desc;

    DomainEventStatus(Integer status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    public Integer getStatus() {
        return status;
    }

    public String getDesc() {
        return desc;
    }

    public static DomainEventStatus of(Integer status) {
        return Stream.of(values())
                .filter(curStatus -> curStatus.status.equals(status))
                .findFirst()
                .orElse(null);
    }
}
