package com.luo.d3s.core.application.assmebler;

import com.luo.d3s.core.application.dto.PageResponse;

import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 分页结果转换器
 *
 * @author luohq
 * @date 2023-01-05 13:02
 */
public class PageResponseAssembler {

    /**
     * 转换分页结果
     *
     * @param pageResp  待转换分页结果
     * @param convertor 对象转换器
     * @param <T>       待转换对象类型
     * @param <R>       转换后对象类型
     * @return 转换后分页结果
     */
    public static <T, R> PageResponse<R> toPageResp(PageResponse<T> pageResp, Function<T, R> convertor) {
        List<R> resultList = Collections.emptyList();
        if (null != pageResp.getData() && !pageResp.getData().isEmpty()) {
            resultList = pageResp.getData().stream()
                    .map(convertor)
                    .collect(Collectors.toList());
        }
        return PageResponse.of(resultList, pageResp.getTotalCount(), pageResp.getPageSize(), pageResp.getPageIndex());
    }
}
