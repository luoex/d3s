package com.luo.d3s.core.domain.adpator.light;

import com.luo.d3s.core.domain.model.light.EntityLight;

import java.io.Serializable;

/**
 * CRUD Repository For Light Implementation
 *
 * @param <T>  Aggregate Root Type (Support EntityLight Or AggregateRootLight)
 * @param <ID> Identity Type (Base Java Type)
 * @author luohq
 * @date 2023-07-07
 */
public interface CrudRepositoryLight<T extends EntityLight, ID extends Serializable> extends RepositoryLight<T> {

    /**
     * save aggregateRoot
     *
     * @param aggregateRoot aggregateRoot
     * @return aggregateRoot
     */
    T save(T aggregateRoot);

    /**
     * find aggregateRoot by id
     *
     * @param id id
     * @return aggregateRoot
     */
    T findById(ID id);

    /**
     * remove aggregateRoot by id
     *
     * @param id id
     * @return count of removed aggregateRoots
     */
    Integer removeById(ID id);
}
