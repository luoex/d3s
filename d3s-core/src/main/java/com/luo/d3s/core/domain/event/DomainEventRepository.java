package com.luo.d3s.core.domain.event;


import java.time.LocalDateTime;
import java.util.List;

/**
 * Domain Event Repository
 *
 * @author luohq
 * @date 2022-12-22
 */
public interface DomainEventRepository {

    /**
     * Save event
     *
     * @param event the event to be saved
     */
    void save(DomainEvent event);

    /**
     * find events which wait to be published
     *
     * @return events which wait to be published
     */
    List<DomainEvent> toPublish();

    /**
     * find events which wait to be published and before lastCreateTime
     *
     * @param lastCreateTime last create time
     * @return events which wait to be published
     */
    List<DomainEvent> toPublish(LocalDateTime lastCreateTime);

    /**
     * Mark Events as Published
     *
     * @param events the events to be marked
     */
    void published(List<DomainEvent> events);


    /**
     * Mark Event as Published
     *
     * @param event the event to be marked
     */
    void published(DomainEvent event);

    /**
     * delete events before retentionDays
     *
     * @param retentionDays retention days
     */
    void deleteBeforeDays(Integer retentionDays);
}
