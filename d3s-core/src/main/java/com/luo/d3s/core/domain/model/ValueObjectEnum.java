package com.luo.d3s.core.domain.model;

import com.luo.d3s.core.exception.ValidateException;

import java.util.stream.Stream;

/**
 * 枚举值对象
 *
 * @param <T> 枚举值类型
 * @author luohq
 * @date 2023-05-05 13:16
 */
public interface ValueObjectEnum<T> extends ValueObject {

    long serialVersionUID = 1L;

    /**
     * 获取枚举值
     *
     * @return 枚举值
     */
    T getValue();

    /**
     * 获取枚举描述（或名称）
     *
     * @return
     */
    String getDesc();

    /**
     * 转换value为具体枚举
     *
     * @param value                枚举值
     * @param valueObjectEnumClass 枚举值对象类型
     * @return 具体枚举
     * @param <E> 枚举值对象类型
     * @param <T> 值类型
     */
    static <E extends ValueObjectEnum, T> E of(T value, Class<E> valueObjectEnumClass) {
        return Stream.of(valueObjectEnumClass.getEnumConstants())
                .filter(curEnum -> curEnum.getValue().equals(value))
                .findFirst()
                .orElseThrow(() -> new ValidateException(String.format("%s.value is illegal", valueObjectEnumClass.getSimpleName())));

    }
}
