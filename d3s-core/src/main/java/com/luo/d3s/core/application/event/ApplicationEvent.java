package com.luo.d3s.core.application.event;

import com.luo.d3s.core.domain.event.DomainEvent;

import java.io.Serializable;

/**
 * A application event is something that is unique, but does not have a lifecycle.
 * The identity may be explicit, for example the sequence number of a payment,
 * or it could be derived from various aspects of the event such as where, when and what
 * has happened.
 *
 * @param <EID>  Event ID Type (Base Java Type)
 * @param <ARID> Aggregate Root ID TYPE (Base Java Type)
 * @author luohq
 * @date 2022-10-25 15:22
 */
public interface ApplicationEvent<EID extends Serializable, ARID extends Serializable> extends DomainEvent<EID, ARID> {

    long serialVersionUID = 1L;

}
