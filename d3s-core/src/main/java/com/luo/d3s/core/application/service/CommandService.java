package com.luo.d3s.core.application.service;

/**
 * Command Service
 *
 * @author luohq
 * @date 2022-11-27
 */
public interface CommandService extends ApplicationService {
}
