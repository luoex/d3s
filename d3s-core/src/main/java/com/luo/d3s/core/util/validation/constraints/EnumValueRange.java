package com.luo.d3s.core.util.validation.constraints;

import javax.validation.Constraint;
import javax.validation.OverridesAttribute;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

/**
 * 枚举value范围 - 验证注解
 *
 * @author luohq
 * @date 2023-06-16
 */
@Documented
@Constraint(validatedBy = {})
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
@EnumRange(value = Enum.class, enumValueMethod = "getValue")
public @interface EnumValueRange {
    /**
     * 枚举类型
     */
    @OverridesAttribute(constraint = EnumRange.class, name = "value")
    Class<? extends Enum> value();

    /**
     * 验证失败时的提示信息
     */
    String message() default "值不在枚举范围内";

    /**
     * 验证分组
     */
    Class<?>[] groups() default {};

    /**
     * 验证负载
     */
    Class<? extends Payload>[] payload() default {};
}