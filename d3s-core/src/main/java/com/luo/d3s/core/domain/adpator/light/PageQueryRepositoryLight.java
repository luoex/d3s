package com.luo.d3s.core.domain.adpator.light;

import com.luo.d3s.core.application.dto.PageQuery;
import com.luo.d3s.core.application.dto.PageResponse;
import com.luo.d3s.core.domain.model.light.EntityLight;

/**
 * PageQuery Repository For Light Implementation
 *
 * @param <T> Aggregate Root Type (Support EntityLight Or AggregateRootLight)
 * @author luohq
 * @date 2023-07-07
 */
public interface PageQueryRepositoryLight<T extends EntityLight> extends RepositoryLight<T> {

    /**
     * find page
     *
     * @param pageQuery page query param
     * @return page response
     */
    PageResponse<T> findPage(PageQuery pageQuery);
}
