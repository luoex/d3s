package com.luo.d3s.core.domain.adpator;

import com.luo.d3s.core.domain.model.AggregateRoot;
import com.luo.d3s.core.domain.model.Identity;

/**
 * CRUD Repository
 *
 * @author luohq
 * @date 2022-11-27
 * @param <T> Aggregate Root Type
 * @param <ID> Identity Type
 */
public interface CrudRepository<T extends AggregateRoot, ID extends Identity> extends Repository<T> {

    /**
     * save aggregateRoot
     *
     * @param aggregateRoot aggregateRoot
     * @return aggregateRoot
     */
    T save(T aggregateRoot);

    /**
     * find aggregateRoot by id
     *
     * @param id id
     * @return aggregateRoot
     */
    T findById(ID id);

    /**
     * remove aggregateRoot by id
     *
     * @param id id
     */
    void removeById(ID id);
}
