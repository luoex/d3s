package com.luo.d3s.core.domain.model;

/**
 * An Aggregate Root
 *
 * @author luohq
 * @date 2022-10-20 15:46
 * @param <ID> ID type
 */
public interface AggregateRoot<ID extends Identity> extends Entity<ID> {

}