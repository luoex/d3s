package com.luo.d3s.core.domain.model.light;

import java.io.Serializable;

/**
 * An Aggregate Root For Light Implementation
 *
 * @author luohq
 * @date 2023-07-07
 * @param <ID> ID type (Base Java Type)
 */
public interface AggregateRootLight<ID extends Serializable> extends EntityLight<ID> {

}