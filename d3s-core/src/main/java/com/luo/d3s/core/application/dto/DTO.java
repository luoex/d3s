package com.luo.d3s.core.application.dto;

import java.io.Serializable;

/**
 * Data Transfer object, including Command, Query and Response,
 *
 * Command and Query is CQRS concept.
 *
 * @author luohq
 * @date 2022-11-27
 */
public abstract class DTO implements Serializable {

    private static final long serialVersionUID = 1L;

}
