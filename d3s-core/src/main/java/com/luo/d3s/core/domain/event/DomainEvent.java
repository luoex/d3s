package com.luo.d3s.core.domain.event;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * A domain event is something that is unique, but does not have a lifecycle.
 * The identity may be explicit, for example the sequence number of a payment,
 * or it could be derived from various aspects of the event such as where, when and what
 * has happened.
 *
 * @param <EID>  Event ID Type
 * @param <ARID> Aggregate Root ID TYPE
 * @author luohq
 * @date 2022-10-25 15:22
 */
public interface DomainEvent<EID, ARID> extends Serializable {

    long serialVersionUID = 1L;

    /**
     * get event id
     *
     * @return event id
     */
    EID getEventId();

    /**
     * get aggregate root id
     *
     * @return aggregate root id
     */
    ARID getAggregateRootId();

    /**
     * get event create time
     *
     * @return event create time
     */
    LocalDateTime getCreateTime();

    /**
     * get event types(default value is EXTERNAL)
     *
     * @return event types
     */
    default DomainEventType[] getTypes() {
        return new DomainEventType[]{DomainEventType.EXTERNAL};
    }

}
