package com.luo.d3s.core.domain.event;

/**
 * Domain Event Publisher
 *
 * @author luohq
 * @date 2022-11-27 10:44
 */
public interface DomainEventPublisher {

    /**
     * publish domain event
     *
     * @param domainEvent DomainEvent
     */
    void publish(DomainEvent domainEvent);

    /**
     * 领域事件类型（默认外部事件）
     *
     * @return 领域事件类型
     */
    default DomainEventType type() {
        return DomainEventType.EXTERNAL;
    }
}
