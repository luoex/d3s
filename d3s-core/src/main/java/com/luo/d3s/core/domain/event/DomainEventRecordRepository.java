package com.luo.d3s.core.domain.event;


/**
 * Domain Event Receive Record Repository
 *
 * @author luohq
 * @date 2022-12-22
 */
public interface DomainEventRecordRepository {

    /**
     * record received domain event
     *
     * @param event the event to be recorded
     * @return whether to save successfully
     */
    Boolean record(DomainEvent event);

    /**
     * delete records before retentionDays
     *
     * @param retentionDays retention days
     */
    void deleteBeforeDays(Integer retentionDays);

}
