package com.luo.d3s.core.domain.model;

/**
 * An Identity
 *
 * @author luohq
 * @date 2022-10-20 15:46
 * @equals/hashCode compare by the values of their attributes.
 */
public interface Identity extends ValueObject {

}
