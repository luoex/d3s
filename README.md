# D3S
D3S(DDD Architecture with SpringBoot)旨在打造一款可落地的DDD（领域驱动设计）实现框架，<br/>
提供了统一的DDD模型抽象<b>[[d3s-core](./d3s-core)]</b>，并扩展实现了DDD架构的一些关键特性<b>[[d3s-extend](./d3s-extend)]</b>，<br/>
同时给出了截取自[电商案例](./d3s-samples/README.md)的基于D3S的示例实现：
- 严格分层完整落地实现<b>[[d3s-samples/d3s-complete-ec](./d3s-samples/d3s-complete-ec)]</b>
- Mybatis-Plus轻量化实现<b>[[d3s-samples/d3s-light-ec-mybatis](./d3s-samples/d3s-light-ec-mybatis)]</b>
- Spring Data JPA轻量化实现<b>[[d3s-samples/d3s-light-ec-jpa](./d3s-samples/d3s-light-ec-jpa)]</b>

力求打造一款即能发挥DDD的优势又能降低上手难度的DDD实现框架。

![d3s-layer](./doc/imgs/d3s-layer.jpg)

## 模块说明
| 模块                                                                                 | 说明                                                                                                                                                                                                                                                                                           |
|:-----------------------------------------------------------------------------------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [d3s-core](./d3s-core)                                                             | DDD核心模型定义 <ul><li>DDD领域模型（AggregateRoot、Entity、ValueObject、DomainEvent...）基础接口定义</li><li>通用参数、结果定义</li><li>通用异常定义</li><li>常用工具定义</li></ul>                                                                                                                                                   |
| [d3s-extend](./d3s-extend)                                                         | D3S扩展                                                                                                                                                                                                                                                                                        |
| [d3s-extend/d3s-aop](./d3s-extend/d3s-aop)                                         | D3S AOP 扩展<br/><ul><li>日志增强</li><li>Web异常封装</li></ul>                                                                                                                                                                                                                                        |
| [d3s-extend/d3s-component-scan](./d3s-extend/d3s-component-scan)                   | D3S 组件扫描 扩展<br/><ul><li>@D3sComponentScan领域模型自动注册</li><li>DomainRegistry</li><li>CommandBus</li><li>QueryBus</li></ul>                                                                                                                                                                       |
| [d3s-extend/d3s-domain-validate-aspectj](./d3s-extend/d3s-domain-validate-aspectj) | D3S 领域模型验证Aspectj 扩展<br/><ul><li>领域模型Validate验证（支持JSR-380）</li><li>支持Aspectj织入</li></ul>                                                                                                                                                                                                     |
| [d3s-extend/d3s-data-permission-mybatis](./d3s-extend/d3s-data-permission-mybatis) | D3S 数据权限Mybatis实现 扩展<br/><ul><li>支持数据权限条件SQL的动态配置</li><li>支持PageHelper分页插件、Mybatis-Plus分页插件</li><li>支持数据权限类型限制（ADMIN、USER、USER_CUSTOM、DEPT、DEPT_AND_CHILD、DEPT_CUSTOM）</li><li>支持数据操作限制（ALL、SELECT、INSERT、UPDATE、DELETE）</li></ul>                                                           |
| [d3s-extend/d3s-event-jdbc](./d3s-extend/d3s-event-jdbc)                           | D3S 事件Jdbc实现 扩展<br/><ul><li>支持Jdbc事件持久化增强（持久化、幂等）</li><li>支持内、外部事件发布（DomainEventType内、外）</li><li>支持EventBus</li></ul>                                                                                                                                                                        |
| [d3s-extend/d3s-test](./d3s-extend/d3s-test)                                       | D3S 单元测试 扩展<br/><ul><li>Junit5扩展（@JsonFileSource）</li><li>mock实现扩展(MockMvc、Redis、RabbitMq)</li></ul>                                                                                                                                                                                         |
| [d3s-samples](./d3s-samples)                                                       | D3S 实现示例（截取自电商案例）                                                                                                                                                                                                                                                                            |
| [d3s-samples/anti-ec-mvc](./d3s-samples/anti-ec-mvc)                               | DDD反模式示例 <br/><ul><li>使用SpringMvc常见的开发模型</li><li>贫血模型</li><li>事务脚本</li></ul>                                                                                                                                                                                                                 |                                                                                                                                                                                                                                     
| [d3s-samples/d3s-complete-ec](./d3s-samples/d3s-complete-ec)                       | D3S 完全遵循DDD的电商示例<br/><ul><li>严格4层架构（maven多模块隔离）</li><li>严格Entity、ValueObject模型</li><li>Always-Valid Domain Model</li><li>DomainEvent</li><li>ACL</li><li>CQRS</li><li>Mybatis-plus仓库实现</li></ul>                                                                                             |
| [d3s-samples/d3s-light-ec-mybatis](./d3s-samples/d3s-light-ec-mybatis)             | D3S 借鉴DDD但又不完全受限于DDD的轻量化电商示例（妥协后的4层架构/Mybatis-Plus），<br/>旨在提供能快速上手且有效降低代码量的DDD实现。<br/><ul><li>宽松4层架构</li><li>Entity与DataObject融合、简化值对象为简单属性（且原属于值对象的领域行为可合并到实体、领域服务中）</li><li>Mybatis-plus仓库实现</li><li>Always-Valid Domain Model</li><li>ApplicationEvent</li><li>ACL</li><li>CQRS</li></ul> |
| [d3s-samples/d3s-light-ec-jpa](./d3s-samples/d3s-light-ec-jpa)                     | D3S 借鉴DDD但又不完全受限于DDD的轻量化电商示例（妥协后的4层架构/Spring Data JPA），<br/>旨在提供一种能充分发挥DDD模型的优势、又能相对减少一定低代码量的DDD实现。<br/><ul><li>宽松4层架构</li><li>保留领域模型完整性（实体、值对象、对象间关联、领域事件等）</li><li>使用Spring Data JPA完成ORM仓库实现</li><li>Always-Valid Domain Model</li><li>DomainEvent</li><li>ACL</li><li>CQRS</li></ul>   |
| [d3s-samples/d3s-share](./d3s-samples/d3s-share)                                   | D3S 共享内核示例                                                                                                                                                                                                                                                                                   |                                                                                                                                                                                                                                     

## 核心功能
- 统一定义DDD基础模型
- 领域模型Validate验证（支持JSR-380）、支持Aspectj织入
- @D3sComponentScan领域模型自动注册
- Event增强
  - 支持Jdbc事件持久化增强（持久化、幂等）
  - 支持内、外部事件发布（DomainEventType内、外）
  - 内部事件使用Spring Event，外部事件支持自定义
  - 同步、异步 可借助Spring @Async实现
  - EventBus
- 支持DomainRegistry、CommandBus, QueryBus
- 支持Spring Aop 日志增强、Web异常自动处理
- 支持数据权限控制
- CQRS轻量化实现


